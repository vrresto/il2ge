set(CONFIG_IS_RELEASE
  $<CONFIG:Release,Release_SSE41,Release_SSE42,Release_AVX,Release_AVX2>
)

set(CONFIG_IS_DEBUG
  $<CONFIG:Debug>
)


target_compile_definitions(render_util_common INTERFACE
  _REENTRANT

  # disable win32 defines
  NODRAWTEXT
  NOGDI

  # plog
  PLOG_ENABLE_WCHAR_INPUT=0
  PLOG_OMIT_LOG_DEFINES=1
  PLOG_CAPTURE_FILE=1

  $<${CONFIG_IS_RELEASE}: GLM_FORCE_DEFAULT_ALIGNED_GENTYPES>
  $<${CONFIG_IS_RELEASE}: GLM_FORCE_INTRINSICS>

  $<$<CONFIG:Release>: GLM_FORCE_SSSE3>
  $<$<CONFIG:Release_SSE41>: GLM_FORCE_SSE41>
  $<$<CONFIG:Release_SSE42>: GLM_FORCE_SSE42>
  $<$<CONFIG:Release_AVX>: GLM_FORCE_AVX>
  $<$<CONFIG:Release_AVX2>: GLM_FORCE_AVX2>
)


target_compile_options(render_util_common INTERFACE
  $<IF:$<BOOL:${ENABLE_WARNINGS}>,
        -Wfatal-errors
        -Wreturn-type
        -Werror
        -Wmissing-field-initializers
        -Wall
        -Wno-error=unused-function
        #-Wno-error=pessimizing-move
        -Wno-error=unused-variable
        -Wno-error=unused-but-set-variable
        -Wno-error=sign-compare
        -Wno-error=unused-local-typedefs
        -Wno-error=reorder
        -Wno-error=strict-aliasing
        -Wno-error=comment
        -Wno-sign-compare
        # -fdata-sections -ffunction-sections
        # -Wall
        # -rdynamic

        $<$<COMPILE_LANGUAGE:CXX>: -Wmissing-declarations>
        $<$<COMPILE_LANGUAGE:CXX>: -Wno-error=overloaded-virtual>
      ,
        -w
    >

  $<${CONFIG_IS_RELEASE}:
      -O2
      -g1
      -mfpmath=sse
      # -ffast-math # makes std::sort<float>() crash
      # -ftree-vectorize
    >

  $<${CONFIG_IS_DEBUG}:
      -O1
      -g3
      -fno-omit-frame-pointer
      -fno-strict-aliasing
    >

  $<$<CONFIG:Release>: -mssse3>
  $<$<CONFIG:Release_SSE41>: -msse4.1>
  $<$<CONFIG:Release_SSE42>: -msse4.2>
  $<$<CONFIG:Release_AVX>: -mavx>
  $<$<CONFIG:Release_AVX2>: -mavx2>

  $<$<COMPILE_LANGUAGE:CXX>: -std=c++17> # FIXME - there is a cmake syntax for this
)
