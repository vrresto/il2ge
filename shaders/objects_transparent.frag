#version 130

uniform sampler2D sampler_0;
uniform vec4 color_scale;

varying vec2 pass_texcoord;

void main(void)
{
  gl_FragColor = color_scale * texture(sampler_0, pass_texcoord);
}
