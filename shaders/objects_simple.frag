#version 130

uniform sampler2D sampler_0;

varying vec2 pass_texcoord;

void main(void)
{
  vec4 color = texture(sampler_0, pass_texcoord);
  gl_FragColor = gl_Color * color;
}
