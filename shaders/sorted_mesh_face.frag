#version 430

uniform sampler2D sampler_0;
uniform vec4 color_scale;

in vec2 pass_texcoords;

out vec4 out_color;

void main(void)
{
  vec4 color = texture(sampler_0, pass_texcoords);
  color *= color_scale;
  out_color = color;

  out_color = vec4(0, 1, 0, 0.5);
}
