#version 130

uniform mat4 projection;

varying vec3 pass_normal;
varying vec2 pass_texcoord;
varying vec4 pass_pos_view;

//FIXME legacy
uniform mat4 view2WorldMatrix;
varying vec3 passObjectPos;
varying vec3 passObjectPosFlat;

void main(void)
{
  pass_pos_view = gl_Vertex;
  gl_Position = projection * gl_Vertex;
  pass_texcoord = gl_MultiTexCoord0.xy;
  pass_normal = gl_Normal;

  passObjectPos = (view2WorldMatrix * pass_pos_view).xyz;
  passObjectPosFlat = passObjectPos;
}
