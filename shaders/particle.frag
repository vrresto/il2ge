#version 430

uniform sampler2D sampler_0;
uniform vec4 color_scale;

in vec2 pass_texcoords;
in vec4 pass_color;

out vec4 out_color;

void main(void)
{
  vec4 color = texture(sampler_0, pass_texcoords);
//   vec4 color = vec4(1);


  color *= color_scale;
  color *= pass_color;
//   out_color = color;

//   out_color = vec4(pass_texcoords, 0, 1);

  out_color = color;
}
