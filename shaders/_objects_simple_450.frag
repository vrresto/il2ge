#version 430

uniform sampler2D sampler_0;
uniform vec4 color_scale;

out vec4 out_color;

in vec2 pass_texcoords;


void main(void)
{
  vec4 color = texture(sampler_0, pass_texcoords);
  color *= color_scale;
  out_color = color;
}
