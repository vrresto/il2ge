#version 430

layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec2 in_texcoords;

out vec2 pass_texcoords;

uniform mat4 projection;
uniform mat4 view;


void main(void)
{
  pass_texcoords = in_texcoords;
  gl_Position = projection * view * vec4(in_pos, 1.0);
}
