#version 430

#extension GL_ARB_shader_draw_parameters : require

struct InstanceData
{
  vec3 pos;
  float size;
  vec4 color;
};


layout(std430, binding = 0) readonly buffer shader_storage
{
  InstanceData instances_data[];
};


layout(location = 0) in vec2 in_pos;
// layout(location = 1) in vec3 in_normal;
layout(location = 1) in vec2 in_texcoords;

out vec2 pass_texcoords;
out vec4 pass_color;


uniform mat4 projection;
uniform mat4 world_to_view;
uniform vec3 camera_pos;

uniform InstanceData instance;


void main(void)
{
  float size = 1;

  pass_texcoords = in_texcoords;


  vec3 pos_model = vec3(in_pos - vec2(0.5), 0);



  vec3 center_world = vec3(0);

#if 1
  center_world = instances_data[gl_InstanceID + gl_BaseInstanceARB].pos;
  size = instances_data[gl_InstanceID + gl_BaseInstanceARB].size;
  pass_color = instances_data[gl_InstanceID + gl_BaseInstanceARB].color;
#else
  center_world = instance.pos;
  size = instance.size;
  pass_color = instance.color;
#endif

  vec3 center_view = (world_to_view * vec4(center_world, 1)).xyz;

//   vec3 view_dir = normalize(center_world - camera_pos);
  vec3 view_dir = normalize(center_view);

  vec3 right = normalize(cross(view_dir, vec3(0.0, 1.0, 0.0)));
  vec3 up = normalize(cross(right, view_dir));

  vec3 vertex_offset = pos_model * size;

  vec3 pos_view = center_view + vertex_offset.x * right + vertex_offset.y * up;

//   pos.xy *= instances_data[gl_InstanceID].size;
//   pos += instances_data[gl_InstanceID].pos;

//   vec4 view = world_to_view * vec4(pos, 1.0);
//   vec4 view = vec4(pos, 1.0);
  vec4 view = vec4(pos_view, 1.0);


  gl_Position = projection * view;
}
