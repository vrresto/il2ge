#version 130

uniform vec4 texcoord_scale;
varying vec2 pass_texcoord;

void main()
{
  gl_Position = vec4(gl_Vertex.x, gl_Vertex.y, 1, 1);
//   pass_texcoord = (vec2(gl_Vertex.x, gl_Vertex.y) + vec2(1)) / 2;
//   pass_texcoord.y = 1 - pass_texcoord.y;
//   pass_texcoord = (gl_MultiTexCoord0.xy + texcoord_scale.xy) * texcoord_scale.zw;
  pass_texcoord = gl_MultiTexCoord0.xy;
}
