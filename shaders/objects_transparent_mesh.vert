#version 130

uniform mat4 model_view;
uniform mat4 projection;

varying vec3 pass_normal;
varying vec2 pass_texcoord;
varying vec4 pass_pos_view;

//FIXME legacy
uniform mat4 view2WorldMatrix;
varying vec3 passObjectPos;
varying vec3 passObjectPosFlat;

void main(void)
{
  vec4 pos = model_view * gl_Vertex;
  pass_pos_view = pos;
  gl_Position = projection * pos;
  pass_texcoord = gl_MultiTexCoord0.xy;
//   pass_normal = gl_Normal;
  pass_normal = (model_view * vec4(gl_Normal, 0)).xyz;

  passObjectPos = (view2WorldMatrix * pass_pos_view).xyz;
  passObjectPosFlat = passObjectPos;
}
