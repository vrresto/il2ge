#version 430


struct MeshInstance
{
  mat4 model_view;
};


layout(std430, binding = 1) readonly buffer shader_storage
{
  MeshInstance instances[];
};


layout(location = 0) in vec3 in_pos;
// layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoords;
layout(location = 3) in uint mesh_instance;

out vec2 pass_texcoords;


uniform mat4 model_view;
uniform mat4 projection;


void main(void)
{
  pass_texcoords = in_texcoords;
  vec4 view = instances[mesh_instance].model_view * vec4(in_pos, 1.0);
  gl_Position = projection * view;
}
