#version 130

uniform sampler2D sampler;
uniform bool texture_is_greyscale;
uniform vec4 color_scale;

varying vec2 pass_texcoord;

void main()
{
  vec4 texcolor = texture(sampler, pass_texcoord);

  if (texture_is_greyscale)
  {
    gl_FragColor = color_scale;
    gl_FragColor.a *= texcolor.r;
  }
  else
  {
    gl_FragColor = texcolor * color_scale;
  }
}
