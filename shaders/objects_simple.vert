#version 130

varying vec2 pass_texcoord;

void main(void)
{
  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

  gl_FrontColor = gl_Color;
  gl_BackColor = gl_Color;

  pass_texcoord = gl_MultiTexCoord0.xy;
}
