#version 430

#extension GL_ARB_shader_draw_parameters : require

struct InstanceData
{
  mat4 model_view;
};


layout(std430, binding = 0) readonly buffer shader_storage
{
  InstanceData instances_data[];
};


layout(location = 0) in vec3 in_pos;
// layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_texcoords;

out vec2 pass_texcoords;


uniform mat4 projection;


void main(void)
{
  pass_texcoords = in_texcoords;

  vec4 view = instances_data[gl_InstanceID + gl_BaseInstanceARB].model_view * vec4(in_pos, 1.0);
  gl_Position = projection * view;
}
