#version 330

// #define USE_HDR @use_hdr:0@
#define USE_HDR 1
#define ENABLE_ATMOSPHERE @enable_atmosphere@

#include lighting_definitions.glsl

vec3 textureColorCorrection(vec3 color);
void getIncomingLight(vec3 pos, out vec3 ambientLight, out vec3 directLight);
vec3 fogAndToneMap(vec3 color, bool no_inscattering);
vec3 toneMap(vec3 color);
vec3 deGamma(vec3 color);
float deGamma(float color);

uniform sampler2D sampler_0;
// uniform vec3 sun_dir_world;
uniform vec3 sun_dir_view;
uniform vec3 cameraPosWorld;
uniform mat4 world2ViewMatrix;
uniform mat4 view2WorldMatrix;

uniform float specular_pow;
uniform float specular_amount;
// float specular_pow = 8;
// float specular_amount = 1;


uniform vec4 color_scale;
uniform bool alpha_test_enabled;
uniform bool texture_is_greyscale;

varying vec3 pass_normal;
varying vec2 pass_texcoord;
varying vec4 pass_pos_view;


#if USE_HDR
const float DIRECT_LIGHT_SCALE = 1.0;
const float AMBIENT_LIGHT_SCALE = 1.0;
#else
const float DIRECT_LIGHT_SCALE = 1.1;
const float AMBIENT_LIGHT_SCALE = 0.8;
#endif

vec4 getSpecular(vec3 incoming, vec3 sun_dir, vec3 view_dir)
{
  vec3 R = reflect(view_dir, pass_normal);
  vec3 lVec = -sun_dir;

  float amount = specular_amount * pow(max(dot(R, lVec), 0.0), specular_pow);

  amount = deGamma(amount);

  vec4 specular;
  specular.rgb = amount * incoming;
  specular.a = amount;

  return specular;
}


vec3 getReflectedDirectLight(vec3 sun_dir, vec3 normal, vec3 incoming)
{
  return incoming * max(dot(normal, sun_dir), 0.0);
}

vec3 getReflectedAmbientLight(vec3 sun_dir, vec3 normal, vec3 incoming)
{
  return incoming;
}


void main(void)
{
  gl_FragColor = texture(sampler_0, pass_texcoord);

  if (texture_is_greyscale)
  {
    gl_FragColor.a = gl_FragColor.r;
    gl_FragColor.xyz = vec3(1);
  }

  gl_FragColor *= color_scale;

  gl_FragColor.rgb = textureColorCorrection(gl_FragColor.rgb);

//   gl_FragColor.rgb = vec3(1);

  vec3 normal_world = (view2WorldMatrix * vec4(pass_normal, 0)).xyz;
  vec3 pos_world = (view2WorldMatrix * pass_pos_view).xyz;

  vec3 view_dir_world = normalize(pos_world - cameraPosWorld);
  vec3 view_dir_view = -normalize(pass_pos_view.xyz);

  vec3 light_ambient_incoming = vec3(0.3);
  vec3 light_direct_incoming = vec3(1.0);
  getIncomingLight(pos_world, light_ambient_incoming, light_direct_incoming);


  vec3 light_direct = getReflectedDirectLight(sun_dir_view, pass_normal, light_direct_incoming);
  vec3 light_ambient = getReflectedAmbientLight(sun_dir_view, pass_normal, light_ambient_incoming);
//   vec3 light_direct = getReflectedDirectLight(sun_dir_world, normal_world, light_direct_incoming);
//   vec3 light_ambient = getReflectedAmbientLight(sun_dir_world, normal_world, light_ambient_incoming);


  light_direct *= DIRECT_LIGHT_SCALE;
  light_ambient *= AMBIENT_LIGHT_SCALE;
  
//   light_direct *= 0;

  vec4 light_specular = getSpecular(light_direct_incoming, sun_dir_view, view_dir_view);

  if (alpha_test_enabled)
  {
    light_specular.a = 0;
  }

  gl_FragColor.xyz = gl_FragColor.xyz * (light_direct + light_ambient);

  gl_FragColor+= light_specular;


//   gl_FragColor.xyz = normal_world;
//   gl_FragColor.xyz = pass_normal;
//   gl_FragColor.a = 1;

//   if (!alpha_test_enabled)
//   {
// //     if (gl_FragCoord.x > 900)
//       gl_FragColor.a = 1;
//   }
//   else
//   {
// //     if (gl_FragCoord.x < 900)
// //       gl_FragColor.a = 0;
//   }

#if ENABLE_ATMOSPHERE
  bool blend_add = false;
  gl_FragColor.rgb = fogAndToneMap(gl_FragColor.rgb, blend_add);
#else
  gl_FragColor.xyz = toneMap(gl_FragColor.xyz);
#endif
}
