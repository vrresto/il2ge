set(CMAKE_SYSTEM_NAME Windows)

set(triple i686-w64-mingw32)

set(CMAKE_C_COMPILER clang)
set(CMAKE_C_COMPILER_TARGET ${triple})
set(CMAKE_CXX_COMPILER clang++)
set(CMAKE_CXX_COMPILER_TARGET ${triple})

set(CMAKE_SHARED_LIBRARY_PREFIX_C "")
set(CMAKE_SHARED_LIBRARY_PREFIX_CXX "")

add_link_options(-static-libgcc -static-libstdc++)

set(platform_mingw 1)
