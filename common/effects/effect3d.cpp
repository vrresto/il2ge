#include <il2ge/effect3d.h>


using namespace glm;


namespace
{


//FIXME see jni Mesh HierMesh
void applyRotation(const glm::vec3 &yaw_pitch_roll, glm::mat4 &mat)
{
  mat = glm::rotate(mat, glm::radians(-yaw_pitch_roll.x), glm::vec3(0, 0, 1));
  mat = glm::rotate(mat, glm::radians(-yaw_pitch_roll.y), glm::vec3(0, 1, 0));
  mat = glm::rotate(mat, glm::radians(+yaw_pitch_roll.z), glm::vec3(1, 0, 0));
}


} // namespace


namespace il2ge
{


void Effect3D::updateRotation()
{
  glm::vec3 default_direction(1,0,0);
  glm::vec3 default_pitch_axis(0,1,0);

  glm::mat4 rotation(1);
  applyRotation(m_yaw_pitch_roll_deg, rotation);

  m_direction = vec3(rotation * vec4(default_direction, 0));
  m_pitch_axis = vec3(rotation * vec4(default_pitch_axis, 0));
}


} // namespace il2ge
