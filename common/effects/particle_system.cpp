/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "factory.h"
#include <il2ge/renderer.h>
#include <render_util/gl_binding/gl_functions.h>
#include <render_util/camera.h>
#include <util.h>

// #define GLM_FORCE_SSE2
// #define GLM_FORCE_AVX
// #define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
// #define GLM_FORCE_INTRINSICS
// #define GLM_FORCE_SIMD_AVX2
// #define GLM_FORCE_AVX512

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <random>
#include <set>
#include <algorithm>

static_assert(glm::vec4::length() == 4);

using namespace il2ge;
using namespace glm;
using namespace render_util::gl_binding;


#include <render_util/draw_box.h>

namespace
{


auto makeDistribution(vec2 param)
{
  auto min_value = min(param.x, param.y);
  auto max_value = max(param.x, param.y);
  return std::uniform_real_distribution<float>(min_value, max_value);
}


struct ParticleSystemParameters : public Effect3DParameters
{
  int nParticles = 0;
  float EmitFrq = 0;
  float VertAccel = 0;
  float Wind = 0;
  float GasResist = 0;
  vec2 EmitVelocity {0};
  vec2 Size {0};
  vec4 Color0 {0};
  vec4 Color1 {0};
  vec2 EmitTheta {0};
  float Rnd = 0;
  float PsiN = 0;


  const char *getJavaClassName() const override
  {
    return "com/maddox/il2/engine/EffParticles";
  }

  void set(const Effect3DParameters &other_) override
  {
    auto other = dynamic_cast<const ParticleSystemParameters*>(&other_);
    assert(other);
    *this = *other;
  }


  void applyFrom(const ParameterFile &file) override
  {
    Effect3DParameters::applyFrom(file);

    auto &general = file.getSection("General");

    general.get_noexcept("LiveTime", LiveTime);
    general.get_noexcept("FinishTime", FinishTime);
    general.get_noexcept("nParticles", nParticles);
    general.get_noexcept("EmitFrq", EmitFrq);
    general.get_noexcept("VertAccel", VertAccel);
    general.get_noexcept("EmitVelocity", EmitVelocity);
    general.get_noexcept("Size", Size);
    general.get_noexcept("Color0", Color0);
    general.get_noexcept("Color1", Color1);
    general.get_noexcept("GasResist", GasResist);
    general.get_noexcept("Wind", Wind);
    general.get_noexcept("Rnd", Rnd);
    general.get_noexcept("EmitTheta", EmitTheta);
    general.get_noexcept("PsiN", PsiN);


//     if (!nParticles)
//     {
//       std::cout<<"nParticles: "<<nParticles<<std::endl;
//       std::cout<<"file: "<<loaded_from<<std::endl;
//       std::cout<<"content:\n"<<loaded_from_content<<std::endl;
//     }

    // assert(LiveTime);
  }


  std::unique_ptr<Effect3D> createEffect() const override;
};


class ParticleSystem : public Effect3D
{
  struct Particle : public Effect3DParticleBase
  {
    float age = -1;
    vec3 speed = vec3(0);
    float intensity = 1;
  };

  const ParticleSystemParameters &m_params;
  std::vector<Particle> m_particles;
  float m_emit_timeout = 0;
  size_t m_num_particles = 0;
  size_t m_oldest_particle = 0;
  float m_age = 0;
  int m_active_particles = 0;
  render_util::Box m_bounding_box;

  // std::default_random_engine m_rand_engine;
  std::minstd_rand m_rand_engine; // faster

  std::uniform_real_distribution<float> m_rand_speed_dist;
  std::uniform_real_distribution<float> m_rand_pitch_dist;
  std::uniform_real_distribution<float> m_rand_yaw_dist { glm::radians(0.f),
                                                          glm::radians(360.f) };

  std::uniform_real_distribution<float> m_rand_emit_timeout_dist { 0.7f, 1.5f };

public:
  ParticleSystem(const ParticleSystemParameters &params) : Effect3D(params), m_params(params)
  {
    m_rand_speed_dist = makeDistribution(m_params.EmitVelocity);
    m_rand_pitch_dist = makeDistribution(glm::radians(90.f * (m_params.EmitTheta / 100.f)));

    m_particles.resize(m_params.nParticles);

    m_bounding_box.set(vec3(0), vec3(0));
  }


  ~ParticleSystem() {}


  void initParticle(Particle &p, vec3 pos, vec3 emitter_speed_vector)
  {
    p.pos = pos;
    p.age = 0;
    p.intensity = getIntensity();
    p.size = m_params.Size.x;

    float rand_pitch_angle_rad = m_rand_pitch_dist(m_rand_engine);
    float rand_yaw_angle_rad = m_rand_yaw_dist(m_rand_engine);

    auto dir = glm::rotate(getDirection(), rand_pitch_angle_rad, getPitchAxis());
    dir = glm::rotate(dir, rand_yaw_angle_rad, getDirection());

    float rand_speed = m_rand_speed_dist(m_rand_engine);

    p.speed = emitter_speed_vector + dir * rand_speed;
  }


  Particle& emitParticle(vec3 pos, vec3 emitter_speed_vector)
  {
    if (m_particles.empty())
    {
      abort();
    }
    else if (m_num_particles < m_particles.size())
    {
      auto&p = m_particles[m_num_particles];
      initParticle(p, pos, emitter_speed_vector);
      m_num_particles++;
      return p;
    }
    else
    {
      auto& p = m_particles[m_oldest_particle];
      initParticle(p, pos, emitter_speed_vector);
      m_oldest_particle = (m_oldest_particle+1) % m_particles.size();
      return p;
    }
  }


  vec3 applyAirResistance(vec3 speed_vec,
                          int steps,
                          float step_delta)
  {
    // https://en.wikipedia.org/wiki/Drag_equation

    auto speed = length(speed_vec);

    if (speed == 0)
      return vec3(0);

    auto dir = normalize(speed_vec);

    //FIXME - can this be integrated?
    for (int i = 0; i < steps; i++)
    {
      auto deceleration = m_params.GasResist * speed * speed;
      speed = max(0.f, speed - step_delta * deceleration);
    }

    return dir * speed;
  }


  bool isFinished()
  {
    return Effect3D::isFinished() || ((m_params.FinishTime > 0) && (m_age > m_params.FinishTime));
  }


  bool updateParticle(Particle& p, float delta, vec2 wind_speed,
                      int air_resistance_steps, float air_resistance_step_delta)
  {
    p.age += delta;

    if (p.age > m_params.LiveTime || p.intensity == 0.f)
      return false;

    float relative_age = p.age / m_params.LiveTime;

    vec3 speed = p.speed;

    vec3 accel {};
    accel.z += m_params.VertAccel;

    speed += accel * delta;

    speed = applyAirResistance(speed,
                                air_resistance_steps,
                                air_resistance_step_delta);

    p.speed = speed;

    speed += vec3(wind_speed, 0);

    p.pos += speed * delta;

    p.size = getSize() * mix(m_params.Size.x, m_params.Size.y, relative_age);

    p.color = mix(m_params.Color0, m_params.Color1, relative_age);
    p.color.a *= p.intensity;

    float turns = relative_age * m_params.PsiN;

    p.rotation = 2 * util::PI * turns;

    return true;
  }


  void update(float delta, const glm::vec2 &wind_speed_) override
  {
    if (isPaused())
      return;

    if (!m_params.EmitFrq)
      return;

    const float air_resistance_step_duration = 0.01;
    int air_resistance_steps = int(delta / air_resistance_step_duration) + 1;
    // std::cout<<"air resistance steps: "<<air_resistance_steps<<std::endl;
    float air_resistance_step_delta = delta / air_resistance_steps;

    m_age += delta;

    auto wind_speed = wind_speed_ * m_params.Wind;

    m_emit_timeout -= delta;

    int active_particles = 0;

    dvec3 min_pos = getPos();
    dvec3 max_pos = getPos();

    for (size_t i = 0; i < m_num_particles; i++)
    {
      auto &p = m_particles[(m_oldest_particle + i) % m_particles.size()];

      if (updateParticle(p, delta, wind_speed,
                        air_resistance_steps,
                        air_resistance_step_delta))
      {
        min_pos = min(min_pos, p.pos);
        max_pos = max(max_pos, p.pos);
        active_particles++;
      }
    }

    if (!isFinished())
    {
      float speed = 0;
      vec3 moving_dir {};
      float prev_pos_dist = distance(getPos(), getPrevPos());
      if (prev_pos_dist > 0.f && delta > 0.f)
      {
        speed = prev_pos_dist / delta;
        moving_dir = normalize(getPos() - getPrevPos());
      }

      while (m_emit_timeout <= 0.f)
      {
        if (active_particles < m_particles.size())
        {
          float age = abs(m_emit_timeout);

          auto pos = getPos() - moving_dir * speed * age;

          auto& p = emitParticle(pos, moving_dir * speed);

          int air_resistance_steps = int(age / air_resistance_step_duration) + 1;
          float air_resistance_step_delta = age / air_resistance_steps;

          if (updateParticle(p, age, wind_speed,
                             air_resistance_steps, air_resistance_step_delta))
          {
            min_pos = min(min_pos, p.pos);
            max_pos = max(max_pos, p.pos);
            active_particles++;
          }
        }

        m_emit_timeout += m_rand_emit_timeout_dist(m_rand_engine)
                          * (1.f / m_params.EmitFrq);
      }
    }

    m_active_particles = active_particles;
    m_bounding_box.set(min_pos, max_pos - min_pos);
  }


  size_t getNumParticles() override { return m_num_particles; }


  void render() override
  {
    if (getIntensity() <= 0)
      return;

    for (size_t i = 0; i < m_num_particles; i++)
    {
      auto &p = m_particles[(m_oldest_particle + i) % m_particles.size()];

      if (p.age > m_params.LiveTime)
        continue;

      auto &pos = p.pos;
      auto size = p.size / 2;
      auto &color = p.color;

      gl::Color4f(color.x, color.y, color.z, color.w);

      render_util::drawBox(pos.x, pos.y, pos.z, size);
    }
  }


  void addToRenderList(renderer::Renderer& renderer, const render_util::Camera& camera) override
  {
    if (!m_active_particles)
      return;

    assert(material);

    std::vector<renderer::ParticleInstance> instances;
    instances.reserve(m_particles.size());

    float nearest_dist = std::numeric_limits<float>::max();

    for (size_t i = 0; i < m_num_particles; i++)
    {
      auto real_index = (m_oldest_particle + i) % m_particles.size();

      auto &p = m_particles[real_index];

      if (p.age > m_params.LiveTime || p.intensity == 0.f)
        continue;

      // nearest_dist = min(nearest_dist, distance(vec3(p.pos), camera.getPos()));
      nearest_dist = min(nearest_dist,
                         max(0.f, distance(vec3(p.pos), camera.getPos()) - (p.size / 2.f)));

#if 0
      const float min_lod_dist = 500;

      int lod = max(log2(distance(vec3(p.pos), camera.getPos()) / min_lod_dist), 0.f);
      int divisor = pow(2, lod);

      if (real_index % divisor != 0)
        continue;
#endif

      instances.emplace_back();
      auto& instance = instances.back();

      instance.pos = p.pos;
      instance.size = p.size;
      instance.color = p.color;
    }

    if (!instances.empty())
      renderer.addToRenderList(std::move(instances), material, nearest_dist);
  }


  void addToRenderList(Effect3DRenderListBase &list, const render_util::Camera &camera) override
  {
    for (size_t i = 0; i < m_num_particles; i++)
    {
      auto &p = m_particles[(m_oldest_particle + i) % m_particles.size()];

      if (p.age >= m_params.LiveTime)
        continue;

      p.dist_from_camera_cm = distance(camera.getPosD(), p.pos) * 1000;


      list.add(p);
    }
  }


  bool cull(const render_util::Camera& camera) override
  {
    if (m_bounding_box.getSize() == vec3(0))
      return true;
    else
      return camera.cull(m_bounding_box);
  }
};


std::unique_ptr<Effect3D> ParticleSystemParameters::createEffect() const
{
  return std::make_unique<ParticleSystem>(*this);
}


} // namespace


namespace il2ge
{
  std::unique_ptr<Effect3DParameters> create_TParticlesSystemParams()
  {
    return std::make_unique<ParticleSystemParameters>();
  }
}
