/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <il2ge/map_resources.h>
#include <il2ge/map_properties.h>
#include <il2ge/sfs_hash.h>
#include <il2ge/map_loader.h>
#include <il2ge/image_loader.h>
#include <util.h>
#include <render_util/image_util.h>
#include <render_util/image_loader.h>
#include <render_util/texture_util.h>
#include <render_util/normal_map.h>

#include <cassert>
#include <iostream>
#include <sstream>
#include <fstream>
#include <set>

using namespace std;
using namespace util;
using namespace il2ge;

//   constexpr auto g_dummy_texture_path = "icons/empty.tga";


namespace
{


class SfsMapResources : public il2ge::MapResources
{
  std::unique_ptr<INIReader> reader;
  std::string map_dir;
  std::string dump_dir;
  std::string water_animation_dir;
  std::string water_animation_frame_prefix;
  std::string water_animation_frame_extension;
  std::set<int64_t> redirected_paths;
  std::set<int64_t> redirected_to_null_paths;
  std::string m_ini_path;
  std::string m_base_map_name;
  bool m_has_base_map = false;
  bool m_has_water_animation = false;
#if ENABLE_BASE_MAP
  std::unique_ptr<render_util::MapGeography> m_map_geography;
#endif
  std::vector<render_util::AltitudinalZone> m_altitudinal_zones;
  std::function<std::unique_ptr<il2ge::Resource>(std::string path)> m_get_resource;

  std::string getSFSPath(std::string entry, bool from_map_dir) const;
  void redirectSFSPath(std::string path) const;
  void redirectSFSPathToNull(std::string path) const;
  std::unique_ptr<il2ge::Resource> getResource(std::string path) const;
  std::unique_ptr<il2ge::Resource> getTextureResource(std::string path, bool redirect) const;
  void cacheNormalMap(std::string bumph_path, std::string cache_path, float scale);
  bool readNormalMapFile(std::string path, bool redirect, float scale, std::vector<char> &content);

  glm::vec3 getWaterColor(const glm::vec3 &default_value) const;

  std::unique_ptr<il2ge::Resource>
  getTexture(const char *section,
                          const char *name,
                          const char *default_path,
                          bool from_map_dir,
                          bool redirect,
                          float *scale = nullptr,
                          bool is_bumpmap = false) const;

  std::unique_ptr<il2ge::Resource>
  getFile(const char *section,
          const char *name,
          const char *default_path,
          bool from_map_dir,
          bool redirect,
          const char *suffix = nullptr) const;

  std::unique_ptr<il2ge::Resource>
  getWaterAnimationFrame(int index, MapResourceID) const;

public:
  SfsMapResources(const std::string &map_dir,
                       const std::string &ini_path,
                       const std::string &dump_path,
                       std::function<std::unique_ptr<il2ge::Resource>(std::string path)> get_resource);

  std::string getDumpDir() const override;

  std::unique_ptr<Resource> getResource(MapResourceID, std::optional<unsigned> index, float* scale) const override;

  MapProperty getProperty(MapPropertyID) const override;

  bool hasBaseLayer() const override;
#if ENABLE_BASE_MAP
  std::unique_ptr<BaseMapResources> getBaseLayerResources() const override;
  std::unique_ptr<render_util::MapGeography> getMapGeography() const override;
#endif
  std::vector<render_util::AltitudinalZone> getAltitudinalZones() const override;
};


void dumpFile(string name, const char *data, size_t data_size, string dir)
{
  if (!il2ge::map_loader::isDumpEnabled())
    return;

  bool res = util::writeFile(dir + name, data, data_size);
  assert(res);
}


string getBaseName(string path)
{
  return path.substr(0, path.find_last_of('.'));
}


float getTextureScale(string entry)
{
  float scale = 1.0;

  size_t comma_pos = entry.find_first_of(',');
  if ( comma_pos != string::npos && entry.size() > comma_pos+1)
  {
    string scale_str = entry.substr(comma_pos+1, string::npos);

    try
    {
      scale = stof(scale_str);
    }
    catch (...)
    {
      LOG_ERROR<<"can't convert '"<<scale_str<<"' to float"<<endl;
      LOG_ERROR<<"entry: "<<entry<<endl;
    }
  }

  return scale;
}


bool SfsMapResources::readNormalMapFile(string path, bool redirect, float scale,
                                             std::vector<char> &content)
{
  string bumph_path = path + ".BumpH";

  LOG_DEBUG<<"bumph_path: "<<bumph_path<<endl;

  util::mkdir(IL2GE_CACHE_DIR);
  util::mkdir(IL2GE_CACHE_DIR "/bumph");

  auto cache_path = string(IL2GE_CACHE_DIR "/bumph/") +
    SFSResourceBase::getUniqueID(bumph_path) + "_" + to_string(scale) + ".tga";

  bool was_read = util::readFile(cache_path, content, true);

  if (!was_read)
  {
    cacheNormalMap(bumph_path, cache_path, scale);
    was_read = util::readFile(cache_path, content, true);
  }

  if (was_read && redirect)
    redirectSFSPathToNull(bumph_path);

  return was_read;
}


void SfsMapResources::cacheNormalMap(string bumph_path, string cache_path, float scale)
{
  LOG_DEBUG<<"bumph_path: "<<bumph_path<<endl;

  auto content = m_get_resource(bumph_path)->readAll();

  auto image = il2ge::loadImageFromMemory(content, bumph_path.c_str());
  if (image)
  {
    auto heightmap = render_util::image::getChannel(*image, 0);

    auto normal_map = std::make_unique<render_util::ImageRGB>(heightmap->getSize());
    render_util::createNormalMapFromUint8(*heightmap, *normal_map, 5.0,
                                          il2ge::TERRAIN_METERS_PER_TEXTURE_TILE, true);

    LOG_DEBUG<<"caching "<<bumph_path<<" -> "<<cache_path<<endl;
    render_util::saveImageToFile(cache_path, normal_map.get());
  }
  else
  {
    LOG_DEBUG<<bumph_path<<": loadImageFromMemory() failed"<<endl;
  }
}


std::unique_ptr<il2ge::Resource> SfsMapResources::getResource(string path) const
{
  auto res = m_get_resource(path);
  assert(res);
  auto file = res->open();
  assert(file != nullptr);
  return res;
}


void SfsMapResources::redirectSFSPath(string path) const
{
  auto hash = sfs::getHash(path.c_str());
//   redirected_paths.insert(hash); FIXME
}


void SfsMapResources::redirectSFSPathToNull(std::string path) const
{
  auto hash = sfs::getHash(path.c_str());
//   redirected_to_null_paths.insert(hash); FIXME
}


std::unique_ptr<il2ge::Resource>
SfsMapResources::getTextureResource(string path, bool redirect) const
{
  if (redirect)
    redirectSFSPath(path);
  return getResource(path);
}


SfsMapResources::SfsMapResources(const string &map_dir,
                                 const string &ini_path,
                                 const std::string &dump_path,
                                 GetSfsResourceFunc get_resource) :
  map_dir(map_dir),
  dump_dir(dump_path + '/'),
  m_ini_path(ini_path),
  m_get_resource(get_resource)
{
  assert(!map_dir.empty());
  assert(!ini_path.empty());

  LOG_TRACE << "reading load.ini (" << ini_path << ")" << endl;

  try
  {
    auto ini_content = m_get_resource(ini_path)->readAll();
    LOG_TRACE<<"done reading load.ini"<<endl;

    dumpFile("load.ini", ini_content.data(), ini_content.size(), dump_dir);

    LOG_TRACE<<"parsing load.ini"<<endl;
    reader = make_unique<INIReader>(ini_content.data(), ini_content.size());
    if (reader->ParseError()) {
      LOG_ERROR << "parse error at line " << reader->ParseError() << endl;
      exit(1);
    }
    LOG_TRACE<<"done parsing load.ini"<<endl;
  }
  catch (std::exception &e)
  {
    LOG_ERROR << "Failed to read " << ini_path << ": " << e.what() << std::endl;
    throw std::runtime_error("Failed to read " + ini_path + ": " + e.what());
  }

#if ENABLE_BASE_MAP
  {
    auto load_il2ge_ini_path =
      map_dir + util::basename(m_ini_path, true) + ".il2ge.ini";

    std::vector<char> data;
    try
    {
      data = m_get_resource(load_il2ge_ini_path)->readAll();
      assert(!data.empty());
    }
    catch (std::exception &e)
    {
      LOG_INFO << "Couldn't read " << load_il2ge_ini_path
              << ": " << e.what() << std::endl;
    }

    if (!data.empty())
    {
      INIReader ini(data.data(), data.size());
      if (ini.ParseError())
      {
        throw std::runtime_error("Error parsing " + load_il2ge_ini_path
                                  + " at line " + std::to_string(ini.ParseError()));
      }
      m_map_geography = loadMapGeography(ini, load_il2ge_ini_path);
      m_altitudinal_zones = loadAltitudinalZones(ini, load_il2ge_ini_path);
    }
  }

  m_base_map_name = util::basename(util::getDirFromPath(ini_path));

  if (m_map_geography && BaseMapResources::exists(m_base_map_name))
  {
    m_has_base_map = true;
  }
#endif

  {
    auto entry = reader->Get("APPENDIX", "WaterNoiseAnimStart", "");

    if (!entry.empty())
    {
      auto first_frame_path = getSFSPath(entry, false);
      auto first_frame_basename = util::basename(first_frame_path, true);

      // FIXME use util::isSuffix()
      assert(first_frame_basename.size() >= 3);
      assert(first_frame_basename.at(first_frame_basename.size()-1) == '0');
      assert(first_frame_basename.at(first_frame_basename.size()-2) == '0');
      assert(first_frame_basename.at(first_frame_basename.size()-3) != '0');

      water_animation_frame_prefix =
        first_frame_basename.substr(0, first_frame_basename.size()-2);
      water_animation_dir = util::getDirFromPath(first_frame_path);
      water_animation_frame_extension = util::getFileExtensionFromPath(first_frame_path);

      m_has_water_animation = true;
    }
  }
}


std::string SfsMapResources::getDumpDir() const
{
  return dump_dir;
}


glm::vec3 SfsMapResources::getWaterColor(const glm::vec3 &default_value) const
{
  glm::vec3 water_color = default_value;

  string value = reader->Get("WATER", "WaterColorATI", "");
//   string value = reader->Get("WATER", "WaterColorNV", "");

  if (!value.empty())
  {
    istringstream stream(value);
    stream >> water_color.r;
    stream >> water_color.g;
    stream >> water_color.b;
  }

 return water_color;
}


string SfsMapResources::getSFSPath(string entry, bool from_map_dir) const
{
  auto comma_pos = entry.find_first_of(',');
  auto filename = entry.substr(0, comma_pos);

  auto dir = from_map_dir ? map_dir : "maps/_Tex/";

  return dir + filename;
}


std::unique_ptr<il2ge::Resource>
SfsMapResources::getTexture(const char *section,
                        const char *name,
                        const char *default_path,
                        bool from_map_dir,
                        bool redirect,
                        float *scale,
                        bool is_bumpmap) const
{
  auto entry = reader->Get(section, name, default_path);

  if (entry.empty())
    throw NoEntryException(section, name);

  if (scale)
    *scale = getTextureScale(entry);

  auto path = getBaseName(getSFSPath(entry, from_map_dir));

  auto get_texture = [&] (std::string suffix)
  {
    if (is_bumpmap)
    {
      assert(0);
      abort();
  //     return openNormalMapFile(path, *scale, redirect);
    }
    else
    {
      try
      {
        return getTextureResource(path + suffix + ".tgb", redirect);
      }
      catch (...)
      {
        return getTextureResource(path + suffix + ".tga", redirect);
      }
    }
  };

  try
  {
    return get_texture("TL");
  }
  catch (...)
  {
    return get_texture("");
  }
}


std::unique_ptr<il2ge::Resource>
SfsMapResources::getFile(const char *section,
                        const char *name,
                        const char *default_path,
                        bool from_map_dir,
                        bool redirect,
                        const char *suffix) const
{
  auto entry = reader->Get(section, name, default_path);

  if (entry.empty())
    throw NoEntryException(section, name);

  auto path = getSFSPath(entry, from_map_dir);

  if (suffix)
    path += suffix;

  return getResource(path);
}


std::unique_ptr<il2ge::Resource>
SfsMapResources::getWaterAnimationFrame(int index, MapResourceID type) const
{
  if (!m_has_water_animation)
    throw std::runtime_error("no water animation");

  assert(index < 100);

  char counter[100];
  snprintf(counter, sizeof(counter), "%.2d", index);

  auto extension = water_animation_frame_extension;

  std::string basename;
  switch (type)
  {
    case MapResourceID::WATER_ANIMATION_NORMAL_MAP:
      basename = water_animation_frame_prefix + counter + "Dot3";
      break;
    case MapResourceID::WATER_ANIMATION_HEIGHT_MAP:
      basename = water_animation_frame_prefix + counter;
      extension = "BumpH";
      break;
    default:
      abort();
  }

  auto path = water_animation_dir + '/' + basename + '.' + extension;

  return getTextureResource(path, true);
}


std::unique_ptr<Resource> SfsMapResources::getResource(MapResourceID id,
                                                            std::optional<unsigned> index,
                                                            float* scale) const
{
  switch (id)
  {
    case MapResourceID::FOAM_TEXTURE:
      return getResource("maps/_tex/water/FoamNV40.tga");
    case MapResourceID::FIELD_TEXTURE:
      return getTexture("FIELDS", getMapFieldName(index.value()), "", false, true, scale, false);
    case MapResourceID::FIELD_NORMAL_MAP:
      return getTexture("FIELDS", getMapFieldName(index.value()), "", false, true, scale, true);
    case MapResourceID::WATER_ANIMATION_HEIGHT_MAP:
    case MapResourceID::WATER_ANIMATION_NORMAL_MAP:
      return getWaterAnimationFrame(index.value(), id);
    case MapResourceID::WATER_TEXTURE:
        return getTexture("WATER", "Water", "", false, true);
    case MapResourceID::WATER_NOISE_TEXTURE:
        return getTexture("APPENDIX", "WaterNoise", "", false, true);
    case MapResourceID::TYPE_MAP:
      return getTexture("MAP", "TypeMap", "map_t.tga", true, false);
    case MapResourceID::HEIGHT_MAP:
      return getTexture("MAP", "HeightMap", "map_h.tga", true, false);
    case MapResourceID::LAND_MAP:
      return getTexture("MAP", "ColorMap", "map_c.tga", true, false);
    case MapResourceID::LAND_MAP_TABLE:
      return getFile("MAP", "ColorMap", "map_c.tga", true, false, "_table");
    case MapResourceID::NOISE_TEXTURE:
      return getTexture("APPENDIX", "ShadeNoise", "land/Noise.tga", false, true);
    case MapResourceID::FAR_NOISE_TEXTURE:
      return getTextureResource("maps/_tex/land/Fnoise.tga", true);
    case MapResourceID::HIGH_CLOUDS:
      return getTexture("APPENDIX", "HighClouds", "", false, true);
    case MapResourceID::FOREST_LAYER:
      return getTexture("WOOD", ("Wood" + std::to_string(index.value())).c_str(),
                        "", false, true);
    default:
      throw std::runtime_error(std::string("Resource ") + getMapResourceName(id) + " not handled");
  }
}


MapProperty SfsMapResources::getProperty(MapPropertyID id) const
{
  switch (id)
  {
    case MapPropertyID::WATER_COLOR:
      return MapProperty(getWaterColor(glm::vec3(0)));
    default:
      throw std::runtime_error("Property "  + getMapPropertyName(id)  + " not handled");
  }
}


bool SfsMapResources::hasBaseLayer() const
{
  return m_has_base_map;
}


#if ENABLE_BASE_MAP
std::unique_ptr<BaseMapResources>
SfsMapResources::getBaseLayerResources() const
{
  assert(m_has_base_map);
  return std::make_unique<BaseMapResources>(m_base_map_name);
}


std::unique_ptr<render_util::MapGeography> SfsMapResources::getMapGeography() const
{
  assert(m_map_geography);
  return  std::make_unique<render_util::MapGeography>(*m_map_geography);
}
#endif


std::vector<render_util::AltitudinalZone>
SfsMapResources::getAltitudinalZones() const
{
  return m_altitudinal_zones;
}


#if 0
glm::vec3 SfsMapResources::getBaseMapOrigin() const
{
  glm::vec3 origin =
  {
    reader->GetReal("il2ge", "base_map_origin_x", 0),
    reader->GetReal("il2ge", "base_map_origin_y", 0),
    reader->GetReal("il2ge", "base_map_origin_z", 0),
  };

  return origin;
}
#endif


} // namespace


namespace il2ge
{


std::unique_ptr<MapResources>
getSfsMapResources(const std::string &map_dir,
                   const std::string &ini_path,
                   const std::string &dump_path,
                   GetSfsResourceFunc get_resource)
{
  return std::make_unique<SfsMapResources>(map_dir, ini_path, dump_path, get_resource);
}


} // namespace il2ge
