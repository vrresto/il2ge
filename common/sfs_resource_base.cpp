/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <il2ge/map_resources.h>
#include <il2ge/sfs_hash.h>

namespace il2ge
{


SFSResourceBase::SFSResourceBase(std::string sfs_path) :
  m_unique_id(getUniqueID(sfs_path)),
  m_sfs_path(sfs_path)
{
}


std::string SFSResourceBase::getUniqueID(int64_t hash)
{
  uint64_t as_unsigned;
  memcpy(&as_unsigned, &hash, sizeof(as_unsigned));

  return std::to_string(as_unsigned);
}


std::string SFSResourceBase::getUniqueID(std::string sfs_path)
{
  return getUniqueID(sfs::getHash(sfs_path.c_str()));
}


} // namespace il2ge
