/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <il2ge/map_properties.h>
#include <render_util/physics.h>

namespace il2ge
{


std::unique_ptr<render_util::MapGeography> createDefaultMapGeography()
{
  auto meters_per_degree_longitude = render_util::physics::EARTH_CIRCUMFERENCE / 360.0;
  auto map_projection = "mercator-spherical";
  auto origin = glm::dvec2(0);

  return std::make_unique<render_util::MapGeography>(origin,
                                                     meters_per_degree_longitude,
                                                     map_projection);
}


std::unique_ptr<render_util::MapGeography> loadMapGeography(INIReader &ini, std::string ini_path)
{
   auto latitude = ini.GetReal("Geography", "MapOriginLatitude", -1000.0);
    auto longitude = ini.GetReal("Geography", "MapOriginLongitude", -1000.0);

    if (latitude < -90 || latitude > 90)
      throw std::runtime_error(ini_path + ": Latitude invalid: " + std::to_string(latitude));

    if (longitude < -180 || longitude > 180)
      throw std::runtime_error(ini_path + ": Longitude invalid: " + std::to_string(longitude));

    auto origin  = glm::dvec2(longitude, latitude);

    auto meters_per_degree_longitude = ini.GetReal("Geography", "MetersPerDegreeLongitude", 0.0);

    if (meters_per_degree_longitude <= 0)
      throw std::runtime_error(ini_path +
        ": MetersPerDegreeLongitude invalid : " +
        std::to_string(meters_per_degree_longitude));

    auto map_projection = ini.Get("Geography", "MapProjection", "");
    if (map_projection.empty())
      throw std::runtime_error(ini_path + ": MapProjection not set");

  return std::make_unique<render_util::MapGeography>(origin, meters_per_degree_longitude, map_projection);
}


void saveMapGeography(const render_util::MapGeography& p, std::ostream &out)
{
  out.precision(std::numeric_limits<double>::digits10);
  out << "[Geography]" << std::endl;
  out << "MapOriginLongitude = " << p.getOriginGeodesic().x << std::endl;
  out << "MapOriginLatitude = " << p.getOriginGeodesic().y << std::endl;
  out << "MetersPerDegreeLongitude = " <<  p.getMetersPerDegreeLongitude() << std::endl;
  out << "MapProjection = " << p.getMapProjection() << std::endl;
}


std::vector<render_util::AltitudinalZone>
loadAltitudinalZones(INIReader &ini, std::string ini_path)
{
  constexpr auto GROUP = "AltitudinalZones";

  std::vector<render_util::AltitudinalZone> ret;

  auto number = ini.GetInteger(GROUP, "Number", 0);
  if (number > 0)
  {
    ret.resize(number);

    for (int i = 0; i < ret.size(); i++)
    {
      auto& zone = ret.at(i);

      auto key = std::to_string(i) + "_TransitionStartHeight";
      zone.transition_start_height = ini.GetReal(GROUP, key.c_str(), 0);

      key = std::to_string(i) + "_TransitionEndHeight";
      zone.transition_end_height = ini.GetReal(GROUP, key.c_str(), 0);

      key = std::to_string(i) + "_Color";
      auto color = ini.Get(GROUP, key.c_str(), "0 0 0");
      if (!color.empty())
      {
        std::istringstream in(color);
        in >> zone.color.r;
        in >> zone.color.g;
        in >> zone.color.b;
      }

      key = std::to_string(i) + "_TypeIndex";
      zone.type_index = ini.GetReal(GROUP, key.c_str(), 0);

      key = std::to_string(i) + "_Name";
      zone.name = ini.Get(GROUP, key.c_str(), "Unnamed");
    }
  }

  return ret;
}


void saveAltitudinalZones(const std::vector<render_util::AltitudinalZone>& zones,
                          std::ostream& out)
{
  out << "[AltitudinalZones]" << std::endl;
  out << "Number = " << zones.size() << std::endl;
  int i = 0;
  for (auto& zone : zones)
  {
    out << i << "_TransitionStartHeight = " << zone.transition_start_height << std::endl;
    out << i << "_TransitionEndHeight = " << zone.transition_end_height << std::endl;
    out << i << "_Color = " << zone.color.r << " "
                            << zone.color.g << " "
                            << zone.color.b << std::endl;
    out << i << "_TypeIndex = " << zone.type_index << std::endl;
    out << i << "_Name = " << zone.name << std::endl;
    i++;
  }
}


std::map<std::string, int> loadLandCoverTypes(INIReader &ini, std::string ini_path)
{
  LOG_INFO<<"loadLandCoverTypes"<<std::endl;;
  std::map<std::string, int> map;
  for (auto& type : BaseMapResources::getLandCoverTypeNames())
  {
    map[type] = ini.GetInteger("LandCoverTypes", type, 0);
  }
  return map;
}


void saveLandCoverTypes(const std::map<std::string, int>& map, std::ostream& out)
{
  out << "[LandCoverTypes]" << std::endl;
  for (auto& it : map)
  {
    out << it.first << " = " << it.second << std::endl;
  }
}


} // namespace il2ge
