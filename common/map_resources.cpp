/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <il2ge/map_resources.h>

using namespace il2ge;


namespace
{


const std::unordered_map<MapPropertyID, std::string> g_map_property_names =
{
  { MapPropertyID::WATER_COLOR, "water_color" },
};


const std::array<const char*, MAP_RESOURCES_NUM_FIELDS> g_field_names =
{
  "LowLand0",
  "LowLand1",
  "LowLand2",
  "LowLand3",
  "MidLand0",
  "MidLand1",
  "MidLand2",
  "MidLand3",
  "Mount0",
  "Mount1",
  "Mount2",
  "Mount3",
  "Country0",
  "Country1",
  "Country2",
  "Country3",
  "City0",
  "City1",
  "City2",
  "City3",
  "AirField0",
  "AirField1",
  "AirField2",
  "AirField3",
  "Wood0",
  "Wood1",
  "Wood2",
  "Wood3",
  "Water0",
  "Water1",
  "Water2",
  "Water3",
};


} // namespace


namespace il2ge
{


const char* getMapResourceName(MapResourceID id)
{
  switch (id)
  {
    case MapResourceID::FIELD_TEXTURE:
      return "field_texture";
    case MapResourceID::FIELD_NORMAL_MAP:
      return "field_normal_map";
    case MapResourceID::FOAM_TEXTURE:
      return "foam";
    case MapResourceID::WATER_ANIMATION_HEIGHT_MAP:
      return "water_animation_height_map";
    case MapResourceID::WATER_ANIMATION_NORMAL_MAP:
      return "water_animation_normal_map";
    case MapResourceID::WATER_TEXTURE:
      return "water_texture";
    case MapResourceID::WATER_NOISE_TEXTURE:
      return "water_noise_texture";
    case MapResourceID::HEIGHT_MAP:
      return "height_map";
    case MapResourceID::TYPE_MAP:
      return "type_map";
    case MapResourceID::LAND_MAP:
      return "land_map";
    case MapResourceID::LAND_MAP_TABLE:
      return "land_map_table";
    case MapResourceID::HIGH_CLOUDS:
      return "high_clouds";
    case MapResourceID::NOISE_TEXTURE:
      return "noise_texture";
    case MapResourceID::FAR_NOISE_TEXTURE:
      return "far_noise_texture";
    case MapResourceID::FOREST_LAYER:
      return "forest_layer";
  }

  abort();
}


std::string getDumpName(il2ge::MapResourceID id, std::optional<unsigned> index = {})
{
  switch (id)
  {
    case MapResourceID::FIELD_TEXTURE:
    case MapResourceID::FIELD_NORMAL_MAP:
      return getMapResourceName(id) + std::string("_") + getMapFieldName(index.value());
    default:
    {
      if (index)
        return getMapResourceName(id) + std::string("_") + std::to_string(index.value());
      else
        return getMapResourceName(id);
    }
  }
}


const char* getMapFieldName(unsigned index)
{
  return g_field_names.at(index);
}


const std::string& getMapPropertyName(MapPropertyID id)
{
  return g_map_property_names.at(id);
}


} // namespace il2ge
