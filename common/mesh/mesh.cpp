/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "mesh.h"
#include "binary_reader.h"
#include <il2ge/material.h>
#include <util.h>
#include <render_util/geometry.h>
#include <render_util/gl_binding/gl_functions.h>


using namespace il2ge::mesh;
using namespace render_util::gl_binding;


namespace
{


using Triangle = std::array<Index, 3>;

// struct NormalsCreator
// {
//   const MeshData &mesh;
// 
//   NormalsCreator(const MeshData &mesh) : mesh(mesh) {}
// 
//   glm::vec3 calcTriangleNormal(const Triangle &triangle)
//   {
//     glm::vec3 vertices[3];
//     for (unsigned int i = 0; i < 3; i++)
//     {
//       auto index = triangle[i];
//       auto vertex = mesh.Verts.at(
//       
// //       vertices[i] = glm::make_vec3(mesh.vertices.at(triangle[i]).data());
//     }
// 
//     return render_util::calcNormal(vertices);
//   }
// 
//   std::vector<glm::vec3> createNormals()
//   {
//     std::vector<glm::vec3> normals(mesh.vertices.size());
// 
//     for (int i = 0; i < mesh.triangles.size(); i++)
//     {
// //       cout<<"i: "<<i<<endl;
//       auto &triangle = mesh.triangles[i];
//       auto normal = calcTriangleNormal(triangle);
// 
//       for (unsigned int i_vertex = 0; i_vertex < 3; i_vertex++)
//       {
//         auto vertex = triangle[i_vertex];
// //         cout
//         normals.at(vertex) = glm::normalize(normals.at(vertex) + normal);
// //         normals.at(vertex) = glm::vec3(0);
//       }
//     }
// 
//     return normals;
//   }
// };


// Calculates Normal For A Quad Using 3 Points
Vector3 calcNormal(int v0, int v1, int v2, const MeshData &data)
{
  glm::vec3 vertices[3];

  vertices[0] = data.Verts.at(v0).Position;
  vertices[1] = data.Verts.at(v1).Position;
  vertices[2] = data.Verts.at(v2).Position;

  return render_util::calcNormal(vertices);

//     Vector3 sv1 = Vector3.Transform(Verts[v2].Position, iw) - Vector3.Transform(Verts[v1].Position, iw);
//     Vector3 sv2 = Vector3.Transform(Verts[v3].Position, iw) - Vector3.Transform(Verts[v1].Position, iw);
//     Vector3 cv3 = Vector3.Cross(sv1, sv2);

//     return cv3;
}


void regenerateNormals(MeshData &data)
{
    auto length = data.Verts.size();
    float counts[length];
    Vector3 normals[length];

    for (int i = 0; i < length; i++)
    {
        counts[i] = 0;
        normals[i] = Vector3(0);
    }

    for (auto &f : data.FaceGroups)
    {
        auto tri = f.StartFace * 3;

        for (int j = 0; j < f.FaceCount; j++)
        {
            auto v1 = data.indices[tri++] + f.StartVertex;
            auto v2 = data.indices[tri++] + f.StartVertex;
            auto v3 = data.indices[tri++] + f.StartVertex;

            Vector3 norm = calcNormal(v1, v2, v3, data);

            normals[v1] += norm;
            normals[v2] += norm;
            normals[v3] += norm;

            counts[v1]++;
            counts[v2]++;
            counts[v3]++;
        }
    }

    for (int i = 0; i < length; i++)
    {
        normals[i] /= counts[i];
        normals[i] = glm::normalize(normals[i]);
        data.Verts[i].Normal = normals[i];
    }
}


}

namespace il2ge::mesh
{

        Lod::Lod(std::unique_ptr<MeshData> data) : m_data(std::move(data))
        {
        }


        Lod::~Lod() {}


        Mesh::Mesh() : MeshBase("") {}


        Mesh::Mesh(String filename) : MeshBase(filename)
        {
            auto dir = util::getDirFromPath(filename);
            mesh_name = util::basename(filename, true);

            std::ifstream in;

            try
            {
              in.exceptions(std::ifstream::failbit | std::ifstream::badbit | std::ifstream::eofbit);
              in.open(filename, std::ios_base::binary);

              #if IL2GE_MESH_ALLOW_UNEXPECTED_EOF
                in.exceptions(std::ifstream::goodbit);
              #endif
            }
            catch (...)
            {
              LOG_ERROR << "Error opening file: " << filename << std::endl;
              throw;
            }

            assert(in.good());

            read(in, dir);
        }


        Mesh::Mesh(String filename, const char *data, size_t data_size) : MeshBase(filename)
        {
            auto dir = util::getDirFromPath(filename);
            mesh_name = util::basename(filename, true);

            std::istringstream in(std::string(data, data_size));

            #if !IL2GE_MESH_ALLOW_UNEXPECTED_EOF
              in.exceptions(std::ifstream::failbit | std::ifstream::badbit | std::ifstream::eofbit);
            #endif

            try
            {
              read(in, dir);
            }
            catch (std::exception &e)
            {
              LOG_ERROR << e.what() << std::endl;
              auto path = "faulty_meshes/" + filename;
              auto dir = util::getDirFromPath(path);
              util::mkdir(dir, true);
              util::writeFile(path, data, data_size);
              std::ofstream errors("faulty_meshes/" + filename + ".errors");
              errors << e.what() << std::endl;

              throw;
            }
        }


        Mesh::~Mesh()
        {
        }


        void Mesh::createRendererData()
        {
            for (int lod = 0; lod < Lods.size(); lod++)
            {
                Lods.at(lod)->m_renderer_data =
                    std::make_unique<renderer::MeshRendererData>(*this, lod);
            }
        }


        void Mesh::read(std::istream &in, std::string dir)
        {
            Binary = false;

            {
                BinaryReader b(in);
                auto test = b.ReadInt16();
                if (test == 16897)
                    Binary = true;
                in.seekg(0);
            }

            if (Binary)
            {
                ReadBinary(in, dir);
            }
            else
            {
                ReadTextMode(in, dir);
            }

            if (LodDistances.empty())
            {
//                 LodDistances.push_back(12999999);
                LodDistances.push_back(100);
            }
            
            for (auto &lod : Lods)
            {
//                 regenerateNormals(*lod->m_data);
            }

            assert(!LodDistances.empty());
            while (LodDistances.size() < Lods.size())
                LodDistances.push_back(2 * LodDistances.back());
        }


        size_t Mesh::getLodCount() const
        {
            return Lods.size();
        }


        int Mesh::getLodNumAtDistance(float distance) const
        {
#if 1
          assert(!Lods.empty());

          if (m_enable_lod)
          {
            for (size_t i = 0; i < Lods.size(); i++)
            {
              if (distance < LodDistances[i])
                return i;
            }
            return Lods.size()-1;
          }
          else
#endif
          {
            return 0;
          }
        }


        const LodBase& Mesh::getLodAtDistance(float distance) const
        {
            assert(Lods.size() <= LodDistances.size());
            return getLod(getLodNumAtDistance(distance));
        }


        int Mesh::findHook(const std::string &name) const
        {
          int i = 0;
          for (auto &hook : Hooks)
          {
            if (util::makeLowercase(hook.getName()) == util::makeLowercase(name))
              return i;
            i++;
          }

          return -1;

//           abort();
//           auto it = m_hook_map.find(name);
//           if (it != m_hook_map.end())
//             return it->second;
//           else
//           {
//             return -1;
//           }
        }


        const std::string &Mesh::getHookName(int index) const
        {
          assert(index >= 0);
          assert(index < Hooks.size());
          return Hooks.at(index).getName();
        }


        const Mat4 &Mesh::getHookMatrix(int index, int frame) const
        {
          assert(index >= 0);
          assert(index < Hooks.size());
          return Hooks.at(index).getLoc(frame);
        }


        size_t Mesh::getMaterialsCount() const
        {
            return m_material_names.size();
        }


        // MaterialList& Mesh::getMaterials()
        // {
        //     assert(m_materials);
        //     return *m_materials;
        // }
        //
        //
        // void Mesh::setMaterials(std::unique_ptr<MaterialList>&& materials)
        // {
        //     m_materials = std::move(materials);
        // }
        //
        //
        // const std::shared_ptr<const Material>& Mesh::getMaterial(int index) const
        // {
        //     assert(m_materials);
        //     return m_materials->at(index);
        // }


        // const bool Mesh::hasMaterial(int index) const
        // {
        //     return m_materials && m_materials->has(index);
        // }

//         #endregion
#if 0

        #region Search functions
        public void FindHook(String name, ref List<Vector3> hooks, Matrix pos)
        {
            string test = name.ToLower();
            foreach (Hook h in Hooks)
            {
                string test2 = h.Name.ToLower();

                if (test2.Contains(test))
                {
                    Matrix mt = h.matrix * pos;
                    Vector3 newpos = Vector3.Transform(Vector3.Zero, mt);
                    hooks.Add(newpos);
                }
            }
        }
        public void FindHook(String name, ref List<Vector3> hooks, ref List<Vector3> directions, Matrix pos)
        {
            string test = name.ToLower();
            foreach (Hook h in Hooks)
            {
                string test2 = h.Name.ToLower();

                if (test2.Contains(test))
                {
                    Matrix mt = h.matrix * pos;
                    Vector3 newpos = Vector3.Transform(Vector3.Zero, mt);
                    hooks.Add(newpos);
                    directions.Add(-mt.Left);//Vector3.Transform(Vector3.UnitY, mt));

                }
            }
        }
        public void FindHook(String name, String ignore, ref List<Vector3> hooks, Matrix pos)
        {
            foreach (Hook h in Hooks)
            {
                if ((h.Name.Contains(name)) && (!h.Name.Contains(ignore)))
                {
                    Matrix mt = h.matrix * pos;
                    Vector3 newpos = Vector3.Transform(Vector3.Zero, mt);
                    hooks.Add(newpos);
                }
            }
        }
        public Hook FindHook(String name)
        {
            string test = name.ToLower();
            test = test.Replace(' ', '@');
            foreach (Hook h in Hooks)
            {
                string test2 = h.Name.ToLower();
                test2 = test2.Replace(' ', '@');

                if (test.Equals(test2))
                    return h;
            }
            return null;
        }

        #endregion

        #region Draw methods
        public void Draw(BasicEffect be, float distance, Matrix world)
        {
            int i = 0;
            if (distance < LodDistances[0])
            {
                foreach (FaceGroup f in FaceGroups)
                {
                    be.World = world;
                    Materials[i++].Apply(be);
                    foreach (EffectPass pass in be.CurrentTechnique.Passes)
                    {
                        pass.Apply();
                        be.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(
                            PrimitiveType.TriangleList,
                            Verts,
                            f.StartVertex,                  // vertex buffer offset to add to each element of the index buffer
                            f.VertexCount,      // number of vertices to draw
                            indices,
                            f.StartFace * 3,      // first index element to read
                            f.FaceCount         // number of primitives to draw
                            );
                    }
                }
            }

        }
        Matrix lastWorld;

        public void Draw(Effect be, float distance, Matrix world, bool sort)
        {
            lastWorld = world;
            bool isDamaged = false;
            if (!mesh_name.EndsWith("D0"))
            {
                isDamaged = false;
            }
            int i = 0;
            if (distance <= LodDistances[0])
            {
                #region animated mesh code
                if (Animated)
                {
                    AnimationTime -= 0.02f;
                    if (AnimationTime < 0)
                    {
                        AnimationTime += 1;

                        AnimationFrame++;
                        if (AnimationFrame == FrameCount)
                            AnimationFrame = 0;
                    }
                    Verts = animation_frames[AnimationFrame].ToArray();
                }
                #endregion

                {
                    foreach (FaceGroup f in FaceGroups)
                    {
                        if (
                             ((ObjectViewer.BumpMapping) && (!Materials[i].BumpMapped))
                            || (!ObjectViewer.BumpMapping))
                        {
                            Materials[i].Apply(be, isDamaged);
                            if (SelectedFaceGroup == f)
                            {
                                be.Parameters["DiffuseColor"].SetValue(new Vector4(1, 0, 0, 0.5f));
                            }
                            be.Parameters["World"].SetValue(world);
                            be.Parameters["WorldInverseTranspose"].SetValue(Matrix.Invert(Matrix.Transpose(world)));

                            if (Materials[i].Sort == sort)
                            {
                                if (f.FaceCount > 0)
                                {
                                    foreach (EffectPass pass in be.CurrentTechnique.Passes)
                                    {
                                        pass.Apply();
                                        be.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(
                                            PrimitiveType.TriangleList,
                                            Verts,
                                            f.StartVertex,                  // vertex buffer offset to add to each element of the index buffer
                                            f.VertexCount,      // number of vertices to draw
                                            indices,
                                            f.StartFace * 3,      // first index element to read
                                            f.FaceCount         // number of primitives to draw
                                            );
                                    }
                                }
                            }
                        }
                        i++;
                    }
                }
            }
            else
            {
                int lod = 0;
                while ((lod < LodDistances.Count) && (distance > LodDistances[lod]))
                {
                    lod++;
                }
                if (lod == LodDistances.Count)
                {
                    return;
                }
                Lods[lod - 1].Draw(be, world, sort);
            }

        }

        public void DrawBumped(Effect be, float distance, Matrix world, bool sort)
        {
            int i = 0;
            if (distance <= LodDistances[0])
            {
                foreach (FaceGroup f in FaceGroups)
                {
                    if (Materials[i].Sort == sort)
                    {
                        if (Materials[i].BumpMapped)
                        {
                            Matrix wiv = Matrix.Invert(world);
                            wiv = Matrix.Transpose(world);
                            be.Parameters["WorldInverseTranspose"].SetValue(wiv);
                            be.Parameters["World"].SetValue(world);

                            Materials[i].ApplyBumped(be);
                            if (f.FaceCount > 0)
                            {
                                foreach (EffectPass pass in be.CurrentTechnique.Passes)
                                {
                                    pass.Apply();
                                    be.GraphicsDevice.DrawUserIndexedPrimitives<BumpVertex>(
                                        PrimitiveType.TriangleList,
                                        BumpVerts,
                                        f.StartVertex,                  // vertex buffer offset to add to each element of the index buffer
                                        f.VertexCount,      // number of vertices to draw
                                        indices,
                                        f.StartFace * 3,      // first index element to read
                                        f.FaceCount         // number of primitives to draw
                                        );
                                }
                            }
                        }
                    }
                    i++;
                }
            }


        }

        public void DrawShadow(Effect be, float distance, Matrix world)
        {
            if (ShadowVerts.Count == 0)
                return;

            if (distance <= LodDistances[0])
            {
                be.Parameters["world"].SetValue(world);
                foreach (EffectPass pass in be.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    be.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(
                        PrimitiveType.TriangleList,
                        ShadowVertsArray,
                        0,                      // vertex buffer offset to add to each element of the index buffer
                        ShadowVerts.Count,      // number of vertices to draw
                        ShadowIndicesArray,
                        0,                      // first index element to read
                        ShadowIndices.Count / 3   // number of primitives to draw
                        );
                }

            }
            else
            {
                int lod = 0;
                while ((lod < LodDistances.Count) && (distance > LodDistances[lod]))
                {
                    lod++;
                }
                if (lod == LodDistances.Count)
                {
                    return;
                }
                Lods[lod - 1].DrawShadow(be, distance, world);
            }
        }

        public void DrawGlass(Effect be, float distance, Matrix world, Matrix vp)
        {
            int i = 0;
            if (distance < LodDistances[0])
            {
                foreach (FaceGroup f in FaceGroups)
                {
                    be.Parameters["wvp"].SetValue(world * vp);
                    be.Parameters["world"].SetValue(world);
                    be.Parameters["WorldInverseTranspose"].SetValue(Matrix.Transpose(Matrix.Invert(world)));
                    if (Materials[i++].Glass)
                    {
                        if (f.FaceCount > 0)
                        {
                            foreach (EffectPass pass in be.CurrentTechnique.Passes)
                            {
                                pass.Apply();
                                be.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionNormalTexture>(
                                    PrimitiveType.TriangleList,
                                    Verts,
                                    f.StartVertex,                  // vertex buffer offset to add to each element of the index buffer
                                    f.VertexCount,      // number of vertices to draw
                                    indices,
                                    f.StartFace * 3,      // first index element to read
                                    f.FaceCount         // number of primitives to draw
                                    );
                            }
                        }
                    }
                }
            }

        }

        public void DrawCollisionMesh(BasicEffect be, Matrix world)
        {
            be.World = world;
            for (int j = 0; j < colmesh.NBlocks; j++)
            {
                foreach (CollisionMeshPart part in colmesh.Blocks[j].Parts)
                {
                    if (part.indices == null)
                    {
                        part.indices = part.Faces.ToArray();
                        part.verts = new VertexPositionColor[part.Verts.Count];
                        for (int i = 0; i < part.Verts.Count; i++)
                        {
                            part.verts[i].Color = Microsoft.Xna.Framework.Color.Red;
                            part.verts[i].Position = part.Verts[i];
                        }
                    }
                    if (part.Faces.Count > 0)
                    {
                        foreach (EffectPass pass in be.CurrentTechnique.Passes)
                        {
                            pass.Apply();
                            be.GraphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(
                                PrimitiveType.TriangleList,
                                part.verts,
                                0,                  // vertex buffer offset to add to each element of the index buffer
                                part.Verts.Count,   // number of vertices to draw
                                part.indices,
                                0,                  // first index element to read
                                part.Faces.Count / 3  // number of primitives to draw
                                );
                        }
                    }
                }
            }
        }

        private int trim(float x, float size)
        {
            float dx = x - (float)((int)x);
            dx *= size;
            return (int)dx;
        }

        public void DrawSkin(Graphics g, Pen p, Brush b, String texture, float size)
        {
            int i = 0;
            System.Drawing.Point[] points = new System.Drawing.Point[3];
            foreach (FaceGroup f in FaceGroups)
            {
                if (Form1.Manager.Names[Materials[i].TextureID].Equals(texture, StringComparison.OrdinalIgnoreCase))
                {
                    int tri = f.StartFace * 3;
                    for (int j = 0; j < f.FaceCount; j++)
                    {
                        int v1 = indices[tri++] + f.StartVertex;
                        int v2 = indices[tri++] + f.StartVertex;
                        int v3 = indices[tri++] + f.StartVertex;

                        points[0].x = trim(Verts[v1].TextureCoordinate.X, size);
                        points[0].y = trim(Verts[v1].TextureCoordinate.Y, size);
                        points[1].x = trim(Verts[v2].TextureCoordinate.X, size);
                        points[1].y = trim(Verts[v2].TextureCoordinate.Y, size);
                        points[2].x = trim(Verts[v3].TextureCoordinate.X, size);
                        points[2].y = trim(Verts[v3].TextureCoordinate.Y, size);
                        g.FillPolygon(b, points);
                        g.DrawPolygon(p, points);
                    }
                }
                i++;
            }

        }

        public void DrawSkinPart(Graphics g, Pen p, Brush b, String texture, float size)
        {
            System.Drawing.Point[] points = new System.Drawing.Point[3];
            FaceGroup f = selected_facegroup;

            int tri = f.StartFace * 3;
            for (int j = 0; j < f.FaceCount; j++)
            {
                int v1 = indices[tri++] + f.StartVertex;
                int v2 = indices[tri++] + f.StartVertex;
                int v3 = indices[tri++] + f.StartVertex;

                points[0].x = trim(Verts[v1].TextureCoordinate.X, size);
                points[0].y = trim(Verts[v1].TextureCoordinate.Y, size);
                points[1].x = trim(Verts[v2].TextureCoordinate.X, size);
                points[1].y = trim(Verts[v2].TextureCoordinate.Y, size);
                points[2].x = trim(Verts[v3].TextureCoordinate.X, size);
                points[2].y = trim(Verts[v3].TextureCoordinate.Y, size);
                g.FillPolygon(b, points);
                g.DrawPolygon(p, points);
            }
        }

        public void DrawAo(BasicEffect effect, int size, ref List<float> ao_values)
        {
            List<VertexPositionColor> verts = new List<VertexPositionColor>();

            foreach (FaceGroup f in FaceGroups)
            {
                if (Form1.Manager.Names[Materials[f.Material].TextureID].Equals("skin1o.tga", StringComparison.OrdinalIgnoreCase))
                {
                    int tri = f.StartFace * 3;
                    for (int j = 0; j < f.FaceCount; j++)
                    {
                        int v1 = indices[tri++] + f.StartVertex;
                        int v2 = indices[tri++] + f.StartVertex;
                        int v3 = indices[tri++] + f.StartVertex;

                        VertexPositionColor pc = new VertexPositionColor();
                        pc.Position.x = trim(Verts[v1].TextureCoordinate.X, size);
                        pc.Position.y = trim(Verts[v1].TextureCoordinate.Y, size);
                        pc.Position.z = -0.5f;
                        float ac = ao_values[0];
                        ao_values.RemoveAt(0);
                        Microsoft.Xna.Framework.Color rc = Microsoft.Xna.Framework.Color.FromNonPremultiplied(new Vector4(ac, ac, ac, 1));
                        pc.Color = rc;
                        verts.Add(pc);

                        pc = new VertexPositionColor();
                        pc.Position.x = trim(Verts[v2].TextureCoordinate.X, size);
                        pc.Position.y = trim(Verts[v2].TextureCoordinate.Y, size);
                        pc.Position.z = -0.5f;
                        ac = ao_values[0];
                        ao_values.RemoveAt(0);
                        rc = Microsoft.Xna.Framework.Color.FromNonPremultiplied(new Vector4(ac, ac, ac, 1));
                        pc.Color = rc;
                        verts.Add(pc);

                        pc = new VertexPositionColor();
                        pc.Position.x = trim(Verts[v3].TextureCoordinate.X, size);
                        pc.Position.y = trim(Verts[v3].TextureCoordinate.Y, size);
                        pc.Position.z = -0.5f;
                        ac = ao_values[0];
                        ao_values.RemoveAt(0);
                        rc = Microsoft.Xna.Framework.Color.FromNonPremultiplied(new Vector4(ac, ac, ac, 1));
                        pc.Color = rc;
                        verts.Add(pc);

                    }
                }
            }
            if (verts.Count > 0)
            {
                VertexPositionColor[] vs = verts.ToArray();

                VertexBuffer vertexBuffer = new VertexBuffer(Form1.graphics, typeof(VertexPositionColor), verts.Count, BufferUsage.WriteOnly);
                vertexBuffer.SetData<VertexPositionColor>(vs);
                Form1.graphics.SetVertexBuffer(vertexBuffer);

                foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    effect.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, verts.Count / 3);
                }

                vertexBuffer.Dispose();

            }
        }

        public void DrawAo(BasicEffect effect, int size, String texture)
        {
            List<VertexPositionColor> verts = new List<VertexPositionColor>();
            foreach (FaceGroup f in FaceGroups)
            {
                if (Form1.Manager.Names[Materials[f.Material].TextureID].Equals(texture, StringComparison.OrdinalIgnoreCase))
                {
                    int tri = f.StartFace * 3;
                    for (int j = 0; j < f.FaceCount; j++)
                    {
                        int v1 = indices[tri++] + f.StartVertex;
                        int v2 = indices[tri++] + f.StartVertex;
                        int v3 = indices[tri++] + f.StartVertex;

                        VertexPositionColor pc = new VertexPositionColor();
                        pc.Position.x = trim(Verts[v1].TextureCoordinate.X, size);
                        pc.Position.y = trim(Verts[v1].TextureCoordinate.Y, size);
                        pc.Position.z = -0.5f;

                        int ic = AOLookUp[v1];
                        float ac = 1.0f - AOValues[ic];
                        Microsoft.Xna.Framework.Color rc = Microsoft.Xna.Framework.Color.FromNonPremultiplied(new Vector4(ac, ac, ac, 1));
                        pc.Color = rc;
                        verts.Add(pc);

                        pc = new VertexPositionColor();
                        pc.Position.x = trim(Verts[v2].TextureCoordinate.X, size);
                        pc.Position.y = trim(Verts[v2].TextureCoordinate.Y, size);
                        pc.Position.z = -0.5f;

                        ic = AOLookUp[v2];
                        ac = 1.0f - AOValues[ic];
                        rc = Microsoft.Xna.Framework.Color.FromNonPremultiplied(new Vector4(ac, ac, ac, 1));
                        pc.Color = rc;
                        verts.Add(pc);

                        pc = new VertexPositionColor();
                        pc.Position.x = trim(Verts[v3].TextureCoordinate.X, size);
                        pc.Position.y = trim(Verts[v3].TextureCoordinate.Y, size);
                        pc.Position.z = -0.5f;

                        ic = AOLookUp[v3];
                        ac = 1.0f - AOValues[ic];
                        rc = Microsoft.Xna.Framework.Color.FromNonPremultiplied(new Vector4(ac, ac, ac, 1));
                        pc.Color = rc;
                        verts.Add(pc);

                    }
                }
            }
            if (verts.Count > 0)
            {
                VertexPositionColor[] vs = verts.ToArray();

                VertexBuffer vertexBuffer = new VertexBuffer(Form1.graphics, typeof(VertexPositionColor), verts.Count, BufferUsage.WriteOnly);
                vertexBuffer.SetData<VertexPositionColor>(vs);
                Form1.graphics.SetVertexBuffer(vertexBuffer);

                foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    effect.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, verts.Count / 3);
                }

                vertexBuffer.Dispose();

            }
        }

        public void DrawNormals(BasicEffect effect)
        {
            List<VertexPositionColor> verts = new List<VertexPositionColor>();

            foreach (VertexPositionNormalTexture f in Verts)
            {
                VertexPositionColor vpc = new VertexPositionColor();
                vpc.Position = f.Position;
                vpc.Color = Microsoft.Xna.Framework.Color.DeepPink;
                verts.Add(vpc);
                vpc = new VertexPositionColor();
                vpc.Position = f.Position - (f.Normal * 0.25f);
                vpc.Color = Microsoft.Xna.Framework.Color.DeepPink;
                verts.Add(vpc);
            }
            VertexPositionColor[] v = verts.ToArray();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                Form1.graphics.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineList, v, 0, verts.Count / 2);
            }


        }

        #endregion
#endif


#if 0
        #endregion

        #region Serializers
        public void SaveShadows(TextWriter tw)
        {
            if (ShadowVerts.Count > 0)
            {
                tw.WriteLine("[ShVertices_Frame0]");
                foreach (VertexPositionColor vp in ShadowVerts)
                {
                    tw.WriteLine(String.Format("{0} {1} {2}", vp.Position.x, vp.Position.y, vp.Position.z));
                }
                tw.WriteLine("");
                tw.WriteLine("[ShFaces]");
                for (int i = 0; i < ShadowIndices.Count; i += 3)
                {
                    tw.WriteLine(String.Format("{0} {1} {2}", ShadowIndices[i], ShadowIndices[i + 1], ShadowIndices[i + 2]));
                }
                tw.WriteLine("");
            }
            if (Animated)
            {
                for (int i = 0; i < FrameCount; i++)
                {
                    tw.WriteLine(String.Format("[ShVertices_Frame{0}]", i));
                    foreach (VertexPositionColor v in animated_shadow_frames[i])
                    {
                        tw.WriteLine(String.Format("{0} {1} {2}", v.Position.x, v.Position.y, v.Position.z));
                    }
                }
                tw.WriteLine("");
                tw.WriteLine("[ShFaces]");
                for (int i = 0; i < ShadowIndices.Count; i += 3)
                {
                    tw.WriteLine(String.Format("{0} {1} {2}", ShadowIndices[i], ShadowIndices[i + 1], ShadowIndices[i + 2]));
                }
                tw.WriteLine("");
            }
        }

        public void SaveLodTable(TextWriter tw)
        {
            tw.WriteLine("[LOD]");
            foreach (int i in LodDistances)
            {
                tw.WriteLine(String.Format("{0}", i));
            }
            tw.WriteLine("");
        }

        public void SaveMaterials(TextWriter tw)
        {
            tw.WriteLine("[Materials]");
            foreach (Material m in Materials)
            {
                String f = m.Name.Replace('"', '_');
                tw.WriteLine(f);
            }
            tw.WriteLine("");
        }

        public void SaveHeader(TextWriter tw)
        {
            tw.WriteLine("//Generated by Stainless");
            tw.WriteLine("");
            tw.WriteLine("[Common]");
            tw.WriteLine("NumBones 0");
            if (Animated)
            {
                tw.WriteLine(" FramesType Animation");
                tw.WriteLine(String.Format(" NumFrames {0}", FrameCount));
            }
            else
            {
                tw.WriteLine(" FramesType Single");
                tw.WriteLine(" NumFrames 1");
            }
            tw.WriteLine("");

        }

        public void SaveHooks(TextWriter tw)
        {
            if (Hooks.Count > 0)
            {
                tw.WriteLine("[Hooks]");
                if (Animated)
                {
                    tw.WriteLine(Hooks[0].Name.Replace((char)7, ' '));
                }
                else
                {
                    foreach (Hook h in Hooks)
                    {
                        tw.WriteLine(h.Name.Replace((char)7, ' '));
                    }
                }
                tw.WriteLine("");
                tw.WriteLine("[HookLoc]");
                foreach (Hook h in Hooks)
                {
                    tw.Write(String.Format("{0} ", h.matrix.M11));
                    tw.Write(String.Format("{0} ", h.matrix.M12));
                    tw.Write(String.Format("{0} ", h.matrix.M13));
                    tw.Write(String.Format("{0} ", h.matrix.M21));
                    tw.Write(String.Format("{0} ", h.matrix.M22));
                    tw.Write(String.Format("{0} ", h.matrix.M23));
                    tw.Write(String.Format("{0} ", h.matrix.M31));
                    tw.Write(String.Format("{0} ", h.matrix.M32));
                    tw.Write(String.Format("{0} ", h.matrix.M33));
                    tw.Write(String.Format("{0} ", h.matrix.M41));
                    tw.Write(String.Format("{0} ", h.matrix.M42));
                    tw.WriteLine(String.Format("{0}", h.matrix.M43));
                }
                tw.WriteLine("");
            }
        }

        public void SaveFaceGroups(TextWriter tw)
        {
            tw.WriteLine("[FaceGroups]");
            tw.WriteLine(String.Format("{0} {1}", Verts.Length, FaceCount));
            foreach (FaceGroup f in FaceGroups)
            {
                tw.WriteLine(String.Format("{0} {1} {2} {3} {4} 0", f.Material, f.StartVertex, f.VertexCount, f.StartFace, f.FaceCount));
            }
            tw.WriteLine("");
        }

        public void SaveFrame0(TextWriter tw)
        {
            if (Animated)
            {
                for (int i = 0; i < FrameCount; i++)
                {
                    tw.WriteLine(String.Format("[Vertices_Frame{0}]", i));
                    foreach (VertexPositionNormalTexture v in animation_frames[i])
                    {
                        tw.WriteLine(String.Format("{0} {1} {2} {3} {4} {5}", v.Position.x, v.Position.y, v.Position.z, v.Normal.x, v.Normal.y, v.Normal.z));
                    }
                    tw.WriteLine("");
                }
            }
            else
            {
                tw.WriteLine("[Vertices_Frame0]");
                foreach (VertexPositionNormalTexture v in Verts)
                {
                    tw.WriteLine(String.Format("{0} {1} {2} {3} {4} {5}", v.Position.x, v.Position.y, v.Position.z, v.Normal.x, v.Normal.y, v.Normal.z));
                }
                tw.WriteLine("");
            }
        }

        public void SaveUVs(TextWriter tw)
        {
            tw.WriteLine("[MaterialMapping]");
            foreach (VertexPositionNormalTexture v in Verts)
            {
                tw.WriteLine(String.Format("{0} {1}", v.TextureCoordinate.X, v.TextureCoordinate.Y));
            }
            tw.WriteLine("");
        }

        public void SaveFaces(TextWriter tw)
        {
            tw.WriteLine("[Faces]");
            for (int i = 0; i < indices.GetLength(0); i += 3)
            {
                tw.WriteLine("{0} {1} {2}", indices[i], indices[i + 1], indices[i + 2]);
            }
            tw.WriteLine("");
        }

        public void SaveLods(TextWriter tw)
        {

            for (int i = 0; i < Lods.Count; i++)
            {
                tw.WriteLine(String.Format("[LOD{0}_Materials]", i + 1));
                foreach (Material m in Lods[i].Materials)
                {
                    tw.WriteLine(m.Name);
                }
                tw.WriteLine("");

                tw.WriteLine(String.Format("[LOD{0}_FaceGroups]", i + 1));
                tw.WriteLine(String.Format("{0} {1}", Lods[i].VertexCount, Lods[i].FaceCount));
                foreach (FaceGroup f in Lods[i].FaceGroups)
                {
                    tw.WriteLine(String.Format("{0} {1} {2} {3} {4} 0", f.Material, f.StartVertex, f.VertexCount, f.StartFace, f.FaceCount));
                }
                tw.WriteLine("");

                if (Animated)
                {
                    for (int j = 0; j < FrameCount; j++)
                    {
                        tw.WriteLine(String.Format("[LOD{0}_Vertices_Frame{1}]", i + 1, j));
                        foreach (VertexPositionNormalTexture v in Lods[i].animation_frames[j])
                        {
                            tw.WriteLine(String.Format("{0} {1} {2} {3} {4} {5}", v.Position.x, v.Position.y, v.Position.z, v.Normal.x, v.Normal.y, v.Normal.z));
                        }
                        tw.WriteLine("");
                    }
                }
                else
                {
                    tw.WriteLine(String.Format("[LOD{0}_Vertices_Frame0]", i + 1));
                    foreach (VertexPositionNormalTexture v in Lods[i].Verts)
                    {
                        tw.WriteLine(String.Format("{0} {1} {2} {3} {4} {5}", v.Position.x, v.Position.y, v.Position.z, v.Normal.x, v.Normal.y, v.Normal.z));
                    }
                    tw.WriteLine("");
                }

                tw.WriteLine(String.Format("[LOD{0}_MaterialMapping]", i + 1));
                foreach (VertexPositionNormalTexture v in Lods[i].Verts)
                {
                    tw.WriteLine(String.Format("{0} {1}", v.TextureCoordinate.X, v.TextureCoordinate.Y));
                }
                tw.WriteLine("");

                tw.WriteLine(String.Format("[LOD{0}_Faces]", i + 1));
                for (int j = 0; j < Lods[i].Indices.GetLength(0); j += 3)
                {
                    tw.WriteLine(String.Format("{0} {1} {2}", Lods[i].Indices[j], Lods[i].Indices[j + 1], Lods[i].Indices[j + 2]));
                }
                tw.WriteLine("");

                if (Lods[i].ShadowIndicesArray != null)
                {
                    if (Animated)
                    {
                        for (int j = 0; j < FrameCount; j++)
                        {
                            tw.WriteLine(String.Format("[LOD{0}_ShVertices_Frame{1}]", i + 1, j));
                            foreach (VertexPositionColor v in Lods[i].animation_shadow_frames[j])
                            {
                                tw.WriteLine(String.Format("{0} {1} {2}", v.Position.x, v.Position.y, v.Position.z));
                            }
                            tw.WriteLine("");
                        }
                    }
                    else
                    {
                        tw.WriteLine(String.Format("[LOD{0}_ShVertices_Frame0]", i + 1));
                        foreach (VertexPositionColor v in Lods[i].ShadowVerts)
                        {
                            tw.WriteLine(String.Format("{0} {1} {2}", v.Position.x, v.Position.y, v.Position.z));
                        }
                        tw.WriteLine("");
                    }


                    tw.WriteLine(String.Format("[LOD{0}_ShFaces]", i + 1));
                    for (int j = 0; j < Lods[i].ShadowIndicesArray.GetLength(0); j += 3)
                    {
                        tw.WriteLine(String.Format("{0} {1} {2}", Lods[i].ShadowIndicesArray[j], Lods[i].ShadowIndicesArray[j + 1], Lods[i].ShadowIndicesArray[j + 2]));
                    }
                }

                tw.WriteLine("");
            }
        }

        public void SaveMaterials(String dir)
        {
            foreach (Material m in Materials)
            {
                m.Serialise(dir);
            }
            foreach (Lod l in Lods)
            {
                l.SaveMaterials(dir);
            }
        }

        public void SaveCollisionMesh(TextWriter tw)
        {
            if (colmesh.NBlocks > 0)
            {
                tw.WriteLine("[CoCommon]");
                tw.WriteLine(String.Format("NBlocks {0}", colmesh.NBlocks));
                tw.WriteLine("");
                for (int i = 0; i < colmesh.NBlocks; i++)
                {
                    CollisionMeshBlock cmb = colmesh.Blocks[i];
                    tw.WriteLine(String.Format("[CoCommon_b{0}]", i));
                    tw.WriteLine(String.Format("NParts {0}", cmb.NParts));
                    tw.WriteLine("");
                    for (int j = 0; j < cmb.NParts; j++)
                    {
                        tw.WriteLine(String.Format("[CoCommon_b{0}p{1}]", i, j));

                        CollisionMeshPart cmp = cmb.Parts[j];
                        tw.WriteLine(String.Format("Type {0}", cmp.Type));
                        tw.WriteLine(String.Format("NFrames {0}", cmp.NFrames));
                        tw.WriteLine(String.Format("Name {0}", cmp.Name));
                        tw.WriteLine("");

                        int VertsPerFrame = cmp.Verts.Count / cmp.NFrames;
                        int Nv = 0;
                        for (int frame = 0; frame < cmp.NFrames; frame++)
                        {
                            tw.WriteLine(String.Format("[CoVer{0}_b{1}p{2}]", frame, i, j));
                            for (int tv = 0; tv < VertsPerFrame; tv++)
                            {
                                Vector3 v = cmp.Verts[tv + Nv];
                                tw.WriteLine(String.Format("{0} {1} {2}", v.X, v.Y, v.Z));
                            }
                            Nv += VertsPerFrame;
                        }
                        tw.WriteLine("");
                        tw.WriteLine(String.Format("[CoNeiCnt_b{0}p{1}]", i, j));
                        foreach (int s in cmp.NeiCount)
                        {
                            tw.WriteLine(String.Format("{0}", s));
                        }
                        tw.WriteLine("");
                        tw.WriteLine(String.Format("[CoNei_b{0}p{1}]", i, j));
                        foreach (int s in cmp.Neighbours)
                        {
                            tw.WriteLine(String.Format("{0}", s));
                        }
                        tw.WriteLine("");
                        tw.WriteLine(String.Format("[CoFac_b{0}p{1}]", i, j));
                        for (int s = 0; s < cmp.Faces.Count; s += 3)
                        {
                            tw.WriteLine(String.Format("{0} {1} {2}", cmp.Faces[s], cmp.Faces[s + 1], cmp.Faces[s + 2]));
                        }

                    }
                }
            }
        }

        public void SaveDAEEffects(TextWriter tw, bool effect)
        {
            foreach (Material m in Materials)
            {
                m.SaveDAEEffects(tw, effect);
            }
        }

        public void SaveDAEMesh(TextWriter tw)
        {
            tw.WriteLine(String.Format("\t\t<geometry id=\"{0}-mesh\">", mesh_name));
            tw.WriteLine("\t\t\t<mesh>");
            tw.WriteLine(String.Format("\t\t\t\t<source id=\"{0}-mesh-positions\">", mesh_name));
            tw.WriteLine(String.Format("\t\t\t\t\t<float_array id=\"{0}-mesh-positions-array\" count=\"{1}\">", mesh_name, Verts.GetLength(0) * 3));
            for (int i = 0; i < Verts.GetLength(0); i++)
            {
                tw.Write(String.Format("{0} {1} {2} ", Verts[i].Position.x, Verts[i].Position.y, Verts[i].Position.z));
            }
            tw.WriteLine("");
            tw.WriteLine("\t\t\t\t\t<technique_common>");
            tw.WriteLine(String.Format("\t\t\t\t\t\t<accessor source=\"#{0}-mesh-positions-array\" count=\"{1}\" stride=\"3\">", mesh_name, Verts.GetLength(0)));
            tw.WriteLine("\t\t\t\t\t\t\t<param name=\"X\" type=\"float\"/>");
            tw.WriteLine("\t\t\t\t\t\t\t<param name=\"Y\" type=\"float\"/>");
            tw.WriteLine("\t\t\t\t\t\t\t<param name=\"Z\" type=\"float\"/>");
            tw.WriteLine("\t\t\t\t\t\t</accessor>");
            tw.WriteLine("\t\t\t\t\t</technique_common>");
            tw.WriteLine("\t\t\t\t</source>");

            tw.WriteLine(String.Format("\t\t\t\t<source id=\"{0}-mesh-normals\">", mesh_name));
            tw.WriteLine(String.Format("\t\t\t\t\t<float_array id=\"{0}-mesh-normals-array\" count=\"{1}\">", mesh_name, Verts.GetLength(0) * 3));
            for (int i = 0; i < Verts.GetLength(0); i++)
            {
                tw.Write(String.Format("{0} {1} {2} ", Verts[i].Normal.x, Verts[i].Normal.y, Verts[i].Normal.z));
            }
            tw.WriteLine("");
            tw.WriteLine("\t\t\t\t\t<technique_common>");
            tw.WriteLine(String.Format("\t\t\t\t\t\t<accessor source=\"#{0}-mesh-normals-array\" count=\"{1}\" stride=\"3\">", mesh_name, Verts.GetLength(0)));
            tw.WriteLine("\t\t\t\t\t\t\t<param name=\"X\" type=\"float\"/>");
            tw.WriteLine("\t\t\t\t\t\t\t<param name=\"Y\" type=\"float\"/>");
            tw.WriteLine("\t\t\t\t\t\t\t<param name=\"Z\" type=\"float\"/>");
            tw.WriteLine("\t\t\t\t\t\t</accessor>");
            tw.WriteLine("\t\t\t\t\t</technique_common>");
            tw.WriteLine("\t\t\t\t</source>");

            tw.WriteLine(String.Format("\t\t\t\t<source id=\"{0}-mesh-map\">", mesh_name));
            tw.WriteLine(String.Format("\t\t\t\t\t<float_array id=\"{0}-mesh-map-array\" count=\"{1}\">", mesh_name, Verts.GetLength(0) * 3));
            for (int i = 0; i < Verts.GetLength(0); i++)
            {
                tw.Write(String.Format("{0} {1} ", Verts[i].TextureCoordinate.X, Verts[i].TextureCoordinate.Y));
            }
            tw.WriteLine("");
            tw.WriteLine("\t\t\t\t\t<technique_common>");
            tw.WriteLine(String.Format("\t\t\t\t\t\t<accessor source=\"#{0}-mesh-map-array\" count=\"{1}\" stride=\"2\">", mesh_name, Verts.GetLength(0)));
            tw.WriteLine("\t\t\t\t\t\t\t<param name=\"S\" type=\"float\"/>");
            tw.WriteLine("\t\t\t\t\t\t\t<param name=\"T\" type=\"float\"/>");
            tw.WriteLine("\t\t\t\t\t\t</accessor>");
            tw.WriteLine("\t\t\t\t\t</technique_common>");
            tw.WriteLine("\t\t\t\t</source>");

            tw.WriteLine(String.Format("\t\t\t\t<vertices id=\"{0}-mesh-vertices\">", mesh_name));
            tw.WriteLine(String.Format("\t\t\t\t\t<input semantic=\"POSITION\" source=\"#{0}-mesh-positions\"/>", mesh_name));
            tw.WriteLine("\t\t\t\t</vertices>");

            foreach (FaceGroup f in FaceGroups)
            {
                tw.WriteLine(String.Format("\t\t\t\t<polylist material=\"{0}\" count=\"{1}\">", Materials[f.Material].Name, f.FaceCount));
                tw.WriteLine(String.Format("\t\t\t\t<input semantic=\"VERTEX\" source=\"#{0}-mesh-vertices\" offset=\"0\"/>", mesh_name));
                tw.WriteLine(String.Format("\t\t\t\t<input semantic=\"NORMAL\" source=\"#{0}-mesh-normals\" offset=\"1\"/>", mesh_name));
                tw.WriteLine(String.Format("\t\t\t\t<input semantic=\"TEXCOORD\" source=\"#{0}-mesh-map-0\" offset=\"2\" set=\"0\"/>", mesh_name));
                tw.WriteLine("\t\t\t\t<vcount>");
                tw.Write("\t\t\t\t");
                for (int i = 0; i < f.FaceCount; i++)
                {
                    tw.Write("3 ");
                }
                tw.WriteLine("\t\t\t\t</vcount>");
                tw.WriteLine("\t\t\t\t<p>");
                tw.Write("\t\t\t\t\t");

                for (int i = 0; i < f.FaceCount; i++)
                {
                    int pos = (f.StartFace + i) * 3;
                    int vert = pos;
                    tw.Write(String.Format("{0} {1} {2} ", indices[vert] + f.StartVertex, indices[vert + 1] + f.StartVertex, indices[vert + 2] + f.StartVertex));

                }
                tw.WriteLine("\t\t\t\t\t</p>");
                tw.WriteLine("\t\t\t\t</polylist>");
            }
            tw.WriteLine("\t\t\t</mesh>");
            tw.WriteLine("\t\t</geometry>");
        }

        public void SaveOGRE(String dir)
        {
            String name = Path.Combine(dir, mesh_name);
            name += ".xml";
            using (TextWriter writer = File.CreateText(name))
            {
                writer.WriteLine("<mesh>");
                writer.WriteLine("<submeshes>");
                foreach (FaceGroup f in FaceGroups)
                {
                    String tname = Materials[f.Material].tname;
                    writer.WriteLine(String.Format("<submesh material=\"{0}\" usesharedvertices=\"false\" use32bitindexes=\"false\" operationtype=\"triangle_list\">", tname));
                    writer.WriteLine(String.Format("<faces count=\"{0}\">", f.FaceCount));
                    for (int j = 0; j < f.FaceCount; j++)
                    {
                        int pos = (f.StartFace + j) * 3;
                        writer.WriteLine(String.Format("<face v1=\"{0}\" v2=\"{1}\" v3=\"{2}\" />",
                            indices[pos],
                            indices[pos + 1],
                            indices[pos + 2]));
                    }
                    writer.WriteLine("</faces>");
                    writer.WriteLine(String.Format("<geometry vertexcount=\"{0}\">", f.VertexCount));
                    writer.WriteLine("<vertexbuffer positions=\"true\" normals=\"true\" texture_coord_dimensions_0=\"float2\" texture_coords=\"1\">");
                    for (int i = f.StartVertex; i < f.StartVertex + f.VertexCount; i++)
                    {
                        writer.WriteLine("<vertex>");
                        writer.WriteLine(String.Format("<position x=\"{0}\" y=\"{1}\" z=\"{2}\" />", Verts[i].Position.x, Verts[i].Position.y, Verts[i].Position.z));
                        writer.WriteLine(String.Format("<normal x=\"{0}\" y=\"{1}\" z=\"{2}\" />", Verts[i].Normal.x, Verts[i].Normal.y, Verts[i].Normal.z));
                        writer.WriteLine(String.Format("<texcoord u=\"{0}\" v=\"~{1}\" />", Verts[i].TextureCoordinate.X, Verts[i].TextureCoordinate.Y));
                        writer.WriteLine("</vertex>");

                    }
                    writer.WriteLine("</vertexbuffer>");
                    writer.WriteLine("</geometry>");
                    writer.WriteLine("</submesh>");

                }
                writer.WriteLine("</submeshes>");
                writer.WriteLine("</mesh>");
                writer.Close();
            }
        }

        public void SaveAsOBJ(String dir, String matname)
        {
            String name = Path.Combine(dir, mesh_name);
            name += ".obj";
            using (TextWriter writer = File.CreateText(name))
            {
                writer.WriteLine("mtllib " + matname);
                writer.WriteLine("");
                writer.WriteLine("");
                for (int i = 0; i < Verts.Length; i++)
                {
                    writer.WriteLine(String.Format("v {0} {1} {2}", Verts[i].Position.x, Verts[i].Position.y, Verts[i].Position.z));
                    writer.WriteLine(String.Format("vn {0} {1} {2}", Verts[i].Normal.x, Verts[i].Normal.y, Verts[i].Normal.z));
                    writer.WriteLine(String.Format("vt {0} {1}", Verts[i].TextureCoordinate.X, Verts[i].TextureCoordinate.Y));
                }
                foreach (FaceGroup f in FaceGroups)
                {
                    writer.WriteLine(String.Format("g group_{0}", Materials[f.Material].Name));
                    writer.WriteLine(String.Format("usemtl {0}", Materials[f.Material].Name));
                    writer.WriteLine("");
                    int si = f.StartFace * 3;
                    for (int i = 0; i < f.FaceCount; i++)
                    {
                        int v1, v2, v3;
                        v1 = 1 + f.StartVertex + indices[si++];
                        v2 = 1 + f.StartVertex + indices[si++];
                        v3 = 1 + f.StartVertex + indices[si++];

                        writer.WriteLine(String.Format("f {0}/{0}/{0} {1}/{1}/{1} {2}/{2}/{2}", v1, v2, v3));

                    }
                    writer.WriteLine("");

                    Material m = Materials[f.Material];
                    if (!ObjectViewer.saved_materials.Contains(m.Name))
                    {
                        ObjectViewer.saved_materials.Add(m.Name);
                        m.Saveobj(ObjectViewer.material_writer);
                    }
                }
                writer.Close();
            }
        }

        public void SaveToFox1(String dir, Node n, TextWriter g)
        {
            String filename = Path.Combine(dir, n.Name);
            filename += ".meshpart";
            FileStream writeStream = new FileStream(filename, FileMode.Create);
            BinaryWriter writeBinary = new BinaryWriter(writeStream);

            writeBinary.Write((int)FaceGroups.Count);
            foreach (FaceGroup f in FaceGroups)
            {
                f.Write(writeBinary);
            }

            writeBinary.Write((int)indices.Length);
            for (int i=0; i<indices.Length; i++)
            {
                writeBinary.Write(indices[i]);
            }

            writeBinary.Write((int)Verts.Length);
            for (int i = 0; i < Verts.Length; i++)
            {
                VertexPositionNormalTexture v = Verts[i];
                writeBinary.Write(v.Position.x);
                writeBinary.Write(v.Position.y);
                writeBinary.Write(v.Position.z);

                writeBinary.Write(v.Normal.x);
                writeBinary.Write(v.Normal.y);
                writeBinary.Write(v.Normal.z);

                writeBinary.Write(v.TextureCoordinate.X);
                writeBinary.Write(v.TextureCoordinate.Y);

            }

            writeBinary.Write(n.base_matrix.M11);
            writeBinary.Write(n.base_matrix.M12);
            writeBinary.Write(n.base_matrix.M13);
            writeBinary.Write(n.base_matrix.M14);

            writeBinary.Write(n.base_matrix.M21);
            writeBinary.Write(n.base_matrix.M22);
            writeBinary.Write(n.base_matrix.M23);
            writeBinary.Write(n.base_matrix.M24);

            writeBinary.Write(n.base_matrix.M31);
            writeBinary.Write(n.base_matrix.M32);
            writeBinary.Write(n.base_matrix.M33);
            writeBinary.Write(n.base_matrix.M34);

            writeBinary.Write(n.base_matrix.M41);
            writeBinary.Write(n.base_matrix.M42);
            writeBinary.Write(n.base_matrix.M43);
            writeBinary.Write(n.base_matrix.M44);

            writeBinary.Close();


           
            filename = Path.Combine(dir, n.Name);
            filename += ".materials";
            writeStream = new FileStream(filename, FileMode.Create);
            writeBinary = new BinaryWriter(writeStream);
            writeBinary.Write(Materials.Count);
            for (int i=0; i<Materials.Count; i++)
            {
                Materials[i].SaveToFox1(writeBinary);
            }
            writeBinary.Close();

           
            


        }
        #endregion

        #region Tests

        public bool Shoot(int x, int y, Matrix projection, Matrix view, Matrix world)
        {
            bool result = false;
            float? distance;
            Ray r = CreateRay(x, y, projection, view, world);
            int j = 0;
            for (int k = 0; k < colmesh.NBlocks; k++)
            {
                foreach (CollisionMeshPart p in colmesh.Blocks[k].Parts)
                {
                    for (int i = 0; i < p.Faces.Count; i += 3)
                    {
                        if (p.indices != null)
                        {
                            distance = Intersects(ref r, k, j, p.indices[i], p.indices[i + 1], p.indices[i + 2], world);
                            if (distance != null)
                            {
                                Vector3? pos = r.Position + (r.Direction * (distance - 1.0f));

                                ObjectViewer.shoot_system.AddParticle((Vector3)pos, Vector3.Zero);
                                ObjectViewer.hitmesh = p.Name;
                                return true;
                            }
                        }

                    }
                    j++;
                }
            }
            return result;
        }

        public bool QuickCheck(Matrix world, Ray r)
        {

            foreach (FaceGroup f in FaceGroups)
            {
                int tri = f.StartFace * 3;
                for (int j = 0; j < f.FaceCount; j++)
                {
                    int v1 = indices[tri++] + f.StartVertex;
                    int v2 = indices[tri++] + f.StartVertex;
                    int v3 = indices[tri++] + f.StartVertex;

                    Vector3 vv1 = Verts[v1].Position;
                    Vector3 vv2 = Verts[v2].Position;
                    Vector3 vv3 = Verts[v3].Position;

                    float? distance = Intersects(ref r, vv1, vv2, vv3, world);
                    if (distance != null)
                    {
                        return true;
                    }
                }
            }


            return false;
        }
        public bool Inside(float x, float y, String texture, float size)
        {
            Vector2 s = new Vector2(x, y);
            int i = 0;
            Vector2[] points = new Vector2[3];
            foreach (FaceGroup f in FaceGroups)
            {
                if (Form1.Manager.Names[Materials[i].TextureID].Equals(texture, StringComparison.OrdinalIgnoreCase))
                {
                    int tri = f.StartFace * 3;
                    for (int j = 0; j < f.FaceCount; j++)
                    {
                        int v1 = indices[tri++] + f.StartVertex;
                        int v2 = indices[tri++] + f.StartVertex;
                        int v3 = indices[tri++] + f.StartVertex;

                        points[0].x = trim(Verts[v1].TextureCoordinate.X, size);
                        points[0].y = trim(Verts[v1].TextureCoordinate.Y, size);
                        points[1].x = trim(Verts[v2].TextureCoordinate.X, size);
                        points[1].y = trim(Verts[v2].TextureCoordinate.Y, size);
                        points[2].x = trim(Verts[v3].TextureCoordinate.X, size);
                        points[2].y = trim(Verts[v3].TextureCoordinate.Y, size);
                        if (intpoint_inside_trigon(s, points[0], points[1], points[2]))
                        {
                            selected_facegroup = f;
                            return true;
                        }

                    }
                }
                i++;
            }
            selected_facegroup = null;
            return false;
        }

        bool intpoint_inside_trigon(Vector2 p, Vector2 p0, Vector2 p1, Vector2 p2)
        {
            float s = p0.Y * p2.X - p0.X * p2.Y + (p2.Y - p0.Y) * p.X + (p0.X - p2.X) * p.Y;
            float t = p0.X * p1.Y - p0.Y * p1.X + (p0.Y - p1.Y) * p.X + (p1.X - p0.X) * p.Y;

            if ((s < 0) != (t < 0))
                return false;

            var A = -p1.Y * p2.X + p0.Y * (p2.X - p1.X) + p0.X * (p1.Y - p2.Y) + p1.X * p2.Y;
            if (A < 0.0)
            {
                s = -s;
                t = -t;
                A = -A;
            }
            return s > 0 && t > 0 && (s + t) < A;
        }

        // Returns the distance from the origin of the ray to the intersection with 
        // the triangle, null if no intersect and negative if behind.
        private float? Intersects(ref Ray ray, Vector3 v1, Vector3 v2, Vector3 v3, Matrix world)
        {
            Vector3[] Vertex = new Vector3[3];
            Vertex[0] = Vector3.Transform(v1, world);
            Vertex[1] = Vector3.Transform(v2, world);
            Vertex[2] = Vector3.Transform(v3, world);

            // Set the Distance to indicate no intersect
            float? distance = null;
            // Compute vectors along two edges of the triangle.
            Vector3 edge1, edge2;

            Vector3.Subtract(ref Vertex[2], ref Vertex[1], out edge1);
            Vector3.Subtract(ref Vertex[0], ref Vertex[1], out edge2);

            // Compute the determinant.
            Vector3 directionCrossEdge2;
            Vector3.Cross(ref ray.Direction, ref edge2, out directionCrossEdge2);

            float determinant;
            Vector3.Dot(ref edge1, ref directionCrossEdge2, out determinant);

            // If the ray is parallel to the triangle plane, there is no collision.
            if (determinant > -float.Epsilon && determinant < float.Epsilon)
            {
                return distance;
            }

            float inverseDeterminant = 1.0f / determinant;

            // Calculate the U parameter of the intersection point.
            Vector3 distanceVector;
            Vector3.Subtract(ref ray.Position, ref Vertex[1], out distanceVector);

            float triangleU;
            Vector3.Dot(ref distanceVector, ref directionCrossEdge2, out triangleU);
            triangleU *= inverseDeterminant;

            // Make sure it is inside the triangle.
            if (triangleU < 0 || triangleU > 1)
            {
                return distance;
            }

            // Calculate the V parameter of the intersection point.
            Vector3 distanceCrossEdge1;
            Vector3.Cross(ref distanceVector, ref edge1, out distanceCrossEdge1);

            float triangleV;
            Vector3.Dot(ref ray.Direction, ref distanceCrossEdge1, out triangleV);
            triangleV *= inverseDeterminant;

            // Make sure it is inside the triangle.
            if (triangleV < 0 || triangleU + triangleV > 1)
            {
                return distance;
            }

            // == By here the ray must be inside the triangle

            // Compute the distance along the ray to the triangle.
            float length = 0;
            Vector3.Dot(ref edge2, ref distanceCrossEdge1, out length);
            distance = length * inverseDeterminant;

            return distance;
        }

        // Returns the distance from the origin of the ray to the intersection with 
        // the triangle, null if no intersect and negative if behind.
        private float? Intersects(ref Ray ray, int block, int part, short v1, short v2, short v3, Matrix world)
        {
            Vector3[] Vertex = new Vector3[3];
            Vertex[0] = Vector3.Transform(colmesh.Blocks[block].Parts[part].Verts[v1], world);
            Vertex[1] = Vector3.Transform(colmesh.Blocks[block].Parts[part].Verts[v2], world);
            Vertex[2] = Vector3.Transform(colmesh.Blocks[block].Parts[part].Verts[v3], world);

            // Set the Distance to indicate no intersect
            float? distance = null;
            // Compute vectors along two edges of the triangle.
            Vector3 edge1, edge2;

            Vector3.Subtract(ref Vertex[2], ref Vertex[1], out edge1);
            Vector3.Subtract(ref Vertex[0], ref Vertex[1], out edge2);

            // Compute the determinant.
            Vector3 directionCrossEdge2;
            Vector3.Cross(ref ray.Direction, ref edge2, out directionCrossEdge2);

            float determinant;
            Vector3.Dot(ref edge1, ref directionCrossEdge2, out determinant);

            // If the ray is parallel to the triangle plane, there is no collision.
            if (determinant > -float.Epsilon && determinant < float.Epsilon)
            {
                return distance;
            }

            float inverseDeterminant = 1.0f / determinant;

            // Calculate the U parameter of the intersection point.
            Vector3 distanceVector;
            Vector3.Subtract(ref ray.Position, ref Vertex[1], out distanceVector);

            float triangleU;
            Vector3.Dot(ref distanceVector, ref directionCrossEdge2, out triangleU);
            triangleU *= inverseDeterminant;

            // Make sure it is inside the triangle.
            if (triangleU < 0 || triangleU > 1)
            {
                return distance;
            }

            // Calculate the V parameter of the intersection point.
            Vector3 distanceCrossEdge1;
            Vector3.Cross(ref distanceVector, ref edge1, out distanceCrossEdge1);

            float triangleV;
            Vector3.Dot(ref ray.Direction, ref distanceCrossEdge1, out triangleV);
            triangleV *= inverseDeterminant;

            // Make sure it is inside the triangle.
            if (triangleV < 0 || triangleU + triangleV > 1)
            {
                return distance;
            }

            // == By here the ray must be inside the triangle

            // Compute the distance along the ray to the triangle.
            float length = 0;
            Vector3.Dot(ref edge2, ref distanceCrossEdge1, out length);
            distance = length * inverseDeterminant;

            return distance;
        }

        private Ray CreateRay(int x, int y, Matrix projection, Matrix view, Matrix world)
        {
            //  Unproject the screen space mouse coordinate into model space 
            //  coordinates. Because the world space matrix is identity, this 
            //  gives the coordinates in world space.
            Viewport vp = Form1.graphics.Viewport;

            Vector3 pos1 = vp.Unproject(new Vector3(x, y, 0), projection, view, Matrix.Identity);
            Vector3 pos2 = vp.Unproject(new Vector3(x, y, 1), projection, view, Matrix.Identity);
            Vector3 dir = Vector3.Normalize(pos2 - pos1);

            return new Ray(pos1, dir);
        }

        public void BuildAoList(Matrix world, ref List<Vector3> verts, ref List<Vector3> normals)
        {
            foreach (FaceGroup f in FaceGroups)
            {
                if (Form1.Manager.Names[Materials[f.Material].TextureID].Equals("skin1o.tga", StringComparison.OrdinalIgnoreCase))
                {
                    int tri = f.StartFace * 3;
                    for (int j = 0; j < f.FaceCount; j++)
                    {
                        int v1 = indices[tri++] + f.StartVertex;
                        int v2 = indices[tri++] + f.StartVertex;
                        int v3 = indices[tri++] + f.StartVertex;

                        verts.Add(Vector3.Transform(Verts[v1].Position, world));
                        verts.Add(Vector3.Transform(Verts[v2].Position, world));
                        verts.Add(Vector3.Transform(Verts[v3].Position, world));

                        normals.Add(Vector3.Transform(Verts[v1].Normal, world));
                        normals.Add(Vector3.Transform(Verts[v2].Normal, world));
                        normals.Add(Vector3.Transform(Verts[v3].Normal, world));

                    }
                }
            }
        }

        public void BuildAoListVertexCamera(String texture, Matrix world, ObjectViewer viewer)
        {
            AOBuilder.Instance.SetNode(mesh_name);
            AOBuilder.Instance.SetProgress(0);
            AOValues.Clear();
            AOLookUp.Clear();
            float prog;
            int i = 0;
            int total = indices.GetLength(0);
            int done = 0;
            foreach (FaceGroup f in FaceGroups)
            {
                if (Form1.Manager.Names[Materials[f.Material].TextureID].Equals(texture, StringComparison.OrdinalIgnoreCase))
                {
                    int tri = f.StartFace * 3;
                    for (int j = 0; j < f.FaceCount; j++)
                    {
                        int v1 = indices[tri++] + f.StartVertex;
                        int v2 = indices[tri++] + f.StartVertex;
                        int v3 = indices[tri++] + f.StartVertex;
                        if (!AOLookUp.ContainsKey(v1))
                        {
                            AOLookUp.Add(v1, i);
                            i++;
                            AOValues.Add(viewer.GetAo(Vector3.Transform(Verts[v1].Position, world),
                                                       Vector3.Transform(Verts[v1].Normal, world), Vector3.UnitY));
                        }
                        if (!AOLookUp.ContainsKey(v2))
                        {
                            AOLookUp.Add(v2, i);
                            i++;
                            AOValues.Add(viewer.GetAo(Vector3.Transform(Verts[v2].Position, world),
                                                      Vector3.Transform(Verts[v2].Normal, world), Vector3.UnitY));
                        }
                        if (!AOLookUp.ContainsKey(v3))
                        {
                            AOLookUp.Add(v3, i);
                            i++;
                            AOValues.Add(viewer.GetAo(Vector3.Transform(Verts[v3].Position, world),
                                                      Vector3.Transform(Verts[v3].Normal, world), Vector3.UnitY));
                        }
                        done += 3;
                        prog = (done * 100) / total;
                        AOBuilder.Instance.SetProgress((int)prog);
                    }

                }
                else
                {
                    done += f.FaceCount * 3;
                    prog = (done * 100) / total;
                    AOBuilder.Instance.SetProgress((int)prog);
                }
            }

        }

        public void BakeAmbientMultiVertex(String texture, Matrix world, ObjectViewer viewer)
        {
            AOBuilder.Instance.SetNode(mesh_name);
            AOBuilder.Instance.SetProgress(0);
            AOValues.Clear();
            AOLookUp.Clear();
            float prog;
            float val;
            int i = 0;
            int total = indices.GetLength(0);
            int done = 0;
            foreach (FaceGroup f in FaceGroups)
            {
                if (Form1.Manager.Names[Materials[f.Material].TextureID].Equals(texture, StringComparison.OrdinalIgnoreCase))
                {
                    int tri = f.StartFace * 3;
                    for (int j = 0; j < f.FaceCount; j++)
                    {
                        int v1 = indices[tri++] + f.StartVertex;
                        int v2 = indices[tri++] + f.StartVertex;
                        int v3 = indices[tri++] + f.StartVertex;

                        if (!AOLookUp.ContainsKey(v1))
                        {
                            AOLookUp.Add(v1, i);
                            i++;

                            val = viewer.GetAo(Vector3.Transform(Verts[v1].Position, world), Vector3.UnitX, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v1].Position, world), -Vector3.UnitX, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v1].Position, world), Vector3.UnitZ, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v1].Position, world), -Vector3.UnitZ, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v1].Position, world), Vector3.UnitY, Vector3.UnitZ);
                            val += viewer.GetAo(Vector3.Transform(Verts[v1].Position, world), -Vector3.UnitY, Vector3.UnitZ);

                            val -= 3;
                            val /= 3;
                            val = Math.Max(0, val);
                            AOValues.Add(val);
                        }
                        if (!AOLookUp.ContainsKey(v2))
                        {
                            AOLookUp.Add(v2, i);
                            i++;

                            val = viewer.GetAo(Vector3.Transform(Verts[v2].Position, world), Vector3.UnitX, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v2].Position, world), -Vector3.UnitX, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v2].Position, world), Vector3.UnitZ, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v2].Position, world), -Vector3.UnitZ, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v2].Position, world), Vector3.UnitY, Vector3.UnitZ);
                            val += viewer.GetAo(Vector3.Transform(Verts[v2].Position, world), -Vector3.UnitY, Vector3.UnitZ);

                            val -= 3;
                            val /= 3;
                            val = Math.Max(0, val);
                            AOValues.Add(val);
                        }
                        if (!AOLookUp.ContainsKey(v3))
                        {
                            AOLookUp.Add(v3, i);
                            i++;
                            val = viewer.GetAo(Vector3.Transform(Verts[v3].Position, world), Vector3.UnitX, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v3].Position, world), -Vector3.UnitX, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v3].Position, world), Vector3.UnitZ, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v3].Position, world), -Vector3.UnitZ, Vector3.UnitY);
                            val += viewer.GetAo(Vector3.Transform(Verts[v3].Position, world), Vector3.UnitY, Vector3.UnitZ);
                            val += viewer.GetAo(Vector3.Transform(Verts[v3].Position, world), -Vector3.UnitY, Vector3.UnitZ);

                            val -= 3;
                            val /= 3;
                            val = Math.Max(0, val);
                            AOValues.Add(val);
                        }
                        done += 3;
                        prog = (done * 100) / total;
                        AOBuilder.Instance.SetProgress((int)prog);
                    }

                }
                else
                {
                    done += f.FaceCount * 3;
                    prog = (done * 100) / total;
                    AOBuilder.Instance.SetProgress((int)prog);
                }
            }

        }

        public void BakeAmbientVertexRay(String texture, Matrix world, ObjectViewer viewer, int count)
        {
            AOBuilder.Instance.SetNode(mesh_name);
            AOBuilder.Instance.SetProgress(0);
            AOValues.Clear();
            AOLookUp.Clear();
            float prog;
            int i = 0;
            int total = indices.GetLength(0);
            int done = 0;
            foreach (FaceGroup f in FaceGroups)
            {
                if (Form1.Manager.Names[Materials[f.Material].TextureID].Equals(texture, StringComparison.OrdinalIgnoreCase))
                {
                    int tri = f.StartFace * 3;
                    for (int j = 0; j < f.FaceCount; j++)
                    {
                        int v1 = indices[tri++] + f.StartVertex;
                        int v2 = indices[tri++] + f.StartVertex;
                        int v3 = indices[tri++] + f.StartVertex;
                        if (!AOLookUp.ContainsKey(v1))
                        {
                            AOLookUp.Add(v1, i);
                            i++;
                            AOValues.Add(viewer.GetRayAo(Vector3.Transform(Verts[v1].Position, world), count));

                        }
                        if (!AOLookUp.ContainsKey(v2))
                        {
                            AOLookUp.Add(v2, i);
                            i++;
                            AOValues.Add(viewer.GetRayAo(Vector3.Transform(Verts[v2].Position, world), count));

                        }
                        if (!AOLookUp.ContainsKey(v3))
                        {
                            AOLookUp.Add(v3, i);
                            i++;
                            AOValues.Add(viewer.GetRayAo(Vector3.Transform(Verts[v3].Position, world), count));
                        }
                        done += 3;
                        prog = (done * 100) / total;
                        AOBuilder.Instance.SetProgress((int)prog);
                    }

                }
                else
                {
                    done += f.FaceCount * 3;
                    prog = (done * 100) / total;
                    AOBuilder.Instance.SetProgress((int)prog);
                }
            }

        }
        #endregion

        #region 3DS support
        public void Initialise(ThreeDSFile.Entity e, String dir)
        {
            Verts = new VertexPositionNormalTexture[e.vertices.GetLength(0)];
            for (int i = 0; i < e.vertices.GetLength(0); i++)
            {
                Verts[i].Position = e.vertices[i];
                Verts[i].TextureCoordinate = e.texcoords[i];
                Verts[i].Normal = e.normals[i];
            }
            indices = new short[e.indices.GetLength(0) * 3];
            int pos = 0;
            for (int i = 0; i < e.indices.GetLength(0); i++)
            {
                indices[pos++] = (short)e.indices[i].vertex1;
                indices[pos++] = (short)e.indices[i].vertex2;
                indices[pos++] = (short)e.indices[i].vertex3;
            }
            LodDistances.push_back(99999);

            FaceGroup f = new FaceGroup();
            f.StartVertex = 0;
            f.StartFace = 0;
            f.VertexCount = e.vertices.GetLength(0);
            f.FaceCount = e.indices.GetLength(0);
            FaceGroups.Add(f);

            Material m = new Material(e.material.name);
            m.AlphaTestVal = 0;
            m.Ambient = e.material.Ambient[0];
            m.Diffuse = 0.9f;
            m.Colour = e.material.Diffuse;
            m.Glass = false;
            m.Shine = e.material.Specular[0];
            m.Specular = m.Shine;
            m.SpecularPow = e.material.Shininess;

            if (!String.IsNullOrEmpty(e.material.texture))
                m.TextureID = Form1.Manager.AddTexture(e.material.texture, dir);
            else
            {
                m.TextureID = Form1.Manager.AddTexture(@"null.tga", @"C:\Aircraft\3do\Plane\textures");
            }

            Materials.Add(m);
        }
        #endregion

        #region Modifiers
        List<Vector3> cache = new List<Vector3>();

        public void CacheNormals()
        {
            cache.Clear();
            for (int j = 0; j < Verts.Length; j++)
            {
                cache.Add(Verts[j].Normal);
            }
            for (int j = 0; j < Verts.Length; j++)
            {
                cache[j].Normalize();
            }
        }

        public void RecoverNormals()
        {
            for (int j = 0; j < Verts.Length; j++)
            {
                Verts[j].Normal = cache[j];
            }
        }
        public void FlipNormals(int axis)
        {
            switch (axis)
            {
                case 0: // z axis
                    {
                        for (int j = 0; j < cache.Count; j++)
                        {
                            Vector3 delta = new Vector3(0, 0, Verts[j].Position.z);
                            delta = delta - Verts[j].Position;
                            delta.Normalize();
                            float a = Vector3.Dot(delta, cache[j]);
                            a = MathHelper.ToDegrees((float)(Math.Acos(a)));
                            if (a > 45)
                            {
                                Verts[j].Normal = -cache[j];
                            }
                            else
                            {
                                Verts[j].Normal = cache[j];
                            }
                        }
                    }
                    break;
                case 1: // y axis
                    {
                        for (int j = 0; j < cache.Count; j++)
                        {
                            Vector3 delta = new Vector3(0, Verts[j].Position.y, 0);
                            delta = delta - Verts[j].Position;
                            delta.Normalize();
                            float a = Vector3.Dot(delta, cache[j]);
                            a = MathHelper.ToDegrees((float)Math.Abs(Math.Acos(a)));
                            if (a > 45)
                            {
                                Verts[j].Normal = -cache[j];
                            }
                            else
                            {
                                Verts[j].Normal = cache[j];
                            }
                        }
                    }
                    break;
                case 2: // X axis
                    {
                        for (int j = 0; j < cache.Count; j++)
                        {
                            Vector3 delta = new Vector3(Verts[j].Position.x, 0, 0);
                            delta = delta - Verts[j].Position;
                            delta.Normalize();
                            float a = Vector3.Dot(delta, cache[j]);
                            a = MathHelper.ToDegrees((float)Math.Abs(Math.Acos(a)));
                            if (a > 45)
                            {
                                Verts[j].Normal = -cache[j];
                            }
                            else
                            {
                                Verts[j].Normal = cache[j];
                            }
                        }
                    }
                    break;

                case 3:// ALWAYS
                    {
                        for (int j = 0; j < cache.Count; j++)
                        {
                            Verts[j].Normal = -cache[j];
                        }
                    }
                    break;

            }

        }

        public void FlipNormals(int axis, int facegroup)
        {
            FaceGroup f = FaceGroups[facegroup];
            for (int i=0; i<f.VertexCount; i++)
            {
                int j = f.StartVertex + i;
                switch (axis)
                {
                    case 0:
                        Verts[j].Normal = Vector3.Reflect(cache[j], Vector3.UnitZ);
                        break;
                    case 1:
                        Verts[j].Normal = Vector3.Reflect(cache[j], Vector3.UnitY);
                        break;
                    case 2:
                        Verts[j].Normal = Vector3.Reflect(cache[j], Vector3.UnitX);
                        break;
                    case 3:
                        Verts[j].Normal = -cache[j];
                        break;
                }
            }

             

        }

        public void RecalculateNormals()
        {
            RegenerateNormals();
        }

        public void MoveUV(float dx, float dy)
        {
            if (selected_facegroup != null)
            {
                int start = selected_facegroup.StartVertex;
                for (int i = 0; i < selected_facegroup.VertexCount; i++)
                {
                    Verts[start].TextureCoordinate.X += dx;
                    Verts[start].TextureCoordinate.Y += dy;
                    start++;
                }

            }
        }

        public void RegenerateNormals()
        {
            int length = Verts.GetLength(0);
            float[] counts = new float[length];
            Vector3[] normals = new Vector3[length];
            for (int i = 0; i < length; i++)
            {
                counts[i] = 0;
                normals[i] = Vector3.Zero;
            }
            foreach (FaceGroup f in FaceGroups)
            {
                int tri = f.StartFace * 3;
                for (int j = 0; j < f.FaceCount; j++)
                {
                    int v1 = indices[tri++] + f.StartVertex;
                    int v2 = indices[tri++] + f.StartVertex;
                    int v3 = indices[tri++] + f.StartVertex;

                    Vector3 norm = calcNormal(v1, v2, v3);
                    normals[v1] += norm;
                    normals[v2] += norm;
                    normals[v3] += norm;
                    counts[v1]++;
                    counts[v2]++;
                    counts[v3]++;


                }
            }
            for (int i = 0; i < length; i++)
            {
                normals[i] /= counts[i];
                normals[i].Normalize();
                Verts[i].Normal = normals[i];
            }

        }

        Vector3 calcNormal(int v1, int v2, int v3)    			// Calculates Normal For A Quad Using 3 Points
        {
            Matrix iw = Matrix.Invert(parent.previous_matrix);

            Vector3 sv1 = Vector3.Transform(Verts[v2].Position, iw) - Vector3.Transform(Verts[v1].Position, iw);
            Vector3 sv2 = Vector3.Transform(Verts[v3].Position, iw) - Vector3.Transform(Verts[v1].Position, iw);
            Vector3 cv3 = Vector3.Cross(sv1, sv2);
            
            return cv3;
        }

        public void SwapTriangles()
        {
            foreach (FaceGroup f in FaceGroups)
            {
                int tri = f.StartFace * 3;
                for (int j = 0; j < f.FaceCount; j++)
                {
                    int v1 = indices[tri] + f.StartVertex;
                    int v2 = indices[tri + 1] + f.StartVertex;
                    int v3 = indices[tri + 2] + f.StartVertex;
                    if (!ClockWise(v1, v2, v3))
                    {
                        indices[tri + 1] = (short)(v3 - f.StartVertex);
                        indices[tri + 2] = (short)(v2 - f.StartVertex);
                    }
                    tri += 3;
                }
            }

        }

        bool ClockWise(int v1, int v2, int v3)
        {
            Vector3 n = calcNormal(v1, v2, v3);
            return (Math.Abs(Vector3.Dot(n, Verts[v1].Normal)) > 0.5);
        }

        public void Rotate90Z()
        {
            Matrix m = Matrix.CreateRotationZ(MathHelper.ToRadians(90));
            for (int i = 0; i < Verts.Length; i++)
            {
                Vector3 res = Vector3.Transform(Verts[i].Position, m);
                Verts[i].Position = res;
            }
        }

        public void AdjustLighting()
        {
            foreach (Material m in Materials)
            {
                m.AdjustLighting();
            }
        }
        #endregion

        #region Binormals and tangents
        public void GenerateBinormalsAndTangents()
        {
            BumpVerts = new BumpVertex[Verts.Length];
            for (int i = 0; i < Verts.Length; i++)
            {
                BumpVerts[i].Position = Verts[i].Position;
                BumpVerts[i].TextureCoordinate = Verts[i].TextureCoordinate;
                BumpVerts[i].Normal = Verts[i].Normal;
            }

            Vector3[] tan2 = new Vector3[Verts.Length];
            Vector3[] tan1 = new Vector3[Verts.Length];

            for (int a = 0; a < (indices.Length); a += 3)
            {
                short i1 = indices[a + 0];
                short i2 = indices[a + 1];
                short i3 = indices[a + 2];

                Vector3 v1 = Verts[i1].Position;
                Vector3 v2 = Verts[i2].Position;
                Vector3 v3 = Verts[i3].Position;

                Vector2 w1 = Verts[i1].TextureCoordinate;
                Vector2 w2 = Verts[i2].TextureCoordinate;
                Vector2 w3 = Verts[i3].TextureCoordinate;

                float x1 = v2.X - v1.X;
                float x2 = v3.X - v1.X;
                float y1 = v2.Y - v1.Y;
                float y2 = v3.Y - v1.Y;
                float z1 = v2.Z - v1.Z;
                float z2 = v3.Z - v1.Z;

                float s1 = w2.X - w1.X;
                float s2 = w3.X - w1.X;
                float t1 = w2.Y - w1.Y;
                float t2 = w3.Y - w1.Y;

                float r = 1.0F / (s1 * t2 - s2 * t1);
                Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
                Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

                tan1[i1] += sdir;
                tan1[i2] += sdir;
                tan1[i3] += sdir;

                tan2[i1] += tdir;
                tan2[i2] += tdir;
                tan2[i3] += tdir;
            }

            for (int a = 0; a < Verts.Length; a++)
            {
                Vector3 n = Verts[a].Normal;
                Vector3 t = tan1[a];

                n.Normalize();
                Vector3 p = n * Vector3.Dot(t, n);
                t = t - p;
                t.Normalize();
                BumpVerts[a].Tangent = t;

                Vector4 temp = new Vector4(t.X, t.Y, t.Z, 1);

                // Calculate handedness
                temp.W = (Vector3.Dot(Vector3.Cross(n, t), tan2[a]) < 0.0f) ? -1.0f : 1.0f;
                BumpVerts[a].BiNormal = Vector3.Normalize(Vector3.Cross(n, t) * temp.W);
            }
        }

        public void EnableBumpMapping(int i, int t)
        {
            for (int j = 0; j < Materials.Count; j++)
            {
                if (Materials[j].TextureID == i)
                {
                    Materials[j].BumpMapped = true;
                    Materials[j].NormalTex = t;
                }
            }
        }
        #endregion

        #region FBX

        FbxVector2 getFBXUV(int i)
        {
            float x = Verts[i].TextureCoordinate.X;
            x -= (int)x;
            if (x < 0)
                x += 1;

            float y = Verts[i].TextureCoordinate.Y;
            y -= (int)y;
            if (y < 0)
                y += 1;
            y = 1 - y;

            return new FbxVector2(x, y);
        }

        public FbxNode SaveFBX(String dir, Matrix world, Node dad, Node mom)
        {
            Form1.Instance.globalSaveAsFBXDialog.Progress(mesh_name);
            FbxMesh mesh;
            String node_name;

            #region Create mesh and add verts
            if (Form1.Instance.fbx_meshes.ContainsKey(mesh_name))
            {
                mesh = Form1.Instance.fbx_meshes[mesh_name];
                node_name = mesh_name + "_inst_" + Form1.Instance.Fbx_node_count;
                Form1.Instance.Fbx_node_count++;
            }
            else
            {
                node_name = dad.Name;
                mesh = FbxMesh.Create(Form1.Instance.sdkManager, mesh_name);
                mesh.InitControlPoints(Verts.Length);

                FbxVector4[] fVerts = new FbxVector4[Verts.Length];
                for (int i = 0; i < Verts.Length; i++)
                {
                    Vector3 vr = Verts[i].Position;
                    fVerts[i] = new FbxVector4(vr.X, vr.Y, vr.Z);
                }
                mesh.ControlPoints = fVerts;

                #region Add normals
                // Set the normals on Layer 0.
                FbxLayer layer = mesh.GetLayer(0);
                if (layer == null)
                {
                    mesh.CreateLayer();
                    layer = mesh.GetLayer(0);
                }

                // We want to have one normal for each vertex (or control point),
                // so we set the mapping mode to eBY_CONTROL_POINT.
                FbxLayerElementNormal layerElementNormal = FbxLayerElementNormal.Create(mesh, "");

                layerElementNormal.Mapping_Mode = FbxLayerElement.MappingMode.ByControlPoint;
                layerElementNormal.Reference_Mode = FbxLayerElement.ReferenceMode.Direct;

                for (int i = 0; i < Verts.Length; i++)
                {
                    FbxVector4 fn = new FbxVector4(Verts[i].Normal.x, Verts[i].Normal.y, Verts[i].Normal.z);
                    layerElementNormal.DirectArray.Add(fn);
                }
                layer.Normals = layerElementNormal;

                #endregion

                #region Add UV coords
                // Create UV for Diffuse channel
                FbxLayerElementUV UVDiffuseLayer = FbxLayerElementUV.Create(mesh, "DiffuseUV");
                UVDiffuseLayer.Mapping_Mode = FbxLayerElement.MappingMode.ByPolygonVertex;
                UVDiffuseLayer.Reference_Mode = FbxLayerElement.ReferenceMode.IndexToDirect;
                for (int i = 0; i < Verts.Length; i++)
                {
                    FbxVector2 fu = getFBXUV(i);
                    //Debug.WriteLine(String.Format("{0}\t\t({1},{2})", i, Verts[i].TextureCoordinate.X, Verts[i].TextureCoordinate.Y));
                    UVDiffuseLayer.DirectArray.Add(fu);
                }
                layer.SetUVs(UVDiffuseLayer, FbxLayerElement.LayerElementType.DiffuseTextures);
                #endregion

                #region Materials

                // Set material mapping.
                FbxLayerElementMaterial materialLayer = FbxLayerElementMaterial.Create(mesh, "");
                materialLayer.Mapping_Mode = FbxLayerElement.MappingMode.ByPolygon;
                materialLayer.Reference_Mode = FbxLayerElement.ReferenceMode.IndexToDirect;
                layer.Materials = materialLayer;

                FbxLayerElementTexture textureDiffuseLayer = FbxLayerElementTexture.Create(mesh, "Diffuse Texture");
                textureDiffuseLayer.Mapping_Mode = FbxLayerElement.MappingMode.ByPolygon;
                textureDiffuseLayer.Reference_Mode = FbxLayerElement.ReferenceMode.IndexToDirect;
                layer.DiffuseTextures = textureDiffuseLayer;


                foreach (Material m in Materials)
                {
                    FbxTexture texture = FbxTexture.Create(Form1.Instance.sdkManager, "DiffuseTexture" + m.Name);
                    String np = Path.Combine(dir, Path.GetFileNameWithoutExtension(m.tname));
                    np += ".png";
                    // Set texture properties.
                    texture.SetFileName(np); // Resource file is in current directory.
                    texture.TextureUseType = FbxTexture.TextureUse.Standard;
                    texture.Mapping = FbxTexture.MappingType.Uv;
                    texture.MaterialUseType = FbxTexture.MaterialUse.Model;
                    texture.SwapUV = false;
                    texture.SetTranslation(0.0, 0.0);
                    texture.SetScale(1.0, 1.0);
                    texture.SetRotation(0.0, 0.0);
                    texture.AlphaSrc = FbxTexture.AlphaSource.RgbIntensity;
                    texture.DefaultAlpha = 0;

                    FbxTexture.WrapMode xwrap = FbxTexture.WrapMode.Repeat;
                    FbxTexture.WrapMode ywrap = FbxTexture.WrapMode.Repeat;
                    if (!m.tfWrapX)
                        xwrap = FbxTexture.WrapMode.Clamp;
                    if (!m.tfWrapY)
                        ywrap = FbxTexture.WrapMode.Clamp;

                    texture.SetWrapMode(xwrap, ywrap);

                    layer.DiffuseTextures.DirectArray.Add(texture);

                    FbxSurfacePhong material = FbxSurfacePhong.Create(Form1.Instance.sdkManager, m.Name);

                    // Generate primary and secondary colors.
                    material.EmissiveColor = new FbxDouble3(0.0, 0.0, 0.0);
                    material.AmbientColor = new FbxDouble3(m.Ambient, m.Ambient, m.Ambient);
                    material.DiffuseColor = new FbxDouble3(m.Diffuse, m.Diffuse, m.Diffuse);
                    material.SpecularColor = new FbxDouble3(m.Specular, m.Specular, m.Specular);
                    material.TransparencyFactor = m.AlphaTestVal;
                    material.Shininess = m.SpecularPow;
                    material.ShadingModel = "phong";
                    material.SpecularFactor = m.Shine;


                    layer.Materials.DirectArray.Add(material);
                }

                #endregion

                #region Polygons
                int count = 0;
                int group = 0;
                foreach (FaceGroup f in FaceGroups)
                {
                    int s = f.StartFace * 3;
                    for (int i = 0; i < f.FaceCount; i++)
                    {
                        mesh.BeginPolygon(f.Material, f.Material, group, true);
                        mesh.AddPolygon(indices[s] + f.StartVertex, indices[s] + f.StartVertex);
                        layer.DiffuseTextures.IndexArray.SetAt(s, f.Material);
                        layer.Materials.IndexArray.SetAt(s, f.Material);
                        s++;

                        mesh.AddPolygon(indices[s] + f.StartVertex, indices[s] + f.StartVertex);
                        layer.DiffuseTextures.IndexArray.SetAt(s, f.Material);
                        layer.Materials.IndexArray.SetAt(s, f.Material);

                        s++;
                        mesh.AddPolygon(indices[s] + f.StartVertex, indices[s] + f.StartVertex);
                        layer.DiffuseTextures.IndexArray.SetAt(s, f.Material);
                        layer.Materials.IndexArray.SetAt(s, f.Material);
                        s++;
                        mesh.EndPolygon();

                        count += 3;
                    }
                    group++;
                }
                #endregion

                UVDiffuseLayer.IndexArray.Count = count;
                Form1.Instance.fbx_meshes.Add(mesh_name, mesh);
                node_name = mesh_name;
            }
            #endregion

            FbxNode node = FbxNode.Create(Form1.Instance.sdkManager, node_name);
            Form1.Instance.fbx_nodes.Add(dad.Name.ToLower(), node); 
            node.NodeAttribute = mesh;
            node.Shading_Mode = FbxNode.ShadingMode.TextureShading;

            Vector3 scale;
            Vector3 translation;
            Quaternion rotation;

            world.Decompose(out scale, out rotation, out translation);

            Vector3 lrot = FromQ2(rotation);
            FbxDouble3 loc = new FbxDouble3(translation.X, translation.Y, translation.Z);
            FbxDouble3 llrot = new FbxDouble3(lrot.X, lrot.Y, lrot.Z);
            FbxDouble3 lscale = new FbxDouble3(scale.X, scale.Y, scale.Z);

            node.Limits.ScalingLimitActive = false;
            node.Limits.RotationLimitActive = false;
            node.RotationActive = true;
            node.ScalingActive = true;

            node.LclTranslation.Set(loc);
            node.LclRotation.Set(llrot);
            node.LclScaling.Set(lscale);

            node.SetRotationOrder(FbxNode.PivotSet.SourceSet, FbxRotationOrder.EulerZXY);

            // Build the node tree.
            FbxNode rootNode = Form1.Instance.scene.RootNode;
            if (dad != null)
            {
                if (!dad.Parent.Contains("ROOT"))
                {
                    rootNode = Form1.Instance.fbx_nodes[dad.Parent.ToLower()];
                    if (rootNode == null)
                        rootNode = Form1.Instance.scene.RootNode;
                }
            }

            if (Form1.ExportBaseMesh)
                rootNode.AddChild(node);

            #region Shadow
            // now add the shadow
            if (ShadowVertsArray != null)
            {
                #region Create mesh and add verts
                FbxMesh smesh = FbxMesh.Create(Form1.Instance.sdkManager, mesh_name + "_shadow");
                smesh.InitControlPoints(ShadowVertsArray.Length);

                FbxVector4[] sVerts = new FbxVector4[ShadowVertsArray.Length];
                for (int i = 0; i < ShadowVertsArray.Length; i++)
                {
                    Vector3 vr = ShadowVertsArray[i].Position;
                    sVerts[i] = new FbxVector4(vr.X, vr.Y, vr.Z);

                }

                smesh.ControlPoints = sVerts;
                #endregion

                #region Add triangles
                for (int j = 0; j < ShadowIndicesArray.Length; j += 3)
                {
                    smesh.BeginPolygon(-1, -1, 0, true);
                    smesh.AddPolygon(ShadowIndicesArray[j], -1);
                    smesh.AddPolygon(ShadowIndicesArray[j + 1], -1);
                    smesh.AddPolygon(ShadowIndicesArray[j + 2], -1);
                    smesh.EndPolygon();
                }

                #endregion

                FbxNode snode = FbxNode.Create(Form1.Instance.sdkManager, mesh_name + "_shadow");
                snode.NodeAttribute = smesh;
                snode.Shading_Mode = FbxNode.ShadingMode.WireFrame;

                if (Form1.ExportShadows)
                    node.AddChild(snode);


            }
            #endregion

            #region Hooks
            foreach (Hook h in Hooks)
            {
                FbxNode hnode = FbxNode.Create(Form1.Instance.sdkManager, h.Name);
                hnode.Shading_Mode = FbxNode.ShadingMode.WireFrame;

                Quaternion qrot;
                Vector3 tran;
                Vector3 scl;
                h.matrix.Decompose(out scl, out qrot, out tran);

                Vector3 hlrot = FromQ2(rotation);

                hnode.Limits.ScalingLimitActive = false;
                hnode.Limits.RotationLimitActive = false;
                hnode.RotationActive = true;
                hnode.ScalingActive = true;


                hnode.SetRotationOrder(FbxNode.PivotSet.SourceSet, FbxRotationOrder.EulerZXY);


                Vector3 rtran = tran;
                hnode.LclTranslation.Set(new FbxDouble3(rtran.X, rtran.Y, rtran.Z));
                hnode.LclScaling.Set(new FbxDouble3(scl.X, scl.Y, scl.Z));
                hnode.LclRotation.Set(new FbxDouble3(hlrot.X, hlrot.Y, hlrot.Z));

                if (Form1.ExportHooks)
                    node.AddChild(hnode);
            }
            #endregion

            #region LODs
            for (int i = 0; i < Lods.Count; i++)
            {
                if (Form1.ExportLODs)
                    node.AddChild(Lods[i].CreateFBXNode(Form1.Instance.sdkManager, mesh_name, i, dir, Matrix.Identity));

                FbxNode shadow_node = Lods[i].AddShadow(Form1.Instance.sdkManager, mesh_name, i, dir, Matrix.Identity);

                if ((shadow_node != null) && (Form1.ExportLODShadows))
                    node.AddChild(shadow_node);
            }
            #endregion

            #region Collision meshes
            if (Form1.ExportCollisionMeshes)
            {
                for (int j = 0; j < colmesh.NBlocks; j++)
                {
                    foreach (CollisionMeshPart part in colmesh.Blocks[j].Parts)
                    {
                        if (part.Faces.Count > 0)
                        {
                            node.AddChild(part.AddCollisionMeshPart(mesh_name, Form1.Instance.sdkManager, Matrix.Identity));
                        }
                    }
                }
            }
            #endregion

            return node;
        }


        /// <summary> 
        /// The function converts a Microsoft.Xna.Framework.Quaternion into a Microsoft.Xna.Framework.Vector3 
        /// </summary> 
        /// <param name="q">The Quaternion to convert</param> 
        /// <returns>An equivalent Vector3</returns> 
        /// <remarks> 
        /// This function was extrapolated by reading the work of Martin John Baker. All credit for this function goes to Martin John. 
        /// http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/index.htm 
        /// </remarks> 
        public Vector3 QuaternionToEuler(Quaternion q)
        {
            Vector3 v = new Vector3();

            v.x = (float)Math.Atan2
            (
                2 * q.Y * q.W - 2 * q.X * q.Z,
                1 - 2 * Math.Pow(q.Y, 2) - 2 * Math.Pow(q.Z, 2)
            );

            v.y = (float)Math.Asin
            (
                2 * q.X * q.Y + 2 * q.Z * q.W
            );

            v.z = (float)Math.Atan2
            (
                2 * q.X * q.W - 2 * q.Y * q.Z,
                1 - 2 * Math.Pow(q.X, 2) - 2 * Math.Pow(q.Z, 2)
            );

            if (q.X * q.Y + q.Z * q.W == 0.5)
            {
                v.x = (float)(2 * Math.Atan2(q.X, q.W));
                v.z = 0;
            }

            else if (q.X * q.Y + q.Z * q.W == -0.5)
            {
                v.x = (float)(-2 * Math.Atan2(q.X, q.W));
                v.z = 0;
            }

            return v;
        }

        Vector3 FromQ2(Quaternion q1)
        {
            float sqw = q1.W * q1.W;
            float sqx = q1.X * q1.X;
            float sqy = q1.Y * q1.Y;
            float sqz = q1.Z * q1.Z;
            float unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
            float test = q1.X * q1.W - q1.Y * q1.Z;
            Vector3 v;

            if (test > 0.4995f * unit)
            { // singularity at north pole
                v.y = (float)(2 * Math.Atan2(q1.Y, q1.X));
                v.x = MathHelper.PiOver2;
                v.z = 0;
                return NormalizeAngles(v);
            }
            if (test < -0.4995f * unit)
            { // singularity at south pole
                v.y = (float)(-2 * Math.Atan2(q1.Y, q1.X));
                v.x = -MathHelper.PiOver2;
                v.z = 0;
                return NormalizeAngles(v);
            }
            Quaternion q = new Quaternion(q1.W, q1.Z, q1.X, q1.Y);
            v.y = (float)Math.Atan2(2 * q.X * q.W + 2 * q.Y * q.Z, 1 - 2 * (q.Z * q.Z + q.W * q.W));        // Yaw
            v.x = (float)Math.Asin(2 * (q.X * q.Z - q.W * q.Y));                                            // Pitch
            v.z = (float)Math.Atan2(2 * q.X * q.Y + 2 * q.Z * q.W, 1 - 2 * (q.Y * q.Y + q.Z * q.Z));        // Roll
            return NormalizeAngles(v);
        }

        Vector3 NormalizeAngles(Vector3 angles)
        {
            angles.x = NormalizeAngle(MathHelper.ToDegrees(angles.X));
            angles.y = NormalizeAngle(MathHelper.ToDegrees(angles.Y));
            angles.z = NormalizeAngle(MathHelper.ToDegrees(angles.Z));
            return angles;
        }

        float NormalizeAngle(float angle)
        {
            while (angle > 360)
                angle -= 360;
            while (angle < 0)
                angle += 360;
            return angle;
        }
        #endregion
#endif

}


namespace il2ge::mesh
{


std::unique_ptr<MeshBase> loadMesh(std::string path)
{
  return std::make_unique<il2ge::mesh::Mesh>(path);
}


std::shared_ptr<MeshBase> loadMesh(std::string path, MeshDeleter deleter)
{
  return std::shared_ptr<MeshBase>(new il2ge::mesh::Mesh(path), deleter);
}


std::unique_ptr<MeshBase> loadMesh(std::string path, const char *data, size_t data_size)
{
//   LOG_INFO<<"loadMesh: "<<path<<std::endl;
  return std::make_unique<il2ge::mesh::Mesh>(path, data, data_size);
}


}
