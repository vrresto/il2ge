#include <il2ge/mesh/mesh_base.h>
#include <il2ge/material.h>
#include <il2ge/renderer.h>


namespace il2ge::mesh
{


bool MeshInstanceBase::hasZSortedFaces(int lod)
{
    return !m_lods.at(lod).z_sorted_faces.empty();
}


const std::vector<Face>& MeshInstanceBase::getZSortedFaces(int lod)
{
    return m_lods.at(lod).z_sorted_faces;
}


void MeshInstanceBase::materialsChanged(const MeshBase& mesh,
                                        const MaterialList& materials,
                                        const renderer::MaterialMapping& mapping)
{
  m_lods.resize(mesh.getLodCount());

  for (int i = 0; i < mesh.getLodCount(); i++)
  {
    auto& lod = m_lods.at(i);
    auto& lod_mesh = mesh.getLod(i);

    lod.z_sorted_faces.clear();

    for (auto& fg : lod_mesh.getFaceGroups())
    {
      auto mat_index = mapping.empty() ?
        fg.Material : mapping.at(fg.Material);

      if (materials.has(mat_index))
      {
        auto& mat = materials.at(mat_index);
        if (mat->tfShouldSort)
        {
          createFaces(lod_mesh, fg, mat_index, lod.z_sorted_faces);
        }
      }
    }
  }
}


MeshInstance::MeshInstance(std::shared_ptr<const mesh::MeshBase> mesh,
                           MaterialList& materials) :
  m_mesh(mesh), m_materials(std::make_unique<MaterialList>(materials))
{
  materialsChanged(*m_mesh, materials, {});
}



MeshInstance::MeshInstance(const MeshInstance& other) :
  m_mesh(other.m_mesh)
{
  UNIMPLEMENTED
}


MeshInstance::~MeshInstance()
{
}


void MeshInstance::draw(const Mat4 &model_view, float distance,
                        renderer::Renderer& renderer, int frame)
{
  assert(m_materials);

  int lod = 0;

  if (!m_mesh->isAnimated())
  {
    assert(frame == 0);
    lod = m_mesh->getLodNumAtDistance(distance);
  }

  auto& lod_mesh = m_mesh->getLod(lod);
  auto renderer_data = lod_mesh.getRendererData();

  renderer::MaterialMapping mapping;

  renderer.addToRenderList(renderer_data, mapping, *m_materials,
#if USE_MESH_INSTANCE_RENDER_LIST_CACHE
                           getRenderListCache(),
#endif
                           model_view, distance, frame);

  if (hasZSortedFaces(lod))
  {
    renderer.addToRenderList(mapping,
                              *m_materials,
                              getZSortedFaces(lod),
                              model_view,
                              true,
                              renderer::RenderGroupID::DEFAULT,
                              distance);
  }
}


std::unique_ptr<MeshInstance> MeshInstance::clone() const
{
  return std::make_unique<MeshInstance>(*this);
}


const MeshBase& MeshInstance::getMesh() const
{
  assert(m_mesh);
  return *m_mesh;
}


#if USE_MESH_INSTANCE_RENDER_LIST_CACHE
renderer::MeshInstanceRenderListCache& MeshInstance::getRenderListCache()
{
  if (!m_render_list_cache)
  {
    assert(m_materials);
    m_render_list_cache =
      std::make_unique<renderer::MeshInstanceRenderListCache>(*m_materials);
  }
  return *m_render_list_cache;
}
#endif


} // namespace il2ge::mesh
