/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MESH_BINARY_READER_H
#define IL2GE_MESH_BINARY_READER_H


#include <fstream>


namespace il2ge::mesh
{


class BinaryReader
{
  std::ifstream file;
  std::istream &in;

public:
  BinaryReader(std::istream &in) : in(in)
  {
    assert(in.good());
  }

  BinaryReader(std::string file_name) : in(file)
  {
    file.exceptions(std::ifstream::failbit | std::ifstream::badbit | std::ifstream::eofbit);
    file.open(file_name, std::ios_base::binary);
  }

  template <typename T>
  T read()
  {
    char data[sizeof(T)];
    in.read(data, sizeof(data));
    assert(in.good());
    return *reinterpret_cast<T*>(&data);
  }

  unsigned char ReadByte()
  {
    return read<unsigned char>();
  }

  int16_t ReadInt16()
  {
    return read<int16_t>();
  }

  int32_t ReadInt32()
  {
    return read<int32_t>();
  }

  float ReadSingle()
  {
    static_assert(sizeof(float) == 4);
    return read<float>();
  }

  void seek(int32_t pos)
  {
    assert(pos >= 0);
    in.seekg(pos);
  }

  int32_t getPos()
  {
    return in.tellg();
  }

};


}

#endif
