/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "collision_node.h"

#if 0
        public CollisionNode(String data)
        {
            char [] seperators = new char[]{' ','\t'};
            string[] parts = data.Split(seperators,StringSplitOptions.RemoveEmptyEntries);
            if (parts[1].Equals("sphere"))
            {
                Sphere = new BoundingSphere();
                Sphere.Radius = float.Parse(parts[2], System.Globalization.CultureInfo.InvariantCulture);
                if (parts.Length > 3)
                {
                    Sphere.Center = new Vector3(float.Parse(parts[3], System.Globalization.CultureInfo.InvariantCulture),
                                                float.Parse(parts[4], System.Globalization.CultureInfo.InvariantCulture),
                                                float.Parse(parts[5], System.Globalization.CultureInfo.InvariantCulture));

                }
                else
                {
                    Sphere.Center = new Vector3(0, 0, 0);
                }

                nType = CollisionNodeType.Sphere;
            }
            else
            {
                nType = CollisionNodeType.Mesh;
                Name = parts[1];
            }
        }
        public void Serialise(TextWriter tw)
        {
            switch (nType)
            {
                case CollisionNodeType.Mesh:
                    {
                        tw.WriteLine(String.Format("CollisionObject {0}", Name));
                    }
                    break;
                case CollisionNodeType.Sphere:
                    {
                        tw.WriteLine(String.Format("CollisionObject sphere {0} {1} {2} {3}",
                                     Sphere.Radius,
                                     Sphere.Center.X,
                                     Sphere.Center.Y,
                                     Sphere.Center.Z));
                    }
                    break;
            }
        }
    }
#endif
