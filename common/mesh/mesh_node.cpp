/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "mesh_node.h"
#include "collision_node.h"
#include "mesh.h"
#include <log.h>


namespace il2ge::mesh
{


MeshNode::MeshNode(String name,
                   std::shared_ptr<const Mesh> mesh,
                   const std::vector<int>& material_mapping) :
  Node(name, NodeType::Mesh),
  m_mesh(mesh),
  m_material_mapping(material_mapping)
{
}


const std::vector<int>& MeshNode::getMaterialMapping()
{
    assert(m_mesh);
    assert(!m_material_mapping.empty());
    assert(m_material_mapping.size() == m_mesh->getMaterialsCount());
    return m_material_mapping;
}


void MeshNode::materialsChanged(const MaterialList& materials)
{
    assert(m_mesh);
    MeshInstanceBase::materialsChanged(*m_mesh, materials, m_material_mapping);
}


} // namespace il2ge::mesh


#if 0

        MeshNode(String name)
        {
            Name = "Mesh_" + name;
            OriginalName = name;
            mesh = new Mesh();
            mesh.parent = this;
            originalHidden = false;
        }

        MeshNode(MeshNode mn)
        {
            filename = mn.filename;
            OriginalName = mn.OriginalName;
            mesh = mn.mesh;
            Angle = mn.Angle;
            mesh.parent = this;
            Name = "pasted_" + mn.Name;
            Hidden = mn.Hidden;
            Seperable = mn.Seperable;
            world = Matrix.Identity * mn.world;

            Type = mn.Type;
            foreach (Node n in mn.children)
            {
                children.Add(CopyNode(n));
            }
            foreach (CollisionNode b in mn.Colliders)
            {
                Colliders.Add(b);
            }
        }

        MeshNode(String name, String dir)
        {
            Name = "Mesh_" + name;
            OriginalName = name;
            filename = dir + "/" + name + ".msh";

            mesh = new Mesh(filename);
            mesh.parent = this;
            if (name.Contains("CAP"))
                Hidden = true;
            if (name.EndsWith("_dmg"))
                Hidden = true;
            if (name.Contains("Damage"))
                Hidden = true;
            if (name.Length > 3)
            {
                if (name.ElementAt(name.Length - 2) == 'D')
                {
                    if (!Name.EndsWith("0"))
                        Hidden = true;
                }
            }
            originalHidden = Hidden;
        }
//         #endregion

//         #region Draw methods
        void Draw(BasicEffect effect, float distance, Matrix World)
        {
            Matrix mv = world * World;
            effect.World = mv;

            if (!Hidden)
            {
                if (Name.Contains("PropRot"))
                {
                    mv = Matrix.CreateRotationZ(Angle) * mv;
                }
                mesh.Draw(effect, distance, mv);
            }

            foreach (Node n in children)
            {
                if (n is MeshNode)
                {
                    MeshNode mn = (MeshNode)n;
                    mn.Draw(effect, distance, mv);
                }
            }
        }

        void Draw(Effect effect, float distance, Matrix World, bool sort)
        {
            

            String test = Name.ToLower(); 
            Matrix mv = world * World;
           
            if (Form1.Animate)
            {
                Angle += 0.42f;
            }
            
            if (test.Contains("proprot"))
            {
                mv = Matrix.CreateRotationY(Angle) * mv;
                if (!Form1.Animate)
                    Hidden = true;
                else
                    Hidden = false;
            }
            else if (test.Contains("prop"))
            {
                if (Form1.Animate)
                    return;
            }
            
            if (test.Contains("rudder"))
            {
                mv = Matrix.CreateRotationY(Form1.Yaw) * mv;
            }
            
            if (!Hidden)
                mesh.Draw(effect, distance, mv, sort);
            
            foreach (Node n in children)
            {
                if (n is MeshNode)
                {
                    MeshNode mn = (MeshNode)n;
                    mn.Draw(effect, distance, mv, sort);
                }
            }

            if (ObjectViewer.HookDisplay)
            {
                foreach (Hook h in mesh.Hooks)
                {
                    h.Draw(mv, ObjectViewer.mView, ObjectViewer.mProjection);
                }
            }
            previous_matrix = mv;
        }

        void DrawGlass(Effect effect, float distance, Matrix World, Matrix vp)
        {
            Matrix mv = world * World;

            if (!Hidden)
                mesh.DrawGlass(effect, distance, mv, vp);

            foreach (Node n in children)
            {
                if (n is MeshNode)
                {
                    MeshNode mn = (MeshNode)n;
                    mn.Draw(effect, distance, mv, false);
                }
            }
        }

        void DrawShadow(Effect effect, float distance, Matrix World)
        {
            Matrix mv = world * World;

            if (!Hidden)
                mesh.DrawShadow(effect, distance, mv);

            foreach (Node n in children)
            {
                if (n is MeshNode)
                {
                    MeshNode mn = (MeshNode)n;
                    mn.DrawShadow(effect, distance, mv);
                }
            }
        }

        void DrawCollisionMesh(BasicEffect be, Matrix World)
        {
            Matrix mv = world * World;
            if (!Hidden)
            {
                mesh.DrawCollisionMesh(be, mv);
            }
            foreach (Node n in children)
            {
                if (n is MeshNode)
                {
                    MeshNode mn = (MeshNode)n;
                    mn.DrawCollisionMesh(be, mv);
                }
            }
        }

        void DrawSkin(Graphics g, Pen p, Brush b, String texture, float size)
        {
            mesh.DrawSkin(g, p, b, texture, size);
            foreach (Node n in children)
            {
                if (n is MeshNode)
                {
                    MeshNode mn = (MeshNode)n;
                    mn.DrawSkin(g, p, b, texture, size);
                }
            }
        }

        void DrawSkinPart(Graphics g, Pen p, Brush b, String texture, float size)
        {
            mesh.DrawSkinPart(g, p, b, texture, size);
            
        }

        void DrawAo(BasicEffect effect, int size, ref List<float> ao_values)
        {
            if (!Hidden)
            {
                mesh.DrawAo(effect, size, ref ao_values);
            }
        }

        void DrawAo(BasicEffect effect, int size, String texture)
        {
            if (!Hidden)
            {
                mesh.DrawAo(effect,size,texture);
            }
        }

        void DrawNormals(BasicEffect be, Matrix world)
        {
            if (!Hidden)
            {
                Matrix mv = this.world * world;
                be.World = mv;
                mesh.DrawNormals(be);
            }
        }

        void DrawBumped(Effect effect, float distance, Matrix World, bool sort)
        {
            String test = Name.ToLower();

            Matrix mv = world * World;

            if (Form1.Animate)
            {
                Angle += 0.42f;
            }

            if (test.Contains("proprot"))
            {
                mv = Matrix.CreateRotationY(Angle) * mv;
                if (!Form1.Animate)
                    return;
            }
            else if (test.Contains("prop"))
            {
                if (Form1.Animate)
                    return;
            }

            if (!Hidden)
                mesh.DrawBumped(effect, distance, mv, sort);

            foreach (Node n in children)
            {
                if (n is MeshNode)
                {
                    MeshNode mn = (MeshNode)n;
                    mn.DrawBumped(effect, distance, mv, sort);
                }
            }
        }
//         #endregion

//         #region Search methods
        void FindHook(String name, ref List<Vector3> hooks, Matrix pos)
        {         
            mesh.FindHook(name, ref hooks, world * pos);
        }
        void FindHook(String name, ref List<Vector3> hooks,ref List<Vector3>directions, Matrix pos)
        {
            mesh.FindHook(name, ref hooks, ref directions, world * pos);
        }
        public Hook FindHook(String name)
        {
            return mesh.FindHook(name);
        }
        void FindHook(String name, String ignore, ref List<Vector3> hooks, Matrix pos)
        {
            mesh.FindHook(name, ignore, ref hooks, world * pos);
        }
        public MeshNode Inside(float x, float y, String texture, float size)
        {
            if (mesh.Inside(x, y, texture, size))
            {
                return this;
            }
            foreach (Node n in children)
            {
                if (n is MeshNode)
                {
                    MeshNode mn = (MeshNode)n;
                    if (mn.Inside(x, y, texture, size) != null)
                    {
                        return mn;
                    }
                }
            }
            return null;
        }

        void BuildAoList(Matrix world, ref List<Vector3> verts, ref List<Vector3> normals)
        {
            mesh.BuildAoList(world, ref verts, ref normals);
        }
        void BuildAoListVertexCamera(String texture, Matrix world, ObjectViewer viewer)
        {
            mesh.BuildAoListVertexCamera(texture, world, viewer);
        }
        void BuildAoListMultiVertexCamera(String texture, Matrix world, ObjectViewer viewer)
        {
            mesh.BakeAmbientMultiVertex(texture, world, viewer);
        }
        void BuildAoListVertexRay(String texture, Matrix world, ObjectViewer viewer, int count)
        {
            mesh.BakeAmbientVertexRay(texture, world, viewer,count);
        }
        public bool Blocked(Ray r, Matrix world)
        {
            return false;
        }

        public Hook RaytraceHook(Ray r)
        {
            foreach (Hook h in mesh.Hooks)
            {
                if (h.Hit(r))
                {
                    return h;
                }
            }
            return null;
        }
//         #endregion

//         #region Serializers
        void Serialize(TextWriter tw)
        {
            mesh.SaveHeader(tw);           
            mesh.SaveLodTable(tw);
            mesh.SaveHooks(tw);
            mesh.SaveMaterials(tw);
            mesh.SaveFaceGroups(tw);
            mesh.SaveFrame0(tw);
            mesh.SaveUVs(tw);
            mesh.SaveFaces(tw);
            mesh.SaveShadows(tw);
            mesh.SaveLods(tw);
            mesh.SaveCollisionMesh(tw);
        }

        void SaveMaterials(String dir)
        {
            mesh.SaveMaterials(dir);
        }

        
//         #endregion

//         #region Tests
        void Shoot(int x, int y, Matrix projection, Matrix view, Matrix World)
        {
            Matrix mv = world * World;
            if (mesh != null)
            {
                if (mesh.Shoot(x, y, projection, view, mv))
                {
                    Damage++;
                    if (Damage > 10)
                    {
                        String target = Name.TrimEnd('0');
                        target += "1";
                        MeshNode mn = ObjectViewer.Instance.FindMeshNode(target);
                        if (mn != null)
                        {
                            mn.Hidden = false;
                        } 
                    }
                    if (Damage > 30)
                    {
                        String target = Name.TrimEnd('0');
                        target += "2";
                        MeshNode mn = ObjectViewer.Instance.FindMeshNode(target);
                        if (mn != null)
                        {
                            mn.Hidden = false;
                        }
                    }
                    if (Damage > 40)
                    {
                        String target = Name.TrimEnd('0');
                        target += "3";
                        MeshNode mn = ObjectViewer.Instance.FindMeshNode(target);
                        if (mn != null)
                        {
                            mn.Hidden = false;
                        }
                    }
                    return;
                }
                foreach (Node n in children)
                {
                    if (n is MeshNode)
                    {
                        MeshNode mn = (MeshNode)n;
                        mn.Shoot(x, y, projection, view, mv);
                    }
                }
            }
        }
        bool QuickCheck(Ray r, Matrix world)
        {
            return mesh.QuickCheck(world, r);
        }
//         #endregion

//         #region Modifiers
        void MoveUV(float dx, float dy)
        {
            mesh.MoveUV(dx, dy);
        }
        void FlipNormals()
        {
            FlipNormals fn = new FlipNormals(mesh);
            fn.Show();
        }
        void ResetHidden()
        {
            Hidden = originalHidden;
        }
        void RegenerateNormals()
        {
            mesh.RegenerateNormals();
        }
        void SwapTriangles()
        {
            mesh.SwapTriangles();
        }
        void AdjustLighting()
        {
            mesh.AdjustLighting();
        }
//         #endregion

//         #region Binormals and tangents
        void GenerateBinormalsAndTangents()
        {
            mesh.GenerateBinormalsAndTangents();
        }

        void EnableBumpMapping(int i, int t)
        {
            mesh.EnableBumpMapping(i, t);
        }
//         #endregion


#endif
