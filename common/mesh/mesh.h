﻿/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MESH_MESH_H
#define IL2GE_MESH_MESH_H

#include "node.h"
#include "hook.h"
#include <il2ge/mesh/face_group.h>
#include <il2ge/mesh/mesh_base.h>
#include <il2ge/renderer.h>

#include <cstdint>
#include <fstream>
#include <iostream>


#define IL2GE_MESH_ALLOW_UNEXPECTED_EOF 1


namespace il2ge::mesh
{
    class BinaryReader;

    using Int32 = int32_t;
    using Int16 = int16_t;


#if 0
    struct BumpVertex : public IVertexType
    {
        Vector3 vertexPosition;
        Vector2 vertexTextureCoordinate;
        Vector3 vertexNormal;
        Vector3 vertexTangent;
        Vector3 vertexBiNormal;

        public readonly static VertexDeclaration VertexDeclaration = new VertexDeclaration
        (
            new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),
            new VertexElement(12, VertexElementFormat.Vector2, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(20, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0),
            new VertexElement(32, VertexElementFormat.Vector3, VertexElementUsage.Tangent, 0),
            new VertexElement(44, VertexElementFormat.Vector3, VertexElementUsage.Binormal, 0)

        );

        public BumpVertex(Vector3 pos, Vector2 textureCoordinate, Vector3 normal, Vector3 tangent, Vector3 binormal)
        {
            vertexPosition = pos;
            vertexTextureCoordinate = textureCoordinate;
            vertexNormal = normal;
            vertexTangent = tangent;
            vertexBiNormal = binormal;
        }

        //Public methods for accessing the components of the custom vertex.
        public Vector3 Position
        {
            get { return vertexPosition; }
            set { vertexPosition = value; }
        }

        public Vector2 TextureCoordinate
        {
            get { return vertexTextureCoordinate; }
            set { vertexTextureCoordinate = value; }
        }

        public Vector3 Normal
        {
            get { return vertexNormal; }
            set { vertexNormal = value; }
        }

        public Vector3 Tangent
        {
            get { return vertexTangent; }
            set { vertexTangent = value; }
        }

        public Vector3 BiNormal
        {
            get { return vertexBiNormal; }
            set { vertexBiNormal = value; }
        }

        VertexDeclaration IVertexType.VertexDeclaration
        {
            get { return VertexDeclaration; }
        }
    };
#endif


    class Materials
    {
        Vector<String> m_names;
        std::unordered_map<String, int> m_index;

    public:
        int getIndex(String name)
        {
            auto it = m_index.find(name);
            if (it != m_index.end())
            {
                return it->second;
            }
            else
            {
                m_names.emplace_back(name);
                auto& index = m_index[name];
                index = m_names.size() - 1;
                return index;
            }
        }

        const Vector<String>& getNames() { return m_names; }
    };


    struct MeshData
    {
        List<FaceGroup> FaceGroups;
        Vector<VertexPositionNormalTexture> Verts;
        Vector<Index> indices;
        Vector<VertexPositionColor> ShadowVerts;
        Vector<short> ShadowIndices;
        size_t frame_count = 1;
        size_t vertex_count = 0;
    };


    class Lod : public LodBase
    {
        friend class Mesh;

        std::unique_ptr<MeshData> m_data;
        std::unique_ptr<il2ge::renderer::MeshRendererData> m_renderer_data;

    public:
        Lod(std::unique_ptr<MeshData> data);

        Lod(const Lod&) = delete;
        Lod(Lod&&) = delete;
        Lod &operator=(const Lod&) = delete;
        Lod &operator=(Lod&&) = delete;

        ~Lod();

        renderer::MeshRendererData* getRendererData() const override
        {
            assert(m_renderer_data);
            return m_renderer_data.get();
        }

        const Vector<VertexPositionNormalTexture>& getVertices() const override
        { return m_data->Verts; }
        const Vector<Index>& getIndices() const override
        { return m_data->indices; }
        const Vector<FaceGroup>& getFaceGroups() const override
        { return m_data->FaceGroups; }
        const size_t getVertexCount() const override
        { return m_data->vertex_count; }
        const size_t getFrameCount() const override
        { return m_data->frame_count; }
    };




#if 1
    class Mesh final : public MeshBase
    {
//         Node &parent;
        bool Binary = false;
        bool Animated = false;
        bool m_enable_lod = true;

#if IL2GE_MESH_ENABLE_ANIMATION
//         #region Animation frame storage
//         public List<List<VertexPositionNormalTexture>> animation_frames = new List<List<VertexPositionNormalTexture>>();
//         public List<List<VertexPositionColor>> animated_shadow_frames = new List<List<VertexPositionColor>>();
//         int AnimationFrame = 0;
//         #endregion
#endif
        int FrameCount = 0;

//         BinaryFile bfile = new BinaryFile();

        List<float> LodDistances;
        List<Hook> Hooks;
        List<std::unique_ptr<Lod>> Lods;

        List<std::string> m_material_names;
        // std::unique_ptr<MaterialList> m_materials;
//         CollisionMesh colmesh = new CollisionMesh();


//         List<VertexPositionColor> ShadowVerts = new List<VertexPositionColor>();
//         List<short> ShadowIndices = new List<short>();
//         List<float> AOValues = new List<float>();
//         Dictionary<int, int> AOLookUp = new Dictionary<int, int>();

//         public BumpVertex[] BumpVerts;
//         public FaceGroup SelectedFaceGroup = null;

//         VertexPositionColor[] ShadowVertsArray;

//         short[] ShadowIndicesArray;
        String mesh_name;
//         FaceGroup selected_facegroup = null;

        // normal cache (?)
//         List<Vector3> cache;

//         Matrix lastWorld;

        static int trim(float x, float size)
        {
            float dx = x - (float)((int)x);
            dx *= size;
            return (int)dx;
        }

        void read(std::istream&, std::string dir);
        void ReadBinary(std::istream&, String dir);
        void ReadTextMode(std::istream&, String dir);
        void ReadTextMode_old(String filename, String dir);

//         bool intpoint_inside_trigon(Vector2 p, Vector2 p0, Vector2 p1, Vector2 p2)
        // Returns the distance from the origin of the ray to the intersection with 
        // the triangle, null if no intersect and negative if behind.
//         float Intersects(ref Ray ray, Vector3 v1, Vector3 v2, Vector3 v3, Matrix world)
        // Returns the distance from the origin of the ray to the intersection with 
        // the triangle, null if no intersect and negative if behind.
//         float Intersects(ref Ray ray, int block, int part, short v1, short v2, short v3, Matrix world)
//         Ray CreateRay(int x, int y, Matrix projection, Matrix view, Matrix world)

//         Vector3 calcNormal(int v1, int v2, int v3);

    public:
        Mesh();
        Mesh(String filename);
        Mesh(String filename, const char *data, size_t data_size);
        ~Mesh();

        bool isAnimated() const override
        {
            return Animated;
        }

        void createRendererData() override;

        void enableLod(bool enable) override
        {
          m_enable_lod = enable;
        }
        size_t getFrameCount() const override
        {
          return Lods.front()->getFrameCount();
        }
        const LodBase& getLod(int lod) const override
        {
            return *Lods.at(std::min(Lods.size()-1, size_t(lod)));
        }
        const LodBase& getBaseLod() const override
        {
          return *Lods.front();
        }
        size_t getLodCount() const override;
        const LodBase& getLodAtDistance(float) const override;
        int findHook(const std::string &name) const override;
        const std::string &getHookName(int index) const override;
        const Mat4 &getHookMatrix(int index, int frame) const override;
        float getMaxVisibleDistance() const override { return LodDistances.back(); }
        const Vector<std::string>& getMaterialNames() const override
        {
            return m_material_names;
        }
        // MaterialList& getMaterials() override;
        // const std::shared_ptr<const Material>& getMaterial(int index) const override;
        // const bool hasMaterial(int index) const override;
        size_t getMaterialsCount() const override;
        // void setMaterials(std::unique_ptr<MaterialList>&& materials) override;

        int getLodNumAtDistance(float) const;

        const List<Hook> &getHooks() const { return Hooks; }

//         #region Search functions
//         void FindHook(String name, ref List<Vector3> hooks, Matrix pos)
//         void FindHook(String name, ref List<Vector3> hooks, ref List<Vector3> directions, Matrix pos)
//         void FindHook(String name, String ignore, ref List<Vector3> hooks, Matrix pos)
//         Hook FindHook(String name)
//         #endregion

//         #region Draw methods
//         void Draw(BasicEffect be, float distance, Matrix world)
//         void Draw(Effect be, float distance, Matrix world, bool sort)
//         void DrawBumped(Effect be, float distance, Matrix world, bool sort)
//         void DrawShadow(Effect be, float distance, Matrix world)
//         void DrawGlass(Effect be, float distance, Matrix world, Matrix vp)
//         void DrawCollisionMesh(BasicEffect be, Matrix world)
//         void DrawSkin(Graphics g, Pen p, Brush b, String texture, float size)
//         void DrawSkinPart(Graphics g, Pen p, Brush b, String texture, float size)
//         void DrawAo(BasicEffect effect, int size, ref List<float> ao_values)
//         void DrawAo(BasicEffect effect, int size, String texture)
//         void DrawNormals(BasicEffect effect)
//         #endregion

//         #region Serializers
//         void SaveShadows(TextWriter tw)
//         void SaveLodTable(TextWriter tw)
//         void SaveMaterials(TextWriter tw)
//         void SaveHeader(TextWriter tw)
//         void SaveHooks(TextWriter tw)
//         void SaveFaceGroups(TextWriter tw)
//         void SaveFrame0(TextWriter tw)
//         void SaveUVs(TextWriter tw)
//         void SaveFaces(TextWriter tw)
//         void SaveLods(TextWriter tw)
//         void SaveMaterials(String dir)
//         void SaveCollisionMesh(TextWriter tw)
//         void SaveDAEEffects(TextWriter tw, bool effect)
//         void SaveDAEMesh(TextWriter tw)
//         void SaveOGRE(String dir)
//         void SaveAsOBJ(String dir, String matname)
//         void SaveToFox1(String dir, Node n, TextWriter g)
//         #endregion

//         #region Tests
//         bool Shoot(int x, int y, Matrix projection, Matrix view, Matrix world)
//         bool QuickCheck(Matrix world, Ray r)
//         bool Inside(float x, float y, String texture, float size)
//         void BuildAoList(Matrix world, ref List<Vector3> verts, ref List<Vector3> normals)
//         void BuildAoListVertexCamera(String texture, Matrix world, ObjectViewer viewer)
//         void BakeAmbientMultiVertex(String texture, Matrix world, ObjectViewer viewer)
//         void BakeAmbientVertexRay(String texture, Matrix world, ObjectViewer viewer, int count)
//         #endregion

//         #region 3DS support
//         public void Initialise(ThreeDSFile.Entity e, String dir);
//         #endregion


//         #region Modifiers
//         void CacheNormals();
//         void RecoverNormals();
//         void FlipNormals(int axis);
//         void FlipNormals(int axis, int facegroup);
//         void RecalculateNormals();
//         void MoveUV(float dx, float dy);
//         void RegenerateNormals();
//         void SwapTriangles();
//         bool ClockWise(int v1, int v2, int v3);
//         void Rotate90Z();
//         void AdjustLighting();
//         #endregion

//         #region Binormals and tangents
//         void GenerateBinormalsAndTangents()
//         void EnableBumpMapping(int i, int t);
//         #endregion

//         #region FBX
//         FbxVector2 getFBXUV(int i);
//         FbxNode SaveFBX(String dir, Matrix world, Node dad, Node mom);
// 
//         /// <summary> 
//         /// The function converts a Microsoft.Xna.Framework.Quaternion into a Microsoft.Xna.Framework.Vector3 
//         /// </summary> 
//         /// <param name="q">The Quaternion to convert</param> 
//         /// <returns>An equivalent Vector3</returns> 
//         /// <remarks> 
//         /// This function was extrapolated by reading the work of Martin John Baker. All credit for this function goes to Martin John. 
//         /// http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/index.htm 
//         /// </remarks> 
//         static Vector3 QuaternionToEuler(Quaternion q);
// 
//         static Vector3 FromQ2(Quaternion q1);
//         static Vector3 NormalizeAngles(Vector3 angles);
// 
//         static float NormalizeAngle(float angle);
//         #endregion
    };
#endif


}




#endif
