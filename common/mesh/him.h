﻿/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MESH_HIM_H
#define IL2GE_MESH_HIM_H

// using IL2Modder.AC3D;
// using IL2Modder.Fox1Animators;

#include "node.h"
#include <il2ge/mesh/mesh_base.h>

#include <string>
#include <vector>
#include <unordered_map>


namespace il2ge
{
    class MaterialList;
}


namespace il2ge::mesh
{
    class RootNode;
    class MeshNode;
    class Mesh;
    class Hook;
    class Ray;


    class HIM : public HierMeshBase
    {
        std::unique_ptr<RootNode> root;
//         List<AircraftActions> actions;
//         List<TurretLimits> turret_limits;
//         Random rand = new Random();
//         List<Animator> animators;
//         String Name;
        int nProps = 0;
        int nRudders = 0;
        int nFlaps = 0;

        std::unordered_map<std::string, size_t> m_node_map;
        std::vector<Node*> m_nodes;
        std::unordered_map<std::string, size_t> m_hook_map;
        std::vector<const Hook*> m_hooks;
        std::unordered_map<size_t, size_t> m_hook_node_map;

        std::unordered_map<Node*, size_t> m_node_index_map;

        std::unique_ptr<MaterialList> m_materials;
        std::vector<std::string> m_material_names;
#if USE_MESH_INSTANCE_RENDER_LIST_CACHE
        std::unique_ptr<renderer::MeshInstanceRenderListCache> m_render_list_cache;
#endif
//         void FindHook(Node n, string name, ref List<Vector3> hooks,
//                       ref List<Vector3>directions, Matrix pos);
//         void FindHook(Node n, string name, ref List<Vector3> hooks, Matrix pos);
//         void FindHook(String name, String ignore, ref List<Vector3> hooks);
//         void FindHook(Node n, string name, String ignore, ref List<Vector3> hooks, Matrix pos);
//         MeshNode Inside(Node n, float x, float y, String texture, float size);

//         void Draw(Node n, BasicEffect effect, float distance, Matrix world);
//         void Draw(Node n, Effect effect, float distance, Matrix world, bool sort);
//         void DrawGlass(Node n, Effect effect, float distance, Matrix world, Matrix vp);
//         void DrawCollisionMesh(Node n, BasicEffect be, Matrix World);
//         void DrawShadow(Node n, Effect effect, float distance, Matrix world);
//         void DrawAo(Node n2, BasicEffect effect, int size, ref List<float> ao_values);

//         String filter(String s);

//         bool CheckCollide(Node n2, Ray r, Matrix world);

//         void Serialise(Node nr, TextWriter writer,String dir);
//         void SerialiseH(Node nr, TextWriter writer, String dir);
//         void SaveDAEEffects(Node n2, TextWriter tw, bool effect);
//         void SaveDAEStructure(TextWriter tw, Node n, Matrix m);
//         void SaveOGRE(Node n2, String dir);
//         void SaveToFox1(Node n2, String dir, TextWriter g);

//         void ResetHidden(Node n);
//         void RegenerateNormals(Node n2);

        /// <summary>
        /// Load in a 3D mesh in AC3D AC format
        /// </summary>
        /// <param name="reader">File</param>
        /// <param name="parent">Tree node of parent</param>
        /// <param name="Count">Number of objects to load</param>
        /// <param name="materials">Materials defined in header</param>
        /// <param name="dir">Holding directory</param>
//         void LoadACObject(TextReader reader, Node parent, int Count, List<Material>materials,
//                           String dir);

//         void GenerateBinormalsAndTangents(Node n);

        void load(String filename, GetMeshFunc);
        void load(String filename, GetMeshFunc, const char *data, size_t data_size);
        void draw(Node *node, const Mat4 &model_view, float distance, renderer::Renderer&);

        void drawMeshNode(MeshNode *node, const Mat4 &model_view, float distance,
                          renderer::Renderer&);

        void getSubTree(Node &node, bool hide_nodes, std::vector<long int> &indices);
        void materialsChanged();

    public:
        HIM(String filename);
        HIM(String filename, GetMeshFunc);
        HIM(String filename, GetMeshFunc, const char *data, size_t data_size);

        void draw(const Mat4 &model_view, float distance, renderer::Renderer&) override;
        const std::vector<std::string>& getMaterialNames() const override;
        void setMaterials(std::unique_ptr<MaterialList>&&) override;
        int findMaterial(const std::string &name) override;
        const std::shared_ptr<Material>& getMaterial(int index) override;
        void replaceMaterial(const std::string& name,
                             const std::shared_ptr<il2ge::Material>&) override;
        int getNodeCount() const override { return m_nodes.size(); }
        int findNode(const std::string &name) override;
        void setNodeVisible(int index, bool visible) override;
        bool isNodeVisible(int index) override;
        void setNodeTransform(int index, const Vec3 &pos, const Mat4 &rotation) override;
        void setNodeRotation(int index, const Mat4&) override;
        Mat4 getNodeTransform(int index) override;
        const std::string &getNodeName(int index) override;
        std::unique_ptr<il2ge::mesh::MeshInstance> getNodeMesh(int index) override;
        int findHook(const std::string &name) override;
        const std::string &getHookName(int index) override;
        int getHookNode(int index) override;
        const Mat4 getHookMatrix(int index) override;
        void getSubTree(int node_index, bool hide_nodes,
                        std::vector<long int> &indices) override;

//         #region Search methods
        const Node &FindNode(const String &name) const;
        const Hook &FindHook(const String &name) const;
        const Hook &FindHook(const Node &n, const String &name) const;
        const Node &FindNode(const Node &n, const String &name) const;
        void FindHook(const String &name, List<Vector3> &hooks) const;
        void FindHook(const String &name, List<Vector3> &hooks, List<Vector3> &directions) const;
        void FindHook(const String &name, List<Vector3> &hooks, List<Vector3> &directions,
                      const Node &host) const;
        void FindHook(const Node &n, const String &name, List<Vector3> &hooks,
                      List<Vector3> &directions, const Matrix &pos, const Node &host) const;
        const Hook &RaytraceHook(const Ray &ray) const;
        const Hook &RaytraceHook(const Node &n2, const Ray &ray) const;
        const MeshNode &Inside(float x, float y, String texture, float size) const;
        Vector3 GetLocation(const String &name) const;
        Vector3 GetLocation(const Node &n2, const String &name, const Matrix &world) const;
//         #endregion


//         #region Draw methods
//         void DrawSkin(Graphics g, Pen p, Brush b, String texture, float size);
//         void DrawSkin(Node n, Graphics g, Pen p, Brush b, String texture, float size);
//         void Draw(BasicEffect effect, float distance);
//         void Draw(Effect effect, float distance, bool sort);
//         void DrawBumped(float distance, Matrix vp, bool sort);
//         void DrawBumped(Node n, float distance, Matrix vp, bool sort);
//         void DrawGlass(Effect effect, float distance, Matrix vp);
//         void DrawShadow(Effect effect, float distance);
//         void DrawCollisionMesh(BasicEffect be);
//         void DrawAo(BasicEffect effect, int size, ref List<float> ao_values);
//         void DrawAO(BasicEffect effect, int size, String texture);
//         void DrawAO(Node n2, BasicEffect effect,int size, String texture);
//         void DrawNormals(BasicEffect effect);
//         void DrawNormals(Node n2, BasicEffect effect, Matrix world);
//         #endregion

//         Matrix AdjustMatrix(Matrix adj, String test);

//         #region Tests
//         void Shoot(int x, int y, Matrix projection, Matrix view);
//         void Shoot(Node n, int x, int y, Matrix projection, Matrix view, Matrix world);
//         bool Blocked(Ray r);
//         bool Blocked(Node n2, Ray r, Matrix world);
//         #endregion

//         #region Helpers
//         void AddRoot(Node n);
//         void ResetDamage();
//         void ResetDamage(Node nroot);
//         #endregion

//         #region Ambient Occlusion
//         void BuildAoList(ref List<Vector3> verts, ref List<Vector3> normals);
//         void BuildAoList(Node n2, Matrix world, ref List<Vector3> verts, ref List<Vector3> normals);
//         void BuildAoListVertexCamera(String texture, ObjectViewer viewer);
//         void BuildAoListVertexCamera(Node n2, Matrix world, String texture, ObjectViewer viewer);
//         void BuildAoListMultiVertexCamera(String texture, ObjectViewer viewer);
//         void BuildAoListMultiVertexCamera(Node n2, Matrix world, String texture, ObjectViewer viewer);
//         void BuildAoListiVertexRay(String texture, ObjectViewer viewer, int count);
//         void BuildAoListVertexRay(Node n2, Matrix world, String texture, ObjectViewer viewer,
//                                   int count);
//         float GetAORay(Vector3 pos, int count)
//         bool CheckCollide(Ray r);
//         #endregion

//         #region Serialisers
//         void Serialise(String name);
//         void SerialiseHim(String name);
//         void SaveDAEEffects(TextWriter writer, bool effect);
//         void SaveDAEMesh(TextWriter tw);
//         void SaveDAEMesh(Node n2, TextWriter tw);
//         void SaveDAEStruture(TextWriter tw);
//         void SaveOGRE(String dir);
//         void SaveAsObj(String dir);
//         void SaveAsObj(Node n2, String dir);
//         void SaveAsFBX(String dir);
//         void SaveAsFBX(Node n2, String dir, Matrix world);
//         void SaveToFox1(String dir, Fox1ExportDialog options);
//         #endregion

//         #region Modifiers
//         void ResetHidden();
//         void RegenerateNormals();
//         void SwapTriangles();
//         void SwapTriangles(Node n2);
//         void AdjustLighting();
//         void AdjustLighting(Node n2);
//         #endregion

//         #region Loaders
//         void LoadAC(String name, String dir);
//         #endregion

//         #region Binormals and tangents
//         void GenerateBinormalsAndTangents();
//         void EnableBumpMapping(int i, int t);
//         void EnableBumpMapping(Node n, int i, int t);
//         #endregion
    };

}

#endif
