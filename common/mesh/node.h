﻿/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MESH_NODE_H
#define IL2GE_MESH_NODE_H

#include <il2ge/mesh/mesh_base.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <string>
#include <vector>
#include <memory>


namespace il2ge::mesh
{

  using Matrix = Mat4;
  using Vector3 = glm::vec3;
  using String = std::string;

  template <typename T>
  using List = Vector<T>;


  class CollisionNode;


  enum class NodeType
  {
    Root,
    Holder,
    Mesh,
    CollisionObject,
    NodeTypes
  };


  class Node
  {
    Matrix base_transform = Matrix(1.0);
    Matrix rotation = Matrix(1);
    Vec3 pos = Vec3(0);
    Matrix transform = Matrix(1.0);
    bool tranform_needs_refresh = true;
    List<std::unique_ptr<Node>> children;

    void invalidateTransform();
    void doRefreshTransform();

    void refreshTransform()
    {
      if (tranform_needs_refresh)
        doRefreshTransform();
    }

  public:
    List<std::unique_ptr<CollisionNode>> Colliders;
    bool Hidden = false;
    float max_visible_distance = 0;

    Node(String name, NodeType);
    virtual ~Node();

    const std::string &getName() { return Name; }
    NodeType getType() { return Type; }

    Node &addChild(std::unique_ptr<Node>);

    void setTransform(const Vec3 &pos, const Mat4 &rotation);
    void setRotation(const Mat4&);
    void setBaseTransform(const Mat4&);

    Matrix getCommandedTransform()
    {
      return glm::translate(Mat4(1.0), pos) * rotation;
    }

    const Matrix &getTransform()
    {
      refreshTransform();
      return transform;
    }

    const List<std::unique_ptr<Node>> &getChildren() { return children; }

    static std::unique_ptr<Node> CopyNode(const Node &copy);

  private:
    Node *parent = nullptr;
    String Name;
    String Parent;
    NodeType Type = NodeType::Root;
    bool Seperable = false;
    int Damage = 0;
  };

}

#endif
