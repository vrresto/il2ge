/**
 *    IL-2 Graphics Extender
 *
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "him.h"
#include "root_node.h"
#include "mesh_node.h"
#include "mesh.h"
#include "collision_node.h"
#include "loader_util.h"
#include <il2ge/parameter_file.h>
#include <il2ge/material.h>
#include <il2ge/renderer.h>
#include <util.h>

using namespace il2ge::mesh;
using namespace il2ge::mesh::loader_util;


namespace
{


class Loader
{
  const il2ge::ParameterFile &m_params;
  std::string m_dir;
  GetMeshFunc m_get_mesh;
  Materials m_materials;

  std::unordered_map<std::string, size_t> &m_node_map;
  std::vector<Node*> &m_nodes;
  std::unordered_map<std::string, size_t> &m_hook_map;
  std::vector<const Hook*> &m_hooks;
  std::unordered_map<size_t, size_t> &m_hook_node_map;
  std::unordered_map<std::string, Node*> m_node_map_lowercase;

public:
  Loader(const il2ge::ParameterFile &params,
         std::string dir, GetMeshFunc get_mesh,
         std::unordered_map<std::string, size_t> &node_map,
         std::vector<Node*> &nodes,
         std::unordered_map<std::string, size_t> &hook_map,
         std::vector<const Hook*> &hooks,
         std::unordered_map<size_t, size_t> &hook_node_map) :
    m_params(params),
    m_dir(dir),
    m_get_mesh(get_mesh),
    m_node_map(node_map),
    m_nodes(nodes),
    m_hook_map(hook_map),
    m_hooks(hooks),
    m_hook_node_map(hook_node_map)
  {
  }


  void loadCollisionObject(Node &node)
  {
    auto &section = m_params.getSection(node.getName());

  //   try
  //   {
  //     for (auto &param : section.getParameters("CollisionObject"))
  //     {
  //       //FIXME
  // //       CollisionNode cn = new CollisionNode(param.values);
  // //       node.Colliders.push_back(cn);
  //     }
  // 
  //   }
  //   catch (...)
  //   {
  //     LOG_ERROR << "Node " << node.getName() << " has no parameter 'CollisionObject'." << std::endl;
  //     throw;
  //   }
  }

  const std::vector<std::string>& getMaterialNames()
  {
    return m_materials.getNames();
  }

  void loadTransform(Node &node)
  {
    auto &section = m_params.getSection(node.getName());

    std::vector<std::string> values;
    try
    {
      section.get("Attaching", values);
    }
    catch (...)
    {
      LOG_ERROR << "Node " << node.getName() << " has no parameter 'Attaching'." << std::endl;
      throw;
    }

    assert(values.size() >= 12);

    node.setBaseTransform(loadTransformationMatrix(values));
  }


#if 0
  void loadChildren(Node &node)
  {
    int num_children = 0;

    for (auto &it : m_params.getSections())
    {
      auto &name = it.first;
      auto &section = it.second;

      if (name == "_root_" || name == util::makeLowercase(node.getName()))
        continue;

      std::string parent;
      try
      {
        section.get("Parent", parent);
      }
      catch (...)
      {
        LOG_ERROR << "Node " << name << " has no parent" << std::endl;
        throw;
      }

      if (util::makeLowercase(parent) == util::makeLowercase(node.getName()))
      {
        node.addChild(loadNode(name));
        num_children++;
      }
    }

    LOG_DEBUG << "Node " << node.getName() << " has " << num_children << " children." << std::endl;
  }
#endif



  void calcMaxVisibleDistance(Node &node)
  {
    for (auto &child : node.getChildren())
    {
      calcMaxVisibleDistance(*child);
      node.max_visible_distance =
        std::max(node.max_visible_distance, child->max_visible_distance);
    }
  }


#if 0
  std::unique_ptr<Node> loadNode(std::string name)
  {
    auto &section = m_params.getSection(name);

    std::unique_ptr<MeshNode> node;

    std::string mesh_name;
    section.get("Mesh", mesh_name);

    auto filename = m_dir + "/" + mesh_name + ".msh";

    std::shared_ptr<const Mesh> mesh;

    try
    {
      mesh = std::dynamic_pointer_cast<const Mesh>(m_get_mesh(filename));
      assert(mesh);
    }
    catch (std::exception &e)
    {
      LOG_ERROR << "Node: " << name << " - Failed to load mesh: " << filename << std::endl;
      LOG_ERROR << "Exception was: " << e.what() << std::endl;
    }

    if (!mesh)
      LOG_ERROR << "Node: " << name << " - Failed to load mesh: " << filename << std::endl;

    node = std::make_unique<MeshNode>(name, mesh);

    if (section.hasKey("Hidden"))
      node->Hidden = true;

    loadTransform(*node);
    loadChildren(*node);

    node->max_visible_distance = mesh ? mesh->getMaxVisibleDistance() : 0;
    calcMaxVisibleDistance(*node);

    m_nodes.push_back(node.get());
    m_node_map[name] = m_nodes.size()-1;

    if (mesh)
    {
      for (auto &hook : mesh->getHooks())
      {
        LOG_INFO << "Hook: " << hook.getName() << std::endl;
        m_hooks.push_back(&hook);
        m_hook_map[hook.getName()] = m_hooks.size()-1;
        m_hook_node_map[m_hooks.size()-1] = m_node_map[name];
      }
    }

    return node;
  }
#endif


  Node *getOrCreateNode(const std::string &name)
  {
    auto &node = m_node_map_lowercase[util::makeLowercase(name)];
    if (!node)
    {
      auto &section = m_params.getSection(name);

      LOG_TRACE << "name: " << name << std::endl;

      bool is_mask = util::isPrefix("z_z_mask", name);
      bool is_reticle = util::isPrefix("z_z_reticle", name);

      std::string mesh_name;
      section.get("Mesh", mesh_name);

      auto filename = m_dir + "/" + mesh_name + ".msh";

      std::shared_ptr<const Mesh> mesh;
      try
      {
        mesh = std::dynamic_pointer_cast<const Mesh>(m_get_mesh(filename));
        assert(mesh);
      }
      catch (std::exception &e)
      {
        LOG_ERROR << "Node: " << name << " - Failed to load mesh: " << filename << std::endl;
        LOG_ERROR << "Exception was: " << e.what() << std::endl;
      }
      if (!mesh)
      {
        LOG_ERROR << "Node: " << name << " - Failed to load mesh: " << filename << std::endl;
      }

      std::vector<int> material_mapping;

      if (mesh)
      {
        material_mapping.reserve(mesh->getMaterialsCount());

        for (auto& name : mesh->getMaterialNames())
        {
          auto index = m_materials.getIndex(name);
          material_mapping.push_back(index);
        }
      }

      auto mesh_node = std::make_unique<MeshNode>(name, mesh, material_mapping);

      if (is_mask)
        mesh_node->m_render_group = il2ge::renderer::RenderGroupID::RETICLE_MASK;
      else if (is_reticle)
        mesh_node->m_render_group = il2ge::renderer::RenderGroupID::RETICLE;

      mesh_node->max_visible_distance = mesh ? mesh->getMaxVisibleDistance() : 0;

      if (section.hasKey("Hidden"))
        mesh_node->Hidden = true;

      loadTransform(*mesh_node);

      std::string parent_name;
      try
      {
        section.get("Parent", parent_name);
        parent_name = util::makeLowercase(parent_name);
      }
      catch (...)
      {
        LOG_ERROR << "Node " << name << " has no parent" << std::endl;
        throw;
      }

      auto parent = getOrCreateNode(parent_name);
      assert(parent);

      node = &parent->addChild(std::move(mesh_node));

      m_nodes.push_back(node);
      m_node_map[name] = m_nodes.size()-1;

      if (mesh)
      {
        for (auto &hook : mesh->getHooks())
        {
          LOG_DEBUG << "Hook: " << hook.getName() << std::endl;
          m_hooks.push_back(&hook);
          m_hook_map[util::makeLowercase(hook.getName())] = m_hooks.size()-1;
          m_hook_node_map[m_hooks.size()-1] = m_node_map[name];
        }
      }
    }
    return node;
  }


  std::unique_ptr<RootNode> loadHIM()
  {
    auto &root_section = m_params.getSection("_ROOT_");
    float visibility_sphere = 0;
    root_section.get("VisibilitySphere", visibility_sphere);

    auto root = std::make_unique<RootNode>(visibility_sphere);

    m_node_map_lowercase["_root_"] = root.get();

    for (auto &it : m_params.getSections())
    {
      auto name = it.first;

      getOrCreateNode(name);
    }

    root->max_visible_distance = 0;
    calcMaxVisibleDistance(*root);

//     abort();
//     auto &root_section = m_params.getSection("_ROOT_");
//     float visibility_sphere = 0;
//     root_section.get("VisibilitySphere", visibility_sphere);

//     auto root = std::make_unique<RootNode>(visibility_sphere);

//     loadChildren(*root);

//     root->max_visible_distance = 0;
//     calcMaxVisibleDistance(*root);

    return root;
  }

};




#if 0
    if (parts[0] == "Mesh")
    {
        MeshNode mn = new MeshNode(parts[1], m_dir);
        node.children.Add(mn);
        used = true;
        if ((parts[1].StartsWith("Prop"))&&(!parts[1].Contains("Rot")) &&(parts[1].EndsWith("D0")))
        {
            nProps++;
        }
        if ((parts[1].StartsWith("Rudder")) && (parts[1].EndsWith("D0")))
        {
            nRudders++;
        }
        if ((parts[1].StartsWith("Flap")) && (parts[1].EndsWith("D0")))
        {
            nFlaps++;
        }
    }
    if (parts[0] == "Hidden", StringComparison.OrdinalIgnoreCase)
    {
        node.Hidden = true;
        node.originalHidden = true;
        if (node.children.Count == 1)
        {
            if (node.children[0] is MeshNode)
            {
                node.children[0].Hidden = true;

            }
        }
        used = true;
    }
    if (parts[0] == "invisible", StringComparison.OrdinalIgnoreCase)
    {
        node.Hidden = true;
        node.originalHidden = true;
        used = true;
    }
    if (parts[0] == "Separable", StringComparison.OrdinalIgnoreCase)
    {
        node.Seperable = true;
        used = true;
    }
#endif


} // namepace


namespace il2ge::mesh
{


void HIM::load(String filename, GetMeshFunc get_mesh, const char *data, size_t data_size)
{
  auto dir = util::getDirFromPath(filename);
  il2ge::ParameterFile params(data, data_size, true);

  try
  {
    Loader loader(params, dir, get_mesh, m_node_map, m_nodes, m_hook_map, m_hooks, m_hook_node_map);
    root = loader.loadHIM();
    m_material_names = loader.getMaterialNames();
    for (size_t i = 0; i < m_nodes.size(); i++)
    {
      m_node_index_map[m_nodes.at(i)] = i;
    }
  }
  catch (...)
  {
    LOG_ERROR << "loadHIM() threw exception." << std::endl;
    throw;
  }

//             for (int i = 0; i < 8; i++)
//             {
//                 turret_limits.Add(new TurretLimits());
//             }
}


void HIM::load(String filename, GetMeshFunc get_mesh)
{
  auto content = util::readFile<char>(filename);
  load(filename, get_mesh, content.data(), content.size());
}


HIM::HIM(String filename)
{
  GetMeshFunc get_mesh = [] (std::string path)
  {
    try
    {
      return std::make_shared<Mesh>(path);
    }
    catch(...)
    {
      LOG_ERROR << "Failed to load mesh: " << path << std::endl;
      throw;
    }
  };

  load(filename, get_mesh);
}


HIM::HIM(String filename, GetMeshFunc get_mesh)
{
  load(filename, get_mesh);
}


HIM::HIM(String filename, GetMeshFunc get_mesh, const char *data, size_t data_size)
{
  load(filename, get_mesh, data, data_size);
}


std::unique_ptr<HierMeshBase> loadHierMesh(std::string path)
{
  return std::make_unique<HIM>(path);
}


std::unique_ptr<HierMeshBase> loadHierMesh(std::string path, GetMeshFunc get_mesh)
{
  return std::make_unique<HIM>(path, get_mesh);
}


std::unique_ptr<HierMeshBase> loadHierMesh(std::string path,
                                           GetMeshFunc get_mesh,
                                           const char *data, size_t data_size)
{
  return std::make_unique<HIM>(path, get_mesh, data, data_size);
}


}
