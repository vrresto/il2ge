/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "mesh.h"
#include "hook.h"
#include "loader_util.h"
#include <il2ge/section_file.h>
#include <il2ge/material.h>
#include <util.h>

#include <fstream>
#include <map>


#define IGNORE_UNKNOWN_MODES 1
#define ALLOW_JUNK_AT_START_OF_FILE 1


using namespace il2ge::mesh;
using namespace il2ge::mesh::loader_util;


namespace
{


class NotEnoughValuesException : public std::exception
{
    std::string m_what;

public:
    NotEnoughValuesException(int expected, int line)
    {
      m_what = "Not enough values. Expected: " + std::to_string(expected)
        + " - Line: " + std::to_string(line);
    }

    const char* what() const noexcept { return m_what.c_str(); }
};


class ParseValueException : public std::exception
{
    std::string m_what;

public:
    ParseValueException(int pos, int line)
    {
      m_what = "Failed to parse value at position " + std::to_string(pos)
        + " - Line: " + std::to_string(line);
    }

    const char* what() const noexcept { return m_what.c_str(); }
};


struct MeshDataLoader
{
  MeshData &m_data;
  const il2ge::SectionFile &m_file;
  Materials& m_materials;
  std::string m_section_prefix;
  std::vector<int> m_material_mapping;

  static void expectValues(const il2ge::SectionFile::Entry &entry, size_t expected_num)
  {
    if (entry.values.size() < expected_num)
    {
      LOG_ERROR << "Expected number of values: " << expected_num
                << " got: " << entry.values.size() << std::endl;
      LOG_ERROR << "Line number: " << entry.line_in_file << std::endl;
      throw NotEnoughValuesException(expected_num, entry.line_in_file);
    }
  }

  struct EntryParser
  {
    const il2ge::SectionFile::Entry &entry;
    size_t pos = 0;

    EntryParser(const il2ge::SectionFile::Entry &entry, size_t expected_num) : entry(entry)
    {
      expectValues(entry, expected_num);
    }

    const std::string &getNextValue()
    {
      try
      {
        return entry.values.at(pos++);
      }
      catch (...)
      {
        LOG_ERROR << "Failed to get value at position " << pos << std::endl;
        LOG_ERROR << "Line number: " << entry.line_in_file << std::endl;
        throw NotEnoughValuesException(pos+1, entry.line_in_file);
      }
    }

    template <typename T>
    T parse()
    {
      try
      {
        return loader_util::parse<T>(getNextValue());
      }
      catch (std::exception &e)
      {
        LOG_ERROR << "Failed to parse value at position " << pos << std::endl;
        LOG_ERROR << "Exception was: " << e.what() << std::endl;
        LOG_ERROR << "Line number: " << entry.line_in_file << std::endl;
        throw ParseValueException(pos, entry.line_in_file);
      }
    }

    float parseFloat()
    {
      return parse<float>();
    }

    int parseInt()
    {
      return parse<int>();
    }
  };


  MeshDataLoader(const il2ge::SectionFile &file, MeshData &data,
                 Materials& materials) :
      m_file(file), m_data(data),
      m_materials(materials) {}

  const il2ge::SectionFile::Section &getSection(const std::string &name)
  {
    auto full_name = m_section_prefix + name;

    try
    {
      return m_file.getSection(full_name);
    }
    catch (...)
    {
      LOG_ERROR << "File has no section " << full_name << std::endl;
      throw;
    }
  }


  bool hasSection(const std::string &name)
  {
    return m_file.hasSection(m_section_prefix + name);
  }


  void readMaterialMapping()
  {
    auto &section = getSection("MaterialMapping");
    int count = 0;

    for (auto &entry : section.entries)
    {
      EntryParser p(entry, 2);
      glm::vec2 v {};
      v.x = p.parseFloat();
      v.y = p.parseFloat();
      assert(count < m_data.Verts.size());
      m_data.Verts[count].TextureCoordinate = v;
      count++;
    }
  }


  void readVertices()
  {
    auto &section = getSection("Vertices_Frame0");

    if (section.entries.size() != m_data.Verts.size())
    {
      LOG_ERROR << "Expected " << m_data.Verts.size()
                << " entries - got: " << section.entries.size() << std::endl;
      throw std::runtime_error("section.entries.size() != Verts.size()");
    }

    size_t count = 0;
    for (auto &entry : section.entries)
    {
      EntryParser p(entry, 6);

      VertexPositionNormalTexture vp;

      vp.Position.x = p.parseFloat();
      vp.Position.y = p.parseFloat();
      vp.Position.z = p.parseFloat();
      vp.Normal.x = p.parseFloat();
      vp.Normal.y = p.parseFloat();
      vp.Normal.z = p.parseFloat();

      assert(count < m_data.Verts.size());
      m_data.Verts[count] = vp;
      count++;
    }
  }

  void readFaces()
  {
    auto &section = getSection("Faces");
    size_t count = 0;

    for (auto &entry : section.entries)
    {
      EntryParser p(entry, 3);

      m_data.indices[count++] = p.parseInt();
      m_data.indices[count++] = p.parseInt();
      m_data.indices[count++] = p.parseInt();
    }
  }


  void readFaceGroups()
  {
    auto &section = getSection("FaceGroups");
    assert(section.entries.size() >= 2);
    assert(section.entries.front().values.size() == 2);

    for (auto &entry : section.entries)
    {
      if (entry.values.size() > 2)
      {
        EntryParser p(entry, 5);
        FaceGroup fg;
        fg.Material = p.parseInt();
        fg.StartVertex = p.parseInt();
        fg.VertexCount = p.parseInt();
        fg.StartFace = p.parseInt();
        fg.FaceCount = p.parseInt();
        m_data.FaceGroups.push_back(fg);
      }
      else
      {
        EntryParser p(entry, 2);
        auto VertexCount = p.parseInt();
        auto FaceCount = p.parseInt();
        m_data.Verts.resize(VertexCount);
        m_data.indices.resize(FaceCount * 3);
      }
    }

    for (auto& fg : m_data.FaceGroups)
    {
      fg.Material = m_material_mapping.at(fg.Material);
    }
  }


  void readMaterials()
  {
    auto &section = getSection("Materials");
    for (auto &entry : section.entries)
    {
      EntryParser p(entry, 1);
      m_material_mapping.push_back(m_materials.getIndex(p.getNextValue()));
    }
  }


  void load()
  {
    readFaceGroups();
    readVertices();
    readMaterialMapping();
    readFaces();
    readMaterials();
  }

};


struct TextModeLoader : public MeshDataLoader
{
  Vector<float> LodDistances;
  Vector<Hook> hooks;


  TextModeLoader(const il2ge::SectionFile &file, MeshData &data, Materials& materials) :
    MeshDataLoader(file, data, materials)
  {
//     LOG_WARNING << "has hooks: " << hasSection("Hooks") << std::endl;
  }


  void load()
  {
    readHooks();
    MeshDataLoader::load();
    readLodDistances();
  }


  void readHooks()
  {
//     LOG_WARNING << "has hooks: " << hasSection("Hooks") << std::endl;
//     abort();
    
    if (hasSection("Hooks"))
    {
      auto &section = getSection("Hooks");
      for (auto &entry : section.entries)
      {
        EntryParser p(entry, 1);
        Hook hook(p.getNextValue());
        hook.locs.resize(1);
        
        LOG_DEBUG << "Hook: " << hook.getName() << std::endl;
        
        hooks.push_back(std::move(hook));
      }
    }

    if (hasSection("HookLoc"))
    {
      auto &section = getSection("HookLoc");
      int index = 0;
      for (auto &entry : section.entries)
      {
        expectValues(entry, 12);
        hooks.at(index).locs.at(0) = loadTransformationMatrix(entry.values);
        index++;
      }
    }
  }


  void readLodDistances()
  {
    if (hasSection("LOD"))
    {
      auto &section = getSection("LOD");
      for (auto &entry : section.entries)
      {
        EntryParser p(entry, 1);
        LodDistances.push_back(p.parseFloat());
      }
    }
    else
    {
//       throw std::runtime_error("No LOD section.");
    }
  }

};

#if 0
class TextModeLoader_old
{
    const Modes m_modes;

    std::string cpart = "[CoCommon_b0]";
    std::string cpart2 = "[CoCommon_b0p0]";
    MeshLoaderMode m_mode = MeshLoaderMode::Start;
    int count = 0;

public:
    List<FaceGroup> FaceGroups;
    Vector<VertexPositionNormalTexture> Verts;
    Vector<Index> indices;
    int VertexCount = 0;
    int FaceCount = 0;
    Vector<std::string> Materials;
    Vector<Hook> Hooks;
    Vector<VertexPositionColor> ShadowVerts;
    Vector<short> ShadowIndices;
    Vector<float> LodDistances;


    MeshLoaderMode getMode(std::string line) const
    {
        if (util::isPrefix(cpart2, line))
            return MeshLoaderMode::CoCommonType;
        else if (util::isPrefix(cpart, line))
            return MeshLoaderMode::CoCommonPart;
        else
            return m_modes.get(line);
    }


    bool isDone()
    {
        return m_mode == MeshLoaderMode::Done;
    }


    MeshLoaderMode getMode()
    {
        return m_mode;
    }


    void processLine(std::string line)
    {
        assert(!isDone());

        if (util::isNullOrWhiteSpace(line))
          return;

        if (util::isPrefix("//", line) || util::isPrefix("#", line))
          return;

        auto line_lowercase = util::makeLowercase(line);
        auto parts = util::tokenize(line);

        auto expect_values = [&parts] (size_t expected_num)
        {
            if (parts.size() < expected_num)
            {
                LOG_ERROR << "Expected number of values: " << expected_num
                          << " got: " << parts.size() << std::endl;
                throw NotEnoughValuesException(expected_num, -1);
            }
        };

        for (auto &p : parts)
        {
          assert(!util::isNullOrWhiteSpace(p));
        }

        if (util::isPrefix(";", line))
        {
            m_mode = MeshLoaderMode::Done;
            return;
        }
        else if (util::isPrefix("[", line))
        {
#if ENABLE_COLLISION_MESH
            if (m_mode == MeshLoaderMode::CoFaces)
            {
//                                   colmesh.CurrentPart++;
//                                   if (colmesh.CurrentPart == colmesh.Blocks[colmesh.CurrentBlock].NParts)
//                                   {
//                                       colmesh.CurrentPart = 0;
//                                       colmesh.CurrentBlock++;
//                                   }
//                                   cpart2 = string.Format("[CoCommon_b{0}p{1}]", colmesh.CurrentBlock, colmesh.CurrentPart);
//                                   cpart = string.Format("[CoCommon_b{0}]", colmesh.CurrentBlock);
              assert(0);
            }
#endif
            m_mode = getMode(line);
            count = 0;

            if ((m_mode == MeshLoaderMode::Unknown) && util::isPrefix("[lod", line_lowercase))
            {
#if ENABLE_LOD
//                                       Lod l = new Lod(reader, dir);
//                                       Lods.Add(l);
//                                       used = true;
//                                       if (l.Done)
//                                       {
//                                           reader.Close();
//                                           if (ShadowVerts.Count > 0)
//                                           {
//                                               ShadowVertsArray = ShadowVerts.ToArray();
//                                               ShadowIndicesArray = ShadowIndices.ToArray();
//                                           }
//                                           return;
//                                       }
//                                       if (l.continueance != "")
//                                       {
//                                           line = l.continueance;
//                                           used = false;
//                                       }
                assert(0);
#else
                return;
#endif
            }

#if !IGNORE_UNKNOWN_MODES
            assert(m_mode != MeshLoaderMode::Unknown);
#endif
            return;
        }

        bool used = false;

        switch (m_mode)
        {
            case MeshLoaderMode::Start:
#if 0

                // error catching
                if (!used)
                {
                    if (util::isPrefix("[CoCommon_b", line))
                    {
//                                           int pp = line.IndexOf('p');
//                                           if (pp > 0)
//                                           {
//                                               if (!line.Equals(cpart2))
//                                               {
//                                                   MessageBox.Show("Collision mesh error in file " + filename + "\r\nMissing collision mesh blocks before " + line,
//                                                       "IL2Modder", MessageBoxButtons.OK, MessageBoxIcon.Error);
//                                                   mode = MeshLoaderMode::Done;
//                                                   line = "";
//                                                   colmesh.Blocks.Clear();
//                                                   colmesh.NBlocks = 0;
//                                               }
//                                           }
                        assert(0);
                    }
                }
#endif

#if ALLOW_JUNK_AT_START_OF_FILE
                used = true;
                break;
#else
                throw std::runtime_error("Unexpected line.");
#endif

            case MeshLoaderMode::Done:
#if ENABLE_COLLISION_MESH
                for (int j = 0; j < colmesh.NBlocks; j++)
                {
                    for (int i = 0; i < colmesh.Blocks[j].NParts; i++)
                    {
                        colmesh.Blocks[j].Parts[i].indices = colmesh.Blocks[j].Parts[i].Faces.ToArray();
                        colmesh.Blocks[j].Parts[i].verts = new VertexPositionColor[colmesh.Blocks[j].Parts[i].Verts.Count];
                        for (int k = 0; k < colmesh.Blocks[j].Parts[i].Verts.Count; k++)
                        {
                            colmesh.Blocks[j].Parts[i].verts[k].Position = colmesh.Blocks[j].Parts[i].Verts[j];
                            colmesh.Blocks[j].Parts[i].verts[k].Color = Microsoft.Xna.Framework.Color.Red;
                        }
                    }
                }
#endif
                assert(0);
                return;
            case MeshLoaderMode::Common:
                used = true;
                break;
            case MeshLoaderMode::Lod:
                {
                    expect_values(1);
                    LodDistances.push_back(parseFloat(parts[0]));
                    used = true;
                }
                break;
            case MeshLoaderMode::Hooks:
                {
                    Hook h (parts[0]);
                    Hooks.push_back(h);
                    used = true;
                }
                break;
            case MeshLoaderMode::Hookloc:
                if (count < Hooks.size())
                {
                    auto &hook = Hooks[count];
                    expect_values(12);
                    hook.matrix = loadTransformationMatrix(parts);
                }
                count++;
                used = true;
                break;
            case MeshLoaderMode::Materials:
                {
                    Materials.push_back(parts[0]);
                    used = true;
                }
                break;
            case MeshLoaderMode::FaceGroups:
                {
                    if (parts.size() > 2)
                    {
                        expect_values(5);
                        FaceGroup fg;
                        fg.Material = stoi(parts[0]);
                        fg.StartVertex = stoi(parts[1]);
                        fg.VertexCount = stoi(parts[2]);
                        fg.StartFace = stoi(parts[3]);
                        fg.FaceCount = stoi(parts[4]);
                        FaceGroups.push_back(fg);
                    }
                    else
                    {
                        expect_values(2);
                        VertexCount = stoi(parts[0]);
                        FaceCount = stoi(parts[1]);
                        Verts.resize(VertexCount);
                        indices.resize(FaceCount * 3);
                    }
                    used = true;
                }
                break;
            case MeshLoaderMode::Vertices:
                {
                    expect_values(6);

                    VertexPositionNormalTexture vp;

//                                       try
//                                       {
                        vp.Position.x = parseFloat(parts[0]);
                        vp.Position.y = parseFloat(parts[1]);
                        vp.Position.z = parseFloat(parts[2]);
                        vp.Normal.x = parseFloat(parts[3]);
                        vp.Normal.y = parseFloat(parts[4]);
                        vp.Normal.z = parseFloat(parts[5]);
//                                       }
//                                       catch (...)
//                                       {
//                                       }

                    Verts[count] = vp;
                    count++;
                    used = true;
                }
                break;
            case MeshLoaderMode::UVS:
                {
                    expect_values(2);
                    glm::vec2 v {};
                    v.x = parseFloat(parts[0]);
                    v.y = parseFloat(parts[1]);
                    Verts[count].TextureCoordinate = v;
                    count++;
                    used = true;
                }
                break;
            case MeshLoaderMode::Faces:
                {
                    expect_values(3);
                    indices[count++] = stoi(parts[0]);
                    indices[count++] = stoi(parts[1]);
                    indices[count++] = stoi(parts[2]);
                    used = true;
                }
                break;
            case MeshLoaderMode::ShadowVerts:
                {
                    expect_values(3);

                    VertexPositionColor vp;
                    vp.Position.x = parseFloat(parts[0]);
                    vp.Position.y = parseFloat(parts[1]);
                    vp.Position.z = parseFloat(parts[2]);
                    vp.Color = glm::vec3(0);

                    ShadowVerts.push_back(vp);
                    used = true;
                }
                break;
            case MeshLoaderMode::ShadowFaces:
                {
                    expect_values(3);

                    ShadowIndices.push_back(stoi(parts[0]));
                    ShadowIndices.push_back(stoi(parts[1]));
                    ShadowIndices.push_back(stoi(parts[2]));

                    used = true;
                }
                break;
#if 0
            case MeshLoaderMode::CoCommon:
                {
                    colmesh.CurrentBlock = 0;
                    colmesh.CurrentPart = 0;
                    cpart = string.Format("[CoCommon_b{0}]", colmesh.CurrentBlock);
                    cpart2 = string.Format("[CoCommon_b{0}p{1}]", colmesh.CurrentBlock, colmesh.CurrentPart);
                    colmesh.NBlocks = int.Parse(parts[1]);
                    used = true;
                }
                break;
            case MeshLoaderMode::CoCommonPart:
                {
                    CollisionMeshBlock cmb = new CollisionMeshBlock();
                    cmb.NParts = int.Parse(parts[1]);
                    colmesh.Blocks.Add(cmb);
                    cpart2 = string.Format("[CoCommon_b{0}p{1}]", colmesh.CurrentBlock, colmesh.CurrentPart);
                    used = true;
                }
                break;
            case MeshLoaderMode::CoCommonType:
                {
                    if (parts[0].Equals("Type"))
                    {
                        CollisionMeshPart p = new CollisionMeshPart();
                        colmesh.Blocks[colmesh.CurrentBlock].Parts.Add(p);

                        p.Type = parts[1];
                        used = true;
                    }
                    if (parts[0].Equals("NFrames"))
                    {
                        colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].NFrames = int.Parse(parts[1]);
                        used = true;
                    }
                    if (parts[0].Equals("Name"))
                    {
                        if (parts.Length > 1)
                            colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].Name = parts[1];
                        else
                            colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].Name = "BUG unnamed part";
                        used = true;
                    }
                    if (parts[0].Equals("TypeIntExt"))
                    {
                        if (parts[1].Equals("EXTERNAL"))
                            colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].TypeIntExt = 1;
                        else if (parts[1].Equals("INTERNAL"))
                            colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].TypeIntExt = 0;
                        else
                            colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].TypeIntExt = int.Parse(parts[1]);
                        used = true;
                    }
                }
                break;
            case MeshLoaderMode::CoVer0:
                {
                    Vector3 v = new Vector3();
                    v.x = float.Parse(parts[0], System.Globalization.CultureInfo.InvariantCulture);
                    v.y = float.Parse(parts[1], System.Globalization.CultureInfo.InvariantCulture);
                    v.z = float.Parse(parts[2], System.Globalization.CultureInfo.InvariantCulture);
                    colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].Verts.Add(v);
                    used = true;
                }
                break;
            case MeshLoaderMode::CoNeiCnt:
                {
                    colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].NeiCount.Add(int.Parse(parts[0]));
                    used = true;
                }
                break;
            case MeshLoaderMode::CoNei:
                {
                    colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].Neighbours.Add(int.Parse(parts[0]));
                    used = true;
                }
                break;
            case MeshLoaderMode::CoFaces:
                {
                    colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].Faces.Add(short.Parse(parts[0]));
                    colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].Faces.Add(short.Parse(parts[1]));
                    colmesh.Blocks[colmesh.CurrentBlock].Parts[colmesh.CurrentPart].Faces.Add(short.Parse(parts[2]));
                    used = true;
                }
                break;
#endif
            default:
                {
#if IGNORE_UNKNOWN_MODES
                    used = true;
#else
                    throw std::runtime_error("Unhandled mesh loader mode: " +
                                           std::to_string(static_cast<int>(m_mode)));
#endif
                }
        }

        assert(used);
    }
};
#endif

}


namespace il2ge::mesh
{


#if FALSE
void Mesh::ReadTextMode_old(String filename, String dir)
{
    assert(!Binary);

    String line = "";
    int line_number = 0;

//       try
    {
        MeshLoaderMode mode = MeshLoaderMode::Start;

        std::ifstream in(filename, std::ios_base::binary);
        assert(in.good());

        TextModeLoader_old loader;

        while (!loader.isDone())
        {
            assert(in.good());

            std::getline(in, line);

            if (util::isNullOrWhiteSpace(line))
                line = "";

            if (util::isPrefix("\\", line))
                line = "";

            if ((util::isPrefix("[NFrames", line)) || (util::isPrefix("[NBlocks", line)))
            {
//FIXME
//                       line = line.TrimStart('[');
//                       line = line.TrimEnd(']');
            }

            try
            {
                loader.processLine(line);

                if (in.eof() && !loader.isDone())
                {
                    #if IL2GE_MESH_ALLOW_UNEXPECTED_EOF
                        break;
                    #else
                        throw std::runtime_error("Unexpexted EOF");
                    #endif
                }
            }
            catch (std::exception &e)
            {
                LOG_ERROR << "Exception: " << e.what() << std::endl;
                LOG_ERROR << "File: " << filename  << std::endl;
                LOG_ERROR << "Line " << line_number+1 << ": " << line << std::endl;
                LOG_ERROR << "Current mode: " << (int)loader.getMode() << std::endl;
                throw;
            }

            line_number++;
        }

        FaceGroups = std::move(loader.FaceGroups);
        Verts = std::move(loader.Verts);
        indices = std::move(loader.indices);
        Materials = std::move(loader.Materials);
        LodDistances = std::move(loader.LodDistances);

        for (auto &m : Materials)
            m = dir + '/' + m + ".mat";
    }
//       catch (Exception e)
//       {
//           MSHLoaderErrorHandler msh = new MSHLoaderErrorHandler();
//           msh.SetMode(0);
//           msh.SetErrorText(e.ToString());
//           msh.SetData(String.Format("Line number {0}", line_number));
//           msh.SetErrorDescription(line);
//           msh.SetFilename(filename);
//           msh.ShowDialog();
// 
//       }
}
#endif


// inline bool writeFile(const std::string &path, std::istream &in)
// {
//   using namespace std;
// 
//   std::ofstream out(path, ios_base::binary | ios_base::trunc);
//   if (!out.good()) {
//     LOG_ERROR<<"can't open output file "<<path<<endl;
//     abort();
//   }
// 
//   assert(out.tellp() == 0);
// 
//   out.write(data, data_size);
// 
//   size_t size = out.tellp();
//   LOG_INFO<<"data_size:"<<data_size<<endl;
//   LOG_INFO<<"size:"<<size<<endl;
//   assert(data_size == size);
// 
//   if (!out.good()) {
//     LOG_ERROR<<"error during writing to output file "<<path<<endl;
//     return false;
//   }
// 
//   return true;
// }


void Mesh::ReadTextMode(std::istream &in, String dir)
{
  const std::vector<std::string> comment_prefixes { "//", "#" };

  assert(!Binary);

  il2ge::SectionFile f(in, comment_prefixes, ';', true);

  auto data = std::make_unique<MeshData>();

  Materials materials;

  TextModeLoader loader(f, *data, materials);
  loader.load();

  LodDistances = std::move(loader.LodDistances);
  Hooks = std::move(loader.hooks);

//   assert(!LodDistances.empty());
//   auto lods_count = LodDistances.size();

  auto base_lod = std::make_unique<Lod>(std::move(data));
  Lods.push_back(std::move(base_lod));

  for (size_t i = 1; i < LodDistances.size(); i++)
  {
    auto section_prefix = "LOD" + std::to_string(i) + "_";

    auto lod_data = std::make_unique<MeshData>();

    {
      MeshDataLoader lod_loader(f, *lod_data, materials);
      lod_loader.m_section_prefix = section_prefix;
      lod_loader.load();
    }

    auto lod = std::make_unique<Lod>(std::move(lod_data));

    Lods.push_back(std::move(lod));
  }

  for (auto& name : materials.getNames())
  {
    m_material_names.emplace_back(name);
  }
}


}
