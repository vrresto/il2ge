/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "mesh.h"
#include "binary_reader.h"
#include <il2ge/material.h>
#include <util.h>

#define IL2GE_MESH_ENABLE_ANIMATION 1


using namespace il2ge::mesh;


namespace
{


struct BinaryHeader
{
  Int32 magic;
  Int32 filesize;
  Int32 sections;
  Int32 unknown;
  Int32 table1;
  Int32 table2;
  Int32 table3;
};


struct T1Entry
{
  Int32 Offset;
  Int32 StartRecord;
  Int16 NumberOfRecords;
  Int16 Type;
};


struct BinaryFile
{
  BinaryHeader header;
  List<T1Entry> table1;
  List<String> sections;
  List<size_t> subsection_size;
  Vector<size_t> subsection_start;
  size_t data_start = 0;

  const T1Entry &getT1Entry(std::string name) const; //FIXME return pointer
  const bool hasT1Entry(std::string name) const;
  int getSubSectionStart(size_t record_index) const;
  int getSectionStart(const T1Entry &e) const;
};


// struct AnimationFrame
// {
//   Vector<VertexPositionNormalTexture> vertices;
// };


struct MeshDataLoader
{
  const BinaryFile &bfile;

  BinaryReader &b;
  MeshData &data;
  Materials& materials;
  std::string dir;
  std::string mode;
  std::string section_prefix;

  std::vector<int> material_mapping;

  bool Animated = false;
//   Vector<AnimationFrame> animation_frames;

  MeshDataLoader(const BinaryFile &bfile, BinaryReader &reader, MeshData &data,
                 Materials& materials) :
    bfile(bfile), b(reader), data(data), materials(materials) {}

  void load();

protected:
  const bool hasT1Entry(std::string name) const
  {
    return bfile.hasT1Entry("[" + section_prefix + name + "]");
  }
  const T1Entry &getT1Entry(std::string name) const
  {
    return bfile.getT1Entry("[" + section_prefix + name + "]");
  }
  void readMaterials();
  void readVertices();
  void readVertices(int animation_frame);
  void readMaterialMapping();
  void readFaces();
  void readFaceGroups();
  void readCommon();
};


struct Loader : public MeshDataLoader
{
//   String block_start = "[CoCommon_b0]";
//   String part_start = "[CoCommon_b0p0]";
  List<Hook> Hooks;

  Vector<float> LodDistances;

  Loader(const BinaryFile &bfile, BinaryReader &reader, MeshData &data,
         Materials& materials) :
    MeshDataLoader(bfile, reader, data, materials)
  {
  }

  void load();

private:
  void readHooks();
  void readLodDistances();
};


float ReadMotorola(BinaryReader &b)
{
    float res = 0;
    unsigned char t = b.ReadByte();
    if (t == 0)
    {
        unsigned char l = b.ReadByte();
        unsigned char h = b.ReadByte();
        short v = (short)((short)((short)h * 256) + ((short)l));
        res = (float)v;
    }
    else
    {
        res = b.ReadSingle();
    }
    return res;
}


String ReadString(BinaryReader &reader)
{
    int nchars = reader.ReadByte();
//             int nchars = reader.read<char>();
    
//             std::cout << "nchars before: " << nchars << std::endl;
    
    nchars--;
    
    assert(nchars > 0);
    
//             std::cout << "nchars: " << nchars << std::endl;

    std::string result(nchars, 0);
    for (int i = 0; i < nchars; i++)
    {
        result[i] = reader.read<char>();
    }

    reader.ReadByte();
    return result;
}

#if 1
String ReadSplitString(BinaryReader &reader, int size)
{
    String result = "";
    while (size > 0)
    {
        String next;
        auto nchars = reader.ReadByte();
        size -= nchars;
        nchars--;

        std::string result(nchars, 0);
        for (int i = 0; i < nchars; i++)
        {
            result[i] = reader.read<char>();
        }

        return result;
//         next = System.Text.Encoding.Default.GetString(chars);

//         result += " " + next;

    }
    return result;
}
#endif

String ReadString2(BinaryReader &reader, int nchars)
{
    int t = reader.ReadByte();
    assert(t == nchars);

    nchars--;

    std::string result(nchars, 0);
    for (int i = 0; i < nchars; i++)
    {
        result[i] = reader.read<char>();
    }

    return result;
}


String ReadString3(BinaryReader &reader)
{
    int nchars = reader.ReadByte();
    nchars--;

    if (nchars <= 0)
      return {};

    std::string result(nchars, 0);
    for (int i = 0; i < nchars; i++)
    {
        result[i] = reader.read<char>();
    }

    return result;
}


float ReadTriple(BinaryReader &b)
{
    float res = 0;
    int a1 = b.ReadByte();
    int a2 = b.ReadByte();
    int a3 = b.ReadByte();
    int a4 = 0;
    if ((a3 & 128) != 0)
        a4 = 255;
    a3 = (a4 << 24) + (a3 << 16) + (a2 << 8) + a1;
    res = (float)a3 / 65536.0f;
    return res;
}


float ReadDouble(BinaryReader &b)
{
    float res = 0;
    int a1 = 0;
    int a2 = b.ReadByte();
    int a3 = b.ReadByte();
    int a4 = 0;
    if ((a3 & 128) != 0)
        a4 = 255;
    a3 = (a4 << 24) + (a3 << 16) + (a2 << 8) + a1;
    res = (float)a3 / 65536.0f;
    return res;
}


template <class ReadValueFunc>
void readMatrix(ReadValueFunc read_value, glm::mat4 &mat)
{
  mat = glm::mat4 (1.0);

  for (int col = 0; col < 4; col++)
  {
    for (int row = 0; row < 3; row++)
    {
      mat[col][row] = read_value();
//       mat[row][col] = read_value();
    }
  }
}


void readMatrix(BinaryReader &reader, int size, glm::mat4 &mat)
{
  auto read_func_24 = [&reader] () { return reader.ReadInt16() / 32768.0f; };
  auto read_func_48 = [&reader] () { return reader.ReadSingle(); };
  auto read_func_default = [&reader] () { return ReadMotorola(reader); };

  switch (size)
  {
    case 24:
      readMatrix(read_func_24, mat);
      break;
    case 48:
      readMatrix(read_func_48, mat);
      break;
    default:
      readMatrix(read_func_default, mat);
      break;
  }
}


BinaryFile parseBinaryFile(BinaryReader &b)
{
    BinaryFile bfile;

    String mode;

    mode = "Reading header";

    b.seek(0);

    bfile.header.magic = b.ReadInt32();
    bfile.header.filesize = b.ReadInt32();
    bfile.header.sections = b.ReadInt32();
    bfile.header.unknown = b.ReadInt32();
    bfile.header.table1 = b.ReadInt32();
    bfile.header.table2 = b.ReadInt32();
    bfile.header.table3 = b.ReadInt32();

//     std::cout << "bfile.header.magic: " << bfile.header.magic << std::endl;
//     std::cout << "bfile.header.filesize: " << bfile.header.filesize<< std::endl;
//     std::cout << "bfile.header.sections: " << bfile.header.sections << std::endl;

    mode = "Reading section names";
//     std::cout << "reading section names ..." << std::endl;
    for (int i = 0; i < (int)bfile.header.sections; i++)
    {
        auto name = ReadString(b);
//         std::cout << "reading section name " << i << ": " << name << std::endl;
        bfile.sections.push_back(name);
    }
//     std::cout << "reading section names ... done" << std::endl;

    assert(!bfile.sections.empty());

    mode = "Reading table 1";
    b.seek(bfile.header.table1);
    for (int i = 0; i < (int)bfile.header.sections; i++)
    {
        T1Entry t;
        t.Offset = b.ReadInt32();
        t.StartRecord = b.ReadInt32();
        t.NumberOfRecords = b.ReadInt16();
        t.Type = b.ReadInt16();
        bfile.table1.push_back(t);
    }

    mode = "Reading table 3";
    b.seek(bfile.header.table3);
    bfile.data_start = b.ReadInt32();
//     std::cout << "bfile.data_start: " << bfile.data_start << std::endl;
    while (b.getPos() < bfile.data_start)
    {
        bfile.subsection_size.push_back(b.ReadInt32());
    }

    assert(!bfile.subsection_size.empty());

    bfile.subsection_start.resize(bfile.subsection_size.size());
    for (size_t start = bfile.data_start, i_record = 0;
         i_record < bfile.subsection_size.size();
         i_record++)
    {
      bfile.subsection_start[i_record] = start;
      start += bfile.subsection_size[i_record];
    }

    b.seek(bfile.data_start);
    assert(b.getPos() == bfile.data_start);

    return bfile;
}


const T1Entry &BinaryFile::getT1Entry(std::string name) const
{
  assert(sections.size());
  assert(table1.size());
  assert(sections.size() == table1.size());

  for (size_t i = 0; i < sections.size(); i++)
  {
    if (sections.at(i) == name)
    {
      return table1.at(i);
    }
  }

  throw std::runtime_error("Section " + name  + " not found.");
}


const bool BinaryFile::hasT1Entry(std::string name) const
{
  assert(sections.size());
  assert(table1.size());
  assert(sections.size() == table1.size());

  for (size_t i = 0; i < sections.size(); i++)
  {
    if (sections.at(i) == name)
    {
      return true;
    }
  }

  return false;
}


int BinaryFile::getSubSectionStart(size_t record_index) const
{
  return subsection_start.at(record_index);
}


int BinaryFile::getSectionStart(const T1Entry &e) const
{
  auto s = getSubSectionStart(e.StartRecord);
//     auto s2 = data_start + (e.Offset-1);
//     std::cout << "s: " << s << " s2: " << s2 << std::endl;
//     assert(s == s2);
  return s;
}


void MeshDataLoader::load()
{
  readMaterials();
  readVertices();
  readMaterialMapping();
  readFaces();
  readFaceGroups();
}


void MeshDataLoader::readMaterials()
{
    mode = "Reading materials";
//     std::cout <<  mode << std::endl;
    
    auto t1_entry = getT1Entry("Materials");
    
//     std::cout << "StartRecord: " << t1_entry.StartRecord << std::endl;
//     std::cout << "Offset: " << t1_entry.Offset << std::endl;

    auto section_start = bfile.getSectionStart(t1_entry);
//     std::cout << "subsection start: " << ss << std::endl;
//     std::cout << "bfile.data_start + t1_entry.Offset = " << bfile.data_start + t1_entry.Offset << std::endl;
    
//     b.seek(bfile.data_start + t1_entry.Offset);
    b.seek(section_start);

    auto sr = t1_entry.StartRecord;

//     assert(!Animated);
    //if (Animated)
    //  b.ReadByte();
    
//     std::cout << "bfile.data_start: " << bfile.data_start << std::endl;
//     std::cout << "b.getPos(): " << b.getPos() << std::endl;

//                     assert(b.getPos() == bfile.data_start);
//                     assert(b.getPos() == 6108);
    
    for (int j = 0; j < t1_entry.NumberOfRecords; j++)
    {
        int ss = bfile.subsection_size[sr++];
        
        auto mat = ReadString2(b, ss);
//                         auto mat = ReadString3(b);
        
//         LOG_TRACE << "material: " << mat << std::endl;
        // mat = dir + '/' + mat + ".mat";

        material_mapping.push_back(materials.getIndex(mat));
    }
}


void MeshDataLoader::readVertices()
{
  for (size_t i = 0; i < data.frame_count; i++)
    readVertices(i);
}


void MeshDataLoader::readVertices(const int CurrentAnimationFrame)
{
  mode = "Reading vertices";
  
//   LOG_WARNING<<"reading vertices. frame: "<<CurrentAnimationFrame<<std::endl;

  auto t1_entry = getT1Entry("Vertices_Frame" + std::to_string(CurrentAnimationFrame));

  if (CurrentAnimationFrame == 0)
  {
    data.vertex_count = t1_entry.NumberOfRecords;
    data.Verts.resize(data.vertex_count * data.frame_count);
  }

  assert(t1_entry.NumberOfRecords == data.vertex_count);
  assert(!data.Verts.empty());

  bool has_motorola = false;

  for (int i = 0; i < t1_entry.NumberOfRecords; i++)
  {
      switch (bfile.subsection_size[t1_entry.StartRecord + i])
      {
          case 20:
          case 22:
          case 30:
          case 26:
          case 28:
              has_motorola = true;
      }
  }

  int firstrecord = t1_entry.StartRecord;

  b.seek(bfile.getSectionStart(t1_entry));

  uint16_t sum = 0;

  for (int vertex_index = 0; vertex_index < data.vertex_count; vertex_index++)
  {
      auto ii = (CurrentAnimationFrame * data.vertex_count) + vertex_index;

      int size = bfile.subsection_size[firstrecord++];
#if 1
#if !TEST
      switch (size)
      {
          case 12:
              {
                  VertexPositionNormalTexture vp;

                  sum += b.read<uint16_t>();
                  vp.Position.x = *reinterpret_cast<int16_t*>(&sum);
                  sum += b.read<uint16_t>();
                  vp.Position.y = *reinterpret_cast<int16_t*>(&sum);
                  sum += b.read<uint16_t>();
                  vp.Position.z = *reinterpret_cast<int16_t*>(&sum);

                  sum += b.read<uint16_t>();
                  vp.Normal.x = *reinterpret_cast<int16_t*>(&sum);
                  sum += b.read<uint16_t>();
                  vp.Normal.y = *reinterpret_cast<int16_t*>(&sum);
                  sum += b.read<uint16_t>();
                  vp.Normal.z = *reinterpret_cast<int16_t*>(&sum);

                  data.Verts[ii] = vp;
              }
              break;
          case 18:
              {
                  VertexPositionNormalTexture vp;
                  if (has_motorola)
                  {
                      vp.Position.x = ReadMotorola(b);
                      vp.Position.y = ReadMotorola(b);
                      vp.Position.z = ReadMotorola(b);
                      vp.Normal.x = ReadMotorola(b);
                      vp.Normal.y = ReadMotorola(b);
                      vp.Normal.z = ReadMotorola(b);

                  }
                  else
                  {
                      vp.Position.x = ReadTriple(b);
                      vp.Position.y = ReadTriple(b);
                      vp.Position.z = ReadTriple(b);
                      vp.Normal.x = ReadTriple(b);
                      vp.Normal.y = ReadTriple(b);
                      vp.Normal.z = ReadTriple(b);
                  }
                  data.Verts[ii] = vp;
              }
              break;
          case 20:
              {
                  VertexPositionNormalTexture vp;

                  vp.Position.x = ReadMotorola(b);
                  vp.Position.y = ReadMotorola(b);
                  vp.Position.z = ReadMotorola(b);
                  vp.Normal.x = ReadMotorola(b);
                  vp.Normal.y = ReadMotorola(b);
                  vp.Normal.z = ReadMotorola(b);

                  data.Verts[ii] = vp;
              }
              break;
          case 22:
              {

                  VertexPositionNormalTexture vp;
                  vp.Position.x = ReadMotorola(b);
                  vp.Position.y = ReadMotorola(b);
                  vp.Position.z = ReadMotorola(b);
                  vp.Normal.x = ReadMotorola(b);
                  vp.Normal.y = ReadMotorola(b);
                  vp.Normal.z = ReadMotorola(b);

                  data.Verts[ii] = vp;
              }
              break;
          case 24:
              {
                  VertexPositionNormalTexture vp;
                  if (has_motorola)
                  {
                      vp.Position.x = ReadMotorola(b);
                      vp.Position.y = ReadMotorola(b);
                      vp.Position.z = ReadMotorola(b);
                      vp.Normal.x = ReadMotorola(b);
                      vp.Normal.y = ReadMotorola(b);
                      vp.Normal.z = ReadMotorola(b);
                  }
                  else
                  {
                      vp.Position.x = b.ReadSingle();
                      vp.Position.y = b.ReadSingle();
                      vp.Position.z = b.ReadSingle();
                      vp.Normal.x = b.ReadSingle();
                      vp.Normal.y = b.ReadSingle();
                      vp.Normal.z = b.ReadSingle();
                  }
                  data.Verts[ii] = vp;
              }
              break;
          case 30:
          case 26:
          case 28:
              {
                  VertexPositionNormalTexture vp;
                  int rsize = 0;
                  int bt = b.ReadByte();
                  if (bt == 0)
                  {
                      vp.Position.x = ReadDouble(b);
                      rsize += 3;
                  }
                  else
                  {
                      vp.Position.x = b.ReadSingle();
                      rsize += 5;
                  }
                  bt = b.ReadByte();
                  if (bt == 0)
                  {
                      vp.Position.y = ReadDouble(b);
                      rsize += 3;
                  }
                  else
                  {
                      vp.Position.y = b.ReadSingle();
                      rsize += 5;
                  }
                  bt = b.ReadByte();
                  if (bt == 0)
                  {
                      vp.Position.z = ReadDouble(b);
                      rsize += 3;
                  }
                  else
                  {
                      vp.Position.z = b.ReadSingle();
                      rsize += 5;
                  }
                  bt = b.ReadByte();
                  if (bt == 0)
                  {
                      vp.Normal.x = ReadDouble(b);
                      rsize += 3;
                  }
                  else
                  {
                      vp.Normal.x = b.ReadSingle();
                      rsize += 5;
                  }
                  bt = b.ReadByte();
                  if (bt == 0)
                  {
                      vp.Normal.y = ReadDouble(b);
                      rsize += 3;
                  }
                  else
                  {
                      vp.Normal.y = b.ReadSingle();
                      rsize += 5;
                  }
                  bt = b.ReadByte();
                  if (bt == 0)
                  {
                      vp.Normal.z = ReadDouble(b);
                      rsize += 3;
                  }
                  else
                  {
                      vp.Normal.z = b.ReadSingle();
                      rsize += 5;
                  }
                  if (rsize != size)
                  {
                      throw std::runtime_error("Bugger");
                  }
                  data.Verts[ii] = vp;
              }
              break;
          default:
              throw std::runtime_error("Unhandled vertex size " + std::to_string(size));
      }
#else
  VertexPositionNormalTexture vp;
  int rsize=0;
  int bt = b.ReadByte();
  if (bt == 0)
  {
      vp.Position.x = ReadDouble(b);
      rsize += 3;
  }
  else
  {
      vp.Position.x = b.ReadSingle();
      rsize += 5;
  }
  bt = b.ReadByte();
  if (bt == 0)
  {
      vp.Position.y = ReadDouble(b);
      rsize += 3;
  }
  else
  {
      vp.Position.y = b.ReadSingle();
      rsize += 5;
  }
  bt = b.ReadByte();
  if (bt == 0)
  {
      vp.Position.z = ReadDouble(b);
      rsize += 3;
  }
  else
  {
      vp.Position.z = b.ReadSingle();
      rsize += 5;
  }
  bt = b.ReadByte();
  if (bt == 0)
  {
      vp.Normal.x = ReadDouble(b);
      rsize += 3;
  }
  else
  {
      vp.Normal.x = b.ReadSingle();
      rsize += 5;
  }
  bt = b.ReadByte();
  if (bt == 0)
  {
      vp.Normal.y = ReadDouble(b);
      rsize += 3;
  }
  else
  {
      vp.Normal.y = b.ReadSingle();
      rsize += 5;
  }
  bt = b.ReadByte();
  if (bt == 0)
  {
      vp.Normal.z = ReadDouble(b);
      rsize += 3;
  }
  else
  {
      vp.Normal.z = b.ReadSingle();
      rsize += 5;
  }
  if (rsize != size)
  {
      throw new Exception("Bugger");
  }
#endif
#endif
  }
}


void MeshDataLoader::readMaterialMapping()
{
  mode = "Reading material mapping";

  assert(!data.Verts.empty());

  auto t1_entry = getT1Entry("MaterialMapping");
  b.seek(bfile.getSectionStart(t1_entry));


  assert(!data.Verts.empty());
  assert(t1_entry.NumberOfRecords == data.vertex_count);

  int CurrentAnimationFrame = -1;
  unsigned char re = 0;


  for (int ii = 0; ii < t1_entry.NumberOfRecords; ii++)
  {
      switch (t1_entry.Type)
      {
          case 257:
              {
                  re += b.ReadByte();
                  data.Verts[ii].TextureCoordinate.x = (float)re;
                  re += b.ReadByte();
                  data.Verts[ii].TextureCoordinate.y = (float)re;

              }
              break;
          case 3:
              {
                  int a1 = b.ReadByte();
                  int a2 = b.ReadByte();
                  int a3 = b.ReadByte();
                  int a4 = 0;
                  if ((a3 & 128) != 0)
                      a4 = 255;
                  a3 = (a4 << 24) + (a3 << 16) + (a2 << 8) + a1;
                  data.Verts[ii].TextureCoordinate.x = (float)(a3 / 65536.0f);

                  a1 = b.ReadByte();
                  a2 = b.ReadByte();
                  a3 = b.ReadByte();
                  a4 = 0;
                  if ((a3 & 128) != 0)
                      a4 = 255;
                  a3 = (a4 << 24) + (a3 << 16) + (a2 << 8) + a1;
                  data.Verts[ii].TextureCoordinate.y = (float)(a3 / 65536.0f);
              }
              break;
          case 4:
              {
                  data.Verts[ii].TextureCoordinate.x = b.ReadSingle();
                  data.Verts[ii].TextureCoordinate.y = b.ReadSingle();

              }
              break;
          default:
              throw std::runtime_error("MaterialMapping size not supported " +
                                       std::to_string(t1_entry.Type));
      }
  }

#if IL2GE_MESH_ENABLE_ANIMATION
  if (Animated)
  {
      assert(!data.Verts.empty());
      assert(data.Verts.size() == data.vertex_count * data.frame_count);

      for (size_t frame = 0; frame < data.frame_count; frame++)
      {
          for (size_t i = 0; i < data.vertex_count; i++)
          {
              auto &v = data.Verts[(frame * data.vertex_count) + i];
              v.TextureCoordinate = data.Verts[i].TextureCoordinate;
          }
      }
  }
#endif

}


void MeshDataLoader::readFaces()
{
  mode = "Reading faces";

  auto t1_entry = getT1Entry("Faces");
  b.seek(bfile.getSectionStart(t1_entry));

  data.indices.resize(t1_entry.NumberOfRecords * 3);

  assert(!data.indices.empty());

  int j = t1_entry.NumberOfRecords;
  switch (t1_entry.Type)
  {
      case 1:
      case 0x0101:
          {
              unsigned char re = 0;
              int k = 0;
              for (int ii = 0; ii < j; ii++)
              {
                  re += b.ReadByte();
                  data.indices[k++] = re;
                  re += b.ReadByte();
                  data.indices[k++] = re;
                  re += b.ReadByte();
                  data.indices[k++] = re;
              }
          }
          break;
      case 258:
      case 0x02:
          {
              int se = 0;
              int k = 0;
              for (int jj = 0; jj < t1_entry.NumberOfRecords; jj++)
              {
                  se += b.ReadInt16();
                  data.indices[k++] = se;
                  se += b.ReadInt16();
                  data.indices[k++] = se;
                  se += b.ReadInt16();
                  data.indices[k++] = se;
              }
          }
          break;


      default:
          throw std::runtime_error("Unhandled face size " + std::to_string(t1_entry.Type));
  }
}


void MeshDataLoader::readFaceGroups()
{
  mode = "Reading face groups";

  auto t1_entry = getT1Entry("FaceGroups");
  b.seek(bfile.getSectionStart(t1_entry));

  int VertexCount = 0;
  int FaceCount = 0;

  unsigned char re = 0;

  int j = t1_entry.NumberOfRecords;
  int firstrecord = t1_entry.StartRecord;
  int size = bfile.subsection_size[firstrecord++];
  if (size == 2)
  {
      switch (t1_entry.Type)
      {
          case 1:
              VertexCount = b.ReadByte();
              FaceCount = b.ReadByte();
              break;
          default:
              re = b.ReadByte();
              VertexCount = re;
              re += b.ReadByte();
              FaceCount = re;
              break;
      }
  }
  else if (size == 4)
  {
      VertexCount = b.ReadInt16();
      FaceCount = VertexCount + b.ReadInt16();
  }
  else
  {
      throw std::runtime_error("FaceGroups start record is larger than 2 bytes");
  }

  assert(VertexCount > 0);
  assert(FaceCount > 0);
  assert(data.indices.size() == (3 * FaceCount));
//   assert(data.Verts.size() == VertexCount);
  assert(data.vertex_count == VertexCount);

  j--;

  auto se = (short)FaceCount;
  while (j > 0)
  {
      FaceGroup f;
      size = bfile.subsection_size[firstrecord++];

      if (size == 6)
      {
          switch (t1_entry.Type)
          {
              case 1:
                  f.Material = b.ReadByte();
                  f.StartVertex = b.ReadByte();
                  f.VertexCount = b.ReadByte();
                  f.StartFace = b.ReadByte();
                  f.FaceCount = b.ReadByte();
                  b.ReadByte();
                  break;
              default:
                  re += b.ReadByte();
                  f.Material = re;
                  re += b.ReadByte();
                  f.StartVertex = re;
                  re += b.ReadByte();
                  f.VertexCount = re;
                  re += b.ReadByte();
                  f.StartFace = re;
                  re += b.ReadByte();
                  f.FaceCount = re;
                  re += b.ReadByte();     // not sure what this byte is for
                  break;
          }
      }
      else if (size == 12)
      {

          se += b.ReadInt16();
          f.Material = se;
          se += b.ReadInt16();
          f.StartVertex = se;
          se += b.ReadInt16();
          f.VertexCount = se;
          se += b.ReadInt16();
          f.StartFace = se;
          se += b.ReadInt16();
          f.FaceCount = se;
          se += b.ReadInt16();

      }
      else
      {
          throw std::runtime_error("FaceGroups unknown size");
      }
      data.FaceGroups.push_back(f);
      j--;
  }

  for (auto& fg : data.FaceGroups)
  {
      fg.Material = material_mapping.at(fg.Material);
  }
}


void Loader::load()
{
  readCommon();
  readHooks();
  readMaterials();
  readVertices();
  readMaterialMapping();
  readFaces();
  readFaceGroups();
  readLodDistances();
}


void MeshDataLoader::readCommon()
{
    mode = "Reading Common";

    auto t1_entry = getT1Entry("Common");

    b.seek(bfile.getSectionStart(t1_entry));

    for (int j = 0; j < t1_entry.NumberOfRecords; j++)
    {
        auto record_num = t1_entry.StartRecord + j;
        b.seek(bfile.getSubSectionStart(record_num));

//         auto pos = b.getPos();
        auto t = ReadString3(b);

        if (t =="FramesType")
        {
            auto h = ReadString3(b);

//             std::cout << "FramesType: " << h << std::endl;

            if (h == "Animation")
            {
                Animated = true;
#if !IL2GE_MESH_ENABLE_ANIMATION
                throw std::runtime_error("Mesh animation not yet supported.");
#endif
            }
        }
        else if (t == "NumFrames")
        {
            int b1 = b.ReadByte();
            int b2 = b.ReadByte();
            if (Animated)
            {
#if IL2GE_MESH_ENABLE_ANIMATION

                data.frame_count = (b2 + (b1 * 256));
                for (short ki = 0; ki < data.frame_count; ki++)
                {
//                     animated_shadow_frames.push_back(new List<VertexPositionColor>());
//                     animation_frames.push_back(new List<VertexPositionNormalTexture>());
                }
                //b.ReadByte();
                //b.ReadByte();
#else
//             assert(0);
#endif
            }
            else
            {
                data.frame_count = 1;
                b.ReadByte();
            }
        }
        else
        {
//             b.seek(pos + bfile.subsection_size[sr++]);
        }
    }

}


void Loader::readHooks()
{
  mode = "Reading Hooks";

  if (!hasT1Entry("Hooks"))
    return;

  auto t1_entry = getT1Entry("Hooks");
//   if (!t1_entry)
//     return;

  auto num_hooks = t1_entry.NumberOfRecords;

  b.seek(bfile.getSectionStart(t1_entry));

//   assert(!Animated);
//   if (Animated)
//     b.ReadByte();

//   auto sr = t1_entry.StartRecord;

  for (int j = 0; j < t1_entry.NumberOfRecords; j++)
  {
//     int ss = bfile.subsection_size[sr++];
    auto record = t1_entry.StartRecord + j;
    

    b.seek(bfile.getSubSectionStart(record));

    String name = ReadSplitString(b, bfile.subsection_size[record]);
    
    //HACK
    assert(name != "<BASE>");
//     if (name == "<BASE>")
//       continue;
    
    LOG_DEBUG << "hook: " << name << std::endl;
    
    Hook h (name);
    h.locs.resize(data.frame_count);

//     assert(!Animated);
//     if (Animated)
//     {
//         for (int ih = 0; ih < data.frame_count; ih++)
//             Hooks.push_back(h);
//     }
//     else
        Hooks.push_back(h);
  }
  
  
  {
    auto t1_entry = getT1Entry("HookLoc");
    
    assert(t1_entry.NumberOfRecords == (num_hooks * data.frame_count));

    b.seek(bfile.getSectionStart(t1_entry));
    auto sr = t1_entry.StartRecord;

    //b.ReadByte();

    for (size_t hook_index = 0; hook_index < num_hooks; hook_index++)
    {
      for (size_t frame = 0; frame < data.frame_count; frame++)
      {
        int size = bfile.subsection_size[sr++];

        readMatrix(b, size, Hooks[hook_index].locs.at(frame));
      }
    }
  }

}


void Loader::readLodDistances()
{
  mode = "Reading LOD distances";

  auto t1_entry = getT1Entry("LOD");
  b.seek(bfile.getSectionStart(t1_entry));

  Int16 lod_distance = 0;
  switch (t1_entry.Type)
  {
      case 1:
          {
              for (int j = 0; j < t1_entry.NumberOfRecords; j++)
              {
                  LodDistances.push_back(b.ReadByte());
              }
          }
          break;
      case 0x0101:
          {
              for (int j = 0; j < t1_entry.NumberOfRecords; j++)
              {
                  lod_distance += b.ReadByte();
                  LodDistances.push_back(lod_distance);
              }
          }
          break;
      case 258:
      case 0x02:
          {
              for (int j = 0; j < t1_entry.NumberOfRecords; j++)
              {
                  lod_distance += b.ReadInt16();
                  LodDistances.push_back(lod_distance);
              }
          }
          break;
  }
}


} // namespace


namespace il2ge::mesh
{


void Mesh::ReadBinary(std::istream &in, String dir)
{
  BinaryReader b(in);

  const auto bfile = parseBinaryFile(b);

  auto data = std::make_unique<MeshData>();

  Materials materials;

  {
    Loader loader(bfile, b, *data, materials);
    loader.dir = dir;
    loader.load();
    LodDistances = std::move(loader.LodDistances);
    Hooks = loader.Hooks;
    Animated = loader.Animated;
  }

  auto base_lod = std::make_unique<Lod>(std::move(data));
  Lods.push_back(std::move(base_lod));

  for (size_t i = 1; i < LodDistances.size()-1; i++)
  {
    auto section_prefix = "LOD" + std::to_string(i) + "_";

    auto lod_data = std::make_unique<MeshData>();

    {
      MeshDataLoader lod_loader(bfile, b, *lod_data, materials);
      lod_loader.section_prefix = section_prefix;
      lod_loader.dir = dir;
      try
      {
        lod_loader.load();
      }
      catch(std::exception& e)
      {
          LOG_ERROR << e.what() << std::endl;
//         throw;
        break;
      }
    }

    auto lod = std::make_unique<Lod>(std::move(lod_data));

    Lods.push_back(std::move(lod));
  }

  for (auto& name : materials.getNames())
  {
    m_material_names.emplace_back(name);
  }
}


#if 0
void Mesh::ReadBinary(String name, String dir)
{
    std::cout << "Mesh::Mesh() ..." << std::endl;

    unsigned char re = 0;
    short se = 0;
    Animated = false;

    {
        BinaryReader b(name);
//                 String bname = "";
        String mode = "";
//                 try
        {
            BinaryFile bfile;
//                     Lod lod = null;

//                     #region Read header
            mode = "Reading header";
            bfile.header.magic = b.ReadInt32();
            bfile.header.filesize = b.ReadInt32();
            bfile.header.sections = b.ReadInt32();
            bfile.header.unknown = b.ReadInt32();
            bfile.header.table1 = b.ReadInt32();
            bfile.header.table2 = b.ReadInt32();
            bfile.header.table3 = b.ReadInt32();
//                     #endregion
            
            std::cout << "bfile.header.magic: " << bfile.header.magic << std::endl;
            std::cout << "bfile.header.filesize: " << bfile.header.filesize<< std::endl;
            std::cout << "bfile.header.sections: " << bfile.header.sections << std::endl;
            

#if READ_SECTION_NAMES
            mode = "Reading section names";
            std::cout << "reading section names ..." << std::endl;
            for (int i = 0; i < (int)bfile.header.sections; i++)
            {
                auto name = ReadString(b);
                std::cout << "reading section name " << i << ": " << name << std::endl;
                bfile.sections.push_back(name);
            }
            std::cout << "reading section names ... done" << std::endl;
#endif

            assert(!bfile.sections.empty());

#if READ_TABLE_1
            mode = "Reading table 1";
            b.seek(bfile.header.table1);
            for (int i = 0; i < (int)bfile.header.sections; i++)
            {
                T1Entry t;
                t.Offset = b.ReadInt32();
                t.StartRecord = b.ReadInt32();
                t.NumberOfRecords = b.ReadInt16();
                t.Type = b.ReadInt16();
                bfile.table1.push_back(t);
            }
#endif

#if READ_TABLE_3
            mode = "Reading table 3";
            b.seek(bfile.header.table3);
            bfile.data_start = b.ReadInt32();
            while (b.getPos() < bfile.data_start)
            {
                bfile.subsection_size.push_back(b.ReadInt32());
            }
#endif

            assert(!bfile.subsection_size.empty());

            String block_start = "[CoCommon_b0]";
            String part_start = "[CoCommon_b0p0]";

//                     #region Read data
            b.seek(bfile.data_start);
            int sr = 0;
#if 1
            for (int i = 0; i < bfile.header.sections; i++)
            {
                auto bname = bfile.sections[i];

                mode = "Reading spacer";
                if (bname.find("Space") != std::string::npos)
                {
                    sr = bfile.table1[i].StartRecord;
                    for (int j = 0; j < bfile.table1[i].NumberOfRecords; j++)
                    {
                        int ss = bfile.subsection_size[sr++];
                        b.skip(ss);
                    }
                }

//                         #region Common
                if (util::isPrefix("[Common]", bname))
                {
                    mode = "Reading Common";

                    for (int j = 0; j < bfile.table1[i].NumberOfRecords; j++)
                    {
                        auto pos = b.getPos();
                        auto t = ReadString3(b);

                        if (t =="FramesType")
                        {
                            auto h = ReadString3(b);
                            if (h == "Animation")
                            {
                                Animated = true;
                            }
                        }
                        else if (t == "NumFrames")
                        {
                            int b1 = b.ReadByte();
                            int b2 = b.ReadByte();
                            if (Animated)
                            {
#if IL2GE_MESH_ENABLE_ANIMATION

                                FrameCount = (b2 + (b1 * 256));
                                for (short ki = 0; ki < FrameCount; ki++)
                                {
                                    animated_shadow_frames.push_back(new List<VertexPositionColor>());
                                    animation_frames.push_back(new List<VertexPositionNormalTexture>());
                                }
                                //b.ReadByte();
                                //b.ReadByte();
#else
                            assert(0);
#endif
                            }
                            else
                            {
                                FrameCount = 1;
                                b.ReadByte();
                            }
                        }
                        else
                        {
                            b.seek(pos + bfile.subsection_size[sr++]);
                        }
                    }
                }
//                         #endregion


//                         #region Lod table
                if (util::isPrefix("[LOD]", bname))
                {
                    mode = "Reading LOD";

                    Int16 lod_distance = 0;
                    switch (bfile.table1[i].Type)
                    {
                        case 1:
                            {
                                for (int j = 0; j < bfile.table1[i].NumberOfRecords; j++)
                                {
                                    LodDistances.push_back(b.ReadByte());
                                }
                            }
                            break;
                        case 0x0101:
                            {
                                for (int j = 0; j < bfile.table1[i].NumberOfRecords; j++)
                                {
                                    lod_distance += b.ReadByte();
                                    LodDistances.push_back(lod_distance);
                                }
                            }
                            break;
                        case 258:
                        case 0x02:
                            {
                                for (int j = 0; j < bfile.table1[i].NumberOfRecords; j++)
                                {
                                    lod_distance += b.ReadInt16();
                                    LodDistances.push_back(lod_distance);
                                }
                            }
                            break;

                    }
                }

//                         #endregion

#if ENABLE_MATERIALS
//                         #region Materials
                if (util::isPrefix("[Materials]", bname))
                {
                    mode = "Reading materials";

                    sr = bfile.table1[i].StartRecord;
                    //if (Animated)
                    //  b.ReadByte();
                    for (int j = 0; j < bfile.table1[i].NumberOfRecords; j++)
                    {
                        int ss = bfile.subsection_size[sr++];
                        String mat = ReadString2(b, ss);
                        Material m = new Material(mat, dir);
                        Materials.push_back(m);
                    }
                }
//                         #endregion
#endif

#if ENABLE_HOOKS
//                         #region Hooks
                if (util::isPrefix("[Hooks]", bname))
                {
                    mode = "Reading hooks";

                    sr = bfile.table1[i].StartRecord;
                    if (Animated)
                        b.ReadByte();
                    for (int j = 0; j < bfile.table1[i].NumberOfRecords; j++)
                    {

                        int ss = bfile.subsection_size[sr++];

                        String mat = ReadSplitString(b, ss);
                        Hook h = new Hook(mat);
                        if (Animated)
                        {
                            for (int ih = 0; ih < FrameCount; ih++)
                                Hooks.push_back(h);
                        }
                        else
                            Hooks.push_back(h);
                    }
                }
//                         #endregion
#endif

#if ENABLE_HOOKS
//                         #region Hookloc
                if (util::isPrefix("[HookLoc]", bname))
                {
                    mode = "Reading hook locations";
                
                    sr = bfile.table1[i].StartRecord;
                    //b.ReadByte();
                    int k = 0;
                    for (int j = 0; j < bfile.table1[i].NumberOfRecords; j++)
                    {
                        int size = bfile.subsection_size[sr++];

                        switch (size)
                        {
                            case 24:
                                Hooks[k].matrix.M11 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M12 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M13 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M21 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M22 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M23 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M31 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M32 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M33 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M41 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M42 = b.ReadInt16() / 32768.0f;
                                Hooks[k].matrix.M43 = b.ReadInt16() / 32768.0f;
                                break;

                            case 48:
                                Hooks[k].matrix.M11 = b.ReadSingle();
                                Hooks[k].matrix.M12 = b.ReadSingle();
                                Hooks[k].matrix.M13 = b.ReadSingle();
                                Hooks[k].matrix.M21 = b.ReadSingle();
                                Hooks[k].matrix.M22 = b.ReadSingle();
                                Hooks[k].matrix.M23 = b.ReadSingle();
                                Hooks[k].matrix.M31 = b.ReadSingle();
                                Hooks[k].matrix.M32 = b.ReadSingle();
                                Hooks[k].matrix.M33 = b.ReadSingle();
                                Hooks[k].matrix.M41 = b.ReadSingle();
                                Hooks[k].matrix.M42 = b.ReadSingle();
                                Hooks[k].matrix.M43 = b.ReadSingle();
                                break;

                            default:
                                {
                                    // one byte out
                                    long pos = b.BaseStream.Position;
                                    Hooks[k].matrix.M11 = ReadMotorola(b);
                                    Hooks[k].matrix.M12 = ReadMotorola(b);
                                    Hooks[k].matrix.M13 = ReadMotorola(b);
                                    Hooks[k].matrix.M21 = ReadMotorola(b);
                                    Hooks[k].matrix.M22 = ReadMotorola(b);
                                    Hooks[k].matrix.M23 = ReadMotorola(b);
                                    Hooks[k].matrix.M31 = ReadMotorola(b);
                                    Hooks[k].matrix.M32 = ReadMotorola(b);
                                    Hooks[k].matrix.M33 = ReadMotorola(b);
                                    Hooks[k].matrix.M41 = ReadMotorola(b);
                                    Hooks[k].matrix.M42 = ReadMotorola(b);
                                    Hooks[k].matrix.M43 = ReadMotorola(b);
                                    pos = b.BaseStream.Position - pos;
                                    if (pos != size)
                                    {
                                        throw new Exception("Hook location format problem");
                                    }
                                }
                                break;

                        }
                        k++;
                    }
                }
//                         #endregion
#endif


                assert(!indices.empty());
                assert(!Verts.empty());


#if ENABLE_FACE_GROUPS
//                         #region Face groups
                if (util::isPrefix("[FaceGroups]", bname))
                {
                    mode = "Reading face groups";

                    FaceGroup f;

                    int j = bfile.table1[i].NumberOfRecords;
                    int firstrecord = bfile.table1[i].StartRecord;
                    int size = bfile.subsection_size[firstrecord++];
                    if (size == 2)
                    {
                        switch (bfile.table1[i].Type)
                        {
                            case 1:
                                VertexCount = b.ReadByte();
                                FaceCount = b.ReadByte();
                                break;
                            default:
                                re = b.ReadByte();
                                VertexCount = re;
                                re += b.ReadByte();
                                FaceCount = re;
                                break;
                        }
                    }
                    else if (size == 4)
                    {
                        VertexCount = b.ReadInt16();
                        FaceCount = VertexCount + b.ReadInt16();
                    }
                    else
                    {
                        throw new NotSupportedException("FaceGroups start record is larger than 2 bytes");
                    }
                    indices = new short[3 * FaceCount];
                    Verts = new VertexPositionNormalTexture[VertexCount];
                    j--;
                    se = (short)FaceCount;
                    while (j > 0)
                    {
                        f = new FaceGroup();
                        size = bfile.subsection_size[firstrecord++];

                        if (size == 6)
                        {
                            switch (bfile.table1[i].Type)
                            {
                                case 1:
                                    f.Material = b.ReadByte();
                                    f.StartVertex = b.ReadByte();
                                    f.VertexCount = b.ReadByte();
                                    f.StartFace = b.ReadByte();
                                    f.FaceCount = b.ReadByte();
                                    b.ReadByte();
                                    break;
                                default:
                                    re += b.ReadByte();
                                    f.Material = re;
                                    re += b.ReadByte();
                                    f.StartVertex = re;
                                    re += b.ReadByte();
                                    f.VertexCount = re;
                                    re += b.ReadByte();
                                    f.StartFace = re;
                                    re += b.ReadByte();
                                    f.FaceCount = re;
                                    re += b.ReadByte();     // not sure what this byte is for
                                    break;
                            }
                        }
                        else if (size == 12)
                        {

                            se += b.ReadInt16();
                            f.Material = se;
                            se += b.ReadInt16();
                            f.StartVertex = se;
                            se += b.ReadInt16();
                            f.VertexCount = se;
                            se += b.ReadInt16();
                            f.StartFace = se;
                            se += b.ReadInt16();
                            f.FaceCount = se;
                            se += b.ReadInt16();

                        }
                        else
                        {
                            throw new NotSupportedException("FaceGroups unknown size");
                        }
                        FaceGroups.Add(f);
                        j--;
                    }
                }
//                         #endregion
#endif
                assert(!FaceGroups.empty());
                assert(!LodDistances.empty());


//                         #region Vertices_frame

                if (util::isPrefix("[Vertices_Frame", bname))
                {
                    mode = "Reading vertices";

                    bool had_motorola = false;
                    int j = bfile.table1[i].NumberOfRecords;
                    int firstrecord = bfile.table1[i].StartRecord;


                    CurrentAnimationFrame++;

                    for (int ii = 0; ii < j; ii++)
                    {
                        int size = bfile.subsection_size[firstrecord++];
#if 1
#if !TEST
                        switch (size)
                        {
                            case 18:
                                {
                                    VertexPositionNormalTexture vp;
                                    if (had_motorola)
                                    {
                                        vp.Position.x = ReadMotorola(b);
                                        vp.Position.y = ReadMotorola(b);
                                        vp.Position.z = ReadMotorola(b);
                                        vp.Normal.x = ReadMotorola(b);
                                        vp.Normal.y = ReadMotorola(b);
                                        vp.Normal.z = ReadMotorola(b);

                                    }
                                    else
                                    {
                                        vp.Position.x = ReadTriple(b);
                                        vp.Position.y = ReadTriple(b);
                                        vp.Position.z = ReadTriple(b);
                                        vp.Normal.x = ReadTriple(b);
                                        vp.Normal.y = ReadTriple(b);
                                        vp.Normal.z = ReadTriple(b);
                                    }
                                    if (Animated)
                                    {
#if IL2GE_MESH_ENABLE_ANIMATION
                                        animation_frames[CurrentAnimationFrame].Add(vp);
#else
                                        assert(0);
#endif
                                    }
                                    else
                                    {
                                        Verts[ii] = vp;
                                    }
                                }
                                break;
                            case 20:
                                {
                                    VertexPositionNormalTexture vp;

                                    vp.Position.x = ReadMotorola(b);
                                    vp.Position.y = ReadMotorola(b);
                                    vp.Position.z = ReadMotorola(b);
                                    vp.Normal.x = ReadMotorola(b);
                                    vp.Normal.y = ReadMotorola(b);
                                    vp.Normal.z = ReadMotorola(b);
                                    had_motorola = true;

                                    if (Animated)
                                    {
#if IL2GE_MESH_ENABLE_ANIMATION
                                        animation_frames[CurrentAnimationFrame].Add(vp);
#else
                                        assert(0);
#endif

                                    }
                                    else
                                    {
                                        Verts[ii] = vp;
                                    }
                                }
                                break;
                            case 22:
                                {

                                    VertexPositionNormalTexture vp;
                                    vp.Position.x = ReadMotorola(b);
                                    vp.Position.y = ReadMotorola(b);
                                    vp.Position.z = ReadMotorola(b);
                                    vp.Normal.x = ReadMotorola(b);
                                    vp.Normal.y = ReadMotorola(b);
                                    vp.Normal.z = ReadMotorola(b);
                                    had_motorola = true;
                                    if (Animated)
                                    {
#if IL2GE_MESH_ENABLE_ANIMATION
                                        animation_frames[CurrentAnimationFrame].Add(vp);
#else
                                        assert(0);
#endif
                                    }
                                    else
                                    {
                                        Verts[ii] = vp;
                                    }

                                }
                                break;
                            case 24:
                                {
                                    VertexPositionNormalTexture vp;
                                    if (had_motorola)
                                    {
                                        vp.Position.x = ReadMotorola(b);
                                        vp.Position.y = ReadMotorola(b);
                                        vp.Position.z = ReadMotorola(b);
                                        vp.Normal.x = ReadMotorola(b);
                                        vp.Normal.y = ReadMotorola(b);
                                        vp.Normal.z = ReadMotorola(b);
                                    }
                                    else
                                    {
                                        vp.Position.x = b.ReadSingle();
                                        vp.Position.y = b.ReadSingle();
                                        vp.Position.z = b.ReadSingle();
                                        vp.Normal.x = b.ReadSingle();
                                        vp.Normal.y = b.ReadSingle();
                                        vp.Normal.z = b.ReadSingle();
                                    }
                                    if (Animated)
                                    {
#if IL2GE_MESH_ENABLE_ANIMATION
                                        animation_frames[CurrentAnimationFrame].push_back(vp);
#else
                                        assert(0);
#endif
                                    }
                                    else
                                    {
                                        Verts[ii] = vp;
                                    }

                                }
                                break;
                            case 30:
                            case 26:
                            case 28:
                                {
                                    VertexPositionNormalTexture vp;
                                    had_motorola = true;
                                    int rsize = 0;
                                    int bt = b.ReadByte();
                                    if (bt == 0)
                                    {
                                        vp.Position.x = ReadDouble(b);
                                        rsize += 3;
                                    }
                                    else
                                    {
                                        vp.Position.x = b.ReadSingle();
                                        rsize += 5;
                                    }
                                    bt = b.ReadByte();
                                    if (bt == 0)
                                    {
                                        vp.Position.y = ReadDouble(b);
                                        rsize += 3;
                                    }
                                    else
                                    {
                                        vp.Position.y = b.ReadSingle();
                                        rsize += 5;
                                    }
                                    bt = b.ReadByte();
                                    if (bt == 0)
                                    {
                                        vp.Position.z = ReadDouble(b);
                                        rsize += 3;
                                    }
                                    else
                                    {
                                        vp.Position.z = b.ReadSingle();
                                        rsize += 5;
                                    }
                                    bt = b.ReadByte();
                                    if (bt == 0)
                                    {
                                        vp.Normal.x = ReadDouble(b);
                                        rsize += 3;
                                    }
                                    else
                                    {
                                        vp.Normal.x = b.ReadSingle();
                                        rsize += 5;
                                    }
                                    bt = b.ReadByte();
                                    if (bt == 0)
                                    {
                                        vp.Normal.y = ReadDouble(b);
                                        rsize += 3;
                                    }
                                    else
                                    {
                                        vp.Normal.y = b.ReadSingle();
                                        rsize += 5;
                                    }
                                    bt = b.ReadByte();
                                    if (bt == 0)
                                    {
                                        vp.Normal.z = ReadDouble(b);
                                        rsize += 3;
                                    }
                                    else
                                    {
                                        vp.Normal.z = b.ReadSingle();
                                        rsize += 5;
                                    }
                                    if (rsize != size)
                                    {
                                        throw std::runtime_error("Bugger");
                                    }
                                    if (Animated)
                                    {
#if IL2GE_MESH_ENABLE_ANIMATION
                                        animation_frames[CurrentAnimationFrame].Add(vp);
#else
                                        assert(0);
#endif
                                    }
                                    else
                                    {
                                        Verts[ii] = vp;
                                    }


                                }
                                break;
                            default:
                                throw std::runtime_error("Unhandled vertex size " + size);
                        }
#else
                    VertexPositionNormalTexture vp;
                    int rsize=0;
                    int bt = b.ReadByte();
                    if (bt == 0)
                    {
                        vp.Position.x = ReadDouble(b);
                        rsize += 3;
                    }
                    else
                    {
                        vp.Position.x = b.ReadSingle();
                        rsize += 5;
                    }
                    bt = b.ReadByte();
                    if (bt == 0)
                    {
                        vp.Position.y = ReadDouble(b);
                        rsize += 3;
                    }
                    else
                    {
                        vp.Position.y = b.ReadSingle();
                        rsize += 5;
                    }
                    bt = b.ReadByte();
                    if (bt == 0)
                    {
                        vp.Position.z = ReadDouble(b);
                        rsize += 3;
                    }
                    else
                    {
                        vp.Position.z = b.ReadSingle();
                        rsize += 5;
                    }
                    bt = b.ReadByte();
                    if (bt == 0)
                    {
                        vp.Normal.x = ReadDouble(b);
                        rsize += 3;
                    }
                    else
                    {
                        vp.Normal.x = b.ReadSingle();
                        rsize += 5;
                    }
                    bt = b.ReadByte();
                    if (bt == 0)
                    {
                        vp.Normal.y = ReadDouble(b);
                        rsize += 3;
                    }
                    else
                    {
                        vp.Normal.y = b.ReadSingle();
                        rsize += 5;
                    }
                    bt = b.ReadByte();
                    if (bt == 0)
                    {
                        vp.Normal.z = ReadDouble(b);
                        rsize += 3;
                    }
                    else
                    {
                        vp.Normal.z = b.ReadSingle();
                        rsize += 5;
                    }
                    if (rsize != size)
                    {
                        throw new Exception("Bugger");
                    }
#endif
#endif
                    }
                }
//                         #endregion


//                         #region Material mapping
                if (util::isPrefix("[MaterialMapping]", bname))
                {
                    mode = "Reading material mapping";

                    CurrentAnimationFrame = -1;
                    re = 0;
                    int j = bfile.table1[i].NumberOfRecords;
                    for (int ii = 0; ii < j; ii++)
                    {
                        switch (bfile.table1[i].Type)
                        {
                            case 257:
                                {
                                    re += b.ReadByte();
                                    Verts[ii].TextureCoordinate.x = (float)re;
                                    re += b.ReadByte();
                                    Verts[ii].TextureCoordinate.y = (float)re;

                                }
                                break;
                            case 3:
                                {
                                    int a1 = b.ReadByte();
                                    int a2 = b.ReadByte();
                                    int a3 = b.ReadByte();
                                    int a4 = 0;
                                    if ((a3 & 128) != 0)
                                        a4 = 255;
                                    a3 = (a4 << 24) + (a3 << 16) + (a2 << 8) + a1;
                                    Verts[ii].TextureCoordinate.x = (float)(a3 / 65536.0f);

                                    a1 = b.ReadByte();
                                    a2 = b.ReadByte();
                                    a3 = b.ReadByte();
                                    a4 = 0;
                                    if ((a3 & 128) != 0)
                                        a4 = 255;
                                    a3 = (a4 << 24) + (a3 << 16) + (a2 << 8) + a1;
                                    Verts[ii].TextureCoordinate.y = (float)(a3 / 65536.0f);
                                }
                                break;
                            case 4:
                                {
                                    Verts[ii].TextureCoordinate.x = b.ReadSingle();
                                    Verts[ii].TextureCoordinate.y = b.ReadSingle();

                                }
                                break;
                            default:
                                throw std::runtime_error("MaterialMapping size not supported " + bfile.table1[i].Type);

                        }


                    }
                    if (Animated)
                    {
#if IL2GE_MESH_ENABLE_ANIMATION
                        for (int frame = 0; frame < FrameCount; frame++)
                        {
                            List<VertexPositionNormalTexture> this_frame = animation_frames[frame];
                            VertexPositionNormalTexture[] vpt = this_frame.ToArray();
                            for (int vert = 0; vert < this_frame.Count; vert++)
                            {
                                vpt[vert].TextureCoordinate.x = Verts[vert].TextureCoordinate.X;
                                vpt[vert].TextureCoordinate.y = Verts[vert].TextureCoordinate.Y;
                            }
                            this_frame.Clear();
                            this_frame.AddRange(vpt);
                            animation_frames[frame] = this_frame;
                        }
#else
                        assert(0);
#endif
                    }
                }
//                         #endregion

//                         #region Faces
                if (util::isPrefix("[Faces]", bname))
                {
                    mode = "Reading faces";
                
                    int j = bfile.table1[i].NumberOfRecords;
                    switch (bfile.table1[i].Type)
                    {
                        case 1:
                        case 0x0101:
                            {
                                re = 0;
                                int k = 0;
                                for (int ii = 0; ii < j; ii++)
                                {
                                    re += b.ReadByte();
                                    indices[k++] = re;
                                    re += b.ReadByte();
                                    indices[k++] = re;
                                    re += b.ReadByte();
                                    indices[k++] = re;
                                }
                            }
                            break;
                        case 258:
                        case 0x02:
                            {
                                se = 0;
                                int k = 0;
                                for (int jj = 0; jj < bfile.table1[i].NumberOfRecords; jj++)
                                {
                                    se += b.ReadInt16();
                                    indices[k++] = se;
                                    se += b.ReadInt16();
                                    indices[k++] = se;
                                    se += b.ReadInt16();
                                    indices[k++] = se;
                                }
                            }
                            break;


                        default:
                            throw std::runtime_error("Unhandled face size " + bfile.table1[i].Type);
                    }
                }
//                         #endregion

//                         #region Shadow verts
                if (util::isPrefix("[ShVertices_Frame", bname))
                {
                    mode = "Reading shadow verts";

                    CurrentAnimationFrame++;
                    int j = bfile.table1[i].NumberOfRecords;
                    VertexPositionColor v;
                    for (int ii = 0; ii < j; ii++)
                    {
                        switch (bfile.table1[i].Type)
                        {
                            case 3:
                                {
                                    v.Position.x = ReadTriple(b);
                                    v.Position.y = ReadTriple(b);
                                    v.Position.z = ReadTriple(b);
                                }
                                break;
                            default:
                                throw std::runtime_error("Unhandled shadow vert type");
                        }
                        v.Color = glm::vec3(1);
                        if (Animated)
                        {
#if IL2GE_MESH_ENABLE_ANIMATION
                            animated_shadow_frames[CurrentAnimationFrame].push_back(v);
#else
                            assert(0);
#endif
                        }
#if ENABLE_SHADOW_VERTS
                        else
                            ShadowVerts.push_back(v);
#else
                        assert(0);
#endif
                    }
                }
//                         #endregion


#if ENABLE_SHADOW_FACES
//                         #region Shadow faces
                if (bname.StartsWith("[ShFaces]"))
                {
                    mode = "Reading shadow faces";
                
                    CurrentAnimationFrame = -1;
                    re = 0;
                    int j = bfile.table1[i].NumberOfRecords;
                    switch (bfile.table1[i].Type)
                    {
                        case 1:
                            {

                                for (int ii = 0; ii < j; ii++)
                                {
                                    re = b.ReadByte();
                                    ShadowIndices.Add((short)re);
                                    re = b.ReadByte();
                                    ShadowIndices.Add((short)re);
                                    re = b.ReadByte();
                                    ShadowIndices.Add((short)re);
                                }
                            }
                            break;
                        case 0x0101:
                            {

                                for (int ii = 0; ii < j; ii++)
                                {
                                    re += b.ReadByte();
                                    ShadowIndices.Add((short)re);
                                    re += b.ReadByte();
                                    ShadowIndices.Add((short)re);
                                    re += b.ReadByte();
                                    ShadowIndices.Add((short)re);
                                }
                            }
                            break;

                        case 0x102:
                            {
                                for (int jj = 0; jj < bfile.table1[i].NumberOfRecords; jj++)
                                {
                                    se += b.ReadInt16();
                                    ShadowIndices.Add((short)se);
                                    se += b.ReadInt16();
                                    ShadowIndices.Add((short)se);
                                    se += b.ReadInt16();
                                    ShadowIndices.Add((short)se);
                                }
                            }
                            break;

                        case 0x02:
                            {
                                for (int jj = 0; jj < bfile.table1[i].NumberOfRecords; jj++)
                                {
                                    se = b.ReadInt16();
                                    ShadowIndices.Add((short)se);
                                    se = b.ReadInt16();
                                    ShadowIndices.Add((short)se);
                                    se = b.ReadInt16();
                                    ShadowIndices.Add((short)se);
                                }
                            }
                            break;
                        default:
                            throw new Exception("Shadow faces unhandled size " + bfile.table1[i].Type);
                    }
                }
//                         #endregion
#endif


#if ENABLE_COLLISION_MESH
//                         #region CoCommon
                if (bname.StartsWith("[CoCommon]"))
                {
                    mode = "Reading CoCommon";

                    String check = ReadString(b);
                    if (!check.Equals("NBlocks"))
                        throw std::runtime_error("Binary reader CoCommon Error " + check);

                    colmesh.CurrentBlock = 0;
                    colmesh.NBlocks = b.ReadInt16();
                    colmesh.CurrentPart = 0;
                    block_start = "[CoCommon_b0]";
                    part_start = "[CoCommon_b0p0]";
                }
//                         #endregion



//                         #region CoCommonBlock
                if (bname.Equals(block_start))
                {
                    mode = "Reading " + block_start;
                
                    CollisionMeshBlock nb = new CollisionMeshBlock();
                    colmesh.Blocks.Add(nb);
                    String check = ReadString(b);
                    if (!check.Equals("NParts"))
                        throw new Exception("Binary reader CoCommonBlock parts error " + check);
                    if (colmesh.NBlocks != colmesh.Blocks.Count)
                    {
                        // account for missing [CoCommon] block
                        colmesh.NBlocks = colmesh.Blocks.Count;
                        colmesh.CurrentBlock = 0;
                        colmesh.CurrentPart = 0;
                        MessageBox.Show("Malformed binary :- Missing CoCommon section " + name, "IL2 Modder", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    nb.NParts = b.ReadInt16();

                }
//                         #endregion

//                         #region CoCommonBlockPart
                if (bname.Equals(part_start))
                {
                    mode = "Reading " + part_start;
                
                    CollisionMeshPart cmp = new CollisionMeshPart();
                    colmesh.Blocks[colmesh.Blocks.Count - 1].Parts.Add(cmp);
                    int j = bfile.table1[i].NumberOfRecords;

                    String check = ReadString3(b);
                    if (!check.Equals("Type"))
                        throw new Exception("Binary reader CoCommonBlock part error " + check);
                    cmp.Type = ReadString3(b);

                    check = ReadString(b);
                    if (!check.Equals("NFrames"))
                        throw new Exception("Binary reader CoCommonBlock part frame error " + check);
                    cmp.NFrames = b.ReadInt16();

                    check = ReadString3(b);
                    if (!check.Equals("Name"))
                        throw new Exception("Binary reader CoCommonBlock part name error " + check);
                    cmp.Name = ReadString3(b);

                    if (j > 3)
                    {
                        check = ReadString3(b);
                        if (!check.Equals("TypeIntExt"))
                            throw new Exception("Binary reader CoCommonBlock TypeIntExt error " + check);
                        check = ReadString3(b);
                        cmp.TypeIntExt = 1;
                        if (check.Equals("INTERNAL"))
                            cmp.TypeIntExt = 0;
                    }

                }
//                         #endregion

//                         #region [CoVer0_b0p0]
                if (bname.StartsWith("[CoVer"))
                {
                    mode = "Reading collision verts";
                
                    CollisionMeshPart cmp = colmesh.Blocks[colmesh.Blocks.Count - 1].Parts[colmesh.CurrentPart];
                    int j = bfile.table1[i].NumberOfRecords;
                    for (int ii = 0; ii < j; ii++)
                    {
                        switch (bfile.table1[i].Type)
                        {
                            case 4:
                                {
                                    float x = b.ReadSingle();
                                    float y = b.ReadSingle();
                                    float z = b.ReadSingle();
                                    cmp.Verts.Add(new Vector3(x, y, z));
                                }
                                break;
                            default:
                                throw new Exception("Binary reader collision vert type error " + bfile.table1[i].Type);
                        }
                    }
                }
//                         #endregion

//                         #region [CoNeiCnt_
                if (bname.StartsWith("[CoNeiCnt_"))
                {
                    mode = "Reading collision neighbours";
                
                    CollisionMeshPart cmp = colmesh.Blocks[colmesh.Blocks.Count - 1].Parts[colmesh.CurrentPart];
                    int j = bfile.table1[i].NumberOfRecords;
                    re = 0;
                    for (int ii = 0; ii < j; ii++)
                    {
                        switch (bfile.table1[i].Type)
                        {
                            case 1:
                                {
                                    re = b.ReadByte();
                                    cmp.NeiCount.Add((int)re);
                                }
                                break;

                            case 257:
                                {
                                    re += b.ReadByte();
                                    cmp.NeiCount.Add((int)re);
                                }
                                break;
                            default:
                                throw new Exception("Binary reader collision NeiCnt type error " + bfile.table1[i].Type);
                        }
                    }
                }
//                         #endregion

//                         #region CoNei_b0p0
                if (bname.StartsWith("[CoNei_b"))
                {
                    mode = "Reading collision neighbour parts";
                
                    CollisionMeshPart cmp = colmesh.Blocks[colmesh.Blocks.Count - 1].Parts[colmesh.CurrentPart];
                    int j = bfile.table1[i].NumberOfRecords;
                    re = 0;
                    for (int ii = 0; ii < j; ii++)
                    {
                        switch (bfile.table1[i].Type)
                        {
                            case 1:
                                {
                                    re = b.ReadByte();
                                    cmp.Neighbours.Add((int)re);
                                }
                                break;
                            case 257:
                                {
                                    re += b.ReadByte();
                                    cmp.Neighbours.Add((int)re);
                                }
                                break;
                            default:
                                throw new Exception("Binary reader collision Nei type error " + bfile.table1[i].Type);
                        }
                    }
                }
//                         #endregion

//                         #region [CoFac_b0p0]
                if (bname.StartsWith("[CoFac_b"))
                {
                    mode = "Reading collision faces";
                
                    CollisionMeshPart cmp = colmesh.Blocks[colmesh.Blocks.Count - 1].Parts[colmesh.CurrentPart];
                    int j = bfile.table1[i].NumberOfRecords;
                    re = 0;
                    for (int ii = 0; ii < j; ii++)
                    {
                        switch (bfile.table1[i].Type)
                        {
                            case 1:
                                {
                                    re = b.ReadByte();
                                    cmp.Faces.Add((short)re);
                                    re = b.ReadByte();
                                    cmp.Faces.Add((short)re);
                                    re = b.ReadByte();
                                    cmp.Faces.Add((short)re);
                                }
                                break;
                            case 257:
                                {
                                    re += b.ReadByte();
                                    cmp.Faces.Add((short)re);
                                    re += b.ReadByte();
                                    cmp.Faces.Add((short)re);
                                    re += b.ReadByte();
                                    cmp.Faces.Add((short)re);
                                }
                                break;
                            default:
                                throw new Exception("Binary reader collision Nei type error " + bfile.table1[i].Type);
                        }
                    }
                    colmesh.CurrentPart++;
                    if (colmesh.CurrentPart == colmesh.Blocks[colmesh.CurrentBlock].NParts)
                    {
                        colmesh.CurrentPart = 0;
                        colmesh.CurrentBlock++;

                    }
                    block_start = String.Format("[CoCommon_b{0}]", colmesh.CurrentBlock);
                    part_start = String.Format("[CoCommon_b{0}p{1}]", colmesh.CurrentBlock, colmesh.CurrentPart);
                }
//                         #endregion
#endif


#if ENABLE_LOD
//                         #region Lod materials
                if ((bname.StartsWith("[LOD")) && (bname.Contains("Materials")))
                {
                    mode = "Reading lod materials";
                
                    CurrentAnimationFrame = -1;
                    if (Animated)
                    {
                        lod = new Lod(Animated, FrameCount);
                    }
                    else
                        lod = new Lod();

                    int type = bfile.table1[i].Type;
                    sr = bfile.table1[i].StartRecord;
                    for (int j = 0; j < bfile.table1[i].NumberOfRecords; j++)
                    {
                        int ss = bfile.subsection_size[sr++];
                        String mat = ReadString2(b, ss);
                        if (mat == null)
                        {
                            Material m = Materials[Lods.Count];
                            lod.Materials.Add(m);
                        }
                        else
                        {
                            Material m = new Material(mat, dir);
                            lod.Materials.Add(m);
                        }
                    }
                    Lods.Add(lod);
                }
//                         #endregion

//                         #region Lod face groups
                if ((bname.StartsWith("[LOD")) && (bname.Contains("FaceGroups")))
                {
                    mode = "Reading LOD face groups";
                
                    FaceGroup f;
                    CurrentAnimationFrame = -1;
                    int j = bfile.table1[i].NumberOfRecords;
                    int type = bfile.table1[i].Type;
                    int firstrecord = bfile.table1[i].StartRecord;
                    int size = bfile.subsection_size[firstrecord++];
                    if (size == 2)
                    {
                        re = b.ReadByte();
                        lod.VertexCount = re;
                        re += b.ReadByte();
                        lod.FaceCount = re;
                    }
                    else if (size == 4)
                    {
                        se += b.ReadInt16();
                        lod.VertexCount = se;// b.ReadInt16();
                        se += b.ReadInt16();
                        lod.FaceCount = se;// lod.VertexCount + b.ReadInt16();
                    }
                    else
                    {
                        throw new NotSupportedException("FaceGroups start record is larger than 2 bytes");
                    }
                    lod.Indices = new short[3 * lod.FaceCount];
                    lod.Verts = new VertexPositionNormalTexture[lod.VertexCount];
                    j--;
                    se = (short)lod.FaceCount;
                    while (j > 0)
                    {
                        f = new FaceGroup();

                        size = bfile.subsection_size[firstrecord++];
                        switch (size)
                        {
                            case 5:
                                {
                                    re += b.ReadByte();
                                    f.Material = re;
                                    re += b.ReadByte();
                                    f.StartVertex = re;
                                    re += b.ReadByte();
                                    f.VertexCount = re;
                                    re += b.ReadByte();
                                    f.StartFace = re;
                                    re += b.ReadByte();
                                    f.FaceCount = re;
                                }
                                break;
                            case 6:
                                {
                                    if (bfile.table1[i].Type == 1)
                                    {
                                        f.Material = b.ReadByte();
                                        f.StartVertex = b.ReadByte();
                                        f.VertexCount = b.ReadByte();
                                        f.StartFace = b.ReadByte();
                                        f.FaceCount = b.ReadByte();
                                        re = b.ReadByte();
                                    }
                                    else
                                    {
                                        re += b.ReadByte();
                                        f.Material = re;
                                        re += b.ReadByte();
                                        f.StartVertex = re;
                                        re += b.ReadByte();
                                        f.VertexCount = re;
                                        re += b.ReadByte();
                                        f.StartFace = re;
                                        re += b.ReadByte();
                                        f.FaceCount = re;
                                        re += b.ReadByte();     // not sure what this byte is for
                                    }
                                }
                                break;

                            case 10:
                                {
                                    se += b.ReadInt16();
                                    f.Material = se;
                                    se += b.ReadInt16();
                                    f.StartVertex = se;
                                    se += b.ReadInt16();
                                    f.VertexCount = se;
                                    se += b.ReadInt16();
                                    f.StartFace = se;
                                    se += b.ReadInt16();
                                    f.FaceCount = se;
                                }
                                break;
                            case 12:
                                {

                                    se += b.ReadInt16();
                                    f.Material = se;
                                    se += b.ReadInt16();
                                    f.StartVertex = se;
                                    se += b.ReadInt16();
                                    f.VertexCount = se;
                                    se += b.ReadInt16();
                                    f.StartFace = se;
                                    se += b.ReadInt16();
                                    f.FaceCount = se;
                                    se += b.ReadInt16();

                                }
                                break;
                            default:
                                {
                                    Debug.WriteLine(bfile.table1[i].Type);
                                    throw new NotSupportedException("FaceGroups unknown size");
                                }
                        }

                        lod.FaceGroups.Add(f);
                        j--;
                    }
                }
//                         #endregion

//                         #region Lod vertices
                if ((bname.StartsWith("[LOD")) && (bname.Contains("_Vertices_Frame")))
                {
                    mode = "Reading lod vertices";
                
                    CurrentAnimationFrame++;
                    int j = bfile.table1[i].NumberOfRecords;
                    int firstrecord = bfile.table1[i].StartRecord;
                    int size = bfile.subsection_size[firstrecord++];
                    switch (size)
                    {
                        case 24:
                            {
                                for (int ii = 0; ii < j; ii++)
                                {
                                    VertexPositionNormalTexture vp;
                                    vp.Position.x = b.ReadSingle();
                                    vp.Position.y = b.ReadSingle();
                                    vp.Position.z = b.ReadSingle();
                                    vp.Normal.x = b.ReadSingle();
                                    vp.Normal.y = b.ReadSingle();
                                    vp.Normal.z = b.ReadSingle();
                                    if (Animated)
                                    {
                                        lod.animation_frames[CurrentAnimationFrame].Add(vp);
                                    }
                                    else
                                    {
                                        lod.Verts[ii] = vp;
                                    }
                                }
                            }
                            break;
                        case 18:
                            {
                                for (int ii = 0; ii < j; ii++)
                                {
                                    VertexPositionNormalTexture vp;
                                    vp.Position.x = ReadTriple(b);
                                    vp.Position.y = ReadTriple(b);
                                    vp.Position.z = ReadTriple(b);
                                    vp.Normal.x = ReadTriple(b);
                                    vp.Normal.y = ReadTriple(b);
                                    vp.Normal.z = ReadTriple(b);
                                    if (Animated)
                                    {
                                        lod.animation_frames[CurrentAnimationFrame].Add(vp);
                                    }
                                    else
                                    {
                                        lod.Verts[ii] = vp;
                                    }
                                }
                            }
                            break;
                        default:
                            throw new Exception("Unhandled vertex size " + size);
                    }
                }
//                         #endregion

//                         #region Lod UVS
                if ((bname.StartsWith("[LOD")) && (bname.Contains("MaterialMapping")))
                {
                    mode = "Reading lod material mapping";
                
                    CurrentAnimationFrame = -1;
                    int j = bfile.table1[i].NumberOfRecords;
                    byte vre = 0;
                    for (int ii = 0; ii < j; ii++)
                    {
                        switch (bfile.table1[i].Type)
                        {
                            case 257:
                                {
                                    vre += b.ReadByte();
                                    lod.Verts[ii].TextureCoordinate.x = (float)vre;
                                    vre += b.ReadByte();
                                    lod.Verts[ii].TextureCoordinate.y = (float)vre;
                                }
                                break;
                            case 3:
                                {
                                    lod.Verts[ii].TextureCoordinate.x = ReadTriple(b);
                                    lod.Verts[ii].TextureCoordinate.y = ReadTriple(b);
                                }
                                break;
                            case 4:
                                {
                                    lod.Verts[ii].TextureCoordinate.x = b.ReadSingle();
                                    lod.Verts[ii].TextureCoordinate.y = b.ReadSingle();
                                }
                                break;
                            default:
                                throw new Exception("MaterialMapping size not supported " + bfile.table1[i].Type);

                        }
                    }
                }
//                         #endregion

//                         #region Lod faces
                if ((bname.StartsWith("[LOD")) && (bname.Contains("_Faces")))
                {
                    mode = "Reading lod faces";
                
                    int j = bfile.table1[i].NumberOfRecords;
                    switch (bfile.table1[i].Type)
                    {
                        case 1:
                        case 0x0101:
                            {
                                re = 0;
                                int k = 0;
                                for (int ii = 0; ii < j; ii++)
                                {
                                    re += b.ReadByte();
                                    lod.Indices[k++] = (short)re;
                                    re += b.ReadByte();
                                    lod.Indices[k++] = (short)re;
                                    re += b.ReadByte();
                                    lod.Indices[k++] = (short)re;
                                }
                            }
                            break;
                        case 258:
                        case 0x02:
                            {
                                se = 0;
                                int k = 0;
                                for (int jj = 0; jj < bfile.table1[i].NumberOfRecords; jj++)
                                {
                                    se += b.ReadInt16();
                                    lod.Indices[k++] = se;
                                    se += b.ReadInt16();
                                    lod.Indices[k++] = se;
                                    se += b.ReadInt16();
                                    lod.Indices[k++] = se;
                                }
                            }
                            break;


                        default:
                            throw new Exception("Unhandled face size " + bfile.table1[i].Type);
                    }
                }
//                         #endregion

//                         #region Lod shadow verts
                if ((bname.StartsWith("[LOD")) && (bname.Contains("ShVertices_Frame")))
                {
                    mode = "Reading lod shadow verts";
                
                    CurrentAnimationFrame++;
                    int j = bfile.table1[i].NumberOfRecords;
                    VertexPositionColor v = new VertexPositionColor();
                    for (int ii = 0; ii < j; ii++)
                    {
                        switch (bfile.table1[i].Type)
                        {
                            case 3:
                                {
                                    int a1 = b.ReadByte();
                                    int a2 = b.ReadByte();
                                    int a3 = b.ReadByte();
                                    int a4 = 0;
                                    if ((a3 & 128) != 0)
                                        a4 = 255;
                                    a3 = (a4 << 24) + (a3 << 16) + (a2 << 8) + a1;
                                    v.Position.x = (float)(a3 / 65536.0f);

                                    a1 = b.ReadByte();
                                    a2 = b.ReadByte();
                                    a3 = b.ReadByte();
                                    a4 = 0;
                                    if ((a3 & 128) != 0)
                                        a4 = 255;
                                    a3 = (a4 << 24) + (a3 << 16) + (a2 << 8) + a1;
                                    v.Position.y = (float)(a3 / 65536.0f);

                                    a1 = b.ReadByte();
                                    a2 = b.ReadByte();
                                    a3 = b.ReadByte();
                                    a4 = 0;
                                    if ((a3 & 128) != 0)
                                        a4 = 255;
                                    a3 = (a4 << 24) + (a3 << 16) + (a2 << 8) + a1;
                                    v.Position.z = (float)(a3 / 65536.0f);
                                }
                                break;
                            default:
                                throw new Exception("Unhandled shadow vert type");
                        }
                        v.Color = Microsoft.Xna.Framework.Color.White;
                        if (Animated)
                        {
                            lod.animation_shadow_frames[CurrentAnimationFrame].Add(v);
                        }
                        else
                            lod.ShadowVerts.Add(v);
                    }
                    lod.ShadowArray = lod.ShadowVerts.ToArray();
                }
//                         #endregion

//                         #region Lod shadow faces
                if ((bname.StartsWith("[LOD")) && (bname.Contains("ShFaces")))
                {
                    mode = "Reading lod shadow faces";
                
                    int j = bfile.table1[i].NumberOfRecords;
                    switch (bfile.table1[i].Type)
                    {
                        case 1:
                        case 0x0101:
                            {
                                re = 0;
                                for (int ii = 0; ii < j; ii++)
                                {
                                    re += b.ReadByte();
                                    lod.ShadowIndices.Add((short)re);
                                    re += b.ReadByte();
                                    lod.ShadowIndices.Add((short)re);
                                    re += b.ReadByte();
                                    lod.ShadowIndices.Add((short)re);
                                }
                            }
                            break;
                        case 258:
                        case 0x02:
                            {
                                for (int jj = 0; jj < bfile.table1[i].NumberOfRecords; jj++)
                                {
                                    se += b.ReadInt16();
                                    lod.ShadowIndices.Add((short)se);
                                    se += b.ReadInt16();
                                    lod.ShadowIndices.Add((short)se);
                                    se += b.ReadInt16();
                                    lod.ShadowIndices.Add((short)se);
                                }
                            }
                            break;
                        default:
                            throw new Exception("Shadow faces unhandled size " + bfile.table1[i].Type);
                    }
                    lod.ShadowIndicesArray = lod.ShadowIndices.ToArray();
                }
//                         #endregion
#endif
            }
#endif
            if (LodDistances.empty())
            {
                LodDistances.push_back(12000);
            }
//                     #endregion
        }
#if 0
        catch (Exception e)
        {
            MSHLoaderErrorHandler msh = new MSHLoaderErrorHandler();
            msh.SetMode(1);
            msh.SetErrorText(e.ToString());
            msh.SetData(mode);
            msh.SetErrorDescription(bname);
            msh.SetFilename(name);
            msh.ShowDialog();
        }
#endif
    }
}
#endif


} // namespace il2ge::mesh
