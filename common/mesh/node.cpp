/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "node.h"
#include "collision_node.h"

namespace il2ge::mesh
{

  Node::Node(String name, NodeType type) : Name(name), Type(type)
  {
  }


  Node::~Node()
  {
  }


  Node &Node::addChild(std::unique_ptr<Node> child)
  {
    child->parent = this;
    children.push_back(std::move(child));
    return *children.back();
  }


  void Node::invalidateTransform()
  {
    tranform_needs_refresh = true;
    for (auto &child : children)
      child->invalidateTransform();
  }


  void Node::doRefreshTransform()
  {
    transform = (parent ? parent->getTransform() : Mat4(1)) *
                base_transform * glm::translate(Mat4(1.0), pos) * rotation;
    tranform_needs_refresh = false;
  }


  void Node::setTransform(const Vec3 &pos, const Mat4 &rotation)
  {
    this->pos = pos;
    this->rotation = rotation;
    invalidateTransform();
  }


  void Node::setRotation(const Mat4 &rotation)
  {
    this->rotation = rotation;
    invalidateTransform();
  }


  void Node::setBaseTransform(const Mat4 &transform)
  {
    this->base_transform = transform;
    invalidateTransform();
  }

#if 0
  Node::Node(String data)
  {
    Name = data.TrimStart('[');
    Name = Name.TrimEnd(']');
    Hidden = false;
    Seperable = false;
    originalHidden = false;
    world = Matrix.Identity;
    base_matrix = Matrix.Identity;
    Damage = 0;
  }

  Node::Node()
  {
    Hidden = false;
    Seperable = false;
    world = Matrix.Identity;
  }

  std::unique_ptr<Node> Node::CopyNode(const Node &copy)
  {
    if (copy is MeshNode)
    {
        MeshNode mn = new MeshNode((MeshNode)copy);
        return mn;
    }
    Node n2 = new Node();
    n2.Name = copy.Name;
    n2.Hidden = copy.Hidden;
    n2.Seperable = copy.Seperable;
    n2.world = Matrix.Identity * copy.world; 

    n2.Type = copy.Type;
    foreach (Node n in copy.children)
    {
        n2.children.Add(CopyNode(n));
    }
    foreach (CollisionNode b in copy.Colliders)
    {
        n2.Colliders.Add(b);
    }
    return n2;
  }
#endif

}
