﻿/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MESH_MESH_NODE_H
#define IL2GE_MESH_MESH_NODE_H

#include "node.h"
#include <il2ge/renderer.h>


namespace il2ge::mesh
{
  class Mesh;

  class MeshNode : public Node, public MeshInstanceBase
  {
    std::shared_ptr<const Mesh> m_mesh;
    std::vector<int> m_material_mapping;

  public:
    renderer::RenderGroupID m_render_group = renderer::RenderGroupID::DEFAULT;

    MeshNode(String name, std::shared_ptr<const Mesh>,
             const std::vector<int>& material_mapping);
    const Mesh &getMesh() { return *m_mesh; }

    const std::shared_ptr<const Mesh>& getMeshPtr() { return m_mesh; }

    renderer::RenderGroupID getRenderGroup() { return m_render_group; }

    bool hasMesh()
    {
      return m_mesh != nullptr;
    }

    const std::vector<int>& getMaterialMapping();

    void materialsChanged(const MaterialList&);
  };
}

#endif
