﻿/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MESH_COLLISION_NODE_H
#define IL2GE_MESH_COLLISION_NODE_H

#include "node.h"

namespace il2ge::mesh
{
    class BoundingSphere;

    enum class CollisionNodeType
    {
        Sphere,
        Mesh,
        CollisionNodeTypes
    };


    class CollisionNode : public Node
    {
        CollisionNodeType nType = CollisionNodeType::Sphere;
//         std::unique_ptr<BoundingSphere> Sphere;

    public:
        CollisionNode(String data);
//         void Serialise(TextWriter tw);
    };
}

#endif
