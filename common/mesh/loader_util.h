/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MESH_LOADER_UTIL_H
#define IL2GE_MESH_LOADER_UTIL_H

#include <log.h>

#include <glm/glm.hpp>
#include <vector>

namespace il2ge::mesh::loader_util
{


inline double parseFloat(const std::string &s)
{
  try
  {
    return stod(s);
  }
  catch (std::exception &e)
  {
    LOG_ERROR << "Failed to parse float: " << s << std::endl;
    throw;
  }
}


template <typename T>
T parse(const std::string &s);


template <>
inline float parse<float>(const std::string &s)
{
  return parseFloat(s);
}


template <>
inline int parse<int>(const std::string &s)
{
  return stoi(s);
}


inline glm::mat4 loadTransformationMatrix(const std::vector<std::string> &values)
{
  assert(values.size() >= 12);

  glm::mat4 matrix(1.0);

  int i = 0;

  for (int col = 0; col < 4; col++)
  {
    for (int row = 0; row < 3; row++)
    {
      matrix[col][row] = parseFloat(values.at(i));
      i++;
    }
  }

  return matrix;
}


}

#endif
