/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MESH_HOOK_H
#define IL2GE_MESH_HOOK_H

#include <glm/glm.hpp>


namespace il2ge::mesh
{


class Hook
{
    std::string Name;
//     BoundingSphere bs;

    bool selected = false;

public:
    Vector<glm::mat4> locs; // one for each animation frame

    Hook(std::string name) : Name(name) {}
//     Draw(Matrix world, Matrix view, Matrix projection);
//     bool Hit(Ray ray);

    const glm::mat4 &getLoc(size_t frame) const
    {
      return locs.at(frame);
    }

    const std::string &getName() const { return Name; }
};


}

#endif
