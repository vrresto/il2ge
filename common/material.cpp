/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <il2ge/material.h>
#include <il2ge/renderer.h>
#include <util.h>

using namespace std;


namespace
{


std::vector<char> readFile(std::string path)
{
  try
  {
    return util::readFile<char>(path);
  }
  catch (std::exception& e)
  {
    throw il2ge::FileReadError(path, e.what());
  }
};


} // namespace


namespace il2ge
{


constexpr int MAX_LAYERS = 8;


void MaterialBase::applyParameters(ParameterFiles &files, std::string path)
{
  LOG_DEBUG << "path: " << path << std::endl;

  auto dir = util::getDirFromPath(path);
  assert(!dir.empty());
  auto &params = files.get(path);

  if (params.hasSection("ClassInfo"))
  {
    auto &class_info = params.getSection("ClassInfo");

    std::string based_on;
    class_info.get_noexcept("BasedOn", based_on);

    if (!based_on.empty())
      applyParameters(files, dir + '/' + based_on);
  }

  bool tfDepthOffset = false;

  if (params.hasSection("General"))
  {
    auto &general = params.getSection("General");

    #define GET_PARAMETER(p) { general.get_noexcept(#p, this->p); }
    GET_PARAMETER(tfDoubleSide);
    GET_PARAMETER(tfShouldSort);
    GET_PARAMETER(tfDepthOffset);
    #undef GET_PARAMETER

    general.get_noexcept("tfDepthOffset", tfDepthOffset);
  }

  if (params.hasSection("LightParams"))
  {
    auto &section = params.getSection("LightParams");

    #define GET_PARAMETER(p) { section.get_noexcept(#p, light_params.p); }
    GET_PARAMETER(Ambient);
    GET_PARAMETER(Diffuse);
    GET_PARAMETER(Specular);
    GET_PARAMETER(SpecularPow);
    GET_PARAMETER(Shine);
    #undef GET_PARAMETER
  }

  for (size_t i = 0; i < MAX_LAYERS; i++)
  {
    string layer_name = "Layer" + to_string(i);

    if (params.hasSection(layer_name))
    {
      auto &section = params.getSection(layer_name.c_str());

      if (m_layers.size() < (i+1))
        m_layers.resize(i+1);

      assert(m_layers.size() > i);
      auto &layer = m_layers.at(i);

      string texture_name;
      section.get_noexcept("TextureName", texture_name);

      layer.tfDepthOffset = tfDepthOffset;

      #define GET_PARAMETER(p) { section.get_noexcept(#p, layer.p); }
      GET_PARAMETER(tfTestZ);
      GET_PARAMETER(tfTestZEqual);
      GET_PARAMETER(tfBlend);
      GET_PARAMETER(tfBlendAdd);
      GET_PARAMETER(tfNoTexture);
      GET_PARAMETER(tfNoWriteZ);
      GET_PARAMETER(AlphaTestVal);
      GET_PARAMETER(tfTestA);
      GET_PARAMETER(ColorScale);
      GET_PARAMETER(tfDepthOffset);
      GET_PARAMETER(TextureCoordScale);
      GET_PARAMETER(tfWrapX);
      GET_PARAMETER(tfWrapY);
      GET_PARAMETER(VisibleDistanceNear);
      GET_PARAMETER(VisibleDistanceFar);
      #undef GET_PARAMETER

      if (!texture_name.empty())
        layer.texture_path = util::resolveRelativePathComponents(dir + '/' + texture_name);
    }
  }
}


Material::Material(const Material& other) : MaterialBase(other)
{
  assert(other.m_renderer_data);
  m_renderer_data = std::make_unique<renderer::MaterialRendererData>(*this, *other.m_renderer_data);
}


Material::Material(std::string path, const GetTexture& get_texture)
{
  LOG_INFO << "Loading material: " << path << std::endl;
  ParameterFiles files(readFile);
  applyParameters(files, path);
  m_renderer_data = std::make_unique<renderer::MaterialRendererData>(*this, get_texture);
}


Material::Material(ParameterFiles& files, std::string path, const GetTexture& get_texture)
{
  LOG_INFO << "Loading material: " << path << std::endl;
  applyParameters(files, path);
  m_renderer_data = std::make_unique<renderer::MaterialRendererData>(*this, get_texture);
}


Material::~Material()
{
}


void Material::refreshRendererData(const GetTexture& get_texture)
{
  m_renderer_data = std::make_unique<renderer::MaterialRendererData>(*this, get_texture);
}


} // namespace il2ge
