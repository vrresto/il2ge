/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <il2ge/base_map_resources.h>
#include <il2ge/map_properties.h>
#include <il2ge/map_loader.h>
#include <render_util/image_loader.h>
#include <render_util/image_util.h>
#include <log.h>

#include <filesystem>


using namespace il2ge;
using namespace render_util;


namespace
{


constexpr auto BASE_DIR = "il2ge_base_map/";


std::string getIniPath(std::string name)
{
  return BASE_DIR + name + ".properties";
}


std::vector<BaseMapResources::LandCoverType> createLandCoverTypes()
{
  std::vector<BaseMapResources::LandCoverType> types;

  auto add_land_use_multi = [&] (std::string name,
                                 std::vector<std::string> values,
                                 int variants = 1)
  {
    types.push_back({ "landuse_" + name, "landuse", values, variants });
  };

  auto add_land_use = [&] (std::string field_value, int variants = 1)
  {
    add_land_use_multi(field_value, { field_value }, variants);
  };

  auto add_natural_multi = [&] (std::string name,
                                std::vector<std::string> values,
                                int variants = 1)
  {
    types.push_back({ "natural_" + name, "natural", values, variants });
  };

  auto add_natural = [&] (std::string field_value, int variants = 1)
  {
    add_natural_multi(field_value, { field_value }, variants);
  };

  add_natural_multi("rock", { "bare_rock", "cliff", "scree" });
  add_natural("wood");
  add_natural("glacier");
  add_natural("wetland");
  add_natural("mud");
  add_natural("beach");
  add_natural("tundra");
  add_natural("grassland");
  add_natural("heath");
  add_natural("scrub");

  add_land_use_multi("meadow", { "meadow", "orchard", "vineyard", "grass" });
  add_land_use("farmland", 4);
  add_land_use("farmyard");
  add_land_use("forest");
  add_land_use("industrial");
  add_land_use_multi("houses", { "commercial", "residential", "retail" });

  add_natural("water");

  return types;
}


std::vector<std::string> createLandCoverTypeNames()
{
  std::vector<std::string> names;
  for (auto& type : BaseMapResources::getLandCoverTypes())
  {
    if (type.variants > 1)
    {
      for (int i = 0; i < type.variants; i++)
      {
        names.push_back(type.name + std::to_string(i));
      }
    }
    else
    {
      names.push_back(type.name);
    }
  }
  return names;
}


} // namespace


namespace il2ge
{


BaseMapResources::BaseMapResources(std::string name) : m_name(name)
{
  m_ini_path = getIniPath(m_name);

  if (!std::filesystem::exists(m_ini_path))
    throw std::runtime_error(m_ini_path + " not found");

  m_ini = std::make_unique<INIReader>(m_ini_path);
  if (m_ini->ParseError())
  {
    throw std::runtime_error("Error parsing " + m_ini_path
                              + " at line " + std::to_string(m_ini->ParseError()));
  }

  m_map_size_px.x = m_ini->GetInteger("", "Width", 0);
  m_map_size_px.y = m_ini->GetInteger("", "Height", 0);

  if (m_map_size_px.x < 1 || m_map_size_px.y < 1)
  {
    LOG_ERROR << m_ini_path << ": Invalid size: " << m_map_size_px << std::endl;
    throw std::runtime_error("Invalid size");
  }
}


std::unique_ptr<MapGeography> BaseMapResources::getMapGeography()
{
  return loadMapGeography(*m_ini, m_ini_path);
}


std::shared_ptr<ElevationMap> BaseMapResources::getElevationMap()
{
  auto data_path = BASE_DIR + m_name + ".data";
  LOG_INFO << "Opening " << data_path << std::endl;
//   try
//   {
    util::NormalFile file(data_path);
    return map_loader::createBaseElevationMap(file, m_map_size_px);
//   }
//   catch (std::exhimception &e)
//   {
//     LOG_ERROR << e.what() << std::endl;
//     return {};
//   }
}


std::shared_ptr<ImageGreyScale> BaseMapResources::getMap(std::string name)
{
  auto path = BASE_DIR + m_name + "." + name + ".tga";

  auto data = util::readFile<char>(path);

  auto image = loadImageFromMemory<ImageGreyScale>(data);
  image::flipYInPlace(image);
  return image;
}


bool BaseMapResources::exists(std::string name)
{
  auto properties_path = BASE_DIR  + name + ".properties";
  return std::filesystem::exists(getIniPath(name));
}


const std::vector<BaseMapResources::LandCoverType>& BaseMapResources::getLandCoverTypes()
{
  static auto types = createLandCoverTypes();
  return types;
}


const std::vector<std::string>& BaseMapResources::getLandCoverTypeNames()
{
  static auto names = createLandCoverTypeNames();
  return names;
}


} // namespace il2ge
