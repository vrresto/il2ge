#pragma once

#include "renderer_p.h"
#include <il2ge/renderer.h>
#include <util/non_copyable.h>

#include <utility>


namespace il2ge::renderer
{


class UnsortedRenderList
{
  template <class Key, class Item>
  struct List : public util::NonCopyable
  {
    std::vector<Item> m_items;
    std::unordered_map<Key, size_t> m_map;

    List() {}

    List(List&& other)
    {
      *this = std::move(other);
    }

    List& operator=(List&& other)
    {
      m_items = std::move(other.m_items);
      m_map = std::move(other.m_map);
      return *this;
    }

    auto size() { return m_items.size(); }
    bool empty() const { return m_items.empty(); }
    auto begin() { return m_items.begin(); }
    auto end() { return m_items.end(); }

    template <class T>
    void visit(T visitor)
    {
      for (auto& item : *this)
      {
        item.visit(visitor);
      }
    }
  };

  struct Batch
  {
    render_util::IndexedVAOBase* vao {};
    std::vector<DrawElementsIndirectCommand> commands;
  };

  struct Instance
  {
    glm::mat4 model_view {};
    size_t frame = 0;

    Instance(const glm::mat4& model_view, int frame) :
      model_view(model_view), frame(frame) {}
  };

  struct FaceGroupItem : public util::NonCopyable
  {
    mesh::FaceGroup fg;
    std::vector<Instance> instances;
    int base_instance = 0;
  };

  struct MeshItem : public util::NonCopyable
  {
    std::unordered_map<int, FaceGroupItem> m_face_groups;

    MeshItem(MeshRendererData* mesh) : m_mesh(mesh)
    {
      m_mesh->ref();
    }

    MeshItem(MeshItem&& other)
    {
      m_face_groups = std::move(other.m_face_groups);
      m_mesh = std::exchange(other.m_mesh, nullptr);
    }

    ~MeshItem()
    {
      if (m_mesh)
        m_mesh->unref();
    }

    void fillBatch(Batch& batch);

    template <class T>
    void visit(T visitor)
    {
      for (auto& it : m_face_groups)
      {
        if (!it.second.instances.empty())
        {
          visitor(*this, it.second);
        }
      }
    }

    MeshRendererData* getMesh() { return m_mesh; }

  private:
    MeshRendererData* m_mesh {};
  };


  struct MeshList : public List<MeshRendererData*, MeshItem>
  {
    void add(MeshRendererData*, int face_group, const glm::mat4&, int frame);
    void clear();
  };

  struct SharedVAOItem : public util::NonCopyable
  {
    MeshList m_meshes;

    SharedVAOItem(const std::shared_ptr<SharedVAO>& vao) : m_vao(vao) {}

    SharedVAOItem(SharedVAOItem&& other)
    {
      m_meshes = std::move(other.m_meshes);
      m_vao = std::move(other.m_vao);
    }

    void fillBatch(Batch& batch);

    template <class T>
    void visit(T visitor)
    {
      for (auto& item : m_meshes)
      {
        for (auto& it : item.m_face_groups)
        {
          if (!it.second.instances.empty())
          {
            visitor(item, it.second);
          }
        }
      }
    }

  private:
    std::shared_ptr<SharedVAO> m_vao;
  };

  struct SharedVAOList : public List<SharedVAO*, SharedVAOItem>
  {
    void add(MeshRendererData*, int face_group, const glm::mat4&, int frame);
    void clear();
  };

  ShaderManager& m_shader_manager;
  std::string m_material_path;
  std::shared_ptr<const Material> m_material;

  size_t m_max_instance_buffer_size = 0;
  unsigned m_instance_buffer_id = 0;
  MeshList m_meshes;
  SharedVAOList m_shared_vaos;

  unsigned m_particle_instance_buffer_id = 0;
  size_t m_max_particle_instance_buffer_size = 0;
  std::vector<std::vector<ParticleInstance>> m_particles;
  std::unique_ptr<render_util::VertexArrayObject> m_quad_vao;

  void updateInstanceBuffer();
  void createBatches(std::vector<Batch>&);
  void renderLegacy();
  void renderVAO();

  void updateParticleInstanceBuffer();

public:
  glm::vec3 m_color {}; // for debugging

  UnsortedRenderList(std::string material_path,
                     std::shared_ptr<const Material>,
                     ShaderManager&);
  ~UnsortedRenderList();

  void render();
  void renderParticles();
  void clear();

  void add(std::vector<ParticleInstance>&&);

  void add(MeshRendererData*, int face_group,
           const glm::mat4& model_view, float distance, int frame);

  bool isEmpty() const
  {
    return m_shared_vaos.empty() && m_meshes.empty();
  }

  const Material& getMaterial()
  {
    return *m_material;
  }

  const std::string& getMaterialPath()
  {
    return m_material_path;
  }
};


} // namespace il2ge::renderer
