#pragma once

#include "renderer_p.h"


namespace il2ge::renderer
{


class SharedVAO
{
  std::vector<mesh::VertexPositionNormalTexture> m_vertices;
  std::vector<mesh::Index> m_indices;
  std::unique_ptr<render_util::IndexedVAOBase> m_vao;

  int m_unused_vertices = 0;
  int m_unused_indices = 0;
  bool m_dirty = false;

public:
  render_util::IndexedVAOBase& getVAO()
  {
    if (!m_vao || m_dirty)
      create();

    assert(m_vao);
    return *m_vao;
  }

  void reserve(const std::vector<MeshRendererData*>& meshes)
  {
    int vertices_count = 0;
    int indices_count = 0;
    for (auto& mesh : meshes)
    {
      auto& lod_mesh = mesh->getLodMesh();
      vertices_count += lod_mesh.getVertices().size();
      indices_count += lod_mesh.getIndices().size();
    }

    m_vertices.reserve(vertices_count);
    m_indices.reserve(indices_count);
  }

  void addMesh(const MeshRendererData& mesh, int& start_vertex, int& start_index)
  {
    start_vertex = m_vertices.size();
    start_index = m_indices.size();

    auto& lod_mesh = mesh.getLodMesh();
    auto& vertices_in = lod_mesh.getVertices();
    auto& indices_in = lod_mesh.getIndices();

    m_vertices.reserve(m_vertices.size() + vertices_in.size());
    m_vertices.insert(m_vertices.end(), vertices_in.begin(), vertices_in.end());

    m_indices.reserve(m_indices.size() + indices_in.size());
    m_indices.insert(m_indices.end(), indices_in.begin(), indices_in.end());

    m_dirty = true;

    // LOG_DEBUG << "Shared VAO size: " << getSizeBytes() / 1024.f << " kb" << std::endl;
  };

  void removeMesh(const MeshRendererData& mesh)
  {
    auto& lod_mesh = mesh.getLodMesh();

    m_unused_vertices += lod_mesh.getVertices().size();
    m_unused_indices += lod_mesh.getIndices().size();
  }

  int getUnusedBytes()
  {
    return m_unused_vertices * sizeof(mesh::VertexPositionNormalTexture) +
           m_unused_indices * sizeof(mesh::Index);
  }

  int getSizeBytes()
  {
    return m_vertices.size() * sizeof(mesh::VertexPositionNormalTexture) +
           m_indices.size() * sizeof(mesh::Index);
  }

  void create()
  {
    m_vao = createVAO(m_vertices, m_indices);
    m_dirty = false;
  }
};


} // namespace il2ge::renderer
