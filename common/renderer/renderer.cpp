/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "unsorted_render_list.h"
#include "shared_vao.h"
#include "shader_manager.h"
#include "z_sorted_face_render_list.h"
#include "renderer_p.h"
#include <render_util/state.h>
#include <util.h>

#include <glm/gtc/type_ptr.hpp>


using namespace il2ge;
using namespace il2ge::renderer;
using namespace render_util::gl_binding;


namespace
{


inline bool materialNeedsZSort(const Material& mat)
{
  return mat.tfShouldSort;
}


} // namespace


namespace il2ge::renderer
{


// ====== StaticBufferBase ======


StaticBufferBase::StaticBufferBase(const void* data, size_t size)
{
  gl::CreateBuffers(1, &m_id);
  assert(m_id);

  gl::NamedBufferStorage(m_id, size, data, 0);
}


StaticBufferBase::~StaticBufferBase()
{
  gl::DeleteBuffers(1, &m_id);
}


// ====== MaterialRendererData ======


MaterialRendererData::Layer::Layer()
{
  gl::CreateSamplers(1, &sampler_id);
}


MaterialRendererData::Layer::~Layer()
{
  if (sampler_id)
    gl::DeleteSamplers(1, &sampler_id);
}


MaterialRendererData::MaterialRendererData(const Material& material,
                                           const MaterialRendererData& other)
{
  assert(material.getLayers().size() == other.layers.size());

  layers.resize(material.getLayers().size());

  for (int i = 0; i < layers.size(); i++)
  {
    if (material.getLayers().at(i).tfWrapX)
      gl::SamplerParameteri(layers.at(i).sampler_id, GL_TEXTURE_WRAP_S, GL_REPEAT);
    else
      gl::SamplerParameteri(layers.at(i).sampler_id, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);

    if (material.getLayers().at(i).tfWrapY)
      gl::SamplerParameteri(layers.at(i).sampler_id, GL_TEXTURE_WRAP_T, GL_REPEAT);
    else
      gl::SamplerParameteri(layers.at(i).sampler_id, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    layers.at(i).texture_and_size = other.layers.at(i).texture_and_size;
  }
}


MaterialRendererData::MaterialRendererData(const Material& material,
                                           const Material::GetTexture& get_texture)
{
  layers.resize(material.getLayers().size());

  for (int i = 0; i < layers.size(); i++)
  {
    if (material.getLayers().at(i).tfWrapX)
      gl::SamplerParameteri(layers.at(i).sampler_id, GL_TEXTURE_WRAP_S, GL_REPEAT);
    else
      gl::SamplerParameteri(layers.at(i).sampler_id, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);

    if (material.getLayers().at(i).tfWrapY)
      gl::SamplerParameteri(layers.at(i).sampler_id, GL_TEXTURE_WRAP_T, GL_REPEAT);
    else
      gl::SamplerParameteri(layers.at(i).sampler_id, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    try
    {
      layers.at(i).texture_and_size = get_texture(material.getLayers().at(i).texture_path);
    }
    catch (std::exception& e)
    {
      LOG_ERROR << e.what() << std::endl;
      continue;
    }
  }
}


MaterialRendererData::~MaterialRendererData()
{
}


// ====== MeshRendererData ======


MeshRendererData::MeshRendererData(const mesh::MeshBase& mesh, int lod) :
  m_mesh(mesh),
  m_lod(lod),
  m_face_groups(mesh.getLod(lod).getFaceGroups())
{
  auto& lod_mesh = getLodMesh();
  m_vao = createVAO(lod_mesh.getVertices(), lod_mesh.getIndices());
}


MeshRendererData::~MeshRendererData()
{
  assert(m_ref_count == 0);
}


void MeshRendererData::addToSharedVAO(const std::shared_ptr<SharedVAO>& vao)
{
  assert(!m_shared_vao);
  vao->addMesh(*this, m_shared_vao_start_vertex, m_shared_vao_start_index);
  m_shared_vao = vao;
}


const std::vector<mesh::FaceGroup>& MeshRendererData::getFaceGroups()
{
  return m_face_groups;
}


void MeshRendererData::ref()
{
  assert(m_ref_count >= 0);
  m_ref_count++;
}


void MeshRendererData::unref()
{
  assert(m_ref_count >= 1);
  m_ref_count--;
}


// ====== MeshInstanceRenderListCache ======

#if USE_MESH_INSTANCE_RENDER_LIST_CACHE
MeshInstanceRenderListCache::MeshInstanceRenderListCache(const MaterialList& materials) :
  m_material_properties(materials.size())
{
  for (int i = 0; i < m_material_properties.size(); i++)
  {
    auto& props = m_material_properties.at(i);

    if (!materials.has(i))
      continue;

    // auto& material = materials.at(i);

    //FIXME
    // auto &layer = material->getLayers().at(0);

    // if (layer.tfNoTexture)
      // props.no_texture = true;

    // props.needs_sorting = layer.tfBlend || layer.tfBlendAdd;
    // props.needs_sorting = material->tfShouldSort;

    props.is_null = false;
  }
}


void MeshInstanceRenderListCache::materialsChanged(const MaterialList& materials)
{
  assert(m_material_properties.size() == materials.size());
  for (int i = 0; i < m_material_properties.size(); i++)
  {
    auto& props = m_material_properties.at(i);

    if (materials.has(i))
    {
      props.is_null = false;
      if (props.render_list)
      {
        if (&props.render_list->getMaterial() != materials.at(i).get())
        {
          props.render_list.reset();
        }
      }
    }
    else
    {
      props.is_null = true;
      props.render_list.reset();
    }
  }
}


MeshInstanceRenderListCache::MaterialProperties&
MeshInstanceRenderListCache::getMaterialProperties(int index)
{
  return m_material_properties.at(index);
}
#endif

// ====== Renderer ======


Renderer::Renderer(ShaderProgramFactory shader_program_factory)
{
  m_shader_manager = std::make_unique<ShaderManager>();

  m_groups.reserve(util::to_underlying(RenderGroupID::Count));
  while (m_groups.size() < util::to_underlying(RenderGroupID::Count))
  {
    m_groups.push_back(std::make_unique<RenderGroup>(*m_shader_manager, m_camera));
  }

  if (shader_program_factory)
  {
    m_shader_manager->createPrograms(shader_program_factory);
  }
}


Renderer::~Renderer()
{
}


void Renderer::applyCamera(const render_util::Camera3D& camera)
{
  m_camera = camera;
}


void Renderer::setMaxTransparentVisibility(float)
{
}


void Renderer::enableInstancedDrawing(bool enable)
{
  m_shader_manager->enableInstancedDrawing(enable);
}


void Renderer::flushRenderList(std::function<void(render_util::ShaderProgramPtr)> set_uniforms)
{
  bool enable_shaders = true;

  gl::ActiveTexture(GL_TEXTURE0);
  state::Enable(GL_TEXTURE_2D); // FIXME

  //FIXME
  gl::MatrixMode(GL_MODELVIEW);
  gl::LoadIdentity();
  gl::MatrixMode(GL_PROJECTION);
  gl::LoadIdentity();
  gl::MatrixMode(GL_TEXTURE);
  gl::LoadIdentity();
  gl::Color4f(1,1,1,1);
  gl::BindTexture(GL_TEXTURE_2D, 0);

  //FIXME
  gl::MatrixMode(GL_PROJECTION);
  gl::LoadMatrixf(glm::value_ptr(m_camera.getProjectionMatrixFar()));

  {
    render_util::StateModifier state;
    state.setDefaults();
    state.setFrontFace(GL_CCW);
    state.setCullFace(GL_BACK);
    state.enableCullFace(true);
    state.setDepthFunc(GL_LEQUAL);
    state.setBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    state.enableDepthTest(true);

    // m_flush_count++;

    for (auto& group : m_groups)
    {
      group->flush(m_camera, enable_shaders);
    }
  }

  //FIXME
  gl::MatrixMode(GL_MODELVIEW);
  gl::LoadIdentity();
  gl::MatrixMode(GL_PROJECTION);
  gl::LoadIdentity();
  gl::MatrixMode(GL_TEXTURE);
  gl::LoadIdentity();
  gl::Color4f(1,1,1,1);
  gl::BindTexture(GL_TEXTURE_2D, 0);

  state::Disable(GL_TEXTURE_2D); // FIXME
  gl::BindTexture(GL_TEXTURE_2D, 0);
  gl::BindSampler(0, 0); //FIXME
}


void Renderer::addToRenderList(std::vector<ParticleInstance>&& instances,
                               const std::shared_ptr<const Material>& material,
                               float dist)
{
  assert(material);

  // auto& list = getRenderGroup(RenderGroupID::DEFAULT).getUnsortedList(material);
  // list->add(std::move(instances));

  auto& list = getRenderGroup(RenderGroupID::DEFAULT).getZSortedList();
  list.add(std::move(instances), material, dist);
}


void Renderer::addToRenderList(const MaterialMapping& material_mapping,
                      const MaterialList& materials,
                      const std::vector<mesh::Face>& faces,
                      const glm::mat4& model_view,
                      bool z_sort,
                      RenderGroupID group,
                      float dist)
{
  if (z_sort)
  {
    getRenderGroup(group).getZSortedList().add(faces, materials, model_view, dist);
  }
  else
  {
    assert(0);
  }
}


void Renderer::addToRenderList(MeshRendererData* mesh,
                               const MaterialMapping& material_mapping,
                               const MaterialList& materials,
#if USE_MESH_INSTANCE_RENDER_LIST_CACHE
                               MeshInstanceRenderListCache& cache,
#endif
                               const glm::mat4& model_view,
                               float distance,
                               size_t animation_frame,
                               RenderGroupID render_group_id)
{
  assert(!(animation_frame && !mesh->isAnimated()));

  int i_face_group = 0;
  for (auto& fg : mesh->getFaceGroups())
  {
    auto mat_index = material_mapping.empty() ?
      fg.Material : material_mapping.at(fg.Material);
#if USE_MESH_INSTANCE_RENDER_LIST_CACHE
    auto& props = cache.getMaterialProperties(mat_index);

    if (!props.is_null)
    {
      // if (props.needs_sorting)
      {
        // assert(m_z_sorted_face_render_list);
        // m_z_sorted_face_render_list->add(mesh, instance, model_view);
      }
      // else if (props.no_texture)
      // {
      // }
      // else
      {
        if (!props.render_list)
        {
          auto& material = materials.at(mat_index);
          assert(material);
          props.render_list = getRenderGroup(render_group_id).getUnsortedList(material);
        }
        props.render_list->add(mesh, i_face_group, model_view,  distance, animation_frame);
      }
    }
#else
    auto& material = materials.at(mat_index);
    assert(material);
    auto& list = getRenderGroup(render_group_id).getUnsortedList(material);
    list->add(mesh, i_face_group, model_view,  distance, animation_frame);
#endif

    i_face_group++;
  }
}


void Renderer::enableAtmosphere(bool)
{
}


void Renderer::printStats(std::ostream &out)
{
  out << "Active unsorted render lists: " << g_stats.active_render_lists << std::endl;

  out << "Rendered tiles: " << g_stats.rendered_tiles << std::endl;

  out << "Draw calls: "
      << g_stats.unsorted_draw_calls + g_stats.mesh_face_draw_calls + g_stats.particle_draw_calls
      << std::endl;

  out << "Unsorted draw calls: " << g_stats.unsorted_draw_calls << std::endl;
  out << "Mesh face draw calls: " << g_stats.mesh_face_draw_calls << std::endl;
  out << "Particle draw calls: " << g_stats.particle_draw_calls << std::endl;

  out << "VAO changes: " << g_stats.vao_changes << std::endl;
  out << "Material changes: " << g_stats.material_changes << std::endl;
  out << "Program changes: " << g_stats.program_changes << std::endl;
}


std::vector<std::pair<std::string, glm::vec3>> Renderer::getMaterialDebugColors()
{
  std::vector<std::pair<std::string, glm::vec3>> materials;

  // for (auto& list : m_unsorted_render_lists)
  // {
  //   materials.push_back(
  //     {
  //       list->getMaterialPath(),
  //       list->m_color,
  //     });
  // }

  return materials;
}


RenderGroup& Renderer::getRenderGroup(RenderGroupID id)
{
  return *m_groups.at(util::to_underlying(id));
}


// ========================================================


void drawTile(glm::vec2 origin, glm::vec2 extent,
              glm::vec2 texcoord_origin, glm::vec2 texcoord_extent,
              const Material& material)
{
  using namespace glm;

  state::Enable(GL_TEXTURE_2D); // FIXME

  //FIXME
  gl::MatrixMode(GL_MODELVIEW);
  gl::LoadIdentity();
  gl::MatrixMode(GL_PROJECTION);
  gl::LoadIdentity();
  gl::MatrixMode(GL_TEXTURE);
  gl::LoadIdentity();
  gl::Color4f(1,1,1,1);
  gl::BindTexture(GL_TEXTURE_2D, 0);

  {
    auto pos = origin;
    auto size = extent;

    render_util::StateModifier state;
    state.setDefaults();

    applyMaterial(&material, state);

    state.setFrontFace(GL_CW);
    state.enableCullFace(false);
    state.enableDepthTest(false);
    state.enableStencilTest(false);

    assert(gl::IsEnabled(GL_COLOR_LOGIC_OP) == GL_FALSE);

    int viewport[4];
    gl::GetIntegerv(GL_VIEWPORT, viewport);

    vec2 viewport_size(viewport[2], viewport[3]);

    vec2 pos_ndc = pos / viewport_size;
    // pos_ndc.y = 1.f - pos_ndc.y;
    pos_ndc = (2.f * pos_ndc) - vec2(1);

    vec2 size_ndc = 2.f * (size / viewport_size);

    auto color = material.getLayers().at(0).ColorScale;

    gl::Color4f(color.r, color.g, color.b, color.a);

    gl::Begin(GL_QUADS);

    gl::TexCoord2f(texcoord_origin.x, texcoord_origin.y);
    gl::Vertex3f(pos_ndc.x, pos_ndc.y, 0);

    gl::TexCoord2f(texcoord_origin.x + texcoord_extent.x, texcoord_origin.y);
    gl::Vertex3f(pos_ndc.x + size_ndc.x, pos_ndc.y, 0);

    gl::TexCoord2f(texcoord_origin.x + texcoord_extent.x, texcoord_origin.y + texcoord_extent.y);
    gl::Vertex3f(pos_ndc.x + size_ndc.x, pos_ndc.y + size_ndc.y, 0);

    gl::TexCoord2f(texcoord_origin.x, texcoord_origin.y + texcoord_extent.y);
    gl::Vertex3f(pos_ndc.x, pos_ndc.y + size_ndc.y, 0);

    gl::End();
  }

  //FIXME
  gl::MatrixMode(GL_MODELVIEW);
  gl::LoadIdentity();
  gl::MatrixMode(GL_PROJECTION);
  gl::LoadIdentity();
  gl::MatrixMode(GL_TEXTURE);
  gl::LoadIdentity();
  gl::Color4f(1,1,1,1);
  gl::BindTexture(GL_TEXTURE_2D, 0);

  state::Disable(GL_TEXTURE_2D); // FIXME
  gl::BindSampler(0, 0); //FIXME

  g_stats.rendered_tiles++;
}


void clearPerFrameStats()
{
  g_stats = {};
}


std::shared_ptr<SharedVAO> createSharedVAO()
{
  return std::make_shared<SharedVAO>();
}



void addToSharedVAO(const std::shared_ptr<SharedVAO>& vao,
                    const std::vector<MeshRendererData*>& meshes)
{
  vao->reserve(meshes);

  for (auto mesh : meshes)
    mesh->addToSharedVAO(vao);

  LOG_INFO << "Shared VAO size: " << vao->getSizeBytes() / 1024.f << " kb" << std::endl;
}


GlobalStats g_stats {};


} // namespace il2ge::renderer
