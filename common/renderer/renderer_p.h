#pragma once

#include "shader_manager.h"
#include <il2ge/renderer.h>
#include <il2ge/mesh/mesh_base.h>
#include <render_util/vao.h>
#include <render_util/gl_binding/gl_functions.h>


namespace il2ge::renderer
{


constexpr auto INDEX_TYPE = GL_UNSIGNED_SHORT;
constexpr auto INDEX_SIZE = 2;
static_assert(sizeof(il2ge::mesh::Index) == INDEX_SIZE);
static_assert(std::is_same<il2ge::mesh::Index, uint16_t>::value);


struct NonCopyable //FIXME: duplicated in util.h
{
  NonCopyable() {}
  NonCopyable(const NonCopyable&) = delete;
  NonCopyable& operator=(const NonCopyable&) = delete;
};


class StaticBufferBase : public NonCopyable
{
  unsigned m_id {};

public:
  StaticBufferBase(const void* data, size_t size);
  ~StaticBufferBase();

  unsigned getID() { return m_id; }
};


template <class T>
class StaticBuffer : public StaticBufferBase
{
public:
  StaticBuffer(const T* data, size_t size) : StaticBufferBase(data, size * sizeof(T))
  {
  }
};


template <class T>
class DynamicBuffer : public NonCopyable
{
  enum class State
  {
    NEW,
    MAPPABLE,
    MAPPED,
    IN_USE,
  };

  unsigned m_id = 0;
  size_t m_max_size = 0;
  size_t m_current_size = 0;
  T* m_mapped {};
  State m_state = State::NEW;

  bool isMapped() { return m_state == State::MAPPED; }

public:
  DynamicBuffer()
  {
      using namespace render_util::gl_binding;

    gl::CreateBuffers(1, &m_id);
    assert(m_id);
  }

  ~DynamicBuffer()
  {
      using namespace render_util::gl_binding;

    gl::DeleteBuffers(1, &m_id);
  }

  unsigned getID() { return m_id; }
  size_t getSize() { return m_current_size; }

  void reserve(size_t elements)
  {
      using namespace render_util::gl_binding;

    assert(!isMapped());
    assert(!m_mapped);

    m_max_size = glm::max(m_max_size, elements);
    m_current_size = elements;

    gl::NamedBufferData(m_id,
                        m_max_size * sizeof(T),
                        nullptr, GL_STREAM_DRAW);

    m_state = State::MAPPABLE;
  }

  T* getData()
  {
      using namespace render_util::gl_binding;

    assert(m_current_size);
    if (!isMapped())
    {
      assert(m_state == State::MAPPABLE);
      m_mapped = static_cast<T*>(gl::MapNamedBuffer(m_id, GL_WRITE_ONLY));
      m_state = State::MAPPED;
    }
    assert(m_mapped);
    return m_mapped;
  }

  T& at(size_t index)
  {
    assert(index < m_current_size);
    return getData()[index];
  }

  void unmap()
  {
    using namespace render_util::gl_binding;

    if (isMapped())
    {
      m_mapped = {};
      gl::UnmapNamedBuffer(m_id);
      m_state = State::IN_USE;
    }
  }
};


struct DrawElementsIndirectCommand // FIXME: duplicated in render_util/terrain
{
  unsigned count;
  unsigned instanceCount;
  unsigned firstIndex;
  int baseVertex;
  unsigned baseInstance;
};


struct InstanceData
{
  glm::mat4 model_view;
};


class GlobalStats
{
public:
  int rendered_tiles;
  int active_render_lists;
  int rendered_meshes;
  int vao_changes;
  int material_changes;
  int program_changes;
  int unsorted_draw_calls;
  int mesh_face_draw_calls;
  int particle_draw_calls;
};


class RenderGroup
{
  ShaderManager& m_shader_manager;

  std::vector<std::shared_ptr<UnsortedRenderList>> m_unsorted_render_lists;
  std::unordered_map<const Material*,
                     std::shared_ptr<UnsortedRenderList>> m_unsorted_render_lists_map;

  std::unique_ptr<ZSortedFaceRenderList> m_z_sorted_face_render_list;

public:
  RenderGroup(ShaderManager&, const render_util::Camera3D&);

  const std::shared_ptr<UnsortedRenderList>&
  getUnsortedList(const std::shared_ptr<const Material>&);

  ZSortedFaceRenderList& getZSortedList();

  void flush(const render_util::Camera3D&, bool enable_shaders = false);
};


template <class T>
struct VertexBufferTraits
{
  using BufferElement = T;

  static constexpr auto VERTEX_COMPONENTS = BufferElement::VERTEX_COMPONENTS;
  static constexpr auto NORMAL_COMPONENTS = BufferElement::NORMAL_COMPONENTS;
  static constexpr auto TEXCOORDS_COMPONENTS = BufferElement::TEXCOORDS_COMPONENTS;

  static constexpr auto COMPONENT_SIZE = sizeof(float);

  static_assert(sizeof(BufferElement::Position) ==
                COMPONENT_SIZE * VERTEX_COMPONENTS);
  static_assert(sizeof(BufferElement::Normal) ==
                COMPONENT_SIZE * NORMAL_COMPONENTS);
  static_assert(sizeof(BufferElement::TextureCoordinate) ==
                COMPONENT_SIZE * TEXCOORDS_COMPONENTS);

  static constexpr auto STRIDE = sizeof(BufferElement);

  static constexpr auto POSITION_OFFSET = offsetof(BufferElement, Position);
  static constexpr auto NORMAL_OFFSET = offsetof(BufferElement, Normal);
  static constexpr auto TEXCOORDS_OFFSET = offsetof(BufferElement, TextureCoordinate);
};


template <>
struct VertexBufferTraits<mesh::VertexPositionNormalTexture>
{
  using BufferElement = mesh::VertexPositionNormalTexture;

  static constexpr int VERTEX_COMPONENTS = 3;
  static constexpr int NORMAL_COMPONENTS = 3;
  static constexpr int TEXCOORDS_COMPONENTS = 2;

  static constexpr auto COMPONENT_SIZE = sizeof(float);

  static_assert(sizeof(BufferElement::Position) ==
                COMPONENT_SIZE * VERTEX_COMPONENTS);
  static_assert(sizeof(BufferElement::Normal) ==
                COMPONENT_SIZE * NORMAL_COMPONENTS);
  static_assert(sizeof(BufferElement::TextureCoordinate) ==
                COMPONENT_SIZE * TEXCOORDS_COMPONENTS);

  static constexpr int TOTAL_COMPONENTS = VERTEX_COMPONENTS +
                                          NORMAL_COMPONENTS +
                                          TEXCOORDS_COMPONENTS;

  static_assert(sizeof(BufferElement) == COMPONENT_SIZE * TOTAL_COMPONENTS);

  static constexpr auto STRIDE = sizeof(BufferElement);

  static constexpr auto POSITION_OFFSET = offsetof(BufferElement, Position);
  static constexpr auto NORMAL_OFFSET = offsetof(BufferElement, Normal);
  static constexpr auto TEXCOORDS_OFFSET = offsetof(BufferElement, TextureCoordinate);
};


class MeshVAOBase
{
public:
  static constexpr auto ATTRIB_INDEX_VERTEX = 0;
  static constexpr auto ATTRIB_INDEX_NORMAL = 1;
  static constexpr auto ATTRIB_INDEX_TEXCOORDS = 2;
  static constexpr auto ATTRIB_INDEX_MAX = 2;

  static constexpr auto BUFFER_BINDING_INDEX = 0;
  static constexpr auto BUFFER_BINDING_INDEX_MAX = 0;

  template <class BufferElementType>
  static void setup(unsigned vao_id, unsigned buffer_id)
  {
    // https://registry.khronos.org/OpenGL-Refpages/gl4/html/glVertexAttribFormat.xhtml
    // description of the relativeoffset parameter seems to be wrong
    // compare with https://registry.khronos.org/OpenGL-Refpages/es3/html/glVertexAttribFormat.xhtml

    using namespace render_util::gl_binding;

    using Traits = VertexBufferTraits<BufferElementType>;

    gl::VertexArrayVertexBuffer(vao_id, BUFFER_BINDING_INDEX, buffer_id,
                                0, Traits::STRIDE);
    gl::VertexArrayBindingDivisor(vao_id, BUFFER_BINDING_INDEX, 0);

    gl::VertexArrayAttribBinding(vao_id, ATTRIB_INDEX_VERTEX, BUFFER_BINDING_INDEX);
    gl::VertexArrayAttribFormat(vao_id, ATTRIB_INDEX_VERTEX,
                                Traits::VERTEX_COMPONENTS,
                                GL_FLOAT, false,
                                Traits::POSITION_OFFSET);
    gl::EnableVertexArrayAttrib(vao_id, ATTRIB_INDEX_VERTEX);

    gl::VertexArrayAttribBinding(vao_id, ATTRIB_INDEX_NORMAL, BUFFER_BINDING_INDEX);
    gl::VertexArrayAttribFormat(vao_id, ATTRIB_INDEX_NORMAL,
                                Traits::NORMAL_COMPONENTS,
                                GL_FLOAT, false,
                                Traits::NORMAL_OFFSET);
    gl::EnableVertexArrayAttrib(vao_id, ATTRIB_INDEX_NORMAL);

    gl::VertexArrayAttribBinding(vao_id, ATTRIB_INDEX_TEXCOORDS, BUFFER_BINDING_INDEX);
    gl::VertexArrayAttribFormat(vao_id, ATTRIB_INDEX_TEXCOORDS,
                                Traits::TEXCOORDS_COMPONENTS,
                                GL_FLOAT, false,
                                Traits::TEXCOORDS_OFFSET);
    gl::EnableVertexArrayAttrib(vao_id, ATTRIB_INDEX_TEXCOORDS);
  }
};


template <class BufferElementType>
class IndexedMeshVAO : public render_util::IndexedVAOBase
{
  StaticBuffer<BufferElementType> m_vertex_buffer;

public:
  template <class VertexData, class IndexData>
  IndexedMeshVAO(VertexData& vertices_in, IndexData& indices_in) :
    m_vertex_buffer(vertices_in.data(), vertices_in.size())
  {
    static_assert(std::is_same<typename IndexData::value_type, il2ge::mesh::Index>::value);

    using namespace render_util::gl_binding;

    MeshVAOBase::setup<BufferElementType>(getID(), m_vertex_buffer.getID());

    gl::NamedBufferStorage(getIndexBufferID(),
                           indices_in.size() * sizeof(typename IndexData::value_type),
                           indices_in.data(),
                           0);
  }
};


template <class VertexData, class IndexData>
auto createVAO(VertexData& vertices, IndexData& indices)
{
  using BufferElementType = typename VertexData::value_type;
  return std::make_unique<IndexedMeshVAO<BufferElementType>>(vertices, indices);
}


inline auto createQuadVAO()
{
  constexpr auto TEXCOORD_COMPONENTS = 2;
  constexpr auto VERTEX_COMPONENTS = 2;
  using Vertex = std::array<float, VERTEX_COMPONENTS>;
  using TexCoord = std::array<float, TEXCOORD_COMPONENTS>;

  std::vector<Vertex> vertices =
  {
    { 0, 0 },
    { 0, 1 },
    { 1, 1 },
    { 1, 0 },
  };

  std::vector<TexCoord> texcoords =
  {
    { 0, 0 },
    { 0, 1 },
    { 1, 1 },
    { 1, 0 },
  };

  std::vector<il2ge::mesh::Index> indices =
  {
    0, 1, 2,
    0, 2, 3
  };

  return std::make_unique<render_util::VertexArrayObject450>(
      vertices.data(), vertices.size() * sizeof(Vertex), VERTEX_COMPONENTS,
      nullptr, 0,
      texcoords.data(), texcoords.size() * sizeof(TexCoord), TEXCOORD_COMPONENTS,
      indices.data(), indices.size() * sizeof(il2ge::mesh::Index));
};


void applyMaterial(const Material *m, render_util::StateModifier &state);


extern GlobalStats g_stats;


} // namespace il2ge::renderer
