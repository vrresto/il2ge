#include "z_sorted_face_render_list.h"
#include "renderer_p.h"
#include <render_util/gl_binding/gl_functions.h>


using namespace render_util::gl_binding;
using namespace il2ge;
using namespace il2ge::renderer;


#define USE_DYNAMIC_BUFFER 1


namespace il2ge::renderer
{


class ZSortedFaceRenderList::MeshFacesVAO : public render_util::VAOBase
{
public:
  using Index = glm::uint;

  struct VertexBufferElement
  {
    static constexpr int VERTEX_COMPONENTS = 3;
    static constexpr int NORMAL_COMPONENTS = 3;
    static constexpr int TEXCOORDS_COMPONENTS = 2;

    glm::vec3 Position {};
    glm::vec3 Normal {};
    glm::vec2 TextureCoordinate {};
    Index mesh_instance {};
  };

  static constexpr auto INDEX_TYPE_GL = GL_UNSIGNED_INT;

private:
  static constexpr auto ATTRIB_INDEX_MESH_INSTANCE_INDEX = 3;
  static_assert(ATTRIB_INDEX_MESH_INSTANCE_INDEX > MeshVAOBase::ATTRIB_INDEX_MAX);

  DynamicBuffer<VertexBufferElement> m_vertex_buffer;

public:
  MeshFacesVAO()
  {
    MeshVAOBase::setup<VertexBufferElement>(getID(), m_vertex_buffer.getID());

    gl::VertexArrayAttribBinding(getID(),
                                 ATTRIB_INDEX_MESH_INSTANCE_INDEX,
                                 MeshVAOBase::BUFFER_BINDING_INDEX);
    gl::VertexArrayAttribIFormat(getID(),
                                ATTRIB_INDEX_MESH_INSTANCE_INDEX,
                                1,
                                INDEX_TYPE_GL,
                                offsetof(VertexBufferElement, mesh_instance));
    gl::EnableVertexArrayAttrib(getID(), ATTRIB_INDEX_MESH_INSTANCE_INDEX);
  }

  void reserveBuffers(size_t triangle_count)
  {
    m_vertex_buffer.reserve(triangle_count * 3);
  }

  auto getVertexBuffer()
  {
    return m_vertex_buffer.getData();
  }

  auto getVertexBufferSize()
  {
    return m_vertex_buffer.getSize();
  }

  void unmapBuffers()
  {
    m_vertex_buffer.unmap();
  }
};


struct ZSortedFaceRenderList::Batch
{
  ZSortedFaceRenderList::Item::Type type;
  const Material* material {};
  int base_instance = 0;
  int instance_count = 0;
};


ZSortedFaceRenderList::
ParticleItem::ParticleItem(const ParticleInstance& instance) :
  instance(instance)
{
}


ZSortedFaceRenderList::
MeshFaceItem::MeshFaceItem(const mesh::Face& face, size_t instance_index) :
  face(face),
  instance_index(instance_index)
{
}


ZSortedFaceRenderList::
ParticleGroupItem::ParticleGroupItem(ParticleGroupItem&& other) :
  instances(std::move(other.instances))
{
}


ZSortedFaceRenderList::
ParticleGroupItem::ParticleGroupItem(std::vector<ParticleInstance>&& instances) :
  instances(std::move(instances))
{
}


ZSortedFaceRenderList::
MeshFaceGroupItem::MeshFaceGroupItem(MeshFaceGroupItem&& other) :
  faces(std::move(other.faces)),
  instance_index(other.instance_index)
{
}


ZSortedFaceRenderList::
MeshFaceGroupItem::MeshFaceGroupItem(std::vector<mesh::Face>&& faces,
                                     size_t instance_index) :
  faces(std::move(faces)),
  instance_index(instance_index)
{
}


ZSortedFaceRenderList::Item::Item(Item&& other) :
  type(other.type),
  distance(other.distance),
  material(std::move(other.material)),
  variant(std::move(other.variant))
{
}


ZSortedFaceRenderList::Item::Item(const ParticleInstance& particle,
      const std::shared_ptr<const Material>& material,
      float dist) :
  type(Type::PARTICLE),
  distance(dist),
  material(material),
  variant(std::in_place_type<ParticleItem>, particle)
{
}


ZSortedFaceRenderList::Item::Item(const mesh::Face& face,
      size_t instance_index,
      const std::shared_ptr<const Material>& material,
      float dist) :
  type(Type::MESH_FACE),
  distance(dist),
  material(material),
  variant(std::in_place_type<MeshFaceItem>, face, instance_index)
{
}


ZSortedFaceRenderList::Item::Item(std::vector<mesh::Face>&& faces,
         const std::shared_ptr<const Material>& material,
         const glm::mat4& model_view,
         size_t mesh_instance_index,
         float dist) :
  type(Type::MESH_FACE_GROUP),
  distance(dist),
  material(material),
  variant(std::in_place_type<MeshFaceGroupItem>, std::move(faces), mesh_instance_index)
{
}


ZSortedFaceRenderList::Item::Item(std::vector<ParticleInstance>&& instances,
      const std::shared_ptr<const Material>& material,
      float dist) :
  type(Type::PARTICLE_GROUP),
  distance(dist),
  material(material),
  variant(std::in_place_type<ParticleGroupItem>, std::move(instances))
{
}


ZSortedFaceRenderList::ZSortedFaceRenderList(ShaderManager& shader_manager,
                                             const render_util::Camera3D& camera) :
  m_camera(camera),
  m_shader_manager(shader_manager),
  m_quad_vao(createQuadVAO()),
  m_mesh_faces_vao(std::make_unique<MeshFacesVAO>())
{
  assert(m_quad_vao);
}


ZSortedFaceRenderList::~ZSortedFaceRenderList()
{
}


void ZSortedFaceRenderList::addFace(const mesh::Face& face,
              const std::shared_ptr<const Material>& mat,
              const glm::mat4 &model_view,
              size_t mesh_instance_index)
{
  double max_dist = 500; //FIXME

  auto center_view = model_view * face.center;
  auto dist = glm::length(center_view);

  if (dist > mat->getLayers().at(0).VisibleDistanceFar)
    return;

  if (dist > max_dist)
    return;

  m_items.emplace_back(face, mesh_instance_index, mat, dist);

  m_mesh_face_count++;
}


void ZSortedFaceRenderList::addFaceGroup(std::vector<mesh::Face>&& faces,
              const std::shared_ptr<const Material>& mat,
              const glm::mat4& model_view,
              size_t mesh_instance_index,
              float dist)
{
  m_mesh_face_count += faces.size();
  m_items.emplace_back(std::move(faces), mat, model_view, mesh_instance_index, dist);
}


void ZSortedFaceRenderList::add(const std::vector<mesh::Face>& faces,
          const MaterialList& materials,
          const glm::mat4& model_view,
          float dist)
{
  m_mesh_instances.emplace_back();
  auto& instance = m_mesh_instances.back();
  auto mesh_instance_index = m_mesh_instances.size() - 1;

  instance.model_view = model_view;

  int material_index = -1;

  int remaining_faces = faces.size();
  assert(remaining_faces > 0);


  if (dist > 50.f) //FIXME
  {
    std::vector<mesh::Face> face_group;
    face_group.reserve(remaining_faces);

    for (auto& face : faces)
    {
      if (face.material != material_index)
      {
        if (!face_group.empty())
        {
          assert(material_index >= 0);
          addFaceGroup(std::move(face_group),
                       materials.at(material_index),
                       model_view,
                       mesh_instance_index,
                       dist);
          assert(remaining_faces > 0);
          face_group.reserve(remaining_faces);
        }

        material_index = face.material;
      }

      face_group.push_back(face);

      remaining_faces--;
    }

    if (!face_group.empty())
    {
      assert(material_index >= 0);
      addFaceGroup(std::move(face_group),
                    materials.at(material_index),
                    model_view,
                    mesh_instance_index,
                    dist);
    }
  }
  else
  {
    for (auto& face : faces)
    {
      addFace(face, materials.at(face.material), model_view, mesh_instance_index);
    }
  }
}


void ZSortedFaceRenderList::add(std::vector<ParticleInstance>&& instances,
                                const std::shared_ptr<const Material>& material,
                                float dist)
{
  auto visible_dist = material->getLayers().at(0).VisibleDistanceFar;

  // if (dist && dist > 500.f) //FIXME
  if (dist && dist > 200.f) //FIXME
  {
    if (dist > visible_dist)
      return;

    m_particle_count += instances.size();

    m_items.emplace_back(std::move(instances), material, dist);

    return;
  }

  for (auto& instance : instances)
  {
    auto dist = glm::distance(instance.pos, m_camera.getPos());

    if (dist < 0)
      continue;

    if (dist > visible_dist)
      continue;

    m_items.emplace_back(instance, material, dist);

    m_particle_count++;
  }
}


void ZSortedFaceRenderList::clear()
{
  m_particle_count = 0;
  m_mesh_face_count = 0;
  m_items.clear();
  m_sorted.clear();
  m_mesh_instances.clear();
}


void ZSortedFaceRenderList::sort()
{
  m_sorted.clear();
  m_sorted.reserve(m_items.size());
  for (int i = 0; i < m_items.size(); i++)
    m_sorted.push_back(i);

  auto is_less = [&] (size_t a, size_t b)
  {
    return (m_items.at(a).distance > m_items.at(b).distance);
  };

  std::sort(m_sorted.begin(), m_sorted.end(), is_less);
}


void ZSortedFaceRenderList::createBatches(std::vector<Batch>& batches)
{
  if (m_particle_count)
    m_particle_instance_buffer.reserve(m_particle_count);

  if (!m_mesh_instances.empty())
  {
    //FIXME: use std::copy?

    m_mesh_instance_buffer.reserve(m_mesh_instances.size());

    for (int i = 0; i < m_mesh_instance_buffer.getSize(); i++)
      m_mesh_instance_buffer.at(i) = m_mesh_instances.at(i);

    m_mesh_instance_buffer.unmap();
  }

  assert(m_mesh_faces_vao);

  if (m_mesh_face_count)
    m_mesh_faces_vao->reserveBuffers(m_mesh_face_count);

  Batch* current_batch {};

  size_t particle_instance_buffer_index = 0;
  size_t mesh_face_buffer_index = 0;

  auto add_mesh_face = [&] (auto& face, auto mesh_instance)
  {
    assert(mesh_face_buffer_index < m_mesh_face_count);

    for (int i = 0; i < 3; i++)
    {
      auto vertex_buffer_index = (mesh_face_buffer_index * 3) + i;
      assert(vertex_buffer_index < m_mesh_faces_vao->getVertexBufferSize());
      auto& vertex = face.vertices.at(i);

      auto& element = m_mesh_faces_vao->getVertexBuffer()[vertex_buffer_index];
      element.Position = vertex.Position;
      element.Normal = vertex.Normal;
      element.TextureCoordinate = vertex.TextureCoordinate;
      element.mesh_instance = mesh_instance;
    }

    mesh_face_buffer_index++;
  };

  for (size_t i = 0; i < m_sorted.size(); i++)
  {
    auto real_index = m_sorted[i];
    auto& item = m_items.at(real_index);

    if (!current_batch
        || item.type != current_batch->type
        || item.material.get() != current_batch->material)
    {
      batches.emplace_back();
      current_batch = &batches.back();
      current_batch->type = item.type;
      current_batch->material = item.material.get();
      current_batch->instance_count = 0;

      if (item.type == Item::PARTICLE || item.type == Item::PARTICLE_GROUP)
      {
        current_batch->base_instance = particle_instance_buffer_index;
        assert(current_batch->base_instance < m_particle_count);
      }
      else if (item.type == Item::MESH_FACE || item.type == Item::MESH_FACE_GROUP)
      {
        current_batch->base_instance = mesh_face_buffer_index; //FIXME
      }
    }

    auto visitor = [&] (auto& arg)
    {
      using T = std::decay_t<decltype(arg)>;

      if constexpr (std::is_same_v<T, ParticleItem>)
      {
        assert(m_particle_count);
        assert(particle_instance_buffer_index < m_particle_count);

        m_particle_instance_buffer.at(particle_instance_buffer_index) = arg.instance;

        particle_instance_buffer_index++;

        current_batch->instance_count++;
      }
      else if constexpr (std::is_same_v<T, ParticleGroupItem>)
      {
        assert(m_particle_count);
        assert(particle_instance_buffer_index < m_particle_count);

        for (auto& instance : arg.instances)
        {
          assert(particle_instance_buffer_index < m_particle_count);

          m_particle_instance_buffer.at(particle_instance_buffer_index) = instance;
          particle_instance_buffer_index++;
        }

        current_batch->instance_count += arg.instances.size();
      }
      else if constexpr (std::is_same_v<T, MeshFaceItem>)
      {
        assert(mesh_face_buffer_index < m_mesh_face_count);

        add_mesh_face(arg.face, arg.instance_index);

        current_batch->instance_count++;
      }
      else if constexpr (std::is_same_v<T, MeshFaceGroupItem>)
      {
        for (auto& face : arg.faces)
        {
          assert(mesh_face_buffer_index < m_mesh_face_count);

          add_mesh_face(face, arg.instance_index);

          current_batch->instance_count++;
        }
      }
    };

    std::visit(visitor, item.variant);
  }

  m_particle_instance_buffer.unmap();
  m_mesh_faces_vao->unmapBuffers();
}


void ZSortedFaceRenderList::render(render_util::StateModifier &state,
                                   const render_util::Camera& camera)
{
  std::vector<Batch> batches;
  createBatches(batches);

  gl::BindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_particle_instance_buffer.getID());
  gl::BindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, m_mesh_instance_buffer.getID());

  const Material* current_material {};
  render_util::ShaderProgram* current_program = nullptr;
  Item::Type current = Item::NONE;

  state.setDefaults();
  // state.enableAlphaTest(false); //HACK

  auto particle_program =
    m_shader_manager.getProgram(ShaderManager::UNSORTED_PARTICLE).get();
  particle_program->setUniform("projection", camera.getProjectionMatrixFar());
  particle_program->setUniform("world_to_view", camera.getWorld2ViewMatrix());
  particle_program->setUniform("camera_pos", camera.getPos());

  auto mesh_face_program =
    m_shader_manager.getProgram(ShaderManager::Z_SORTED_MESH_FACE).get();
  mesh_face_program->setUniform("projection", camera.getProjectionMatrixFar());
  mesh_face_program->setUniform("world_to_view", camera.getWorld2ViewMatrix());
  mesh_face_program->setUniform("camera_pos", camera.getPos());

  int particle_batches = 0;

  for (auto& batch : batches)
  {
    if (batch.type != current)
    {
      if (batch.type == Item::MESH_FACE || batch.type == Item::MESH_FACE_GROUP)
      {
        state.setProgram(mesh_face_program->getID());
        current_program = mesh_face_program;

        g_stats.program_changes++;

        gl::BindVertexArray(m_mesh_faces_vao->getID());
        gl::BindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        g_stats.vao_changes++;
      }
      else if (batch.type == Item::PARTICLE || batch.type == Item::PARTICLE_GROUP)
      {
        state.setProgram(particle_program->getID());
        current_program = particle_program;
        g_stats.program_changes++;

        gl::BindVertexArray(m_quad_vao->getID());
        gl::BindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_quad_vao->getIndexBufferID());
        g_stats.vao_changes++;

        particle_batches++;
      }

      current = batch.type;
    }

    if (batch.material != current_material)
    {
      // gl::Enable(GL_TEXTURE_2D); //FIXME
      // gl::Disable(GL_TEXTURE_2D); //FIXME

      current_material = batch.material;

      applyMaterial(current_material, state);

      if (current_program)
      {
        auto& layer = current_material->getLayers().at(0);
        current_program->setUniform("color_scale", layer.ColorScale);
      }
    }

    if (batch.type == Item::MESH_FACE || batch.type == Item::MESH_FACE_GROUP)
    {
      assert(current_program);
      current_program->assertUniformsAreSet();

      auto vao_index = batch.base_instance * 3;
      assert(vao_index < m_mesh_faces_vao->getVertexBufferSize());

      gl::DrawArrays(GL_TRIANGLES, vao_index, batch.instance_count * 3);

      g_stats.mesh_face_draw_calls++;
    }
    else if (batch.type == Item::PARTICLE || batch.type == Item::PARTICLE_GROUP)
    {
      state.enableAlphaTest(false); //HACK FIXME

      assert((batch.base_instance + batch.instance_count) <= m_particle_count);

      assert(current_program);
      current_program->assertUniformsAreSet();

      gl::DrawElementsInstancedBaseInstance(GL_TRIANGLES, 6, INDEX_TYPE, nullptr,
                                            batch.instance_count, batch.base_instance);

      g_stats.particle_draw_calls++;
    }
  }

  gl::BindVertexArray(0);
  gl::BindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  state.setProgram(0);

  gl::BindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
  gl::BindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, 0);

  // auto average_particle_batch_size = m_particle_count / float(particle_batches);
  // LOG_INFO << "average_particle_batch_size: " << average_particle_batch_size << std::endl;
}


} // namespace il2ge::renderer
