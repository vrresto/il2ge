#pragma once

#include <il2ge/renderer.h>
#include <render_util/shader.h>

namespace il2ge::renderer
{


class ShaderManager
{
  struct ProgramVariants
  {
    render_util::ShaderProgramPtr no_atmosphere;
    render_util::ShaderProgramPtr with_atmosphere;
  };

  ProgramVariants m_unsorted_mesh_program;
  ProgramVariants m_unsorted_mesh_program_instanced;
  ProgramVariants m_z_sorted_mesh_face_program;
  ProgramVariants m_unsorted_particle_program;

  bool m_is_atmosphere_enabled = false;
  bool m_is_instancing_enabled = true;

  ProgramVariants createProgram(std::string name, ShaderProgramFactory& factory);

public:
  enum ProgramType
  {
    UNSORTED_MESH,
    UNSORTED_PARTICLE,
    Z_SORTED_MESH_FACE,
    Z_SORTED_PARTICLE,
  };

  void createPrograms(ShaderProgramFactory& factory);

  void enableAtmosphere(bool enable)
  {
    m_is_atmosphere_enabled = enable;
  }

  void enableInstancedDrawing(bool enable)
  {
    m_is_instancing_enabled = enable;
  }


  bool isInstancingEnabled()
  {
    return m_is_instancing_enabled;
  }

  const render_util::ShaderProgramPtr& getProgram(ProgramType type);
};


} // namespace il2ge::renderer
