#include "unsorted_render_list.h"
#include "shared_vao.h"
#include "renderer_p.h"
#include <render_util/state.h>
#include <render_util/vao.h>
#include <render_util/gl_binding/gl_functions.h>


using namespace render_util::gl_binding;


namespace il2ge::renderer
{


void UnsortedRenderList::MeshList::add(MeshRendererData* mesh,
                                       int face_group,
                                       const glm::mat4& model_view,
                                       int frame)
{
  MeshItem* item {};

  auto it = m_map.find(mesh);
  if (it != m_map.end())
  {
    item = &m_items.at(it->second);
  }
  else
  {
    m_items.emplace_back(mesh);
    m_map[mesh] = m_items.size() - 1;
    item = &m_items.back();
  }

  auto& fg = item->m_face_groups[face_group];
  fg.fg = mesh->getFaceGroups().at(face_group);
  fg.instances.emplace_back(model_view, frame);
}


void UnsortedRenderList::MeshList::clear()
{
  m_map.clear();
  m_items.clear();
}


void UnsortedRenderList::SharedVAOList::add(MeshRendererData* mesh,
                                            int face_group,
                                            const glm::mat4& model_view,
                                            int frame)
{
  SharedVAOItem* item {};

  auto it = m_map.find(mesh->getSharedVAO().get());
  if (it != m_map.end())
  {
    item = &m_items.at(it->second);
  }
  else
  {
    m_items.emplace_back(mesh->getSharedVAO());
    m_map[mesh->getSharedVAO().get()] = m_items.size() - 1;
    item = &m_items.back();
  }

  item->m_meshes.add(mesh, face_group, model_view, frame);
}


void UnsortedRenderList::SharedVAOList::clear()
{
  m_map.clear();
  m_items.clear();
}


void UnsortedRenderList::MeshItem::fillBatch(Batch& batch)
{
  batch.vao = &m_mesh->getVAO();

  auto& lod = m_mesh->getLodMesh();

  int cmd_count = 0;

  auto count_commands = [&] (auto& item, auto& fg_item)
  {
    if (m_mesh->isAnimated())
      cmd_count += fg_item.instances.size();
    else
      cmd_count++;
  };

  auto add_commands = [&] (auto& item,  auto& fg_item)
  {
    auto& fg = fg_item.fg;

    auto add_command = [&] (auto start_vertex, auto base_instance, auto instance_count)
    {
      batch.commands.emplace_back();
      auto& cmd = batch.commands.back();

      cmd.count = fg.FaceCount * 3;
      cmd.firstIndex = fg.StartFace * 3;
      cmd.instanceCount = instance_count;
      cmd.baseVertex = start_vertex;
      cmd.baseInstance = base_instance;
    };

    if (m_mesh->isAnimated())
    {
      int i_instance = 0;
      for (auto& instance : fg_item.instances)
      {
        auto start_vertex = fg.StartVertex + lod.getVertexCount() * instance.frame;
        auto base_instance = fg_item.base_instance + i_instance;

        add_command(start_vertex, base_instance, 1);

        i_instance++;
      }
    }
    else
    {
      add_command(fg.StartVertex, fg_item.base_instance, fg_item.instances.size());
    }
  };

  visit(count_commands);
  assert(cmd_count);
  batch.commands.reserve(cmd_count);
  visit(add_commands);
}


void UnsortedRenderList::SharedVAOItem::fillBatch(Batch& batch)
{
  batch.vao = &m_vao->getVAO();

  int cmd_count = 0;

  auto count_commands = [&] (auto& item, auto& fg_item)
  {
    if (item.getMesh()->isAnimated())
      cmd_count += fg_item.instances.size();
    else
      cmd_count++;
  };

  auto add_commands = [&] (auto& item, auto& fg_item)
  {
    assert(item.getMesh()->getSharedVAOStartVertex() >= 0);
    assert(item.getMesh()->getSharedVAOStartIndex() >= 0);

    auto& lod = item.getMesh()->getLodMesh();
    auto& fg = fg_item.fg;

    auto add_command = [&] (auto start_vertex, auto base_instance, auto instance_count)
    {
      batch.commands.emplace_back();
      auto& cmd = batch.commands.back();

      cmd.count = fg.FaceCount * 3;
      cmd.firstIndex = item.getMesh()->getSharedVAOStartIndex() + fg.StartFace * 3;
      cmd.instanceCount = instance_count;
      cmd.baseVertex = start_vertex;
      cmd.baseInstance = base_instance;
    };

    if (item.getMesh()->isAnimated())
    {
      int i_instance = 0;
      for (auto& instance : fg_item.instances)
      {
        auto start_vertex =
          item.getMesh()->getSharedVAOStartVertex() +
          fg.StartVertex + lod.getVertexCount() * instance.frame;

        auto base_instance = fg_item.base_instance + i_instance;

        add_command(start_vertex, base_instance, 1);

        i_instance++;
      }
    }
    else
    {
      auto start_vertex = item.getMesh()->getSharedVAOStartVertex() + fg.StartVertex;
      add_command(start_vertex, fg_item.base_instance, fg_item.instances.size());
    }
  };

  visit(count_commands);
  assert(cmd_count);
  batch.commands.reserve(cmd_count);
  visit(add_commands);
}


UnsortedRenderList::UnsortedRenderList(std::string material_path,
                                       std::shared_ptr<const Material> material,
                                       ShaderManager& shader_manager) :
  m_shader_manager(shader_manager),
  m_material_path(material_path),
  m_material(material)
{
  gl::CreateBuffers(1, &m_instance_buffer_id);
  gl::CreateBuffers(1, &m_particle_instance_buffer_id);
}


UnsortedRenderList::~UnsortedRenderList()
{
  gl::DeleteBuffers(1, &m_instance_buffer_id);
  gl::DeleteBuffers(1, &m_particle_instance_buffer_id);
}


void UnsortedRenderList::add(std::vector<ParticleInstance>&& instances)
{
  assert(!instances.empty());
  m_particles.push_back(std::move(instances));
}


void UnsortedRenderList::add(MeshRendererData* mesh, int face_group,
                             const glm::mat4& model_view, float distance, int frame)
{
  if (mesh->getSharedVAO() && m_shader_manager.isInstancingEnabled())
    m_shared_vaos.add(mesh, face_group, model_view, frame);
  else
    m_meshes.add(mesh, face_group, model_view, frame);
}


#if 0
void UnsortedRenderList::renderWireFrame(const render_util::Camera& camera)
{
  render_util::StateModifier state;
  applyMaterial(m_material.get(), state);
  gl::Color4f(m_color.r, m_color.g, m_color.b, 1);
  gl::Disable(GL_TEXTURE_2D); //FIXME
  gl::PolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  render(camera);
  gl::PolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}


void UnsortedRenderList::renderWireFrameVAO(const render_util::Camera& camera)
{
  render_util::StateModifier state;
  applyMaterial(m_material.get(), state);
  gl::Color4f(m_color.r, m_color.g, m_color.b, 1);
  gl::Disable(GL_TEXTURE_2D); //FIXME
  gl::PolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  renderVAO(camera);
  gl::PolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}


void UnsortedRenderList::renderTextured(const render_util::Camera& camera)
{
  gl::Enable(GL_TEXTURE_2D); //FIXME
  render_util::StateModifier state;
  applyMaterial(m_material.get(), state);
  render(camera);
}


void UnsortedRenderList::renderTexturedVAO(const render_util::Camera& camera)
{
  gl::Enable(GL_TEXTURE_2D); //FIXME
  render_util::StateModifier state;
  applyMaterial(m_material.get(), state);
  renderVAO(camera);
}


void UnsortedRenderList::renderWireFrameInstanced(const render_util::Camera& camera)
{
  render_util::StateModifier state;
  applyMaterial(m_material.get(), state);
  gl::Color4f(m_color.r, m_color.g, m_color.b, 1);
  gl::Disable(GL_TEXTURE_2D); //FIXME
  gl::PolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  renderInstanced(camera);
  gl::PolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}


void UnsortedRenderList::renderTexturedInstanced(const render_util::Camera& camera)
{
  gl::Enable(GL_TEXTURE_2D); //FIXME
  render_util::StateModifier state;
  applyMaterial(m_material.get(), state);
  renderInstanced(camera);
}


void UnsortedRenderList::renderAnimatedTexturedInstanced(const render_util::Camera& camera)
{
  gl::Enable(GL_TEXTURE_2D); //FIXME
  render_util::StateModifier state;
  applyMaterial(m_material.get(), state);
  renderAnimatedInstanced(camera);
}
#endif


void UnsortedRenderList::renderLegacy()
{
  if (m_material->getLayers().at(0).tfNoTexture)
    state::Disable(GL_TEXTURE_2D); //FIXME

  //HACK
  // if (!m_material->getLayers().at(0).texture)
    // return;

  for (auto& item : m_meshes)
  {
    assert(item.getMesh());
    auto &lod = item.getMesh()->getLodMesh();

    const auto &verts = lod.getVertices();
    const auto &indices = lod.getIndices();

    for (auto& it : item.m_face_groups)
    {
      if (!it.second.instances.empty())
        g_stats.rendered_meshes++;

      auto& fg = it.second.fg;

      for (auto& instance : it.second.instances)
      {
        //FIXME
        gl::MatrixMode(GL_MODELVIEW);
        gl::LoadMatrixf(glm::value_ptr(instance.model_view));

        auto start_vertex = lod.getVertexCount() * instance.frame;

        gl::Begin(GL_TRIANGLES);

        for (int i = 0; i < fg.FaceCount * 3; i++)
        {
          auto index = start_vertex + fg.StartVertex + indices[fg.StartFace * 3 + i];

          const auto& v = verts[index].Position;
          const auto& texcoords = verts[index].TextureCoordinate;

          gl::TexCoord2f(texcoords.x, texcoords.y);
          gl::Vertex3f(v.x, v.y, v.z);

          g_stats.unsorted_draw_calls += 2;
        }

        gl::End();

        g_stats.unsorted_draw_calls += 2;
      }
    }
  }

  // gl::Disable(GL_TEXTURE_2D);
}


void UnsortedRenderList::renderVAO()
{
  // if (m_material->getLayers().at(0).tfNoTexture)
    // gl::Disable(GL_TEXTURE_2D); //FIXME

  for (auto& item : m_meshes)
  {
    auto& lod = item.getMesh()->getLodMesh();
    auto& vao = item.getMesh()->getVAO();

    render_util::VertexArrayObjectBinding vao_binding(vao);
    render_util::IndexBufferBinding index_buffer_binding(vao);

    for (auto& it : item.m_face_groups)
    {
      if (!it.second.instances.empty())
        g_stats.rendered_meshes++;

      auto& fg = it.second.fg;

      for (auto& instance : it.second.instances)
      {
        //FIXME
        gl::MatrixMode(GL_MODELVIEW);
        gl::LoadMatrixf(glm::value_ptr(instance.model_view));

        auto start_vertex = fg.StartVertex + lod.getVertexCount() * instance.frame;

        intptr_t index_buffer_offset = INDEX_SIZE * 3 * fg.StartFace;

        gl::DrawElementsBaseVertex(GL_TRIANGLES,
                                   fg.FaceCount * 3,
                                   INDEX_TYPE,
                                   (void*)index_buffer_offset,
                                   start_vertex);

        g_stats.unsorted_draw_calls++;
      }
    }
  }
}


void UnsortedRenderList::render()
{
  // g_shared_vao_count +=  m_shared_vaos.size();

  if (!m_shader_manager.isInstancingEnabled())
  {
    renderLegacy();
    // renderVAO();
    return;
  }

  updateInstanceBuffer();

  std::vector<Batch> batches;
  createBatches(batches);

  gl::BindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_instance_buffer_id);

  for (auto& batch : batches)
  {
    assert(!batch.commands.empty());
    assert(batch.vao);

    render_util::VertexArrayObjectBinding vao_binding(*batch.vao);
    render_util::IndexBufferBinding index_buffer_binding(*batch.vao);
    g_stats.vao_changes++;

    gl::MultiDrawElementsIndirect(GL_TRIANGLES, INDEX_TYPE,
                                  batch.commands.data(), batch.commands.size(), 0);
    g_stats.unsorted_draw_calls++;
  }

  gl::BindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
}


void UnsortedRenderList::renderParticles()
{
  if (!m_shader_manager.isInstancingEnabled())
  {
    // renderParticlesLegacy();
    return;
  }

  if (m_particles.empty())
    return;

  //FIXME
  int instance_count = 0;
  for (auto& list : m_particles)
    instance_count += list.size();

  assert(instance_count);

  if (!instance_count)
    return;

  updateParticleInstanceBuffer();

  gl::BindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_particle_instance_buffer_id);

  {
    if (!m_quad_vao)
      m_quad_vao = createQuadVAO();

    render_util::VertexArrayObjectBinding vao_binding(*m_quad_vao);
    render_util::IndexBufferBinding index_buffer_binding(*m_quad_vao);

    //FIXME use DrawElementsInstancedBaseInstance
    gl::DrawElementsInstanced(GL_TRIANGLES, 6, INDEX_TYPE, nullptr, instance_count);
    g_stats.unsorted_draw_calls++;
  }

  gl::BindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, 0);
}


void UnsortedRenderList::createBatches(std::vector<Batch>& batches)
{
  batches.reserve(m_meshes.size() + m_shared_vaos.size());

  for (auto& item : m_meshes)
  {
    batches.emplace_back();
    auto& batch = batches.back();
    item.fillBatch(batch);
  }

  for (auto& item : m_shared_vaos)
  {
    batches.emplace_back();
    auto& batch = batches.back();
    item.fillBatch(batch);
  }
}


void UnsortedRenderList::updateParticleInstanceBuffer()
{
  assert(m_particle_instance_buffer_id);
  assert(gl::IsBuffer(m_particle_instance_buffer_id));

  int instance_count = 0;

  for (auto& list : m_particles)
    instance_count += list.size();

  if (!instance_count)
    return;

  auto required_size = sizeof(ParticleInstance) * instance_count;
  m_max_particle_instance_buffer_size = glm::max(required_size, m_max_particle_instance_buffer_size);

  const auto buffer_size = m_max_particle_instance_buffer_size;

  gl::NamedBufferData(m_particle_instance_buffer_id, buffer_size, nullptr, GL_STREAM_DRAW);
  auto buffer = (ParticleInstance*) gl::MapNamedBuffer(m_particle_instance_buffer_id, GL_WRITE_ONLY);
  assert(buffer);

  int index = 0;
  for (auto& list : m_particles)
  {
    for (auto& p : list)
    {
      assert(index < instance_count);
      buffer[index] = p;
      index++;
    }
  }

  buffer = nullptr;
  gl::UnmapNamedBuffer(m_particle_instance_buffer_id);
}


void UnsortedRenderList::updateInstanceBuffer()
{
  assert(gl::IsBuffer(m_instance_buffer_id));

  int base_instance = 0;
  int instance_count = 0;

  auto count_instances = [&] (auto& item, auto& fg_item)
  {
    instance_count += fg_item.instances.size();
  };

  m_meshes.visit(count_instances);
  m_shared_vaos.visit(count_instances);

  if (!instance_count)
    return;

  auto required_size = sizeof(InstanceData) * instance_count;

  m_max_instance_buffer_size = glm::max(required_size, m_max_instance_buffer_size);

  // avoid different buffer sizes on each call
  const auto buffer_size = m_max_instance_buffer_size;

  gl::NamedBufferData(m_instance_buffer_id, buffer_size, nullptr, GL_STREAM_DRAW);
  auto buffer = (InstanceData*) gl::MapNamedBuffer(m_instance_buffer_id, GL_WRITE_ONLY);
  assert(buffer);

  int index = 0;

  auto visitor = [&] (auto& mesh_item, auto& fg_item)
  {
    for (auto& instance : fg_item.instances)
    {
      assert(index < instance_count);
      buffer[index].model_view = instance.model_view;
      index++;
    }

    fg_item.base_instance = base_instance;
    base_instance += fg_item.instances.size();
  };

  m_meshes.visit(visitor);
  m_shared_vaos.visit(visitor);

  buffer = nullptr;
  gl::UnmapNamedBuffer(m_instance_buffer_id);
}


void UnsortedRenderList::clear()
{
  m_meshes.clear();
  m_shared_vaos.clear();
  m_particles.clear();
}


} // namespace il2ge::renderer
