#include "shader_manager.h"

namespace il2ge::renderer
{


ShaderManager::ProgramVariants
ShaderManager::createProgram(std::string name, ShaderProgramFactory& factory)
{
  ProgramVariants program;
  render_util::ShaderParameters params;

  params.set("enable_atmosphere", false);
  program.no_atmosphere = factory(name, params);
  program.no_atmosphere->setUniformi("sampler_0", 0);

  params.set("enable_atmosphere", true);
  program.with_atmosphere = factory(name, params);
  program.with_atmosphere->setUniformi("sampler_0", 0);

  return program;
}


void ShaderManager::createPrograms(ShaderProgramFactory& factory)
{
  m_unsorted_mesh_program = createProgram("objects_simple", factory);
  m_unsorted_mesh_program_instanced = createProgram("objects_instanced", factory);
  m_unsorted_particle_program = createProgram("particle", factory);
  m_z_sorted_mesh_face_program = createProgram("sorted_mesh_face", factory);
}


const render_util::ShaderProgramPtr& ShaderManager::getProgram(ProgramType type)
{
  ProgramVariants* program = nullptr;

  if (m_is_instancing_enabled)
  {
    switch (type)
    {
      case ProgramType::UNSORTED_MESH:
        program = &m_unsorted_mesh_program_instanced;
        break;
      case ProgramType::UNSORTED_PARTICLE:
        program = &m_unsorted_particle_program;
        break;
      case ProgramType::Z_SORTED_MESH_FACE:
        program = &m_z_sorted_mesh_face_program;
      default:
        break;
    }
  }
  else
  {
    switch (type)
    {
      case ProgramType::UNSORTED_MESH:
        program = &m_unsorted_mesh_program;
        break;
      default:
        break;
    }
  }

  assert(program);
  assert(program->with_atmosphere);
  assert(program->no_atmosphere);
  return m_is_atmosphere_enabled ?  program->with_atmosphere : program->no_atmosphere;
}


} // namespace il2ge::renderer
