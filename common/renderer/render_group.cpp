#include "renderer_p.h"
#include "unsorted_render_list.h"
#include "z_sorted_face_render_list.h"
#include <render_util/state.h>
#include <render_util/gl_binding/gl_functions.h>

#define GLM_ENABLE_EXPERIMENTAL 1
#include <glm/gtx/color_space.hpp>

#include <random>


using namespace render_util::gl_binding;

namespace
{


glm::vec3 getRandomColor()
{
  float saturation = 1.0;

  {
    static std::random_device rd;
    static std::default_random_engine engine(rd());
    std::uniform_int_distribution<int> dist(70, 100);
    saturation = dist(engine) / 100.f;
  }

  float brightness = 1.0;

  {
    static std::random_device rd;
    static std::default_random_engine engine(rd());
    std::uniform_int_distribution<int> dist(70, 100);
    brightness = dist(engine) / 100.f;
  }

  // saturation = 1.0 - brightness;

  // static std::random_device rd;
  // static std::default_random_engine engine(rd());
  // std::uniform_int_distribution<int> dist(0, 150);
  // float hue = dist(engine);

  static float last_hue = 0;
  float hue = last_hue;
  last_hue += 30;

  return glm::rgbColor(glm::vec3(hue, saturation, brightness));
}


} // namespace


namespace il2ge::renderer
{


RenderGroup::RenderGroup(ShaderManager& shader_manager,
                         const render_util::Camera3D& camera) :
  m_shader_manager(shader_manager),
  m_z_sorted_face_render_list(std::make_unique<ZSortedFaceRenderList>(shader_manager, camera))
{
}


ZSortedFaceRenderList& RenderGroup::getZSortedList()
{
  return *m_z_sorted_face_render_list;
}


void RenderGroup::flush(const render_util::Camera3D& camera, bool enable_shaders)
{
  assert(enable_shaders);

  render_util::StateModifier state;

  m_z_sorted_face_render_list->sort();

  auto program = m_shader_manager.getProgram(ShaderManager::UNSORTED_MESH);
  program->setUniform("projection", camera.getProjectionMatrixFar());
  state.setProgram(program->getID());
  g_stats.program_changes++;

  for (auto& list : m_unsorted_render_lists)
  {
    if (list->isEmpty())
      continue;

    if (list->getMaterial().tfShouldSort)
      continue;

    auto& layer = list->getMaterial().getLayers().at(0);
    program->setUniform("color_scale", layer.ColorScale);
    program->assertUniformsAreSet();

    {
      render_util::StateModifier state;
      applyMaterial(&list->getMaterial(), state);
      list->render();
    }

    g_stats.active_render_lists++;
  };

#if 0
  program = m_shader_manager.getProgram(ShaderManager::UNSORTED_PARTICLE);
  program->setUniform("projection", camera.getProjectionMatrixFar());
  program->setUniform("world_to_view", camera.getWorld2ViewMatrix());
  program->setUniform("camera_pos", camera.getPos());
  gl::UseProgram(program->getID());
  for (auto& list : m_unsorted_render_lists)
  {
    // if (list->isEmpty())
      // continue;

    // if (list->getMaterial().tfShouldSort)
      // continue;

    auto& layer = list->getMaterial().getLayers().at(0);
    program->setUniform("color_scale", layer.ColorScale);
    program->assertUniformsAreSet();

    {
      render_util::StateModifier state;

      state.setDefaults();

      applyMaterial(&list->getMaterial(), state);

      state.enableAlphaTest(false); //HACK

      // state.enableCullFace(false);
      // state.enableDepthTest(false);

      list->renderParticles();
    }
  };
#endif

  state.setProgram(0);

  m_z_sorted_face_render_list->render(state, camera);

  for (auto& list : m_unsorted_render_lists)
    list->clear();

  m_z_sorted_face_render_list->clear();
}


const std::shared_ptr<UnsortedRenderList>&
RenderGroup::getUnsortedList(const std::shared_ptr<const Material>& material)
{
  assert(material);
  auto& entry = m_unsorted_render_lists_map[material.get()];
  if (!entry)
  {
    entry = std::make_shared<UnsortedRenderList>(material->path, material,
                                                 m_shader_manager);
    entry->m_color = getRandomColor();
    m_unsorted_render_lists.emplace_back(entry);
  }
  return entry;
}


} // namespace il2ge::renderer
