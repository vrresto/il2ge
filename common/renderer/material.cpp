#include "renderer_p.h"
#include <render_util/state.h>
#include <render_util/gl_binding/gl_functions.h>

using namespace render_util::gl_binding;


namespace il2ge::renderer
{


void applyMaterial(const Material *m, render_util::StateModifier &state
    // , render_util::ShaderProgram &program
                   )
{
  assert(m);

  g_stats.material_changes++;

  state.enableCullFace(!m->tfDoubleSide);
  // state.enableCullFace(true);

  // program.setUniform("specular_pow", m->light_params.SpecularPow);
  // program.setUniform("specular_amount", m->light_params.Specular);

  auto& layers = m->getLayers();
  auto& renderer_data = m->getRendererData();

  assert(!layers.empty());
  assert(!renderer_data.layers.empty());

  if (!layers.empty())
  {
    auto& l = layers.front();
    auto& data = renderer_data.layers.front();

    if (data.texture_and_size)
      gl::BindTexture(GL_TEXTURE_2D, data.texture_and_size->texture->getID());
    else
      gl::BindTexture(GL_TEXTURE_2D, 0);

    gl::BindSampler(0, data.sampler_id);

    gl::Color4f(l.ColorScale.r, l.ColorScale.g, l.ColorScale.b, l.ColorScale.a);

    state.enableDepthTest(true);

    if (l.tfTestZ)
      state.setDepthFunc(l.tfTestZEqual ? GL_LEQUAL : GL_LESS);
    else
      state.setDepthFunc(GL_ALWAYS);

    state.enableBlend(l.tfBlend || l.tfBlendAdd);

    if (l.tfBlendAdd)
      state.setBlendFunc(GL_SRC_ALPHA, GL_ONE);
    else
      state.setBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // program.setUniform<bool>("alpha_test_enabled", (l.tfBlend || l.tfBlendAdd) ? false : l.tfTestA);
    state.enableAlphaTest(l.tfTestA);
    state.setAlphaFunc(GL_GREATER, l.AlphaTestVal);

    // program.setUniform("color_scale",  l.ColorScale);
    // program.setUniform<bool>("texture_is_greyscale",  l.texture_is_greyscale);

    state.setDepthMask(!l.tfNoWriteZ);

    if (l.tfDepthOffset || m->tfDepthOffset)
    {
      state.enablePolygonOffsetFill(true);
      state.setPolygonOffset(-0.15, -3.0);
    }
    else
    {
      state.enablePolygonOffsetFill(false);
      state.setPolygonOffset(0, 0);
    }
  }
  else
    gl::BindTexture(GL_TEXTURE_2D, 0);

  // program.assertUniformsAreSet();
}


} // namespace il2ge::renderer
