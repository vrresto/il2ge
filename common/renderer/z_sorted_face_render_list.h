#pragma once

#include "renderer_p.h"
#include <il2ge/renderer.h>
#include <il2ge/mesh/mesh_base.h>
#include <render_util/state.h>
#include <render_util/camera.h>


#include <variant>


namespace il2ge::renderer
{


struct ZSortedFaceRenderList
{
  struct Batch;
  class MeshFacesVAO;

  struct ParticleItem
  {
    ParticleInstance instance;

    ParticleItem(const ParticleInstance&);
  };

  struct ParticleGroupItem : public util::NonCopyable
  {
    std::vector<ParticleInstance> instances;

    ParticleGroupItem(ParticleGroupItem&&);
    ParticleGroupItem(std::vector<ParticleInstance>&&);
  };

  struct MeshFaceItem
  {
    mesh::Face face {};
    size_t instance_index {};

    MeshFaceItem(const mesh::Face&, size_t instance_index);
  };

  struct MeshFaceGroupItem : public util::NonCopyable
  {
    std::vector<mesh::Face> faces;
    size_t instance_index {};

    MeshFaceGroupItem(MeshFaceGroupItem&&);
    MeshFaceGroupItem(std::vector<mesh::Face>&&, size_t instance_index);
  };

  struct MeshInstance
  {
    glm::mat4 model_view;
  };

  struct Item : public util::NonCopyable
  {
    enum Type
    {
      NONE, PARTICLE, MESH_FACE, PARTICLE_GROUP, MESH_FACE_GROUP
    };

    const Type type = Type::NONE;
    const float distance = 0;
    std::shared_ptr<const Material> material; //FIXME

    std::variant<MeshFaceItem,
                 MeshFaceGroupItem,
                 ParticleItem,
                 ParticleGroupItem> variant;

    Item(Item&&);

    Item(const ParticleInstance&,
         const std::shared_ptr<const Material>&,
         float dist);

    Item(std::vector<mesh::Face>&& faces,
         const std::shared_ptr<const Material>& mat,
         const glm::mat4& model_view,
         size_t mesh_instance_index,
         float dist);

    Item(const mesh::Face&,
         size_t instance_index,
         const std::shared_ptr<const Material>&,
         float dist);

    Item(std::vector<ParticleInstance>&&,
         const std::shared_ptr<const Material>&,
         float dist);
  };

  const render_util::Camera3D& m_camera;
  ShaderManager& m_shader_manager;

  std::vector<Item> m_items;
  std::vector<size_t> m_sorted;

  DynamicBuffer<ParticleInstance> m_particle_instance_buffer;
  int m_particle_count = 0;
  std::unique_ptr<render_util::VertexArrayObject> m_quad_vao;

  std::vector<MeshInstance> m_mesh_instances;
  DynamicBuffer<MeshInstance> m_mesh_instance_buffer;
  std::unique_ptr<MeshFacesVAO> m_mesh_faces_vao;
  size_t m_mesh_face_count = 0;

  ZSortedFaceRenderList(ShaderManager&, const render_util::Camera3D&);
  ~ZSortedFaceRenderList();

  void addFace(const mesh::Face& face,
               const std::shared_ptr<const Material>& mat,
               const glm::mat4& model_view,
               size_t mesh_instance_index);

  void addFaceGroup(std::vector<mesh::Face>&& faces,
               const std::shared_ptr<const Material>& mat,
               const glm::mat4& model_view,
               size_t mesh_instance_index,
               float dist);

  void add(const std::vector<mesh::Face>& faces,
           const MaterialList& materials,
           const glm::mat4& model_view,
           float dist);

  void add(std::vector<ParticleInstance>&&,
           const std::shared_ptr<const Material>&,
           float dist = 0);

  void clear();
  void sort();
  void render(render_util::StateModifier &state, const render_util::Camera& camera);

private:
  void createBatches(std::vector<Batch>&);
  void reserveParticleInstanceBuffer();
};


} // namespace il2ge::renderer
