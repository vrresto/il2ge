/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <il2ge/renderer.h>
#include <il2ge/material.h>
#include <il2ge/image_loader.h>
#include <il2ge/mesh/face_group.h>
#include <il2ge/mesh/mesh_base.h>
#include <render_util/vao.h>
#include <render_util/texture_util.h>
#include <render_util/image_util.h>
#include <render_util/shader_util.h>
#include <render_util/state.h>
#include <render_util/quad_2d.h>
#include <render_util/gl_binding/gl_functions.h>
#include <util.h>
#include <block_allocator.h>


#include <glm/gtc/type_ptr.hpp>
#include <map>


#define ENABLE_TRANSPARENCY 1


using namespace il2ge::renderer;
using namespace render_util::gl_binding;


namespace
{


constexpr GLuint VERTEX_ATTRIB_MODEL_VIEW_COL0_INDEX = 4;
constexpr GLuint VERTEX_ATTRIB_MODEL_VIEW_COL1_INDEX = 5;
constexpr GLuint VERTEX_ATTRIB_MODEL_VIEW_COL2_INDEX = 6;
constexpr GLuint VERTEX_ATTRIB_MODEL_VIEW_COL3_INDEX = 7;

constexpr auto INDEX_TYPE = GL_UNSIGNED_SHORT;
constexpr auto INDEX_SIZE = 2;
static_assert(sizeof(il2ge::mesh::Index) == INDEX_SIZE);
static_assert(std::is_same<il2ge::mesh::Index, uint16_t>::value);

using MatrixColumn = glm::vec4;
using MatrixBufferElement = glm::mat4;

static_assert(sizeof(MatrixColumn) == sizeof(float) * 4);
static_assert(sizeof(MatrixBufferElement) == sizeof(MatrixColumn) * 4);


class ProgramVariations
{
  render_util::ShaderProgramPtr m_program_no_atmosphere;
  render_util::ShaderProgramPtr m_program_with_atmosphere;
  bool m_is_atmosphere_enabled = false;

public:
  void create(std::string name,
              render_util::TextureManager &txmgr,
              render_util::ShaderParameters shader_params,
              render_util::ShaderSearchPath shader_search_path,
              const std::map<unsigned int, std::string>  &attribute_locations)
  {
    {
      shader_params.set("enable_atmosphere", false);

      m_program_no_atmosphere = render_util::createShaderProgram(name,
                                                                 txmgr,
                                                                 shader_search_path,
                                                                 attribute_locations,
                                                                 shader_params);
      m_program_no_atmosphere->setUniformi("sampler_0", 0);
    }
    {
      shader_params.set("enable_atmosphere", true);

      m_program_with_atmosphere = render_util::createShaderProgram(name,
                                                                 txmgr,
                                                                 shader_search_path,
                                                                 attribute_locations,
                                                                 shader_params);
      m_program_with_atmosphere->setUniformi("sampler_0", 0);
    }
  }

  void enableAtmosphere(bool enable)
  {
    m_is_atmosphere_enabled = enable;
  }

  const render_util::ShaderProgramPtr &get( )
  {
    assert(m_program_with_atmosphere);
    assert(m_program_no_atmosphere);
    return m_is_atmosphere_enabled ?  m_program_with_atmosphere : m_program_no_atmosphere;
  }
};


struct Face
{
  struct Vertex
  {
    glm::vec4 Position;
    glm::vec4 Normal;
    glm::vec2 TextureCoordinate;
  };

  glm::vec4 center {};
  std::array<Vertex, 3> vertices {};
  std::shared_ptr<il2ge::Material> material;
};


struct RenderListItemInstance
{
  glm::mat4 model_view {};
  size_t frame = 0;
};


struct RenderListItem
{
  Mesh *mesh = nullptr;
  std::vector<RenderListItemInstance> instances;
};


bool isTransparent(il2ge::Material &m)
{
  if (m.getLayers().empty())
    return false;

  //FIXME
  auto &l = m.getLayers().at(0);

  return l.tfBlend || l.tfBlendAdd;
}


std::vector<Face> createFaces(il2ge::mesh::LodBase &mesh_in,
                              il2ge::mesh::FaceGroup &fg,
                              std::shared_ptr<il2ge::Material> mat)
{
  auto &vertices_in = mesh_in.getVertices();
  auto &indices_in = mesh_in.getIndices();

  std::vector<Face> faces;

  auto fg_start_index = fg.StartFace * 3;

  for (size_t i = 0; i < fg.FaceCount; i++)
  {
    auto face_start_index = fg_start_index + i * 3;

    Face face;
    face.material = mat;

    for (size_t i = 0; i < face.vertices.size(); i++)
    {
      auto &v = vertices_in.at(fg.StartVertex + indices_in.at(face_start_index+i));
      face.vertices[i].Position = glm::vec4(v.Position, 1);
      face.vertices[i].Normal = glm::vec4(v.Normal, 0);
      face.vertices[i].TextureCoordinate = v.TextureCoordinate;
    }

    face.center = (face.vertices[0].Position +
                   face.vertices[1].Position +
                   face.vertices[2].Position) / glm::vec4(3);

    faces.push_back(face);
  }

  assert(!faces.empty());

  return faces;
}


} // namespace


namespace il2ge::renderer
{


struct Mesh
{
  std::unique_ptr<render_util::VertexArrayObject> vao;
  std::vector<il2ge::mesh::FaceGroup> face_groups;
  std::vector<std::shared_ptr<Material>> materials;
  std::vector<Face> transparent_faces;
  glm::vec4 center {};
  bool has_solid_faces = false;
  bool has_transparent_faces = false;
  size_t vertex_count = 0;
  size_t frame_count = 1;

  size_t getFrameStartVertex(size_t frame)
  {
    assert(frame < frame_count);
    return vertex_count * frame;
  }
};


struct Renderer::RenderListBase
{
  ProgramVariations m_program;

  const render_util::ShaderProgramPtr &getProgram()
  {
    return m_program.get();
  }

  virtual size_t render(render_util::StateModifier &state, size_t base_instance) = 0;
};


struct Renderer::ZSortingMeshRenderList : public RenderListBase
{
  struct Item
  {
    float distance = 0;
    Mesh *mesh = nullptr;
    glm::mat4 model_view {};
  };

  util::BlockAllocator<Item, 10000> m_allocator;

  std::vector<Item*> m_list_sorted;

//   render_util::ShaderProgramPtr m_program;

  int m_draw_calls = 0;


  ZSortingMeshRenderList(render_util::TextureManager &txmgr,
                            render_util::ShaderParameters shader_params,
                            render_util::ShaderSearchPath shader_search_path)
  {
    m_program.create("objects_transparent_mesh",
                     txmgr, shader_params,
                     shader_search_path,
                     {});
//     m_program =
//         render_util::createShaderProgram("objects_transparent_mesh",
//                                          txmgr,
//                                          shader_search_path,
//                                          {},
//                                          shader_params);
//     m_program->setUniformi("sampler_0", 0);
  }


  void add(Mesh *mesh, const glm::mat4 &model_view)
  {
    assert(mesh->has_transparent_faces);
    assert(!mesh->transparent_faces.empty());

    auto center_view = model_view * mesh->center;
    auto dist = glm::length(center_view);

    auto item = m_allocator.alloc();
    item->distance = dist;
    item->mesh = mesh;
    item->model_view = model_view;

    m_list_sorted.push_back(item);
  }


  bool isEmpty() { return m_list_sorted.empty(); }


  int getSize() { return m_list_sorted.size(); }


  void clear()
  {
    m_list_sorted.clear();
    m_allocator.clear();
  }


  void sort()
  {
    struct
    {
      bool operator()(const Item *a, const Item *b) const
      {
        return (a->distance > b->distance);
      }
    } customLess;

    std::sort(m_list_sorted.begin(),
              m_list_sorted.end(),
              customLess);
  }


  size_t render(render_util::StateModifier &state, size_t base_instance) override
  {
    m_draw_calls = 0;

    if (isEmpty())
      return 0;

    state.setDepthMask(false);
    state.enableDepthTest(true);

    il2ge::Material *last_material = nullptr;

    for (size_t i = 0; i < m_list_sorted.size(); i++)
    {
      auto &item = m_list_sorted[i];
      auto &mesh = item->mesh;
      auto &vao = *mesh->vao;

      render_util::VertexArrayObjectBinding vao_binding(vao);
      render_util::IndexBufferBinding index_buffer_binding(vao);

      getProgram()->setUniform("model_view", item->model_view);

      for (auto &fg : mesh->face_groups)
      {
        auto mat = mesh->materials.at(fg.Material).get();

        if (!mat)
          continue;

        auto &layers = mat->getLayers();
        if (layers.empty())
          continue;

        auto &l = layers.at(0);

        if (l.tfNoTexture || !l.texture)
          continue;

        if (!(l.tfBlend || l.tfBlendAdd))
          continue;

        if (mat != last_material)
        {
          last_material = mat;
          applyMaterial(mat, state, *getProgram());
        }

        intptr_t index_buffer_offset = INDEX_SIZE * 3 * fg.StartFace;

        gl::DrawElementsBaseVertex(GL_TRIANGLES,
                                   fg.FaceCount * 3,
                                   INDEX_TYPE,
                                   (void*)index_buffer_offset,
                                   fg.StartVertex);
        m_draw_calls++;
      }
    }

    return 0;
  }

};


struct Renderer::ZSortingFaceRenderList : public RenderListBase
{
  template <typename T>
  using Triple = std::array<T, 3>;

  using VertexBufferElement = Triple<glm::vec4>;
  using NormalBufferElement = Triple<glm::vec3>;
  using TexcoordBufferElement = Triple<glm::vec2>;

  struct Item
  {
    float distance = 0;
    const Face *face = nullptr;
    VertexBufferElement vertices;
    NormalBufferElement normals;
  };

  struct Batch
  {
    size_t start_element = 0;
    size_t num_elements = 0;
    Material *material = nullptr;
  };

  util::BlockAllocator<Item, 10000> m_allocator;

  std::vector<Item*> m_list_sorted;
  unsigned int m_vao_id = 0;
  unsigned int m_vertex_buffer_id = 0;
  unsigned int m_normal_buffer_id = 0;
  unsigned int m_texcoord_buffer_id = 0;

//   render_util::ShaderProgramPtr m_program;

  int m_draw_calls = 0;


  ZSortingFaceRenderList(render_util::TextureManager &txmgr,
                            render_util::ShaderParameters shader_params,
                            render_util::ShaderSearchPath shader_search_path)
  {
    gl::GenBuffers(1, &m_vertex_buffer_id);
    assert(m_vertex_buffer_id > 0);
    gl::GenBuffers(1, &m_normal_buffer_id);
    assert(m_normal_buffer_id > 0);
    gl::GenBuffers(1, &m_texcoord_buffer_id);
    assert(m_texcoord_buffer_id > 0);
    gl::GenVertexArrays(1, &m_vao_id);
    assert(m_vao_id > 0);

    gl::BindVertexArray(m_vao_id);

    gl::EnableClientState(GL_VERTEX_ARRAY);
    gl::BindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer_id);
    gl::VertexPointer(4, GL_FLOAT, 0, nullptr);
    gl::BindBuffer(GL_ARRAY_BUFFER, 0);

    gl::EnableClientState(GL_NORMAL_ARRAY);
    gl::BindBuffer(GL_ARRAY_BUFFER, m_normal_buffer_id);
    gl::NormalPointer(GL_FLOAT, 0, nullptr);
    gl::BindBuffer(GL_ARRAY_BUFFER, 0);

    gl::EnableClientState(GL_TEXTURE_COORD_ARRAY);
    gl::BindBuffer(GL_ARRAY_BUFFER, m_texcoord_buffer_id);
    gl::TexCoordPointer(2, GL_FLOAT, 0, nullptr);
    gl::BindBuffer(GL_ARRAY_BUFFER, 0);

    gl::BindVertexArray(0);

    m_program.create("objects_transparent",
                     txmgr, shader_params,
                     shader_search_path,
                     {});

//     m_program = render_util::createShaderProgram("objects_transparent",
//                                                   txmgr,
//                                                   shader_search_path,
//                                                   {},
//                                                   shader_params);
//     m_program->setUniformi("sampler_0", 0);
  }


  ~ZSortingFaceRenderList()
  {
    gl::DeleteVertexArrays(1, &m_vao_id);
    gl::DeleteBuffers(1, &m_vertex_buffer_id);
    gl::DeleteBuffers(1, &m_normal_buffer_id);
    gl::DeleteBuffers(1, &m_texcoord_buffer_id);
  }


  void addFace(Face &face, const glm::mat4 &model_view)
  {
    auto center_view = model_view * face.center;
    auto dist = glm::length(center_view);

    auto item = m_allocator.alloc();
    item->distance = dist;
    item->face = &face;

    for (size_t i = 0; i < item->vertices.size(); i++)
      item->vertices[i] = model_view * face.vertices[i].Position;

    for (size_t i = 0; i < item->normals.size(); i++)
      item->normals[i] = model_view * face.vertices[i].Normal;
//       item->normals[i] = face.vertices[i].Normal;

    m_list_sorted.push_back(item);
  }


  void add(Mesh *mesh, const glm::mat4 &model_view)
  {
    assert(mesh->has_transparent_faces);
    assert(!mesh->transparent_faces.empty());

    for (auto &f : mesh->transparent_faces)
    {
      addFace(f, model_view);
    }
  }


  bool isEmpty() { return m_list_sorted.empty(); }


  int getSize() { return m_list_sorted.size(); }


  void clear()
  {
    m_list_sorted.clear();
    m_allocator.clear();
  }


  void sort()
  {
    struct
    {
      bool operator()(const Item *a, const Item *b) const
      {
        return (a->distance > b->distance);
      }
    } customLess;

    std::sort(m_list_sorted.begin(),
              m_list_sorted.end(),
              customLess);
  }


  size_t render(render_util::StateModifier &state, size_t base_instance) override
  {
    m_draw_calls = 0;

    if (isEmpty())
      return 0;

    state.setDepthMask(false);
    state.enableDepthTest(true);

    gl::BindVertexArray(m_vao_id);

    gl::NamedBufferData(m_vertex_buffer_id,
                        m_list_sorted.size() * sizeof(VertexBufferElement),
                        nullptr, GL_STREAM_DRAW);

    gl::NamedBufferData(m_texcoord_buffer_id,
                        m_list_sorted.size() * sizeof(TexcoordBufferElement),
                        nullptr, GL_STREAM_DRAW);

    gl::NamedBufferData(m_normal_buffer_id,
                        m_list_sorted.size() * sizeof(NormalBufferElement),
                        nullptr, GL_STREAM_DRAW);

    auto vertex_buffer = (VertexBufferElement*)
      gl::MapNamedBuffer(m_vertex_buffer_id, GL_WRITE_ONLY);

    auto texcoord_buffer = (TexcoordBufferElement*)
      gl::MapNamedBuffer(m_texcoord_buffer_id, GL_WRITE_ONLY);

    auto normal_buffer = (NormalBufferElement*)
      gl::MapNamedBuffer(m_normal_buffer_id, GL_WRITE_ONLY);

    std::vector<Batch> batches;
    for (size_t i = 0; i < m_list_sorted.size(); i++)
    {
      auto item = m_list_sorted[i];
      auto face = item->face;

      vertex_buffer[i] = item->vertices;
      normal_buffer[i] = item->normals;

      for (size_t i_vertex = 0; i_vertex < 3; i_vertex++)
      {
        texcoord_buffer[i][i_vertex] = face->vertices[i_vertex].TextureCoordinate;
      }

      if (batches.empty())
      {
        batches.push_back({0, 0, face->material.get()});
      }
      else if (batches.back().material != face->material.get())
      {
        batches.push_back({i, 0, face->material.get()});
      }

      batches.back().num_elements++;
    }

    vertex_buffer = nullptr;
    gl::UnmapNamedBuffer(m_vertex_buffer_id);
    texcoord_buffer = nullptr;
    gl::UnmapNamedBuffer(m_texcoord_buffer_id);
    normal_buffer = nullptr;
    gl::UnmapNamedBuffer(m_normal_buffer_id);

    for (size_t i = 0; i < batches.size(); i++)
    {
      auto &batch = batches[i];
      applyMaterial(batch.material, state, *getProgram());
      gl::DrawArrays(GL_TRIANGLES, batch.start_element * 3, batch.num_elements * 3);
      m_draw_calls++;
    }

    gl::BindVertexArray(0);

    return 0;
  }

};


class Renderer::RenderList : public RenderListBase
{
  std::vector<std::unique_ptr<RenderListItem>> items;
  std::unordered_map<Mesh*, RenderListItem*> map;

  std::vector<std::unique_ptr<RenderListItem>> items_animated;
  std::unordered_map<Mesh*, RenderListItem*> map_animated;

  int m_total_instances = 0;

public:
//   render_util::ShaderProgramPtr m_object_program;
//   ProgramVariations m_program;
  int m_draw_calls = 0;
  bool m_transparent = false;

  RenderList(render_util::TextureManager &txmgr,
             render_util::ShaderParameters shader_params,
             render_util::ShaderSearchPath shader_search_path)
  {
    std::map<unsigned int, std::string>  attribute_locations =
    {
      { VERTEX_ATTRIB_MODEL_VIEW_COL0_INDEX, "model_view_col0" },
      { VERTEX_ATTRIB_MODEL_VIEW_COL1_INDEX, "model_view_col1" },
      { VERTEX_ATTRIB_MODEL_VIEW_COL2_INDEX, "model_view_col2" },
      { VERTEX_ATTRIB_MODEL_VIEW_COL3_INDEX, "model_view_col3" },
    };

    m_program.create("objects", txmgr, shader_params, shader_search_path, attribute_locations);
//     m_object_program = render_util::createShaderProgram("objects",
//                                                         txmgr,
//                                                         shader_search_path,
//                                                         attribute_locations,
//                                                         shader_params);
//     m_object_program->setUniformi("sampler_0", 0);
  }

  ~RenderList()
  {
  }


  size_t fillInstanceBuffer(MatrixBufferElement *buffer, size_t buffer_size)
  {
    const auto buffer_elements = getTotalInstances();

    assert(buffer);
    assert(buffer_size > 0);
    assert(buffer_elements <= buffer_size);

    size_t written_elements = 0;

    try
    {
      size_t index = 0;

      for (auto &item : items)
      {
        for (auto &instance : item->instances)
        {
          assert(index < buffer_size);
          buffer[index] = instance.model_view;
          index++;
        }
      }

      for (auto &item : items_animated)
      {
        for (auto &instance : item->instances)
        {
          assert(index < buffer_size);
          buffer[index] = instance.model_view;
          index++;
        }
      }
      written_elements = index;
      assert(written_elements == buffer_elements);
    }
    catch(...)
    {
      abort();
    }

    return written_elements;
  }

  int getAnimatedMeshesCount()
  {
    return items_animated.size();
  }

  size_t render(render_util::StateModifier &state, size_t base) override;


  void addAnimatedInstance(Mesh *mesh, const RenderListItemInstance &instance)
  {
    auto it = map_animated.find(mesh);

    if (it != map_animated.end())
    {
      it->second->instances.push_back(instance);
    }
    else
    {
      auto item = std::make_unique<RenderListItem>();
      item->mesh = mesh;
      item->instances.push_back(instance);

      map_animated[mesh] = item.get();

      items_animated.push_back(std::move(item));
    }

    m_total_instances++;
  }

  void addInstance(Mesh *mesh, const RenderListItemInstance &instance)
  {
    if (mesh->frame_count > 1)
    {
      addAnimatedInstance(mesh, instance);
      return;
    }

    auto it = map.find(mesh);

    if (it != map.end())
    {
      it->second->instances.push_back(instance);
    }
    else
    {
      auto item = std::make_unique<RenderListItem>();
      item->mesh = mesh;
      item->instances.push_back(instance);

      map[mesh] = item.get();

      items.push_back(std::move(item));
    }

    m_total_instances++;
  }

  size_t getTotalInstances()
  {
    return m_total_instances;
  }

  void clear()
  {
    m_total_instances = 0;
    map.clear();
    items.clear();
    map_animated.clear();
    items_animated.clear();
  }

  bool empty() { return items.empty() && items_animated.empty(); }

//   const std::vector<std::unique_ptr<RenderListItem>> &getItems() const { return items; }
};


Renderer::Renderer(render_util::ShaderSearchPath shader_search_path,
                   render_util::ShaderParameters shader_params,
                   ReadFileFunc read_file) :
  m_parameter_files(read_file),
  m_shader_search_path(shader_search_path),
  m_read_file(read_file)
{
  m_render_list_solid = std::make_unique<RenderList>(m_txmgr, shader_params, shader_search_path);
  m_render_list_transparent_unsorted = std::make_unique<RenderList>(m_txmgr, shader_params, shader_search_path);
  m_render_list_transparent_unsorted->m_transparent = true;

  m_render_list_z_sorted_faces =
    std::make_unique<ZSortingFaceRenderList>(m_txmgr, shader_params, shader_search_path);
  m_render_list_z_sorted_meshes =
    std::make_unique<ZSortingMeshRenderList>(m_txmgr, shader_params, shader_search_path);

  m_instancing_render_lists.push_back(m_render_list_solid.get());
  m_instancing_render_lists.push_back(m_render_list_transparent_unsorted.get());

  gl::GenBuffers(1, &m_instance_buffer_id);
  assert(m_instance_buffer_id > 0);
  gl::BindBuffer(GL_ARRAY_BUFFER, m_instance_buffer_id);
  gl::BindBuffer(GL_ARRAY_BUFFER, 0);
  assert(gl::IsBuffer(m_instance_buffer_id));
}


Renderer::~Renderer()
{
  gl::DeleteBuffers(1, &m_instance_buffer_id);
}


void Renderer::updateInstanceBuffer()
{
  size_t num_elements = 0;
  for (auto &list : m_instancing_render_lists)
    num_elements += list->getTotalInstances();

  if (!num_elements)
  {
    m_num_instances = 0;
    return;
  }

  const auto required_size = sizeof(MatrixBufferElement) * num_elements;

  m_max_instance_buffer_size = glm::max(required_size, m_max_instance_buffer_size);

  // avoid different buffer sizes on each call
  const auto buffer_size = m_max_instance_buffer_size;

  assert(buffer_size >= required_size);

  m_num_instances = num_elements;

  assert(gl::IsBuffer(m_instance_buffer_id));

  gl::NamedBufferData(m_instance_buffer_id, buffer_size, nullptr, GL_STREAM_DRAW);
  auto buffer = (MatrixBufferElement*) gl::MapNamedBuffer(m_instance_buffer_id, GL_WRITE_ONLY);

  auto remaining_buffer_elements = num_elements;
  size_t base_instance = 0;

//   LOG_INFO << "num_elements: " << num_elements << std::endl;

  for (auto &list : m_instancing_render_lists)
  {
    if (list->getTotalInstances() == 0)
      continue;
//     LOG_INFO << "base_instance: " << base_instance << std::endl;
//     LOG_INFO << "m_num_instances: " << m_num_instances << std::endl;
//     LOG_INFO << "render list instances: " << list->getTotalInstances() << std::endl;
//     LOG_INFO << "(base_instance < num_elements) = " << (base_instance < num_elements) << std::endl;
    assert(base_instance < num_elements);
    auto written_elements = list->fillInstanceBuffer(buffer + base_instance,
                                                     remaining_buffer_elements);
    assert(written_elements == list->getTotalInstances());
    remaining_buffer_elements -= written_elements;
    assert(remaining_buffer_elements >= 0);
    base_instance += written_elements;
    assert(base_instance <= num_elements);
  }

  buffer = nullptr;
  gl::UnmapNamedBuffer(m_instance_buffer_id);
}


void Renderer::addToRenderList(Mesh *mesh, const Mat4& model_view, float distance,
                               size_t frame)
{
  RenderListItemInstance instance;
  instance.model_view = model_view;
  instance.frame = frame;

  if (mesh->has_solid_faces)
  {
    m_render_list_solid->addInstance(mesh, instance);
  }

#if ENABLE_TRANSPARENCY
  if (mesh->has_transparent_faces)
  {
    if (distance < 50)
        m_render_list_z_sorted_faces->add(mesh, instance.model_view);
    else if (distance < m_max_transparent_visibility)
      m_render_list_z_sorted_meshes->add(mesh, instance.model_view);
    else
      m_render_list_transparent_unsorted->addInstance(mesh, instance);
  }
#endif
}


void Renderer::bindVertexAttribBuffers(render_util::VertexArrayObject &vao)
{
  render_util::VertexArrayObjectBinding vao_binding(vao);

  gl::VertexAttribDivisor(VERTEX_ATTRIB_MODEL_VIEW_COL0_INDEX, 1);
  gl::VertexAttribDivisor(VERTEX_ATTRIB_MODEL_VIEW_COL1_INDEX, 1);
  gl::VertexAttribDivisor(VERTEX_ATTRIB_MODEL_VIEW_COL2_INDEX, 1);
  gl::VertexAttribDivisor(VERTEX_ATTRIB_MODEL_VIEW_COL3_INDEX, 1);

  gl::EnableVertexAttribArray(VERTEX_ATTRIB_MODEL_VIEW_COL0_INDEX);
  gl::EnableVertexAttribArray(VERTEX_ATTRIB_MODEL_VIEW_COL1_INDEX);
  gl::EnableVertexAttribArray(VERTEX_ATTRIB_MODEL_VIEW_COL2_INDEX);
  gl::EnableVertexAttribArray(VERTEX_ATTRIB_MODEL_VIEW_COL3_INDEX);

  assert(gl::IsVertexArray(vao.getID()));
  assert(gl::IsBuffer(m_instance_buffer_id));

  gl::BindBuffer(GL_ARRAY_BUFFER, m_instance_buffer_id);

  constexpr auto stride = sizeof(MatrixBufferElement);
  intptr_t offset = 0;

  gl::VertexAttribPointer(VERTEX_ATTRIB_MODEL_VIEW_COL0_INDEX, 4, GL_FLOAT, false,
                          stride, (void*)offset);
  offset += sizeof(MatrixColumn);

  gl::VertexAttribPointer(VERTEX_ATTRIB_MODEL_VIEW_COL1_INDEX, 4, GL_FLOAT, false,
                          stride, (void*)offset);
  offset += sizeof(MatrixColumn);

  gl::VertexAttribPointer(VERTEX_ATTRIB_MODEL_VIEW_COL2_INDEX, 4, GL_FLOAT, false,
                          stride, (void*)offset);
  offset += sizeof(MatrixColumn);

  gl::VertexAttribPointer(VERTEX_ATTRIB_MODEL_VIEW_COL3_INDEX, 4, GL_FLOAT, false,
                          stride, (void*)offset);

  gl::BindBuffer(GL_ARRAY_BUFFER, 0);
}


size_t Renderer::processRenderList(RenderListBase &list, render_util::StateModifier &state,
      std::function<void(render_util::ShaderProgramPtr)> set_uniforms,
      size_t base_instance)
{
  list.m_program.enableAtmosphere(m_is_atmosphere_enabled);

  auto &program = list.m_program.get();

  render_util::updateUniforms(program, m_camera);
  set_uniforms(program);
  program->setUniform("projection", m_camera.getProjectionMatrixFar());

  gl::UseProgram(program->getID());
  auto rendered_instances = list.render(state, base_instance);
  gl::UseProgram(0);

  return rendered_instances;
}


void Renderer::flushRenderList(std::function<void(render_util::ShaderProgramPtr)> set_uniforms)
{
  m_flush_count++;

  GLenum active_unit_save;
  gl::GetIntegerv(GL_ACTIVE_TEXTURE, reinterpret_cast<GLint*>(&active_unit_save));

  gl::ActiveTexture(GL_TEXTURE0);

  GLint prev_texture = 0;
  gl::GetIntegerv(GL_TEXTURE_BINDING_2D, reinterpret_cast<GLint*>(&prev_texture));

  render_util::StateModifier state;
  state.setDefaults();
  state.setCullFace(GL_BACK);
  state.setFrontFace(GL_CCW);
  state.enableCullFace(true);
  state.setDepthFunc(GL_LEQUAL);
  state.setBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  updateInstanceBuffer();

  size_t base_instance = 0;

  if (m_num_instances > 0)
  {
    for (auto &list : m_instancing_render_lists)
    {
      if (list->getTotalInstances() == 0)
        continue;

      assert(base_instance < m_num_instances);
      auto rendered_instances = processRenderList(*list, state, set_uniforms, base_instance);
      assert(rendered_instances == list->getTotalInstances());
      base_instance += rendered_instances;
    }
  }

  assert(base_instance == m_num_instances);

//   render_util::updateUniforms(m_render_list_solid->m_program, m_camera);
//   render_util::updateUniforms(m_render_list_z_sorted_meshes->m_program, m_camera);
//   render_util::updateUniforms(m_render_list_z_sorted_faces->m_program, m_camera);
//   set_uniforms(m_render_list_solid->m_object_program);
//   set_uniforms(m_render_list_z_sorted_meshes->m_program);
//   set_uniforms(m_render_list_z_sorted_faces->m_program);

//   m_render_list_solid->m_object_program->setUniform("projection", m_camera.getProjectionMatrixFar());
//   m_render_list_solid->m_object_program->assertUniformsAreSet();
//   render_util::getCurrentGLContext()->setCurrentProgram(m_render_list_solid->m_program.get(m_is_atmosphere_enabled));
//   m_render_list_solid->render(false, state);
//   render_util::getCurrentGLContext()->setCurrentProgram(nullptr);
//   processRenderList(*m_render_list_solid, state, set_uniforms);

#if ENABLE_TRANSPARENCY
//   m_render_list_z_sorted_meshes->m_program->setUniform("projection",
//                                                        m_camera.getProjectionMatrixFar());
//   m_render_list_z_sorted_meshes->m_program->assertUniformsAreSet();
//   render_util::getCurrentGLContext()->setCurrentProgram(m_render_list_z_sorted_meshes->m_program);
  m_render_list_z_sorted_meshes->sort();
  processRenderList(*m_render_list_z_sorted_meshes, state, set_uniforms, 0);
//   m_render_list_z_sorted_meshes->render(state);
//   render_util::getCurrentGLContext()->setCurrentProgram(nullptr);

//   m_render_list_z_sorted_faces->m_program->setUniform("projection",
//                                                        m_camera.getProjectionMatrixFar());
//   m_render_list_z_sorted_faces->m_program->assertUniformsAreSet();
//   render_util::getCurrentGLContext()->setCurrentProgram(m_render_list_z_sorted_faces->m_program);
  m_render_list_z_sorted_faces->sort();
  processRenderList(*m_render_list_z_sorted_faces, state, set_uniforms, 0);
//   m_render_list_z_sorted_faces->render(state);
//   render_util::getCurrentGLContext()->setCurrentProgram(nullptr);
#endif

  m_last_render_list_solid_size = m_render_list_solid->getTotalInstances();
  m_last_render_list_z_sorted_meshes_size = m_render_list_z_sorted_meshes->getSize();
  m_last_render_list_z_sorted_faces_size = m_render_list_z_sorted_faces->getSize();
  m_last_animated_meshes_count = m_render_list_solid->getAnimatedMeshesCount();

  m_render_list_solid->clear();
#if ENABLE_TRANSPARENCY
  m_render_list_z_sorted_faces->clear();
  m_render_list_z_sorted_meshes->clear();
#endif

  for (auto &list : m_instancing_render_lists)
    list->clear();

  gl::BindTexture(GL_TEXTURE_2D, prev_texture);
  gl::ActiveTexture(active_unit_save);

  //FIXME
  gl::Disable(GL_POLYGON_OFFSET_FILL);
  gl::PolygonOffset(0, 0);
}


size_t Renderer::RenderList::render(render_util::StateModifier &state, size_t base_instance)
{
  m_draw_calls = 0;
  size_t rendered_instances = 0;

  if (empty())
    return 0;

  state.setDepthMask(!m_transparent);
  state.enableDepthTest(true);

  for (auto &item : items)
  {
    auto &mesh = item->mesh;
    auto &vao = *mesh->vao;

    render_util::VertexArrayObjectBinding vao_binding(vao);
    render_util::IndexBufferBinding index_buffer_binding(vao);

    for (auto &fg : mesh->face_groups)
    {
      auto mat = mesh->materials.at(fg.Material).get();

      if (!mat)
        continue;

      auto &layers = mat->getLayers();
      if (layers.empty())
        continue;

      auto &l = layers.at(0);

//       if (l.tfTestA)
//         continue;

      if (l.tfNoTexture || !l.texture)
        continue;
#if 1

      if (m_transparent && !isTransparent(*mat))
        continue;

      if (!m_transparent && isTransparent(*mat))
        continue;
#endif

      applyMaterial(mat, state, *getProgram());

      intptr_t index_buffer_offset = INDEX_SIZE * 3 * fg.StartFace;

      gl::DrawElementsInstancedBaseVertexBaseInstance(GL_TRIANGLES,
                                                      fg.FaceCount * 3,
                                                      INDEX_TYPE,
                                                      (void*)index_buffer_offset,
                                                      item->instances.size(),
                                                      fg.StartVertex,
                                                      base_instance);
      m_draw_calls++;
    }

    base_instance += item->instances.size();
    rendered_instances += item->instances.size();
  }

  for (auto &item : items_animated)
  {
    auto &mesh = item->mesh;
    auto &vao = *mesh->vao;

    render_util::VertexArrayObjectBinding vao_binding(vao);
    render_util::IndexBufferBinding index_buffer_binding(vao);

    for (auto &fg : mesh->face_groups)
    {
      auto mat = mesh->materials.at(fg.Material).get();
      assert(mat);

      if (!mat)
        continue;

      auto &layers = mat->getLayers();
      if (layers.empty())
        continue;

      auto &l = layers.at(0);

      if (l.tfNoTexture || !l.texture)
        continue;

      if (m_transparent && !(l.tfBlend || l.tfBlendAdd))
        continue;

      if (!m_transparent && (l.tfBlend || l.tfBlendAdd))
        continue;

      applyMaterial(mat, state, *getProgram());

      intptr_t index_buffer_offset = INDEX_SIZE * 3 * fg.StartFace;
      
      assert(!item->instances.empty());

      for (auto &instance : item->instances)
      {
        auto frame_start_vertex =
          mesh->getFrameStartVertex(instance.frame);

        gl::DrawElementsInstancedBaseVertexBaseInstance(GL_TRIANGLES,
                                                        fg.FaceCount * 3,
                                                        INDEX_TYPE,
                                                        (void*)index_buffer_offset,
                                                        1, // one instance FIXME UGLY
                                                        frame_start_vertex + fg.StartVertex,
                                                        base_instance);
        base_instance++;
        m_draw_calls++;
      }
    }

    base_instance += item->instances.size();
    rendered_instances += item->instances.size();
  }

  return rendered_instances;
}


void Renderer::releaseMesh(Mesh *mesh)
{
  delete mesh;
}


const Renderer::TextureWrapper &Renderer::getTexture(const std::string path)
{
  auto it = m_textures.find(path);
  if (it != m_textures.end())
  {
    return it->second;
  }
  else
  {
    TextureWrapper wrapper;

    try
    {
      wrapper = loadTexture(path);
    }
    catch (std:: exception &e)
    {
      LOG_ERROR << "Failed to load texture: " << path << std::endl;
      LOG_ERROR << "Exception was: " << e.what() << std::endl;
    }

    if (!wrapper.texture)
    {
      LOG_ERROR << "Failed to load texture: " << path << std::endl;
    }

    m_textures[path] = wrapper;
    return m_textures.at(path);
  }
}


Renderer::TextureWrapper Renderer::loadTexture(const std::string path)
{
  std::vector<char> data;

  try
  {
    auto texture_path = path;

    //HACK
    auto dir = util::getDirFromPath(path);
    auto name = util::basename(path);
    if (util::isPrefix("skin1", name))
    {
      texture_path = dir + "/summer/" + name;
    }

    if (util::isSuffix(".tga", util::makeLowercase(texture_path)))
      texture_path.at(texture_path.size()-1) = 'b';
    data = m_read_file(texture_path);
  }
  catch (...)
  {
    data = m_read_file(path);
  }

  auto image = loadImageFromMemory(data, path.c_str());

  TextureWrapper wrapper;

  if (image)
  {
    wrapper.size = image->getSize();
    wrapper.num_channels = image->getNumComponents();
    wrapper.texture = render_util::createTexture(image);
    render_util::TextureParameters<int> params;
//     params.set(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
//     params.set(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    params.apply(wrapper.texture);
  }

  return wrapper;
}


std::shared_ptr<Material> Renderer::getMaterial(const std::string path)
{
  auto it = m_materials.find(path);
  if (it != m_materials.end())
  {
    return it->second;
  }
  else
  {
    auto material = loadMaterial(path);
    m_materials[path] = material;
    return material;
  }
}


std::shared_ptr<Material> Renderer::loadMaterial(const std::string path)
{
  std::shared_ptr<Material> mat;

  try
  {
    mat = il2ge::loadMaterial(m_parameter_files, path);
  }
  catch (std::exception &e)
  {
    LOG_ERROR << "Failed to create Material from: " << path << std::endl;
    LOG_ERROR << "Exception was: " << e.what() << std::endl;
    return {};
  }

  for (auto &l : mat->getLayers())
  {
    if (!l.texture_path.empty())
    {
      auto wrapper = getTexture(l.texture_path);
      l.texture = wrapper.texture;
      l.texture_is_greyscale = (wrapper.num_channels == 1);
      l.texture_size = wrapper.size;
    }
  }

  return mat;
}


void Renderer::applyMaterial(const Material *m, render_util::StateModifier &state,
                             render_util::ShaderProgram &program)
{
  if (!m)
  {
    gl::BindTexture(GL_TEXTURE_2D, 0);
    return;
  }

  state.enableCullFace(!m->tfDoubleSide);

  program.setUniform("specular_pow", m->light_params.SpecularPow);
  program.setUniform("specular_amount", m->light_params.Specular);

  auto &layers = m->getLayers();

  if (!layers.empty())
  {
    auto &l = layers.front();

    if (l.texture)
      gl::BindTexture(GL_TEXTURE_2D, l.texture->getID());
    else
      gl::BindTexture(GL_TEXTURE_2D, 0);

    state.enableBlend(l.tfBlend || l.tfBlendAdd);

    program.setUniform<bool>("alpha_test_enabled", (l.tfBlend || l.tfBlendAdd) ? false : l.tfTestA);
    state.enableAlphaTest((l.tfBlend || l.tfBlendAdd) ? false : l.tfTestA);
    state.setAlphaFunc(GL_GREATER, l.AlphaTestVal);
    
//     assert(!l.tfTestA);
//     state.enableAlphaTest(false); 

    program.setUniform("color_scale",  l.ColorScale);
    program.setUniform<bool>("texture_is_greyscale",  l.texture_is_greyscale);

    if (l.tfDepthOffset)
    {
      gl::Enable(GL_POLYGON_OFFSET_FILL);
      gl::PolygonOffset(-0.15, -3.0);
    }
    else
    {
      gl::Disable(GL_POLYGON_OFFSET_FILL);
      gl::PolygonOffset(0, 0);
    }
  }
  else
    gl::BindTexture(GL_TEXTURE_2D, 0);

  program.assertUniformsAreSet();
}


Mesh* Renderer::createMesh(il2ge::mesh::LodBase &mesh_in)
{
  constexpr auto TEXCOORD_COMPONENTS = 2;
  using Vertex = std::array<float,3>;
  using Normal = std::array<float,3>;
  using TexCoord = std::array<float, TEXCOORD_COMPONENTS>;

  auto &vertices_in = mesh_in.getVertices();
  auto &indices_in = mesh_in.getIndices();

  glm::vec3 center = glm::vec3(0);

  std::vector<Vertex> vertices(vertices_in.size());
  for (int i = 0; i < vertices.size(); i++)
  {
    auto &v = vertices_in[i].Position;
    vertices[i] = { v.x, v.y, v.z };
    center += v;
  }

  center /= glm::vec3(vertices.size());

  std::vector<TexCoord> texcoords(vertices_in.size());
  for (int i = 0; i < texcoords.size(); i++)
  {
    auto &c = vertices_in[i].TextureCoordinate;
    texcoords[i] = { c.x, c.y };
  }

  std::vector<Normal> normals(vertices_in.size());
  for (int i = 0; i < normals.size(); i++)
  {
    auto &n = vertices_in[i].Normal;
    normals[i] = { n.x, n.y, n.z };
  }

  auto mesh = new Mesh;

  mesh->center = glm::vec4(center, 1);

  mesh->vao = std::make_unique<render_util::VertexArrayObject>(
      vertices.data(), vertices.size() * sizeof(Vertex),
      normals.data(), normals.size() * sizeof(Normal),
      texcoords.data(), texcoords.size() * sizeof(TexCoord), TEXCOORD_COMPONENTS,
      indices_in.data(), indices_in.size() * sizeof(il2ge::mesh::Index));

  bindVertexAttribBuffers(*mesh->vao);

  mesh->face_groups = mesh_in.getFaceGroups();

  for (auto &path : mesh_in.getMaterials())
    mesh->materials.push_back(getMaterial(path));

  for (auto &fg : mesh->face_groups)
  {
    auto &mat = mesh->materials.at(fg.Material);

    if (mat)
    {
      if (isTransparent(*mat))
      {
        mesh->has_transparent_faces = true;
        for (auto &face : createFaces(mesh_in, fg, mat))
          mesh->transparent_faces.push_back(face);
      }
      else
      {
        mesh->has_solid_faces = true;
      }
    }
  }

  mesh->vertex_count = mesh_in.getVertexCount();
  mesh->frame_count = mesh_in.getFrameCount();

  return mesh;
}


void Renderer::applyCamera(const render_util::Camera3D& camera)
{
  m_camera = camera;
}


class TexturedQuad2D : public render_util::Quad2D
{
public:
  TexturedQuad2D(render_util::TextureManager &tex_mgr,
                 const render_util::ShaderSearchPath &shader_search_path) :
    render_util::Quad2D(tex_mgr, shader_search_path, "textured_quad_2d")
  {
    getProgram().setUniformi("sampler", 0);
  }


  void draw(glm::vec2 origin, glm::vec2 extent,
            glm::vec2 texcoord_origin, glm::vec2 texcoord_extent)
  {
    GLenum active_unit_save;
    gl::GetIntegerv(GL_ACTIVE_TEXTURE, reinterpret_cast<GLint*>(&active_unit_save));
    gl::ActiveTexture(GL_TEXTURE0);
    GLint prev_texture = 0;
    gl::GetIntegerv(GL_TEXTURE_BINDING_2D, reinterpret_cast<GLint*>(&prev_texture));

    assert(m_texture);
    gl::BindTexture(GL_TEXTURE_2D, m_texture->getID());

//     auto pos_extent = origin + extent;
    // auto pos_extent = extent;
    
    getProgram().setUniform("texcoord_scale", m_texcoord_scale);
//     render_util::Quad2D::draw(origin.x, origin.y, pos_extent.x, pos_extent.y);
    render_util::Quad2D::draw(origin, extent, texcoord_origin, texcoord_extent);

    gl::BindTexture(GL_TEXTURE_2D, prev_texture);
    gl::ActiveTexture(active_unit_save);
  }


  render_util::TexturePtr m_texture;
  glm::vec2 m_texcoord_origin;
  glm::vec2 m_texcoord_extent;
  glm::vec4 m_texcoord_scale;
};


void Renderer::drawTile(glm::vec2 origin,
                        glm::vec2 extent,
                        glm::vec2 texcoord_origin,
                        glm::vec2 texcoord_extent,
                        const std::string &material_path)
{
//   LOG_INFO << "origin: " << origin << " extent: " << extent << std::endl;
//   LOG_INFO << "texcoord_origin: " << texcoord_origin << " texcoord_extent: " << texcoord_extent << std::endl;
//   abort();

  auto material = getMaterial(material_path);
  assert(material);

  render_util::StateModifier mod;
  mod.setDefaults();


  //FIXME
  static TexturedQuad2D quad(m_txmgr, m_shader_search_path);

  applyMaterial(material.get(), mod, quad.getProgram());
  
  mod.setFrontFace(GL_CW);
  mod.enableCullFace(false);


  assert(material->getLayers().at(0).texture);
  quad.m_texture = material->getLayers().at(0).texture;
  // glm::vec2 texture_size = material->getLayers().at(0).texture_size;
  
  quad.m_texcoord_scale = material->getLayers().at(0).TextureCoordScale;

//   LOG_INFO<<"quad.m_texcoord_scale: "<<quad.m_texcoord_scale <<std::endl;
//   quad.m_texcoord_bottom_left = texcoord_bottom_left;
//   quad.m_texcoord_top_right = texcoord_top_right;

  quad.setColor(material->getLayers().at(0).ColorScale);
//   quad.draw(origin, extent, texcoord_origin / texture_size, texcoord_extent / texture_size);
  quad.draw(origin, extent, texcoord_origin, texcoord_extent);
}


void Renderer::printStats(std::ostream &out)
{
  out << "draw calls (total): " << (m_render_list_solid->m_draw_calls +
                            m_render_list_z_sorted_faces->m_draw_calls +
                            m_render_list_z_sorted_meshes->m_draw_calls) << std::endl;

  out << "draw calls (solid): " << m_render_list_solid->m_draw_calls << std::endl;
  out << "draw calls (transparent meshes): "
    << m_render_list_z_sorted_meshes->m_draw_calls << std::endl;
  out << "draw calls (transparent faces): "
    << m_render_list_z_sorted_faces->m_draw_calls << std::endl;

  out << "solid meshes: " << m_last_render_list_solid_size << std::endl;
  out << "animated meshes: " << m_last_animated_meshes_count << std::endl;
  out << "transparent meshes: " << m_last_render_list_z_sorted_meshes_size << std::endl;
  out << "transparent faces: " << m_last_render_list_z_sorted_faces_size << std::endl;
}


}
