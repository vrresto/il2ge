/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <il2ge/parameter_file.h>
#include <il2ge/section_file.h>
#include <util.h>

#include <glm/glm.hpp>
#include <sstream>
#include <unordered_set>

using namespace std;


namespace
{


void stripComment(string &line, const std::vector<string> &prefixes)
{
  for (auto &p : prefixes)
  {
    auto comment_start = line.find(p);
    if (comment_start != string::npos)
    {
      line = line.substr(0, comment_start);
      return;
    }
  }
}



template <class Section, class Handler, class Stream>
void readSectionFile(Stream &in, Handler &handler,
                     const std::vector<string> &comment_prefixes,
                     char eof = 0)
{
  std::unordered_set<Section*> encountered_sections;

  Section *section = nullptr;
  bool ignore_section = false;

  int line_num = 0;

  while (in.good())
  {
    line_num++;

    string line;

    try
    {
      getline(in, line);
    }
    catch (std::exception &e)
    {
      line = util::trim(line);

      if ((eof != 0) && !line.empty() && line.front() == eof)
      {
        break;
      }
      else
      {
        LOG_ERROR << "Line " << line_num << ": "
                  << "Exception: " << e.what() << endl;
        throw;
      }
    }

    stripComment(line, comment_prefixes);
    line = util::trim(line);

    if (line.empty())
    {
      continue;
    }
    else if (line.front() == '[')
    {
      if (line.back() != ']')
      {
        LOG_ERROR << "Line " << line_num << ": "
                  << "Unterminated section: " << line << endl;
        throw std::runtime_error("Unterminated section.");
      }

      string section_name = line.substr(1, line.size()-2);
      section = handler.getSection(section_name);
      assert(section);

      if (encountered_sections.find(section) != encountered_sections.end())
      {
        LOG_ERROR << "Line " << line_num << ": "
                  << "More than one section of the name " << section_name << std::endl;
//         throw std::runtime_error("More than one section of the same name.");
        ignore_section = true;
      }
      else
      {
        encountered_sections.insert(section);
        ignore_section = false;
      }
    }
    else if ((eof != 0) && line.front() == eof)
    {
      break;
    }
    else
    {
      if (!section)
      {
        LOG_ERROR << "Line " << line_num << ": "
                  << "Unexpected line outside of section: " << line << std::endl;
        continue;
      }
      else if (ignore_section)
      {
        continue;
      }

      auto tokens = util::tokenize(line);
      if (!tokens.empty())
      {
        vector<string> values;

        for (auto &t : tokens)
        {
          string value = util::trim(t);
          if (!value.empty())
          {
            if (util::isPrefix("//", value))
              break;
            values.push_back(std::move(value));
          }
        }

        if (!values.empty())
          handler.handleValues(section, line_num, std::move(values));
      }
    }
  }
}


template <class Section, class Handler>
void readSectionFile(const char *content, size_t size, Handler &handler,
                     const std::vector<string> &comment_prefixes,
                     char eof = 0)
{
  assert(content);
  assert(size);

  stringstream in(string(content, size));
  readSectionFile<Section, Handler>(in, handler, comment_prefixes, eof);
}


} // namespace


namespace il2ge
{

SectionFile::SectionFile(std::istream &in,
                         const std::vector<std::string> &comment_prefixes,
                         char eof,
                         bool case_insensitive) :
  Base(!case_insensitive)
{
  struct Handler
  {
    SectionFile *file = nullptr;

    Section *getSection(const string &name)
    {
      return &file->getOrCreateSection(name);
    }

    void handleValues(Section *section, int line, vector<string> &&values)
    {
      section->entries.push_back( { std::move(values), line } );
    }
  };

  Handler handler;
  handler.file = this;

  readSectionFile<Section>(in, handler, comment_prefixes, eof);
}


const std::string &ParameterSection::at(const char *param) const
{
  return getEntry(param).values.at(0);
}


void ParameterSection::getImp(const char *name, std::vector<std::string> &values) const
{
  values = getEntry(name).values;
}


void ParameterSection::getImp(const char *name, glm::vec2 &value_) const
{
  glm::vec4 value{0};
  getImp(name, value);
  value_ = glm::vec2(value.x, value.y);
}


void ParameterSection::getImp(const char *name, glm::vec4 &value_) const
{
  try
  {
    glm::vec4 value{0};

    auto &tokens = getEntry(name).values;

//     try
//     {
      if (tokens.size() > 0)
        value.x = std::stof(tokens.at(0));
      if (tokens.size() > 1)
        value.y = std::stof(tokens.at(1));
      if (tokens.size() > 2)
        value.z = std::stof(tokens.at(2));
      if (tokens.size() > 3)
        value.w = std::stof(tokens.at(3));
//     }
//     catch (...)
//     {
//       cout<<"stof"<<endl;
//       abort();
//     }

    value_ = value;
  }
  catch(std::out_of_range&)
  {
    LOG_DEBUG<<"no such parameter: "<<name<<endl;
    throw;
  }
  catch(EntryNotFoundException&)
  {
    throw;
  }
  catch(std::exception &e)
  {
    LOG_ERROR << "Exception: " << e.what() << std::endl;
    throw;
  }
}


ParameterFile::ParameterFile(const char *content, size_t size, bool case_insensitive) :
  Base(!case_insensitive)
{
  const std::vector<string> comment_prefixes { "//" };

  struct Handler
  {
    ParameterFile *file = nullptr;

    Section *getSection(const string &name)
    {
      return &file->getOrCreateSection(name);
    }

    void handleValues(Section *section, int line, vector<string> &&values)
    {
      auto key = values.front();
      values.erase(values.begin());
      section->getOrCreateEntry(key) = { std::move(values), line };
    }
  };

  Handler handler;
  handler.file = this;

  readSectionFile<Section>(content, size, handler, comment_prefixes);
}


const ParameterFile &ParameterFiles::get(const string &file_path)
{
  Entry* entry = nullptr;

  auto it = m_file_map.find(file_path);

  if (it != m_file_map.end())
  {
    entry = &it->second;
  }
  else
  {
    auto& new_element = m_file_map[file_path];

    try
    {
      auto content = m_read_file(file_path);
      new_element.file = make_unique<ParameterFile>(content.data(), content.size(), true);
    }
    catch (FileReadError& e)
    {
      new_element.error = e;
    }

    entry = &new_element;
  }

  assert(entry);

  if (entry->file)
    return *entry->file;
  else
    throw *entry->error;
}


} // namespace il2ge
