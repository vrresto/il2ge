/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MAP_LOADER_PRIVATE
#define IL2GE_MAP_LOADER_PRIVATE

#include <il2ge/map_loader.h>
#include <il2ge/map_resources.h>
#include <render_util/image.h>
#include <render_util/image_loader.h>
#include <render_util/terrain_resources.h>
#include <ostream_overloads.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>


#define ENABLE_LAND 1
#define ENABLE_WATER 1
#define ENABLE_FOREST 1


using namespace util::ostream_overloads;


namespace il2ge::map_loader
{


class TerrainResources : public render_util::TerrainResourcesBase
#if ENABLE_LAND
  , public render_util::TerrainLandResources
#endif
#if ENABLE_WATER
  , public render_util::TerrainWaterResources
#endif
#if ENABLE_FOREST
  , public render_util::TerrainForestResources
#endif
{
  // using ImageResourceList = render_util::TerrainResources::ImageResourceList;

  struct Layer;
  struct DetailLayer;
  struct BaseLayer;

  const il2ge::MapResources &m_map;
  ImageResourceList m_land_textures;
  std::vector<float> m_land_textures_scale;
  ImageResourceList m_water_animation_height_maps;
  ImageResourceList m_water_animation_normal_maps;
  ImageResourceList m_water_animation_foam_masks;
  glm::vec3 m_water_color = glm::vec3(0);

  std::unique_ptr<Layer> m_detail_layer;
#if ENABLE_BASE_MAP
  std::unique_ptr<BaseLayer> m_base_layer;
#endif
  std::vector<TerrainResourcesBase::Layer*> m_layers;

public:
  TerrainResources(const il2ge::MapResources &map);
  ~TerrainResources();

  bool hasBaseLayer() override;
  TerrainResourcesBase::Layer& getLayer(int number) override;
#if ENABLE_LAND
  const ImageResourceList& getLandTextures() override;
  const ImageResourceList& getLandTexturesNM() override;
  const std::vector<float>& getLandTexturesScale() override;
#endif

#if ENABLE_FOREST
  std::vector<render_util::ImageRGBA::Ptr> getForestLayers() override;
#endif

#if ENABLE_WATER
  const ImageResourceList& getWaterAnimationHeightMaps() override
  {
    return m_water_animation_height_maps;
  }
  const ImageResourceList& getWaterAnimationNormalMaps() override
  {
    return m_water_animation_normal_maps;
  }
  const ImageResourceList& getWaterAnimationFoamMasks() override
  {
    return m_water_animation_foam_masks;
  }

  render_util::ImageGreyScale::Ptr getWaterFoamMask() override;

  std::shared_ptr<render_util::GenericImage> getWaterFoamTexture() override;
  std::shared_ptr<render_util::GenericImage> getWaterTexture() override;
  std::shared_ptr<render_util::GenericImage> getWaterNoiseTexture() override;

  glm::vec3 getWaterColor() override
  {
    return m_water_color;
  }
#endif

  render_util::ImageGreyScale::Ptr getNoiseTexture() override;
  std::shared_ptr<render_util::GenericImage> getFarNoiseTexture() override;
  bool farNoiseTextureIsNormalMap() override;

  // bool hasTypeMap() override { return true; }

  std::vector<render_util::AltitudinalZone> getAltitudinalZones() override;

  int getSupportedModules() override;
};


bool isForest(unsigned int index);


unsigned getFieldIndex(const std::string &name);


void createChunks(render_util::ImageGreyScale::ConstPtr image, int chunk_size,
                  std::vector<render_util::ImageGreyScale::Ptr> &chunks);


std::shared_ptr<render_util::ImageResource>
getTextureResource(il2ge::MapResourceID id,
                   std::optional<unsigned> index,
                   const il2ge::MapResources &resources,
                   float *scale = nullptr);


inline void dump(const std::string &content, const std::string &name, const std::string &dump_dir)
{
  if (isDumpEnabled() && !dump_dir.empty())
    util::writeFile(dump_dir + name, content.c_str(), content.size());
}


template <typename T>
void dumpValue(const T& value, const std::string &name, const std::string &dump_dir)
{
  std::ostringstream stream;
  stream << value;
  dump(stream.str(), name, dump_dir);
}


inline void dump(const render_util::ImageResource &res, const std::string &name, const std::string &dump_dir)
{
  if (isDumpEnabled() && !dump_dir.empty())
  {
    auto image = res.load(0);
    render_util::saveImageToFile(dump_dir + name + ".tga", image.get());
  }
}


template <typename T>
void dump(const T& image, const std::string &name, const std::string &dump_dir)
{
  if (isDumpEnabled() && !dump_dir.empty())
  {
    std::string dump_path = dump_dir + name + ".tga";
    render_util::saveImageToFile<T>(dump_path, &image);
  }
}


template <typename T>
void dump(const std::vector<typename T::ConstPtr> &images, const std::string &name, const std::string &dump_dir)
{
  if (isDumpEnabled() && !dump_dir.empty())
  {
    for (size_t i = 0; i < images.size(); i++)
    {
      std::stringstream dump_path;
      dump_path << dump_dir << name << '_' << i << ".tga";
      render_util::saveImageToFile<T>(dump_path.str(), images[i].get());
    }
  }
}


inline std::unique_ptr<render_util::GenericImage> getTexture(const il2ge::MapResources& resources,
                                         MapResourceID id,
                                         std::optional<unsigned> index = {})
{
  std::string name;

  try
  {
    auto res = getTextureResource(id, index, resources);
    assert(res);

    name = res->getName();

    return res->load(0, res->getNumComponents());
  }
  catch (std::exception &e)
  {
    std::string suffix = index ? " " + std::to_string(index.value()) : "";

    LOG_ERROR << "getTexture() " << getMapResourceName(id)
              << suffix << " (" << name << ")  error" << std::endl
              << e.what() << std::endl;
    throw;
  }

  return {};
}


template <typename T>
typename std::shared_ptr<T> getTexture(const il2ge::MapResources& resources,
                                       MapResourceID id,
                                       std::optional<unsigned> index = {})
{
  std::string name;

  try
  {
    auto res = getTextureResource(id, index, resources);
    assert(res);

    name = res->getName();

    auto image = res->load(0, res->getNumComponents());
    assert(image);

    return std::move(render_util::image::genericTo<T>(std::move(image)));
  }
  catch (std::exception &e)
  {
    std::string suffix = index ? " " + std::to_string(index.value()) : "";

    LOG_ERROR << "getTexture() " << getMapResourceName(id)
              << suffix << " (" << name << ")  error" << std::endl
              << e.what() << std::endl;
    throw;
  }

  return {};
}


} // namespace il2ge::map_loader

#endif
