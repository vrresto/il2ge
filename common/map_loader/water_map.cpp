/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "water_map.h"
#include <render_util/image_util.h>
#include <render_util/image_loader.h>
#include <render_util/image_resample.h>
#include <render_util/terrain_base.h>
#include <render_util/config.h>

#include <glm/glm.hpp>
#include <iostream>

using namespace glm;
using namespace std;


namespace
{


using namespace il2ge;
using namespace il2ge::water_map;


render_util::ImageGreyScale::Ptr createEmptyImage(int size)
{
  return render_util::image::create<unsigned char>(0, ivec2(size));
}


render_util::ImageGreyScale::Ptr createFullImage(int size)
{
  return render_util::image::create<unsigned char>(255, ivec2(size));
}


void splitChunks(render_util::ImageGreyScale::ConstPtr image,
                  int chunk_size,
                  std::vector<render_util::ImageGreyScale::ConstPtr> &chunks)
{
  int chunks_per_row = image->w() / chunk_size;
  int num_rows = image->h() / chunk_size;

  for (int y = 0; y < num_rows; y++)
  {
    for (int x = 0; x < chunks_per_row; x++)
    {
      auto chunk = render_util::image::subImage(image.get(),
                                                x * chunk_size,
                                                y * chunk_size,
                                                chunk_size,
                                                chunk_size);

      chunk = render_util::image::flipY(chunk);
      chunks.push_back(chunk);
    }
  }
}


std::vector<render_util::ImageGreyScale::ConstPtr>
mergeChunks(const std::vector<render_util::ImageGreyScale::ConstPtr>& chunks_in,
            int texture_size)
{
  assert(texture_size % CHUNK_SIZE_PX == 0);

  auto chunks_per_row = texture_size / CHUNK_SIZE_PX;
  auto chunks_per_block = chunks_per_row * chunks_per_row;

  auto block_count = chunks_in.size() / chunks_per_block;
  if (chunks_in.size() % chunks_per_block != 0)
    block_count++;

  LOG_INFO << "block_count: " << block_count << std::endl;

  std::vector<render_util::ImageGreyScale::Ptr> blocks;
  blocks.reserve(block_count);

  while (blocks.size() < block_count)
  {
    auto block = std::make_shared<render_util::ImageGreyScale>(glm::ivec2(texture_size));
    blocks.push_back(block);
  }

  for (int i = 0; i < chunks_in.size(); i++)
  {
    auto block_index = i / chunks_per_block;
    auto block_subindex = i % chunks_per_block;

    glm::ivec2 pos_in_block;
    pos_in_block.x = block_subindex % chunks_per_row;
    pos_in_block.y = block_subindex / chunks_per_row;

    auto chunk = render_util::image::flipY(chunks_in.at(i));

    render_util::image::blit(chunk.get(), blocks.at(block_index).get(),
                             pos_in_block * CHUNK_SIZE_PX);
  }

  std::vector<render_util::ImageGreyScale::ConstPtr> ret;
  ret.assign(blocks.begin(), blocks.end());

  return ret;
}


ChunkType classifyChunk(render_util::ImageGreyScale::ConstPtr image)
{
  int num_empty = 0;
  int num_full = 0;
  int num_other = 0;

  for (int y = 0; y < image->w(); y++)
  {
    for (int x = 0; x < image->h(); x++)
    {
      unsigned int color = image->get(x,y);

      if (color == 0)
        num_empty++;
      else if (color == 255)
        num_full++;
      else
        num_other++;
    }
  }

  if (num_other)
    return CHUNK_MIXED;
  else
  {
    if (num_full && !num_empty)
      return CHUNK_FULL;
    else if (num_empty && !num_full)
      return CHUNK_EMPTY;
    else
      return CHUNK_MIXED;
  }
}


class Chunk
{
  render_util::ImageGreyScale::ConstPtr m_image;
  ChunkType m_type = CHUNK_MIXED;

public:
  ChunkType getType() { return m_type; }

  bool isEmpty() const
  {
    return m_type == CHUNK_EMPTY;
  }

  bool isFull() const
  {
    return m_type == CHUNK_FULL;
  }

  void setEmpty()
  {
    m_type = CHUNK_EMPTY;
    m_image = nullptr;
  }

  void setFull()
  {
    m_type = CHUNK_FULL;
    m_image = nullptr;
  }

  void setType(ChunkType type)
  {
    m_type = type;
    if (type != CHUNK_MIXED)
      m_image = nullptr;
  }

  render_util::ImageGreyScale::ConstPtr getImage()
  {
    assert(!isEmpty());
    assert(!isFull());
    assert(m_image);

    return m_image;
  }

  void setImage(render_util::ImageGreyScale::ConstPtr image)
  {
    assert(image);
    m_type = CHUNK_MIXED;
    m_image = image;
  }
};


class Map
{
  render_util::Array2D<Chunk> chunks;

public:
  Map(int w, int h) : chunks(w, h) {}

  Map(const WaterMap &src,
      const std::vector<render_util::ImageGreyScale::ConstPtr>& chunks_in) :
    chunks(src.table->w(), src.table->h())
  {
    auto table = render_util::image::flipY(src.table);

    for (int y = 0; y < table->h(); y++)
    {
      for (int x = 0; x < table->w(); x++)
      {
        unsigned int index = table->get(x,y);
        assert(index < chunks_in.size());

        Chunk &dst_chunk = chunks.at(x, y);
        auto image = chunks_in[index];

        dst_chunk.setImage(image);
        dst_chunk.setType(classifyChunk(image));
      }
    }
  }

  Chunk *getChunk(ivec2 pos)
  {
    if (pos.x < 0 || pos.y < 0)
    {
      return nullptr;
    }
    if (pos.x < chunks.w() && pos.y < chunks.h())
    {
      return &chunks.at(pos.x, pos.y);
    }
    else
    {
      return nullptr;
    }

  }

  int w() { return chunks.w(); }
  int h() { return chunks.h(); }
  ivec2 getSize() { return ivec2(chunks.w(), chunks.h()); }
};


// coords are in chunk grid space
uint8_t sample(Map& map, vec2 coords)
{
  ivec2 chunk_coords = glm::floor(coords);
  chunk_coords = glm::min(map.getSize() - ivec2(1), chunk_coords);

  auto chunk = map.getChunk(chunk_coords);
  if (!chunk)
    return 0;

  assert(chunk);

  if (chunk->isEmpty())
  {
    return 0;
  }
  else if (chunk->isFull())
  {
    return 255;
  }

  render_util::Sampler<render_util::ImageGreyScale> sampler(chunk->getImage().get());

  vec2 uv {};
  uv.x = coords.x < float(map.getSize().x) ? fract(coords.x) : 1.f;
  uv.y = coords.y < float(map.getSize().y) ? fract(coords.y) : 1.f;

  float border = CHUNK_BORDER_PX / CHUNK_SIZE_PX;

  uv *= 1.0 - 2 * border;
  uv += border;

  return sampler.sample(uv, 0);
}


render_util::Image<ChunkType>::Ptr createChunkTypeMap(Map &map,
                                        glm::ivec2 terrain_attribute_map_offset_px)
{
  constexpr int pixel_per_chunk = CHUNK_SIZE_M / render_util::TerrainBase::GRID_RESOLUTION_M;
  static_assert(CHUNK_SIZE_M % render_util::TerrainBase::GRID_RESOLUTION_M == 0);
  static_assert(pixel_per_chunk == 8);

  const ivec2 size = ivec2(map.w(), map.h()) * pixel_per_chunk;

  auto dst_map = make_shared<render_util::Image<ChunkType>>(size);

  for (int y = 0; y < dst_map->h(); y++)
  {
    for (int x = 0; x < dst_map->w(); x++)
    {
      auto pixel_coords = glm::ivec2(x,y) - terrain_attribute_map_offset_px;

      ivec2 chunk_coords = pixel_coords / pixel_per_chunk;
      chunk_coords = clamp(chunk_coords, glm::ivec2(0), map.getSize() - glm::ivec2(1));
      Chunk *chunk = map.getChunk(chunk_coords);
      assert(chunk);

      dst_map->at(x,y) = chunk->getType();
    }

  }

  return dst_map;
}


render_util::ImageGreyScale::Ptr createSmallMap(Map &map,
                                    glm::ivec2 terrain_attribute_map_offset_px)
{
  constexpr int pixel_per_chunk =
    CHUNK_SIZE_M / render_util::TerrainBase::GRID_RESOLUTION_M;
  static_assert(CHUNK_SIZE_M % render_util::TerrainBase::GRID_RESOLUTION_M == 0);
  static_assert(pixel_per_chunk == 8);

  const ivec2 size = ivec2(map.w(), map.h()) * pixel_per_chunk;

  auto dst_map = make_shared<render_util::ImageGreyScale>(size);

  for (int y = 0; y < dst_map->h(); y++)
  {
    for (int x = 0; x < dst_map->w(); x++)
    {
      auto pixel_coords = vec2(x,y) - vec2(terrain_attribute_map_offset_px);
      pixel_coords -= 0.5;

      // in chunk grid space
      auto sample_area_origin = pixel_coords / vec2(pixel_per_chunk);
      float sample_area_size = (1.0 / pixel_per_chunk);

      const int samples = 4;
      float sample_size = sample_area_size / samples;

      int sum = 0;

      for (int yy = 0; yy < samples; yy++)
      {
        for (int xx = 0; xx < samples; xx++)
        {
          // in chunk grid space
          auto sample_coords =
            sample_area_origin + (vec2(xx, yy) + vec2(0.5)) * sample_size;

          sum += sample(map, sample_coords);
        }
      }

      uint8_t value =  sum / (samples * samples);

      dst_map->at(x,y) = 255 - value;
    }
  }

//   render_util::saveImageToFile("small_water_map.tga", dst_map.get());

  return dst_map;
}


} // namespace


namespace il2ge
{


void processWaterMap(const WaterMap &src,
                     WaterMap &dst,
                     render_util::Image<ChunkType>::Ptr& chunk_type_map,
                     render_util::ImageGreyScale::Ptr& small_map,
                     glm::ivec2 terrain_attribute_map_offset_px,
                     int& chunks_per_row)
{
  std::vector<render_util::ImageGreyScale::ConstPtr> chunks_in;
  assert(src.chunk_blocks.size() == 1);
  splitChunks(src.chunk_blocks.at(0), water_map::CHUNK_SIZE_PX, chunks_in);

  Map src_map(src, chunks_in);

  chunk_type_map = createChunkTypeMap(src_map, terrain_attribute_map_offset_px);
#if DEBUG_SMALL_WATER_MAP
  small_map = make_shared<render_util::ImageGreyScale>(chunk_type_map->getSize());
  auto visitor = [] (auto& in, auto& out)
  {
    out = (in != CHUNK_FULL) ? 255 : 0;
  };
  render_util::image::visit(*chunk_type_map, *small_map, visitor);
#else
  small_map = createSmallMap(src_map, terrain_attribute_map_offset_px);
#endif

  dst = src;

  auto chunk_block_texture_size = 1024;
  dst.chunk_blocks = mergeChunks(chunks_in, chunk_block_texture_size);
  chunks_per_row = chunk_block_texture_size / CHUNK_SIZE_PX;
}


} // namespace il2ge
