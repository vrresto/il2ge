/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "forest.h"
#include "map_loader_private.h"
#include <render_util/texture_util.h>
#include <render_util/image_util.h>
#include <render_util/image_resample.h>
#include <render_util/texunits.h>
#include <log.h>

using namespace il2ge;
using namespace il2ge::map_loader;
using namespace std;
using namespace glm;
using namespace render_util;


namespace il2ge::map_loader
{


vector<ImageRGBA::Ptr> createForestLayers(const il2ge::MapResources &resources)
{
  vector<ImageRGBA::Ptr> textures;

  for (int i = 0; i <= 4; i++)
  {
    LOG_TRACE<<"getForestTexture() - layer "<<i<<endl;

    auto img = getTexture(resources, MapResourceID::FOREST_LAYER, i);
    assert(img);
    assert(img->numComponents() == 4);

    textures.push_back(image::genericTo<ImageRGBA>(std::move(img)));
  }

  return textures;
}


ImageGreyScale::Ptr createForestMap(ImageGreyScale::ConstPtr type_map)
{
  auto map = image::clone(*type_map);

  map->forEach([] (unsigned char &pixel)
  {
    if (isForest(pixel & 0x1F))
      pixel = 255;
    else
      pixel = 0;
  });

  return map;
}


} // namespace il2ge
