/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "forest.h"
#include "water_map.h"
#include "map_loader_private.h"
#include <il2ge/image_loader.h>
#include <il2ge/map_loader.h>
#include <il2ge/map_resources.h>
#include <render_util/terrain_base.h>
#include <render_util/image.h>
#include <render_util/normal_map.h>
#include <render_util/base_map_config.h>
#include <util.h>

#include <FastNoise.h>

#include <filesystem>
#include <random>
#include <memory>
#include <string>
#include <cassert>


constexpr bool CREATE_FAR_NOISE_NORMAL_MAP = true;


using namespace std;
using namespace render_util;
using namespace glm;
using namespace il2ge;
using namespace il2ge::map_loader;


namespace
{


TerrainResourcesBase::MaterialMap::ConstPtr createDummyMaterialMap(glm::ivec2 size)
{
  auto map = std::make_shared<TerrainResourcesBase::MaterialMap>(size);
  image::visit(*map, [=] (auto& c) { c = TerrainBase::MaterialID::ALL; });
  return map;
}


constexpr auto RANDOM_CIRRUS_TEXTURE_DIR = "il2ge_random_cirrus_textures";
const vec3 default_water_color = vec3(45,51,40) / vec3(255);


float elevation_table[256] =
{
  #include "height_table"
};


class ImageResourceIMF : public render_util::ImageResource
{
  std::unique_ptr<il2ge::Resource> m_resource;
  glm::ivec2 m_size = glm::ivec2(0);

public:
  ImageResourceIMF(std::unique_ptr<il2ge::Resource> &&res) :
      m_resource(std::move(res))
  {
    auto file = m_resource->open();
    getIMFInfo(*file, m_size.x, m_size.y);
  }

  const std::string& getUniqueID() const override
  {
    return m_resource->getUniqueID();
  }

  const std::string& getName() const override
  {
    return m_resource->getName();
  }

  glm::ivec2 getSize() const override
  {
    return m_size;
  }

  unsigned int getNumComponents() const override
  {
    return 4;
  }

  std::unique_ptr<GenericImage> load(int scale_exponent, int num_components) const override
  {
    auto file = m_resource->open();

    std::vector<char> data;
    file->readAll(data);

    return il2ge::loadIMF(data, num_components);
  }
};


class ImageResource : public render_util::ImageResource
{
  std::unique_ptr<il2ge::Resource> m_resource;
  glm::ivec2 m_size = glm::ivec2(0);
  int m_num_components = 0;

public:
  ImageResource(std::unique_ptr<il2ge::Resource> &&res) :
    m_resource(std::move(res))
  {
    auto file = m_resource->open();
    render_util::getImageInfo(*file, m_size, m_num_components);
  }

  const std::string& getUniqueID() const override
  {
    return m_resource->getUniqueID();
  }

  const std::string& getName() const override
  {
    return m_resource->getName();
  }

  glm::ivec2 getSize() const override
  {
    return m_size;
  }

  unsigned int getNumComponents() const override
  {
    return m_num_components;
  }

  std::unique_ptr<GenericImage> load(int scale_exponent, int num_components) const override
  {
    auto file = m_resource->open();
    auto image = render_util::loadImage(*file, num_components);
    assert(image->getSize() == m_size);

    assert(!scale_exponent);
    if (scale_exponent)
    {
      abort();
    }

    return image;
  }
};


auto &getRandomNumberGenerator()
{
  static std::mt19937 gen(time(nullptr));
  return gen;
}


void dumpFile(string name, const char *data, size_t data_size, const string &dump_dir)
{
  if (!isDumpEnabled() || dump_dir.empty())
    return;
  util::writeFile(dump_dir + '/' +  name, data, data_size);
}


std::shared_ptr<render_util::ImageResource>
createImageResource(std::unique_ptr<Resource>&& res)
{
  auto file = res->open();

  if (il2ge::isIMF(*file))
    return std::make_shared<ImageResourceIMF>(std::move(res));
  else
    return std::make_shared<ImageResource>(std::move(res));
}


ImageGreyScale::Ptr loadTypeMap(const il2ge::MapResources &resources)
{
  LOG_INFO<<"Loading type map ..."<<endl;

  auto type_map = getTexture<ImageGreyScale>(resources, MapResourceID::TYPE_MAP);
  assert(type_map);
  type_map = image::flipY(type_map);

  for (int y = 0; y < type_map->h(); y++)
  {
    for (int x = 0; x < type_map->w(); x++)
    {
      unsigned int index = type_map->get(x,y) & 0x1F;

      // FIXME UGLY
      if (strcmp(getMapFieldName(index), "Wood1") == 0 ||
          strcmp(getMapFieldName(index), "Wood3") == 0)
      {
        index -= 1;
      }

      type_map->at(x,y) = index;
    }
  }

  LOG_INFO<<"Loading type map ... done."<<endl;

  return type_map;
}


render_util::ElevationMap::Ptr loadHeightMap(const il2ge::MapResources &resources)
{
  return createElevationMap(createPixelMapH(&resources));
}


std::shared_ptr<render_util::GenericImage> loadRandomCirrusTexture()
{
  std::vector<std::string> file_paths;

  try
  {
    std::filesystem::directory_iterator it(RANDOM_CIRRUS_TEXTURE_DIR);
    for (auto &entry : it)
    {
      if (!entry.is_regular_file())
        continue;

      if (util::makeLowercase(entry.path().extension().generic_string()) == ".tga")
        file_paths.push_back(entry.path().generic_string());
    }
  }
  catch (std::exception &e)
  {
    LOG_WARNING << e.what() << std::endl;
  }

  if (!file_paths.empty())
  {
    uniform_int_distribution<unsigned int> dist(0, file_paths.size()-1);
    auto random_index = dist(getRandomNumberGenerator());
    auto data = util::readFile<char>(file_paths.at(random_index));
    if (!data.empty())
      return loadImageFromMemory(data, "cirrus");
  }

  return {};
}


void createWaterMap
  (
    ivec2 type_map_size,
    const il2ge::MapResources *resources,
    il2ge::WaterMap &map_out,
    render_util::Image<water_map::ChunkType>::Ptr& chunk_type_map,
    ImageGreyScale::Ptr& small_map,
    glm::ivec2 terrain_attribute_map_offset_px,
    int& chunks_per_row
  )
{
  il2ge::WaterMap map;

  render_util::ImageGreyScale::ConstPtr color_map;

  auto chunks = map_loader::getTexture<ImageGreyScale>(*resources, MapResourceID::LAND_MAP);
  assert(chunks);
  assert(chunks->w() == 1024);

  map.chunk_blocks.push_back(chunks);

  // read table
  {
    ivec2 table_size = (type_map_size * ivec2(4)) / ivec2(water_map::CHUNK_SIZE_PX);
    size_t data_size = table_size.x * table_size.y;

    istringstream file;

    {
      auto data = resources->getResource(MapResourceID::LAND_MAP_TABLE)->readAll();

      dumpFile("MAP_ColorMap.tga_table", data.data(), data.size(), resources->getDumpDir());

      file.str(string(data.data(), data.size()));
    }

    vector<unsigned int> data;

    assert(file.good());

    file.seekg(16);
    assert(file.good());

    while (data.size() < data_size)
    {
      assert(file.good());
      unsigned int value = file.get() << 24 | file.get() << 16 | file.get() << 8 | file.get();
      data.push_back(value);
    }

    assert(data.size() == data_size);

    map.table = std::make_shared<Image<unsigned int>>(table_size, data);
  }

  il2ge::processWaterMap(map, map_out, chunk_type_map, small_map,
                         terrain_attribute_map_offset_px,
                         chunks_per_row);
}


TerrainResourcesBase::MaterialMap::Ptr
createMaterialMap(ImageGreyScale::ConstPtr type_map,
                  const Image<water_map::ChunkType> *small_water_map)
{
  auto material_map = image::convert<TerrainResourcesBase::MaterialMap::ComponentType>(type_map);

  material_map->forEach
  (
    [] (auto &pixel)
    {
      unsigned int material = 0;
      if (isForest(pixel & 0x1F))
        material = TerrainBase::MaterialID::FOREST;
      else
        material = TerrainBase::MaterialID::LAND;
      pixel = material;
    }
  );

  if (small_water_map)
  {
    assert(material_map->w() <= small_water_map->w());
    assert(material_map->h() <= small_water_map->h());

    for (int y = 0; y < material_map->h(); y++)
    {
      for (int x = 0; x < material_map->w(); x++)
      {
        unsigned int material = material_map->get(x,y);

        auto water_map_coords = ivec2(x,y);
        water_map_coords = clamp(water_map_coords, ivec2(0),
                                 small_water_map->getSize() - ivec2(1));

        switch (small_water_map->get(water_map_coords))
        {
          case water_map::CHUNK_EMPTY:
            material = TerrainBase::MaterialID::WATER;
            break;
          case water_map::CHUNK_MIXED:
            material |= TerrainBase::MaterialID::WATER;
          case water_map::CHUNK_FULL:
            break;
        }
        material_map->at(x,y) = material;
      }
    }
  }

  return material_map;
}


void getWaterAnimationTextures(const il2ge::MapResources &resources,
                    std::vector<std::shared_ptr<render_util::ImageResource>> &height_maps,
                    std::vector<std::shared_ptr<render_util::ImageResource>> &normal_maps,
                    std::vector<std::shared_ptr<render_util::ImageResource>> &foam_masks)
{
  int i = 0;

  while (true)
  {
    try
    {
      auto height_map = getTextureResource(MapResourceID::WATER_ANIMATION_HEIGHT_MAP,
                                           i, resources);

      auto normal_map = getTextureResource(MapResourceID::WATER_ANIMATION_NORMAL_MAP,
                                           i, resources);

      height_maps.push_back(height_map);
      normal_maps.push_back(normal_map);
    }
    catch (...)
    {
      break;
    }

    i++;
  }

  assert(normal_maps.size() == height_maps.size());
  // assert(normal_maps.size() == foam_masks.size());
}


void getFieldTextures(const il2ge::MapResources &resources,
                      bool enable_normal_maps,
                      std::vector<std::shared_ptr<render_util::ImageResource>> &textures,
                      std::vector<float> &textures_scale)
{
  for (int i = 0; i < MAP_RESOURCES_NUM_FIELDS; i++)
  {
    float scale = 1.0;

    std::shared_ptr<render_util::ImageResource> resource;

    try
    {
      resource = getTextureResource(il2ge::MapResourceID::FIELD_TEXTURE, i,
                                    resources, &scale);
    }
    catch (il2ge::MapResources::NoEntryException&)
    {
    }

//     if (enable_normal_maps)
//     {
//       std::vector<char> content;
//
//       float scale = 1;
//       bool was_read = false;
//
//       was_read = resources->readTextureFile("FIELDS", field_name, "", content,
//                                          false, true, &scale, true);
//       if (was_read)
//       {
//         dumpFile(string("FIELDS_") +  field_name + "_nm.tga", content.data(), content.size(),
//                  resources->getDumpDir());
//
//         auto normal_map = loadImageRGBFromMemory(content, field_name);
//         normal_map = image::flipY(normal_map);
//         textures_nm.push_back(normal_map);
//       }
//       else
//         textures_nm.push_back({});
//     }
//     else
//     {
//       assert(textures_nm.empty());
//     }

//     if (image)
//     {
//       image = image::flipY(image);
//       assert(image->w() == image->h());
//     }

    textures.push_back(resource);
    textures_scale.push_back(scale);
  }
}


} // namespace


namespace il2ge::map_loader
{


bool isForest(unsigned int index)
{
  assert(index < MAP_RESOURCES_NUM_FIELDS);

  if (index < MAP_RESOURCES_NUM_FIELDS)
    return
      strcmp(getMapFieldName(index), "Wood0") == 0
      ||
      strcmp(getMapFieldName(index), "Wood1") == 0
      ||
      strcmp(getMapFieldName(index), "Wood2") == 0
      ||
      strcmp(getMapFieldName(index), "Wood3") == 0
      ;
  else
    return false;
}


render_util::ElevationMap::Ptr createElevationMap(render_util::ImageGreyScale::ConstPtr height_map)
{
  auto elevation_map = make_shared<ElevationMap>(height_map->getSize());
  for (int y = 0; y < elevation_map->h(); y++)
  {
    for (int x = 0; x < elevation_map->w(); x++)
    {
      unsigned char elevation_index = height_map->get(x,y);
      elevation_map->at(x,y) = elevation_table[elevation_index];
    }
  }

  image::flipYInPlace(elevation_map);

  return elevation_map;
}


#if ENABLE_BASE_MAP
render_util::ElevationMap::Ptr createBaseElevationMap(util::File &file, glm::ivec2 size)
{
  using PixelType = int16_t;

  if (size.x < 1 || size.y < 1)
  {
    LOG_ERROR << "Invalid size: " << size << endl;
    throw std::runtime_error("Base elevation map has invalid size");
  }

  LOG_INFO << "Base elevation map size: " << size << endl;

  std::vector<uint8_t> data(size.x * size.y * sizeof(PixelType));

  auto res = file.read(reinterpret_cast<char*>(data.data()), data.size());
  assert(res == data.size());

  auto image = make_shared<render_util::Image<PixelType>>(size, std::move(data));

  image::flipYInPlace(image);

  //FIXME use half float or better don't convert at all
  return render_util::image::convert<float>(image);
}
#endif


render_util::ImageGreyScale::Ptr createPixelMapH(const il2ge::MapResources *resources)
{
  auto height_map = getTexture<ImageGreyScale>(*resources, MapResourceID::HEIGHT_MAP);
  assert(height_map);

  return height_map;
}


std::shared_ptr<render_util::GenericImage> createCirrusTexture(const il2ge::MapResources &resources)
{
  auto cirrus_texture = loadRandomCirrusTexture();
  if (!cirrus_texture)
    cirrus_texture = getTexture<GenericImage>(resources, MapResourceID::HIGH_CLOUDS);

  return cirrus_texture;
}


std::shared_ptr<render_util::ImageResource>
getTextureResource(il2ge::MapResourceID id,
                   std::optional<unsigned> index,
                   const il2ge::MapResources &resources,
                   float *scale)
{
  auto dump_name = getDumpName(id, index);

  auto file_resource = resources.getResource(id, index, scale);
  assert(file_resource);

  auto resource = createImageResource(std::move(file_resource));
  dump(*resource, dump_name, resources.getDumpDir());

  if (scale)
    dump(to_string(*scale), dump_name + "_scale", resources.getDumpDir());

  return resource;
}


struct TerrainResources::Layer : public TerrainResourcesBase::Layer
{
  const il2ge::MapResources &m_map;

  Layer(const il2ge::MapResources &resources) : m_map(resources)
  {
  }
};


struct TerrainResources::DetailLayer : public TerrainResources::Layer
#if ENABLE_LAND
  , public render_util::TerrainLandResources::Layer
#endif
#if ENABLE_WATER
  , public render_util::TerrainWaterResources::Layer
#endif
#if ENABLE_FOREST
  , public render_util::TerrainForestResources::Layer
#endif
{
  ElevationMap::ConstPtr m_height_map;
  TerrainLandResources::TypeMap::ConstPtr m_type_map;
  Image<water_map::ChunkType>::Ptr m_water_map_chunk_type_map;
  ImageGreyScale::Ptr m_small_water_map;
  TerrainWaterResources::WaterMap m_water_map;
  bool m_water_map_created = false;

  DetailLayer(const il2ge::MapResources &map) : TerrainResources::Layer(map)
  {
  }


  glm::ivec2 getAttributeMapOffsetPx()
  {
    return glm::ivec2(0, -1);
  }

  glm::vec2 getMapTextureOffset() override
  {
    return getAttributeMapOffsetPx();
  }

  glm::ivec2 getSizePx() override
  {
    return getHeightMap()->getSize();
  }

  glm::vec3 getOriginM() override
  {
    return glm::vec3(0);
  }

  float getScale() override
  {
//     assert(0);
    return 1;
  }

  unsigned int getResolutionM() override
  {
    return il2ge::HEIGHT_MAP_METERS_PER_PIXEL;
  }

  ElevationMap::ConstPtr getHeightMap() override
  {
    if (!m_height_map)
      m_height_map = ::loadHeightMap(m_map);
    return m_height_map;
  }

#if ENABLE_LAND
  TypeMap::ConstPtr getTypeMap() override
  {
    if (!m_type_map)
      m_type_map = ::loadTypeMap(m_map);
    return m_type_map;
  }
#endif

#if ENABLE_WATER
  Image<water_map::ChunkType>::ConstPtr getWaterMapChunkTypeMap()
  {
    if (!m_water_map_chunk_type_map)
      getWaterMap();
    assert(m_water_map_chunk_type_map);
    return m_water_map_chunk_type_map;
  }
#endif

#if ENABLE_FOREST
  ImageGreyScale::ConstPtr getForestMap() override
  {
    return createForestMap(getTypeMap());
  }
#endif

  MaterialMap::ConstPtr getMaterialMap() override
  {
//     auto type_map = loadTypeMap();

//     il2ge::WaterMap water_map;
//     render_util::Image<water_map::ChunkType>::Ptr small_water_map;
//     createWaterMap(type_map->getSize(), &m_map, water_map, small_water_map);


#if ENABLE_LAND && ENABLE_WATER
    return createMaterialMap(getTypeMap(), getWaterMapChunkTypeMap().get());
#else
    return createDummyMaterialMap(getSizePx());
#endif

//       abort();
//     return nullptr;
  }

#if ENABLE_WATER
  const WaterMap &getWaterMap() override
  {
    if (!m_water_map_created)
    {
      il2ge::WaterMap map;
      ::createWaterMap(getTypeMap()->getSize(), &m_map, map,
                       m_water_map_chunk_type_map,
                       m_small_water_map,
                       getAttributeMapOffsetPx(),
                       m_water_map.chunks_per_row);

      m_water_map.chunk_blocks = map.chunk_blocks;
      m_water_map.table = map.table;
      m_water_map.chunk_size_px = water_map::CHUNK_SIZE_PX;
      m_water_map.chunk_size_m = water_map::CHUNK_SIZE_M;
      m_water_map.chunk_border_px = water_map::CHUNK_BORDER_PX;
      m_water_map.y_flip = true;

      m_water_map_created = true;
    }
    return m_water_map;
  }

  render_util::ImageGreyScale::ConstPtr getSmallWaterMap() override
  {
    if (!m_small_water_map)
      getWaterMap();
    assert(m_small_water_map);
    return m_small_water_map;
  }
#endif
};


#if ENABLE_BASE_MAP
struct TerrainResources::BaseLayer : public render_util::TerrainResourcesBase::BaseLayer
{
  const il2ge::MapResources &m_map;
  std::unique_ptr<BaseMapResources> m_base_map;
  std::unique_ptr<render_util::MapGeography> m_map_geography;
  std::unique_ptr<render_util::MapGeography> m_detail_map_geography;
  std::shared_ptr<render_util::ElevationMap> m_height_map;
  ImageGreyScale::Ptr m_type_map;

  BaseLayer(const il2ge::MapResources &resources) : m_map(resources)
  {
    m_base_map = resources.getBaseLayerResources();
    assert(m_base_map);

    m_map_geography = m_base_map->getMapGeography();
    m_detail_map_geography = m_map.getMapGeography();
  }

  glm::vec2 getMapTextureOffset() override
  {
    return glm::vec2(0);
  }

  ImageGreyScale::ConstPtr getSmallWaterMap() override
  {
      // FIXME BaseMapResources::WATER should be called LAND
    auto land_map = m_base_map->getMap("water");
    auto water_map = m_base_map->getMap("natural_water");

    auto map = std::make_shared<ImageGreyScale>(water_map->size());

    for (int y = 0; y < map->h(); y++)
    {
      for (int x = 0; x < map->w(); x++)
      {
        auto water = 255 - land_map->get(x,y);
        water = glm::clamp(water + water_map->get(x,y), 0, 255);
        map->at(x,y) = water;
      }
    }

    return map;
  }

  ImageGreyScale::ConstPtr getTypeMap() override
  {
    if (!m_type_map)
    {
      m_type_map = std::make_shared<ImageGreyScale>(m_base_map->getMapSizePx());
      image::clear(m_type_map);

      // FIXME BaseMapResources::WATER should be called LAND
      ImageGreyScale::Ptr land_map;

      try
      {
        land_map = m_base_map->getMap("water");
      }
      catch (std::exception& e)
      {
        LOG_ERROR << "Cant open map \"water\": " << e.what() << std::endl;
      }

      FastNoise noise_generator;

      int type_index = 0;

      for (auto& type : BaseMapResources::getLandCoverTypes())
      {
        assert(type.variants > 0);

        try
        {
          auto map = m_base_map->getMap(type.name);

          //HACK FIXME add type.is_water
          bool is_water = type.name == "natural_water";

          for (int y = 0; y < m_type_map->h(); y++)
          {
            for (int x = 0; x < m_type_map->w(); x++)
            {
              auto water = land_map ? 255 - land_map->get(x,y) : 0;

              auto value = map->get(x,y);

              //FIXME HACK
              if (is_water)
                value = glm::clamp(value + water, 0, 255);

              if (value > 20)
              {
                int bits = log(type.variants) / log(2);
                assert(pow(2, bits) == type.variants);

                unsigned variant = 0;
                for(int i = 0; i < bits; i++)
                {
                  float scale = pow(2, i) * 10;

//                   auto noise = noise_generator.GetPerlin(scale * x, scale * y);
                  auto noise = noise_generator.GetCellular(scale * x, scale * y);
//                   auto noise = noise_generator.GetCubicFractal(scale * x, scale * y);

                  assert(noise >= -1 && noise <= 1);

                  if (noise > 0)
                    variant |= 1 << i;
                }

                assert(variant >= 0 && variant < type.variants);

                m_type_map->at(x,y) = type_index + variant + 1;
              }
            }
          }
        }
        catch (std::exception& e)
        {
          LOG_ERROR << e.what() << std::endl;
        }

        type_index += type.variants;
      }

      render_util::saveImageToFile("type_map_dump.tga", m_type_map.get());

//       auto height_map = getHeightMap();


//       apply_map(BaseMapResources::WATER);
    }
    return m_type_map;
  }

  glm::ivec2 getSizePx() override
  {
    return getHeightMap()->getSize();
  }

  glm::vec3 getOriginM() override
  {
    return glm::vec3(m_map_geography->getOriginWorld(*m_detail_map_geography), 0);
  }

  unsigned int getResolutionM() override
  {
    return il2ge::HEIGHT_MAP_METERS_PER_PIXEL;
  }

  float getScale() override
  {
    return m_map_geography->getScaleWorld(*m_detail_map_geography);
  }

  render_util::ElevationMap::ConstPtr getHeightMap() override
  {
    if (!m_height_map)
    {
      m_height_map = m_base_map->getElevationMap();

      try
      {
        const int radius = 4;
        const int inner_radius = 1;

        // FIXME BaseMapResources::WATER should be called LAND
        auto land_map = m_base_map->getMap("water");

        for (int y = 0; y < m_height_map->h(); y++)
        {
          for (int x = 0; x < m_height_map->w(); x++)
          {
            float dist_to_water = std::numeric_limits<int>::max();

            for (int yy = -radius; yy <= radius; yy++)
            {
              for (int xx = -radius; xx <= radius; xx++)
              {
                if (land_map->getClamped(x + xx, y + yy) < 255)
                {
                  dist_to_water = glm::min(glm::distance(glm::vec2(xx, yy), glm::vec2(0)),
                                           dist_to_water);
                }
              }
            }

            if (dist_to_water <= radius)
            {
              auto scale = glm::smoothstep(0.f, 1.f,
                                           glm::max(dist_to_water - float(inner_radius), 0.f)
                                           / float(radius - inner_radius));
              m_height_map->at(x,y) *= scale;
            }
          }
        }
      }
      catch (std::exception &e)
      {
        LOG_ERROR << e.what() << std::endl;
      }
    }
    return m_height_map;
  }

//   render_util::TerrainBase::TypeMap::Ptr loadTypeMap() const override
//   {
//     auto hm = loadHeightMap();
//     assert(hm);
//     LOG_INFO << "Generating type map ..." << endl;
//     return il2ge::map_generator::generateTypeMap(hm);
//     LOG_INFO << "Generating type map ... done." << endl;
//   }


  ImageGreyScale::ConstPtr getForestMap() override
  {
    return std::make_shared<ImageGreyScale>(m_base_map->getMapSizePx());
  }


//   render_util::ImageGreyScale::Ptr loadForestMap() const override
//   {
//     return createForestMap(loadTypeMap());
//   }

  MaterialMap::ConstPtr getMaterialMap() override
  {
    auto material_map =
      std::make_shared<MaterialMap>(getSizePx());
    image::fill(material_map, 0);

    return material_map;

//     auto type_map = getTypeMap();

//     auto small_water_map =
//       createSmallWaterMap(type_map);

//     return createMaterialMap(type_map, small_water_map.get());
//       abort();
  }

//   void loadWaterMap(std::vector<render_util::ImageGreyScale::Ptr> &chunks,
//                     render_util::Image<unsigned int>::Ptr &table) const override
//   {
//     throw std::runtime_error("not implemented");
//   }

  const WaterMap &getWaterMap() override
  {
    abort();
  }

};
#endif


TerrainResources::TerrainResources(const il2ge::MapResources &map) :
  m_map(map)
{
  getFieldTextures(m_map, false, m_land_textures, m_land_textures_scale);

  getWaterAnimationTextures(m_map,
                            m_water_animation_height_maps,
                            m_water_animation_normal_maps,
                            m_water_animation_foam_masks);

  m_water_color = m_map.getProperty<glm::vec3>(MapPropertyID::WATER_COLOR);
  dumpValue(m_water_color, getMapPropertyName(MapPropertyID::WATER_COLOR),
            m_map.getDumpDir());

  m_detail_layer = std::make_unique<DetailLayer>(m_map);
  m_layers.push_back(m_detail_layer.get());

#if ENABLE_BASE_MAP
  if (m_map.hasBaseLayer())
  {
    m_base_layer = std::make_unique<BaseLayer>(m_map);
    m_layers.push_back(m_base_layer.get());
  }
#endif
}


TerrainResources::~TerrainResources()
{
}


bool TerrainResources::hasBaseLayer()
{
#if ENABLE_BASE_MAP
  return m_base_layer != nullptr;
#else
  return false;
#endif
}


render_util::TerrainResourcesBase::Layer &TerrainResources::getLayer(int number)
{
  return *m_layers.at(number);
}


#if ENABLE_LAND
const std::vector<std::shared_ptr<render_util::ImageResource>> &TerrainResources::getLandTextures()
{
  return m_land_textures;
}


const std::vector<std::shared_ptr<render_util::ImageResource>> &TerrainResources::getLandTexturesNM()
{
  UNIMPLEMENTED
}


const std::vector<float> &TerrainResources::getLandTexturesScale()
{
  return m_land_textures_scale;
}
#endif


#if ENABLE_FOREST
std::vector<render_util::ImageRGBA::Ptr> TerrainResources::getForestLayers()
{
  return il2ge::map_loader::createForestLayers(m_map);
}
#endif


ImageGreyScale::Ptr TerrainResources::getNoiseTexture()
{
  LOG_INFO << "loading noise texture ..." << endl;

  auto noise_texture = getTexture(m_map, MapResourceID::NOISE_TEXTURE);

  if (!noise_texture)
  {
    noise_texture = std::make_unique<GenericImage>(glm::ivec2(8), 1);
    image::visit(*noise_texture, [] (auto& c) { c = 127; });
  }

  if (noise_texture->numComponents() != 1)
    return render_util::image::getChannel(*noise_texture, 0);
  else
    return render_util::image::genericTo<ImageGreyScale>(std::move(noise_texture));
}


std::shared_ptr<GenericImage> TerrainResources::getFarNoiseTexture()
{
  LOG_INFO << "loading far noise texture ..." << endl;

  auto texture = getTexture(m_map, MapResourceID::FAR_NOISE_TEXTURE);
  if (texture->numComponents() != 1)
    texture = image::toGeneric(render_util::image::getChannel(*texture, 0));

  if (CREATE_FAR_NOISE_NORMAL_MAP)
  {
    auto normal_map = std::make_unique<GenericImage>(texture->getSize(), 3);
    createNormalMap(*texture, *normal_map, 1.0, 1.0, true);
    return normal_map;
  }
  else
  {
    return texture;
  }
}


bool TerrainResources::farNoiseTextureIsNormalMap()
{
  return CREATE_FAR_NOISE_NORMAL_MAP;
}


#if ENABLE_WATER
ImageGreyScale::Ptr TerrainResources::getWaterFoamMask()
{
  UNIMPLEMENTED
  // LOG_INFO << "Loading water foam mask ..." << endl;
  //
  // auto image_res = getTextureResource("APPENDIX", "BeachFoam", "water/Foam.tga",
  //                                     m_map, false, nullptr);
  // assert(image_res);
  //
  // auto image = image_res->load(0, image_res->getNumComponents());
  // assert(image);
  //
  // return image::getChannelShared(*image, 3);
}


std::shared_ptr<render_util::GenericImage> TerrainResources::getWaterFoamTexture()
{
  LOG_INFO << "Loading water foam texture ..." << endl;

#if 0
  auto image_res = getTextureResource("APPENDIX", "BeachFoam", "water/Foam.tga",
                                      m_map, false, nullptr);
#else
  auto res = m_map.getResource(MapResourceID::FOAM_TEXTURE);
  auto image_res = createImageResource(std::move(res));
  dump(*image_res, "foam", m_map.getDumpDir()); //FIXME get proper dump name
#endif

  assert(image_res);

  auto image = image_res->load(0, image_res->getNumComponents());
  assert(image);

  return image;
}


std::shared_ptr<GenericImage> TerrainResources::getWaterTexture()
{
  return getTexture(m_map, MapResourceID::WATER_TEXTURE);
}


std::shared_ptr<GenericImage> TerrainResources::getWaterNoiseTexture()
{
  return getTexture(m_map, MapResourceID::WATER_NOISE_TEXTURE);
}
#endif


std::vector<AltitudinalZone> TerrainResources::getAltitudinalZones()
{
  return m_map.getAltitudinalZones();
}


int TerrainResources::getSupportedModules()
{
  return render_util::TerrainBase::ModuleMask::LAND |
         render_util::TerrainBase::ModuleMask::WATER |
         render_util::TerrainBase::ModuleMask::FOREST;
}


std::unique_ptr<render_util::TerrainResourcesBase>
getTerrainResources(const il2ge::MapResources& map)
{
  return std::make_unique<TerrainResources>(map);
}


} // namespace il2ge::map_loader
