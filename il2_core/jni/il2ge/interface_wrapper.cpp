#include <swig/output/GraphicsExtender_wrap.cxx>

static_assert(std::is_same<il2ge::java_interface::CppObjectID, jint>::value);
static_assert(std::is_signed<il2ge::java_interface::CppObjectID>::value ==
              std::is_signed<intptr_t>::value);
static_assert(sizeof(il2ge::java_interface::CppObjectID) == sizeof(intptr_t));
