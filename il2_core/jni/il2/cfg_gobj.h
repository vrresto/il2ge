#ifndef CORE_JNI_WRAPPER_CFG_GOBJ_H
#define CORE_JNI_WRAPPER_CFG_GOBJ_H

#include <core/objects.h>


namespace il2ge::il2_jni
{


struct CfgGObj : public core::GObj
{
  std::string m_name;

  CfgGObj(const std::string& name, const core::GObjType& type_obj) :
    core::GObj(type_obj),
    m_name(name)
  {
    assert(!getType().getJavaClassName().empty());
  }
};


std::string getCfgGObjName(jint);
core::ObjRef<CfgGObj> createCfgInt(const std::string& name);
core::ObjRef<CfgGObj> createCfgFlags(const std::string& name);


} // namespace il2ge::il2_jni


#endif
