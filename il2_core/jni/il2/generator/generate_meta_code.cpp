/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <util.h>

#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <sstream>
#include <fstream>
#include <cassert>
#include <algorithm>
#include <unordered_set>
#include <unordered_map>

using namespace std;


namespace
{


constexpr char TAB[] = "  ";

struct MethodInfo
{
  string name;
  string short_name;
  string return_type;
  vector<string> arg_types;
};

struct ClassInfo
{
  vector<MethodInfo> methods;
};


map<string, ClassInfo> g_classes;


ClassInfo &getClassInfo(const string &name)
{
  return g_classes[name];
}


string getTypeCode(const std::string& cpp_name)
{
  if (util::isSuffix("Array", cpp_name))
    return "_3" + getTypeCode(util::stripSuffix("Array", cpp_name));

  if (cpp_name == "jint")
    return "I";
  else if (cpp_name == "jfloat")
    return "F";
  else if (cpp_name == "jshort")
    return "S";
  else if (cpp_name == "jbyte")
    return "B";
  else if (cpp_name == "jchar")
    return "C";
  else if (cpp_name == "jboolean")
    return "Z";
  else if (cpp_name == "jlong")
    return "J";
  else if (cpp_name == "jdouble")
    return "D";
  else if (cpp_name == "jstring")
    return "Ljava_lang_String_2";
  else if (cpp_name == "jobject")
    return "Ljava_lang_Object_2";

  cerr << "Type not handled: " << cpp_name << endl;
  exit(1);
}


string makeFullName(const MethodInfo& mi)
{
  if (mi.arg_types.empty())
    return mi.short_name;

  ostringstream s;

  s << mi.short_name << "__";

  for (auto& type : mi.arg_types)
  {
    s << getTypeCode(type);
  }

  return s.str();
}


void parseSignatures(istream &in)
{
  while (in.good())
  {
    string line;
    getline(in, line);

    line = util::trim(line);

    if (util::isPrefix("#", line) || line.empty())
      continue;

    istringstream line_stream(line);

    MethodInfo mi;

    line_stream >> mi.return_type;
//     cerr<<"mi.return_type: "<<mi.return_type<<endl;
    if(mi.return_type.empty())
      continue;

    string class_name;
    line_stream >> class_name;
//     cerr<<"class name: "<<class_name<<endl;
    if (class_name.empty())
      continue;

    line_stream >> mi.name;
    if (mi.name.empty())
      continue;

    while (line_stream.good())
    {
      string arg_type;
      line_stream >> arg_type;
      mi.arg_types.push_back(arg_type);
    }

    {
      auto separator_pos = mi.name.find("__");
      if (separator_pos != std::string::npos)
      {
        assert(separator_pos < mi.name.size());
        mi.short_name = mi.name.substr(0, separator_pos);
      }
      else
      {
        mi.short_name = mi.name;
      }
    }

    mi.name = makeFullName(mi);

    ClassInfo &ci = getClassInfo(class_name);
    ci.methods.push_back(mi);
  }

  for (auto& it : g_classes)
  {
    auto& ci = it.second;

    unordered_set<string> all_names;
    unordered_map<string, int> overloads_map;

    for (auto& mi : ci.methods)
    {
      if (all_names.find(mi.name) != all_names.end())
      {
        cerr << mi.name << " already encountered" << endl;
        exit(1);
      }

      all_names.insert(mi.name);

      overloads_map[mi.short_name]++;
    }

    for (auto& mi : ci.methods)
    {
      auto overloads = overloads_map[mi.short_name];
      assert(overloads > 0);

      if (overloads == 1)
        mi.name = mi.short_name;
    }
  }
}


void emitClassName(const std::string& class_name, ostream &out)
{
  std::string class_path = "com/maddox/" + class_name;
  std::replace(class_path.begin(), class_path.end(), '.', '/');

  cout << "const std::string g_java_class_name = \"" << class_name << "\";" << std::endl;
  cout << "const std::string g_java_class_path = \"" << class_path << "\";" << std::endl;
}


void emitMethodDefinitions(const ClassInfo &info, ostream &out)
{
  for (const MethodInfo &m : info.methods)
  {
    out << "typedef MethodSpec<" << m.return_type;
    for (size_t i = 0; i < m.arg_types.size(); i++)
    {
      out << ", " << m.arg_types[i];
    }
    out << "> " << m.name << "_t;" << endl;
  }
}


void emitInterfaceDefinition(const ClassInfo &info, ostream &out)
{
  out << "struct Interface" << endl;
  out << "{" << endl;
  for (const MethodInfo &m : info.methods)
  {
    out << TAB << m.name << "_t::Signature *" << m.short_name << " = nullptr;" << endl;
  }
  out << "};" << endl;
}


void emitMetaClassRegistration(const string &name, ostream &out, bool no_import)
{
  ClassInfo &info = getClassInfo(name);

  size_t pos = name.find_last_of('.');
  assert(pos != std::string::npos);
  assert(pos < name.size()-1);

  string package = name.substr(0, pos);
  string class_name = name.substr(pos + 1);

  auto path_tokens = util::tokenize(package, '.');
  assert(!path_tokens.empty());

  out << "void il2ge::il2_jni::registrator::";

  for (size_t i = 0; i < path_tokens.size(); i++)
  {
    out << path_tokens[i] << "::";
  }

  out << class_name << "(MetaClass &meta_class)" << endl;

  out << "{" << endl;
  out << TAB << "meta_class.package = \"com.maddox." << package << "\";" <<endl;
  out << TAB << "meta_class.name = \"" << class_name << "\";" << endl;

  for (MethodInfo &mi : info.methods)
  {
    if (no_import)
    {
      out << TAB << "meta_class.addMethod<" << mi.name << "_t>"
          << "(\"" << mi.name << "\", nullptr, &::" << mi.short_name << ");" << endl;
    }
    else
    {
      out << TAB << "meta_class.addMethod<" << mi.name << "_t>"
          << "(\"" << mi.name << "\", &::import." << mi.name << ", &::" << mi.short_name << ");" << endl;
    }
  }

  out << "}" << endl;
  out << endl;
}


void emitMethodImplementation(const MethodInfo &mi, ostream &out)
{
  // head
  out << mi.return_type << " JNICALL " << mi.short_name << "(JNIEnv *env, jobject obj";
  for (size_t i = 0; i < mi.arg_types.size(); i++)
  {
    out << "," << endl << TAB << TAB << mi.arg_types[i] << " arg" << i;
  }
  out << ")" << endl;

  // body
  out << "{" << endl;
  out << TAB << "return import." << mi.name << "(env, obj";
  for (size_t i = 0; i < mi.arg_types.size(); i++)
  {
    out << "," << " arg" << i;
  }
  out << ");" << endl;
  out << "}" << endl;

  out << endl;
}


void emitMethodStubImplementation(const string &class_name, const MethodInfo &mi, ostream &out)
{
  // head
  out << mi.return_type << " JNICALL " << mi.short_name << "(JNIEnv *env, jobject obj";
  for (size_t i = 0; i < mi.arg_types.size(); i++)
  {
    out << "," << endl << TAB << TAB << mi.arg_types[i] << " arg" << i;
  }
  out << ")" << endl;

  // body
  out << "{" << endl;
  out << TAB << "LOG_ERROR<<\"UNIMPLEMENTED: "
      << class_name << "." << mi.short_name << "()\"<<std::endl;" << endl;
  out << TAB << "abort();" << endl;
  out << "}" << endl;

  out << endl;
}


void dumpClassWrappers(const string &output_dir)
{
  for (auto it : g_classes)
  {
    string class_name = it.first;
    const ClassInfo &ci = it.second;

    ofstream out(output_dir + '/' + "wrap_" + class_name + ".cpp");
    assert(out.good());

    out << "#include \"resolver.h\"" << endl;
    out << "#include \"meta_class_registrators.h\"" << endl;
    out << endl;
    out << "using namespace il2ge::il2_jni;" << endl;
    out << endl;
    out << "namespace" << endl;
    out << "{" << endl;
    out << endl;
    out << "#include <_generated/il2_jni/" << class_name << "_definitions>" << endl;
    out << endl;
    out << "Interface import;" << endl;
    out << endl;

    for (const MethodInfo &mi : ci.methods)
    {
      emitMethodImplementation(mi, out);
    }

    out << endl;
    out << "} // namespace" << endl;

    out << endl;
    out << endl;
    out << "#include <_generated/il2_jni/" << class_name << "_registration>" << endl;
  }
}


void dumpClassStubs(const string &output_dir)
{
  for (auto it : g_classes)
  {
    string class_name = it.first;
    const ClassInfo &ci = it.second;

    ofstream out(output_dir + '/' + "stub_" + class_name + ".cpp");
    assert(out.good());

    out << "#include \"resolver.h\"" << endl;
    out << "#include \"meta_class_registrators.h\"" << endl;
    out << "#include <log.h>" << endl;
    out << endl;
    out << "using namespace il2ge::il2_jni;" << endl;
    out << endl;
    out << "namespace" << endl;
    out << "{" << endl;
    out << endl;
    out << "#include <_generated/il2_jni/" << class_name << "_definitions>" << endl;
    out << endl;

    for (const MethodInfo &mi : ci.methods)
    {
      emitMethodStubImplementation(class_name, mi, out);
    }

    out << endl;
    out << "} // namespace" << endl;

    out << endl;
    out << endl;
    out << "#include <_generated/il2_jni/" << class_name << "_registration>" << endl;
  }
}


} // namespace


int main(int argc, char **argv)
{
  assert(argc >= 3);

  string cmd = argv[1];
  assert(!cmd.empty());

  vector<string> cmd_args;

  for (int i = 2; i < argc; i++)
    cmd_args.push_back(argv[i]);

  bool no_import = false;

  if (cmd == "definitions" || cmd == "registration")
  {
    if (cmd_args.at(0) == "no_import")
      no_import = true;
    else if (cmd_args.at(0) == "import")
      no_import = false;
    else
      abort();
  }

  if(cmd == "registrator-table")
  {
    for (auto name : util::tokenize(cmd_args.at(0)))
    {
      auto path_tokens = util::tokenize(name, '.');
      assert(!path_tokens.empty());

      cout << "&il2ge::il2_jni::registrator::";

      for (size_t i = 0; i < path_tokens.size() - 1; i++)
      {
        cout << path_tokens[i] << "::";
      }

      cout << path_tokens.back() << "," << endl;
    }
  }
  else if(cmd == "registrator-definitions")
  {
    for (auto name : util::tokenize(cmd_args.at(0)))
    {
      std::replace(name.begin(), name.end(), '.', '/');
      auto path_tokens = util::tokenize(name, '/');
      assert(!path_tokens.empty());
      assert(path_tokens.size() > 1);

      cout << "namespace il2ge::il2_jni::registrator";

      for (size_t i = 0; i < path_tokens.size() - 1; i++)
      {
        cout << "::" << path_tokens[i];
      }

      cout << " { ";
      cout << "MetaClassInitFunc " << path_tokens.back() << ";";
      cout << " }" << endl;
    }
  }
  else if (cmd == "definitions")
  {
    parseSignatures(cin);

    string class_name = cmd_args.at(1);

    emitMethodDefinitions(getClassInfo(class_name), cout);

    if (!no_import)
    {
      cout << endl;
      emitInterfaceDefinition(getClassInfo(class_name), cout);
    }

    emitClassName(class_name, cout);
  }
  else if (cmd == "registration")
  {
    parseSignatures(cin);

    string class_name = cmd_args.at(1);
//     class_name[0] = toupper(class_name[0]);

    emitMetaClassRegistration(class_name, cout, no_import);
  }
  // else if (cmd == "definitions_no_import")
  // {
  //   parseSignatures(cin);
  //
  //   string class_name = cmd_arg;
  //
  //   emitMethodDefinitions(getClassInfo(class_name), cout);
  //   cout << endl;
  //
  //   emitClassName(class_name, cout);
  // }
  // else if (cmd == "registration_no_import")
  // {
  //   parseSignatures(cin);
  //
  //   string class_name = cmd_arg;
  //
  //   emitMetaClassRegistration(class_name, cout, true);
  // }
  else if (cmd == "class-wrappers")
  {
    parseSignatures(cin);

    dumpClassWrappers(cmd_args.at(0));
  }
  else if (cmd == "class-stubs")
  {
    parseSignatures(cin);

    dumpClassStubs(cmd_args.at(0));
  }
  else
  {
    assert(0);
  }
}
