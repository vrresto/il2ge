#ifndef JNI_WRAPPER_GOBJ_FACTORIES_H
#define JNI_WRAPPER_GOBJ_FACTORIES_H

namespace il2ge::il2_jni
{



void registerGObjTTFont();
void registerGObjMat();
void registerGObjEffectParameters();



} // namespace il2ge::il2_jni

#endif
