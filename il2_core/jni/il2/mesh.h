#ifndef IL2GE_CORE_MESH_H
#define IL2GE_CORE_MESH_H

#include <il2ge/mesh/mesh_base.h>


namespace il2ge::core
{
  class GObj;
}


namespace il2ge::il2_jni
{


bool isMesh(jint cpp_obj); // NOT HierMesh


std::unique_ptr<il2ge::mesh::MeshInstance> getHierMeshChunk(jint cpp_obj);

void renderMeshes(jint* objs,
                  const glm::dvec3* pos,
                  const glm::vec3* yaw_pitch_roll,
                  jint count);

} // namespace il2ge::il2_jni


#endif
