#ifndef IL2GE_CORE_MAT_H
#define IL2GE_CORE_MAT_H

#include <il2ge/material.h>

namespace il2ge::il2_jni
{
  void *getDummyMaterial();
  // const std::string &getMaterialPath(int cpp_obj);
  // int createMaterial(std::string path);

  // const il2ge::Material& getMaterial(jint cpp_obj);

  const core::ObjRef<il2ge::Material> getMaterial(jint cpp_obj);
}

#endif
