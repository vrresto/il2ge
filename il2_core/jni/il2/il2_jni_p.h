/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_IL2_JNI_P_H
#define IL2GE_IL2_JNI_P_H

#include <il2_jni.h>
#include <core/object_handle.h>
#include <main_thread.h>

#include <jni.h>


namespace il2ge::il2_jni
{


class CfgGObj;


template <class T, class TypeClass = core::GObjType>
class GObjSubClass
{
  static inline std::unique_ptr<core::GObjType> s_type_obj;

public:
  static const core::GObjType& getStaticType()
  {
    if (!s_type_obj)
      s_type_obj = T::createType();
    return *s_type_obj;
  }
};


inline const core::GObjRef& getGObj(jint handle)
{
  static_assert(std::is_same<jint, core::CppObjectHandle>::value);
  return core::objectFromHandle(handle);
}


inline const core::GObjRef& getGObj(jint handle, core::GObjType& type)
{
  auto& obj = getGObj(handle);
  assert(obj->getType() == type);
  return obj;
}


template <class T>
T* castGObj(core::GObj& obj)
{
  assert(obj.getType() == T::getStaticType());
  return static_cast<T*>(&obj);
}


template <class T>
T* castGObj(jint id)
{
  auto& gobj = getGObj(id);
  assert(gobj);
  return castGObj<T>(*gobj);
}


void initGObj();
jobject createJavaObject(const core::GObjRef&);
void finalizeGObj(jint id);


} // namespace il2ge::il2_jni


#endif
