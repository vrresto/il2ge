#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "mat.h"
#include "mesh.h"
#include <core/objects.h>
#include <core.h>
#include <il2ge/mesh/mesh_base.h>
#include <il2ge/renderer.h>
#include <render_util/camera_3d.h>
#include <util.h>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/il2.engine.Mesh_definitions>


//FIXME
void applyRotation(const glm::vec3 &yaw_pitch_roll, glm::mat4 &mat)
{
  mat = glm::rotate(mat, glm::radians(-yaw_pitch_roll.x), glm::vec3(0, 0, 1));
  mat = glm::rotate(mat, glm::radians(-yaw_pitch_roll.y), glm::vec3(0, 1, 0));
  mat = glm::rotate(mat, glm::radians(+yaw_pitch_roll.z), glm::vec3(1, 0, 0));
}


//FIXME
void matrixToArray(const glm::mat4 &mat, std::array<double, 16> &array)
{
  size_t index = 0;
  for (int col = 0; col < 4; col++)
  {
    for (int row = 0; row < 4; row++)
    {
      array[index] = mat[row][col];
      index++;
    }
  }
}


struct Mesh : public core::GObj, public GObjSubClass<Mesh>
{
  static auto createType() { return GObj::createType(g_java_class_path); }

  std::unique_ptr<il2ge::mesh::MeshInstance> m_instance;
  glm::dvec3 m_pos {};
  glm::vec3 m_yaw_pitch_roll {};
  int m_current_animation_frame = 0;

  Mesh() : core::GObj(getStaticType()) {}

  void render()
  {
    render(m_pos, m_yaw_pitch_roll, m_current_animation_frame);
  }

  void render(const glm::dvec3 &pos, const glm::vec3 &yaw_pitch_roll, int frame)
  {
    if (!m_instance)
      return;

    auto camera = core::getCamera();

    glm::mat4 rotation(1);
    applyRotation(yaw_pitch_roll, rotation);

    auto model_world = glm::translate(glm::dmat4(1.0), pos);
    model_world = model_world * glm::dmat4(rotation);
    auto model_view = camera->getWorld2ViewMatrixD() * model_world;
    auto dist = glm::distance(camera->getPosD(), pos);

    m_instance->draw(model_view, dist, core::getRenderer(), m_current_animation_frame);
  }

  int findHook(const std::string &name)
  {
    return m_instance->getMesh().findHook(name);
  }

  const glm::mat4 &getHookMatrix(int hook)
  {
    return m_instance->getMesh().getHookMatrix(hook, m_current_animation_frame);
  }
};


Mesh *getMesh(int cpp_obj, bool may_be_null = false)
{
  if (!cpp_obj && may_be_null)
  {
    return nullptr;
  }
  else
  {
    assert(cpp_obj);
    return castGObj<Mesh>(cpp_obj);
  }

  // auto obj = core::getGObj(cpp_obj);
  // assert(obj);


  // if (obj->getType() == core::GObj::Type::MESH)
  // {
  //   return static_cast<Mesh*>(obj);
  // }
  // else if (may_be_null)
  // {
  //   return nullptr;
  // }
  // else
  // {
  //   abort();
  // }
}

// Mesh *get(int cpp_obj, bool may_be_null = false)
// {
  // return getMesh(cpp_obj, may_be_null);
// }


jstring JNICALL collisionNameMulti(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  return nullptr;
//   return import.collisionNameMulti(env, obj, arg0, arg1);
}

jfloat JNICALL collisionDistMulti(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 0;
//   return import.collisionDistMulti(env, obj, arg0);
}

jstring JNICALL collisionChunk(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 0;
//   return import.collisionChunk(env, obj, arg0);
}

jint JNICALL Load(JNIEnv *env, jobject obj,
    jstring arg0)
{
  jsize len = env->GetStringUTFLength(arg0);
  char buf[len+1];
  env->GetStringUTFRegion(arg0, 0, len, buf);

  std::string path(buf);

  auto mesh = core::GObj::create<Mesh>();
  try
  {
    if (util::isSuffix(".sim", util::makeLowercase(path)))
    {
      auto &params = core::getParameterFile(path);
      auto mesh_name = params.getSection("Body").get("Mesh");
      path = util::getDirFromPath(path) + '/' + mesh_name + ".msh";
    }

    mesh->m_instance = core::createMeshInstance(path);
  }
  catch (...)
  {
    // throw;
  }

  mesh->onJavaObjectCreate();

  return core::createObjectHandle(mesh);
}

jint JNICALL CreateMeshFromHiermeshChunk(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
  auto mesh = core::GObj::create<Mesh>();
  mesh->m_instance = getHierMeshChunk(cpp_obj);

  mesh->onJavaObjectCreate();

  return core::createObjectHandle(mesh);
}

jstring JNICALL GetFileName(JNIEnv *env, jobject obj,
    jint arg0)
{
  return nullptr;
//   return import.GetFileName(env, obj, arg0);
}

void JNICALL GetBoundBox(JNIEnv *env, jobject obj,
    jint arg0,
    jfloatArray arg1)
{
  // FIXME
  glm::vec3 origin(0);
  glm::vec3 extent(1);

  std::array<float, 6> data =
  {
    origin.x,
    origin.y,
    origin.z,
    extent.x,
    extent.y,
    extent.z,
  };

  env->SetFloatArrayRegion(arg1, 0, data.size(), data.data());
}

void JNICALL SetPosXYZATK(JNIEnv *env, jobject obj,
    jint arg0,
    jdouble arg1,
    jdouble arg2,
    jdouble arg3,
    jfloat arg4,
    jfloat arg5,
    jfloat arg6)
{
  auto mesh = getMesh(arg0);

  mesh->m_pos = glm::dvec3(arg1, arg2, arg3);
  mesh->m_yaw_pitch_roll = glm::vec3(arg4, arg5, arg6);

//   return import.SetPosXYZATK(env, obj, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
}

jint JNICALL PreRenderXYZ(JNIEnv *env, jobject obj,
    jint arg0,
    jdouble arg1,
    jdouble arg2,
    jdouble arg3)
{
//   abort();
  
  return 1;
  
//   return 1;
  
//   return import.PreRenderXYZ(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL PreRender(JNIEnv *env, jobject obj,
    jint arg0)
{
  // 1 = solid, 2 = transparent, 4 = shadow ?s
  
//   return 4;
  
  return 1;
  
//   return import.PreRender(env, obj, arg0);
}

void JNICALL SetFastShadowVisibility(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
//   return import.SetFastShadowVisibility(env, obj, arg0, arg1);
}

void JNICALL Render(JNIEnv *env, jobject obj,
    jint arg0)
{
  getMesh(arg0)->render();
//   return import.Render(env, obj, arg0);
}

void JNICALL RenderShadowProjective(JNIEnv *env, jobject obj,
    jint arg0)
{
//   return import.RenderShadowProjective(env, obj, arg0);
}

void JNICALL RenderShadowVolume(JNIEnv *env, jobject obj,
    jint arg0)
{
//   return import.RenderShadowVolume(env, obj, arg0);
}

void JNICALL RenderShadowVolumeHQ(JNIEnv *env, jobject obj,
    jint arg0)
{
//   return import.RenderShadowVolumeHQ(env, obj, arg0);
}

jint JNICALL DetectCollision(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jint arg2,
    jdoubleArray arg3)
{
  return 0;
//   return import.DetectCollision(env, obj, arg0, arg1, arg2, arg3);
}

jfloat JNICALL DetectCollisionLine(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2,
    jdoubleArray arg3)
{
  return -1;
//   return import.DetectCollisionLine(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL DetectCollisionLineMulti(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2,
    jdoubleArray arg3)
{
  UNIMPLEMENTED
//   return import.DetectCollisionLineMulti(env, obj, arg0, arg1, arg2, arg3);
}

jfloat JNICALL DetectCollisionQuad(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2)
{
  UNIMPLEMENTED
//   return import.DetectCollisionQuad(env, obj, arg0, arg1, arg2);
}

jint JNICALL DetectCollisionQuadMultiM(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2)
{
  UNIMPLEMENTED
//   return import.DetectCollisionQuadMultiM(env, obj, arg0, arg1, arg2);
}

jint JNICALL DetectCollisionPoint(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2)
{
  return 0;
//   return import.DetectCollisionPoint(env, obj, arg0, arg1, arg2);
}

jfloat JNICALL DetectCollisionPlane(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2)
{
  return 0;
//   return import.DetectCollisionPlane(env, obj, arg0, arg1, arg2);
}

void JNICALL SetScale(JNIEnv *env, jobject obj,
    jint arg0,
    jfloat arg1)
{
//   return import.SetScale(env, obj, arg0, arg1);
}

jfloat JNICALL Scale(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 1;
//   return import.Scale(env, obj, arg0);
}

void JNICALL SetScaleXYZ(JNIEnv *env, jobject obj,
    jint arg0,
    jfloat arg1,
    jfloat arg2,
    jfloat arg3)
{
//   return import.SetScaleXYZ(env, obj, arg0, arg1, arg2, arg3);
}

void JNICALL ScaleXYZ(JNIEnv *env, jobject obj,
    jint arg0,
    jfloatArray arg1)
{
  UNIMPLEMENTED
//   return import.ScaleXYZ(env, obj, arg0, arg1);
}

jfloat JNICALL VisibilityR(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 0;
//   return import.VisibilityR(env, obj, arg0);
}

jfloat JNICALL CollisionR(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 0;
//   return import.CollisionR(env, obj, arg0);
}

jfloat JNICALL GetUniformMaxDist(JNIEnv *env, jobject obj,
    jint arg0)
{
  auto mesh = getMesh(arg0);
  return mesh->m_instance ? mesh->m_instance->getMesh().getMaxVisibleDistance() : 0;
}

jint JNICALL Frames(JNIEnv *env, jobject obj,
    jint arg0)
{
  return getMesh(arg0)->m_instance->getMesh().getFrameCount();
//   return import.Frames(env, obj, arg0);
}

void JNICALL SetFrame(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
//   LOG_INFO<<"SetFrame__II: "<<arg1<<std::endl;
  getMesh(arg0)->m_current_animation_frame = arg1;
//   return import.SetFrame__II(env, obj, arg0, arg1);
}

void JNICALL SetFrame(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jfloat arg3)
{
  UNIMPLEMENTED

//   LOG_INFO<<"SetFrame__IIIF: "<<arg1<<", "<<arg2<<", "<<arg3<<std::endl;
//   getMesh(arg0)->m_current_animation_frame = arg1;
//   return import.SetFrame__IIIF(env, obj, arg0, arg1, arg2, arg3);
}

void JNICALL SetFrameFromRange(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jfloat arg3)
{
//   LOG_INFO<<"SetFrameFromRange: "<<arg1<<", "<<arg2<<", "<<arg3<<std::endl;
  
  size_t frame = glm::mix((float)arg1, (float)arg2, arg3);
//   LOG_INFO<<"frame: "<<frame<<std::endl;
  
  getMesh(arg0)->m_current_animation_frame = frame;
//   return import.SetFrameFromRange(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL Hooks(JNIEnv *env, jobject obj,
    jint arg0)
{
  UNIMPLEMENTED
//   return import.Hooks(env, obj, arg0);
}

jint JNICALL HookFind(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1)
{
  jsize len = env->GetStringUTFLength(arg1);
  char buf[len+1];
  env->GetStringUTFRegion(arg1, 0, len, buf);

  return getMesh(arg0)->findHook(buf);
}

jstring JNICALL HookName(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  return env->NewStringUTF(getMesh(arg0)->m_instance->getMesh().getHookName(arg1).c_str());
}

void JNICALL HookMatrix(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jdoubleArray arg2)
{
  std::array<double, 16> data;
  matrixToArray(getMesh(arg0)->getHookMatrix(arg1), data);

  env->SetDoubleArrayRegion(arg2, 0, data.size(), data.data());
}

jint JNICALL HookFaceFind(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2)
{
  return -1;
//   return import.HookFaceFind(env, obj, arg0, arg1, arg2);
}

void JNICALL HookFaceMatrix(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jdoubleArray arg3)
{
  UNIMPLEMENTED
//   return import.HookFaceMatrix(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL Materials(JNIEnv *env, jobject obj,
    jint arg0)
{
  UNIMPLEMENTED
//   return import.Materials(env, obj, arg0);
}

jint JNICALL MaterialFind(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1,
    jint arg2,
    jint arg3)
{
  return -1;
//   return import.MaterialFind(env, obj, arg0, arg1, arg2, arg3);
}

jobject JNICALL Material(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  return (jobject) getDummyMaterial();

//   return import.Material(env, obj, arg0, arg1);
}

void JNICALL MaterialReplace(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2)
{
//   return import.MaterialReplace__III(env, obj, arg0, arg1, arg2);
}

void JNICALL MaterialReplace(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jstring arg2)
{
//   return import.MaterialReplace__IILjava_lang_String_2(env, obj, arg0, arg1, arg2);
}

void JNICALL MaterialReplace(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1,
    jint arg2)
{
//   return import.MaterialReplace__ILjava_lang_String_2I(env, obj, arg0, arg1, arg2);
}

void JNICALL MaterialReplace(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1,
    jstring arg2)
{
//   return import.MaterialReplace__ILjava_lang_String_2Ljava_lang_String_2(env, obj, arg0, arg1, arg2);
}

jint JNICALL IsSimilar(JNIEnv *env, jobject obj,
    jint cpp_obj_1,
    jint cpp_obj_2)
{
  if (!cpp_obj_1 || !cpp_obj_2)
  {
    return false;
  }
  else if (isMesh(cpp_obj_1) && isMesh(cpp_obj_2))
  {
    // FIXME: compare Mesh::m_instance ?
    //        - that would need MeshInstance::isSimilar()
    return getMesh(cpp_obj_1) == getMesh(cpp_obj_2);
  }
  else
  {
    return false;
  }
}

jint JNICALL IsAllowsSimpleRendering(JNIEnv *env, jobject obj,
    jint arg0)
{
  return true;
//   return false;
}

jfloat JNICALL HeightMapMeshGetHeight(JNIEnv *env, jobject obj,
    jint arg0,
    jdouble arg1,
    jdouble arg2)
{
  return 0;
//   return import.HeightMapMeshGetHeight(env, obj, arg0, arg1, arg2);
}

jint JNICALL HeightMapMeshGetNormal(JNIEnv *env, jobject obj,
    jint arg0,
    jdouble arg1,
    jdouble arg2,
    jfloatArray arg3)
{
  UNIMPLEMENTED
//   return import.HeightMapMeshGetNormal(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL HeightMapMeshGetPlane(JNIEnv *env, jobject obj,
    jint arg0,
    jdouble arg1,
    jdouble arg2,
    jdoubleArray arg3)
{
  UNIMPLEMENTED
//   return import.HeightMapMeshGetPlane(env, obj, arg0, arg1, arg2, arg3);
}

jfloat JNICALL HeightMapMeshGetRayHit(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1)
{
  UNIMPLEMENTED
//   return import.HeightMapMeshGetRayHit(env, obj, arg0, arg1);
}


} // namespace


#include <_generated/il2_jni/il2.engine.Mesh_registration>

namespace il2ge::il2_jni
{


bool isMesh(jint cpp_obj)
{
  return getGObj(cpp_obj)->getType() == Mesh::getStaticType();
}


void renderMeshes(jint* objs,
                  const glm::dvec3* pos,
                  const glm::vec3* yaw_pitch_roll,
                  jint count)
{
  for (int i = 0; i < count; i++)
  {
    auto mesh = getMesh(objs[i]);
    if (mesh)
      mesh->render(pos[i], yaw_pitch_roll[i], 0);
  }
}


} // namespace il2ge::il2_jni


// std::shared_ptr<const il2ge::mesh::MeshBase> core::getMeshFromGObj(int cpp_obj)
// {
//   auto mesh = ::getMesh(cpp_obj, true);
//     return mesh ? mesh->m_mesh : nullptr;
// }
