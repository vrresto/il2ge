#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "cfg_gobj.h"
#include <log.h>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/il2.engine.CfgIntEngine_definitions>



struct CfgInt : public CfgGObj, public GObjSubClass<CfgInt>
{
  static auto createType() { return GObj::createType(g_java_class_path); }

  int m_state = 0;
  int m_first_state = 0;
  int m_default_state = 0;
  int m_states = 10; //FIXME

  CfgInt(const std::string&  name) : CfgGObj(name, getStaticType()) {}
};



CfgInt* getObject(jint cpp_obj)
{
  return castGObj<CfgInt>(cpp_obj);
}



jstring JNICALL Name(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgIntEngine.Name()"<<std::endl;
//   abort();
  return env->NewStringUTF(getObject(cpp_obj)->m_name.c_str());
}

jboolean JNICALL IsPermanent(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgIntEngine.IsPermanent()"<<std::endl;
//   abort();
  return false;
}

jint JNICALL FirstState(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgIntEngine.FirstState()"<<std::endl;
//   abort();
  return getObject(cpp_obj)->m_first_state;
}

jint JNICALL CountStates(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgIntEngine.CountStates()"<<std::endl;
//   abort();
  return getObject(cpp_obj)->m_states;
}

jint JNICALL DefaultState(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgIntEngine.DefaultState()"<<std::endl;
//   abort();
  return getObject(cpp_obj)->m_default_state;
}

jstring JNICALL NameState(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint state)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgIntEngine.NameState()"<<std::endl;
//   abort();

  auto str = "state_" + std::to_string(state);
  return env->NewStringUTF(str.c_str());
}

jint JNICALL GetStateStatus(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint arg1)
{
  // LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgIntEngine.GetStateStatus()"<<std::endl;
  // abort();
  return 0xFF;
}

jint JNICALL GetState(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgIntEngine.GetState()"<<std::endl;
//   abort();
  return getObject(cpp_obj)->m_state;
}

jint JNICALL SetState(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint state)
{
  auto object = getObject(cpp_obj);
  LOG_INFO << object->m_name <<  "->SetState: "  << state << std::endl;
  object->m_state = state;
  return 0;
}


} // namespace


core::ObjRef<CfgGObj> il2ge::il2_jni::createCfgInt(const std::string& name)
{
  return core::GObj::create<CfgInt>(name);
}


#include <_generated/il2_jni/il2.engine.CfgIntEngine_registration>
