/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include <core/scene.h>
#include <core/context.h>
#include <core.h>
#include <render_util/camera_3d.h>
#include <render_util/state.h>

#include <deque>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/intersect.hpp>
#include <glm/gtx/normal.hpp>

#include <render_util/gl_binding/gl_functions.h>


namespace core = il2ge::core;
using namespace il2ge::il2_jni;
using namespace render_util::gl_binding;


namespace
{


#include <_generated/il2_jni/il2.engine.Landscape_definitions>


constexpr auto TERRAIN_RESOLUTION_M = render_util::TerrainBase::GRID_RESOLUTION_M;


struct DebugData
{
  std::vector<std::array<glm::vec3, 3>> queried_triangles;
  std::vector<glm::vec2> queried_points;
};


std::deque<DebugData> g_debug_data; // FIXME


DebugData& getDebugData()
{
  if (g_debug_data.empty())
    g_debug_data.emplace_back();

  return g_debug_data.back();
}


float getHeight(const render_util::ElevationMap& hm, glm::ivec2 coords)
{
  if (coords.x < 0 || coords.y < 0 || coords.x >= hm.w() || coords.y >= hm.h())
    return 0;
  else
    return hm.get(coords);
}


auto getNormalizedGridMeshTriangleVertexCoords(glm::vec2 xy)
{
  using namespace glm;

  auto quad_origin = ivec2(floor(xy));
  auto quad_relative_pos = fract(xy);

  std::array<ivec2, 3> ret;

  //FIXME this depends on the triangle arrangement

  if (quad_relative_pos.x < quad_relative_pos.y)
  {
    // top left
    ret[0] = { 0, 0 };
    ret[1] = { 0, 1 };
    ret[2] = { 1, 1 };
  }
  else
  {
    // bottom right
    ret[0] = { 0, 0 };
    ret[1] = { 1, 1 };
    ret[2] = { 1, 0 };
  }

  for (auto& coords : ret)
    coords += quad_origin;

  return ret;
}


template <class GetHeight>
auto getGridMeshTriangle(glm::vec2 xy, float grid_scale, GetHeight get_height)
{
  auto triangle_vertex_coords =
    getNormalizedGridMeshTriangleVertexCoords(xy / glm::vec2(grid_scale));

  std::array<glm::vec3,3> triangle;
  for (int i = 0; i < 3; i++)
  {
    auto& grid_coords = triangle_vertex_coords.at(i);

    auto xy = glm::vec2(grid_coords) * glm::vec2(grid_scale);
    auto z = get_height(grid_coords);

    triangle[i] = glm::vec3(xy, z);
  }

  return triangle;
}


template <typename T>
auto calcTriangleNormal(T& vertices)
{
  assert(vertices.size() == 3);
  return -glm::triangleNormal(vertices[0],
                              vertices[1],
                              vertices[2]);
}


auto getCellCornerVertexHeights(glm::vec2 xy,
                                const render_util::ElevationMap& height_map)
{
  auto cell_coords = glm::ivec2(glm::floor(xy)) / TERRAIN_RESOLUTION_M;

  auto get_height = [&] (auto coords)
  {
    return getHeight(height_map, coords);
  };

  std::array<float, 4> heights
  {
    get_height(cell_coords + glm::ivec2(0,0)),
    get_height(cell_coords + glm::ivec2(0,1)),
    get_height(cell_coords + glm::ivec2(1,1)),
    get_height(cell_coords + glm::ivec2(1,0)),
  };

  return heights;
}


float getTerrainHeight(const glm::dvec2& xy)
{
  auto& scene = core::getContext().getScene();

  if (!scene.isMapLoaded())
    return 0.f;

  auto& height_map = scene.getHeightMap();

  auto get_height = [&] (glm::ivec2 coords)
  {
    return getHeight(height_map, coords);
  };

  float grid_scale = TERRAIN_RESOLUTION_M;

  auto triangle =
    getGridMeshTriangle(xy, grid_scale, get_height);

  //FIXME
  getDebugData().queried_triangles.push_back(triangle);
  getDebugData().queried_points.push_back(xy);

  auto normal = -calcTriangleNormal(triangle);

  float dist = 0;
  bool hit = glm::intersectRayPlane(glm::vec3(xy, -1),
                                    glm::vec3(0, 0, 1),
                                    triangle[0],
                                    normal,
                                    dist);

  assert(hit);

  return dist - 1;
}


glm::vec3 getTerrainNormal(const glm::dvec2& xy,
                           const render_util::ElevationMap& height_map)
{
  auto get_height = [&] (glm::ivec2 coords)
  {
    return getHeight(height_map, coords);
  };

  float grid_scale = TERRAIN_RESOLUTION_M;

  auto triangle =
    getGridMeshTriangle(xy, grid_scale, get_height);

  return calcTriangleNormal(triangle);
}


void JNICALL cPreRender(JNIEnv *env, jobject obj,
    jfloat arg0,
    jboolean arg1,
    jfloatArray arg2)
{
  const jint sun_moon_num_elements = env->GetArrayLength(arg2);
  assert(sun_moon_num_elements == 6);
  float sun_moon[sun_moon_num_elements];
  env->GetFloatArrayRegion(arg2, 0, sun_moon_num_elements, sun_moon);

  core::setSunDir(glm::normalize(glm::vec3(sun_moon[0], sun_moon[1], sun_moon[2])));
}

jint JNICALL cRender0(JNIEnv *env, jobject obj,
    jint is_mirror)
{
  core::drawTerrain(is_mirror);
  return 1;
}

void JNICALL cRender1(JNIEnv *env, jobject obj,
    jint arg0)
{
}

jboolean JNICALL cLoadMap(JNIEnv *env, jobject obj,
    jstring arg0,
    jintArray arg1,
    jint arg2,
    jboolean arg3)
{
  jsize len = env->GetStringUTFLength(arg0);
  char buf[len+1];
  env->GetStringUTFRegion(arg0, 0, len, buf);

  core::loadMap(buf, env);

  return true;
}

void JNICALL cUnloadMap(JNIEnv *env, jobject obj)
{
  core::unloadMap();
}

void JNICALL setPixelMapH(JNIEnv *env, jobject obj, jint x, jint y, jint value)
{
  UNIMPLEMENTED
}

void JNICALL cRenderBridgeRoad(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jint arg3,
    jint arg4,
    jint arg5,
    jfloat arg6)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.cRenderBridgeRoad()"<<std::endl;
//   abort();
}

jint JNICALL cRefBeg(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 0;
}

jint JNICALL cRefEnd(JNIEnv *env, jobject obj)
{
  return 0;
}

jboolean JNICALL cRayHitHQ(JNIEnv *env, jobject obj,
    jfloatArray ray_start,
    jfloatArray ray_end,
    jfloatArray ray_hit)
{
  assert(env->GetArrayLength(ray_start) == 3);
  assert(env->GetArrayLength(ray_end) == 3);
  assert(env->GetArrayLength(ray_hit) == 3);

//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.cRayHitHQ()"<<std::endl;
//   abort();
  return false;
}

void JNICALL setAstronomic(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jfloat arg3,
    jfloat arg4)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.setAstronomic()"<<std::endl;
//   abort();
}


jint JNICALL getPixelMapT(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  UNIMPLEMENTED
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.getPixelMapT()"<<std::endl;
//   abort();
  // return 0;
}

void JNICALL setPixelMapT(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2)
{
  UNIMPLEMENTED
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.setPixelMapT()"<<std::endl;
//   abort();
}

jint JNICALL getPixelMapH(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  UNIMPLEMENTED
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.getPixelMapH()"<<std::endl;
//   abort();
//   return 0;
}


jboolean JNICALL isWater(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.isWater()"<<std::endl;
//   abort();
  return false;
}

jint JNICALL estimateNoWater(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.estimateNoWater()"<<std::endl;
  abort();
}

jint JNICALL getSizeXpix(JNIEnv *env, jobject obj)
{
//   return core::getPixelMapH()->w();
  return 1000;
}

jint JNICALL getSizeYpix(JNIEnv *env, jobject obj)
{
//   return core::getPixelMapH()->h();
  return 1000;
}


jfloat JNICALL cHmax(JNIEnv *env, jobject obj,
    jfloat x,
    jfloat y)
{
  auto& hm = core::getContext().getScene().getHeightMap();
  auto heights = getCellCornerVertexHeights(glm::vec2(x,y), hm);

  float max = 0;

  for (auto& h :  heights)
  {
    if (h > max)
      max = h;
  }

  return max;
}


jfloat JNICALL cHmin(JNIEnv *env, jobject obj,
    jfloat x,
    jfloat y)
{
  auto& hm = core::getContext().getScene().getHeightMap();
  auto heights = getCellCornerVertexHeights(glm::vec2(x,y), hm);

  float min = std::numeric_limits<float>::max();

  for (auto& h : heights)
  {
    if (h < min)
      min = h;
  }

  return min;
}


// used by DrawEnvXY.setUniformMaxDist
//FIXME return average height of cell?
jfloat JNICALL cH(JNIEnv *env, jobject obj,
    jfloat x,
    jfloat y)
{
  // return core::getContext().getScene().getTerrainHeight(glm::dvec2(arg0, arg1));

  return cHmax(env, obj, x, y);

  // return (cHmax(env, obj, x, y) + cHmin(env, obj, x, y) / 2;

  // UNIMPLEMENTED
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.cH()"<<std::endl;
//   abort();
  // return 0;
}


// used to determine minimum height for camera placement
// use by RealFlightModel.checkAirborneState
jfloat JNICALL cHQ_1Air(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.cHQ_Air()"<<std::endl;
//   abort();
  // return 0;

  return getTerrainHeight(glm::dvec2(arg0, arg1));
}

jfloat JNICALL cHQ_1forestHeightHere(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.cHQ_forestHeightHere()"<<std::endl;
//   abort();
  return 0;
}

jint JNICALL HQRoadTypeHere(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1)
{
  return 2; //FIXME
}


// used for object placement and collision detection
jfloat JNICALL cHQ(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1)
{
  return getTerrainHeight(glm::dvec2(arg0, arg1));
}


// surface normal
void JNICALL cEQN(JNIEnv *env, jobject obj,
    jfloatArray arg0)
{
  assert(env->GetArrayLength(arg0) >= 3);
  auto array = static_cast<jfloat*>(env->GetPrimitiveArrayCritical(arg0, nullptr));

  glm::vec2 pos(array[0], array[1]);

  auto& hm = core::getContext().getScene().getHeightMap();
  auto normal = getTerrainNormal(glm::dvec2(pos), hm);

  array[0] = normal.x;
  array[1] = normal.y;
  array[2] = normal.z;

  env->ReleasePrimitiveArrayCritical(arg0, array, JNI_COMMIT);
}


jint JNICALL getFogAverageRGBA(JNIEnv *env, jobject obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.getFogAverageRGBA()"<<std::endl;
//   abort();
  return 0;
}

jfloat JNICALL getDynamicFogAlpha(JNIEnv *env, jobject obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.getDynamicFogAlpha()"<<std::endl;
//   abort();
  return 0;
}

jint JNICALL getDynamicFogRGB(JNIEnv *env, jobject obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.getDynamicFogRGB()"<<std::endl;
//   abort();
  return 0;
}

jint JNICALL getFogRGBA(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1,
    jfloat arg2)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.getFogRGBA()"<<std::endl;
//   abort();
  return 0;
}

jint JNICALL ComputeVisibilityOfLandCells(JNIEnv *env, jobject obj,
    jfloat max_visual_distance,
    jint arg1,
    jint arg2,
    jint arg3,
    jint arg4,
    jint arg5,
    jint arg6,
    jint arg7,
    jint arg8,
    jintArray array_out)
{
  auto array_out_size = env->GetArrayLength(array_out);

  auto array_out_data = static_cast<jint*>(env->GetPrimitiveArrayCritical(array_out, nullptr));
  for (size_t i = 0; i < array_out_size; i++)
  {
    array_out_data[i] = 0xFFFFFFFF;
  }
  env->ReleasePrimitiveArrayCritical(array_out, array_out_data, JNI_COMMIT);

  return 1;
}

jint JNICALL Compute2DBoundBoxOfVisibleLand(JNIEnv *env, jobject obj,
    jfloat max_visual_distance,
    jint arg1,
    jint arg2,
    jfloatArray arg3)
{
  auto camera_pos = core::getCamera()->getPos();

  std::array<jfloat, 4> values =
  {
    camera_pos.x - 40000,
    camera_pos.y - 40000,
    camera_pos.x + 40000,
    camera_pos.y + 40000,
  };

  env->SetFloatArrayRegion(arg3, 0, values.size(), values.data());

  return 1;
}

void JNICALL MarkStaticActorsCells(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jint arg3,
    jint arg4,
    jintArray arg5)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.MarkStaticActorsCells()"<<std::endl;
//   abort();
}

void JNICALL MarkActorCellWithTrees(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jintArray arg3,
    jint arg4)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.MarkActorCellWithTrees()"<<std::endl;
//   abort();
}

jboolean JNICALL cIsCubeUpdated(JNIEnv *env, jobject obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.cIsCubeUpdated()"<<std::endl;
//   abort();
  return false;
}

void JNICALL cSetRoadsFunDrawing(JNIEnv *env, jobject obj,
    jboolean arg0)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Landscape.cSetRoadsFunDrawing()"<<std::endl;
  abort();
}


} // namespace


namespace il2ge::core
{


void mapClearDebugFrameData()
{
  g_debug_data.emplace_back();

  while (g_debug_data.size() > 10)
    g_debug_data.pop_front();
}


void mapRenderDebug()
{
  gl::MatrixMode(GL_PROJECTION);
  gl::LoadMatrixf(glm::value_ptr(core::getCamera()->getProjectionMatrixFar()));
  gl::MatrixMode(GL_MODELVIEW);
  gl::LoadMatrixf(glm::value_ptr(core::getCamera()->getWorld2ViewMatrix()));

  render_util::StateModifier state;

  for (auto& it : g_debug_data)
  {
    auto& d = it;

    state.setPolygonMode(GL_LINE);

    gl::Begin(GL_TRIANGLES);

    gl::Color4f(1, 0, 0, 1);

    for (auto& tri : d.queried_triangles)
    {
      for (auto& v : tri)
        gl::Vertex3f(v.x, v.y, v.z);
    }

    gl::End();

    state.setPolygonMode(GL_FILL);

    gl::PointSize(5);

    gl::Begin(GL_POINTS);

    gl::Color4f(0, 1, 0, 1);

    for (auto& v : d.queried_points)
    {
      gl::Vertex3f(v.x, v.y, 0);
    }

    gl::End();

    gl::PointSize(1);
  }

  gl::MatrixMode(GL_PROJECTION);
  gl::LoadIdentity();
  gl::MatrixMode(GL_MODELVIEW);
  gl::LoadIdentity();
}


} // namespace il2ge::core


#include <_generated/il2_jni/il2.engine.Landscape_registration>
