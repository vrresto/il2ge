/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include <core.h>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/il2.engine.Renders_definitions>

// Interface import;

void JNICALL PrePreRenders(JNIEnv *env, jobject obj)
{
  core::onPrePreRenders();
//   return import.PrePreRenders(env, obj);
}

void JNICALL PostPreRenders(JNIEnv *env, jobject obj)
{
  core::onPostPreRenders();
//   return import.PostPreRenders(env, obj);
}

void JNICALL PostRenders(JNIEnv *env, jobject obj)
{
  core::onPostRenders();
//   return import.PostRenders(env, obj);
}


} // namespace

#include <_generated/il2_jni/il2.engine.Renders_registration>
