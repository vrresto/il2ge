#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "cfg_gobj.h"
#include <core/objects.h>
#include <core.h>
#include <java_util.h>
#include <log.h>

#include <unordered_set>
#include <unordered_map>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/il2.engine.CfgGObj_definitions>


// std::unordered_map<std::string, jobject> g_objects;

//FIXME
std::unordered_map<std::string, core::ObjRef<CfgGObj>> g_objects;


// CfgGObj *getObject(jint cpp_obj)
// {
//   return static_cast<CfgGObj*>(getGObj(cpp_obj, core::GObj::Type::CFG_GOBJ));
// }


// CfgGObj* createObject(std::string name, std::string class_name)
// {
//   // return new CfgGObj(class_name);
//
//   // auto cfg = std::make_unique<CfgGObj>();
//   // cfg->name = name;
//   // auto cpp_obj = core::addGObj(std::move(cfg));
//
//   // auto env = il2ge::java::getEnv();
//
//   // auto java_class = env->FindClass(("com/maddox/il2/engine/" + class_name).c_str());
//   // assert(java_class);
//   // auto constructor_id = env->GetMethodID(java_class, "<init>", "(I)V");
//   // assert(constructor_id);
//
//   // auto java_obj = env->NewObject(java_class, constructor_id, cpp_obj);
//   // assert(java_obj);
//
//   // return env->NewGlobalRef(java_obj);
// }


// enum class CfgClass
// {
  // FLAGS_ENGINE,
  // INT_ENGINE,
// };


// std::unordered_map<std::string, CfgClass> g_cfg_class_for_option =
// {
  // { "ShadowsFlags", INT_ENGINE }
// };


// struct SettingClass
// {
//   std::string m_java_class_name;
//   std::vector<std::string> m_settings;
// };
//
//
//
//
// std::vector<SettingClass> g_setting_classes =
// {
//   {
//     "com/maddox/il2/engine/CfgFlagsEngine",
//     {
//       "ShadowsFlags",
//       "TexFlags",
//     }
//   },
//   {
//     "com/maddox/il2/engine/CfgIntEngine",
//     {
//       #include "cfg_int_settings.inc"
//     }
//   },
// };


std::unordered_set<std::string> g_int_settings =
{
  #include "cfg_int_settings.inc"
};


std::unordered_set<std::string> g_flags_settings =
{
  "ShadowsFlags",
  "TexFlags",
};


//FIXME move to util
template <class T, typename T_value>
bool contains(T& set, T_value& value)
{
  return set.find(value) != set.end();
}



// std::unordered_map<std::string, SettingClass> g_class_for_setting;


// const std::string& getJavaClassForSetting(const std::string& setting)
// {
//   if (g_class_for_setting.empty())
//   {
//     for (auto& c : g_setting_classes)
//     {
//       for (auto& s : c.m_settings)
//       {
//         g_class_for_setting[s] = c;
//       }
//     }
//   }
//
//   try
//   {
//     return g_class_for_setting.at(setting).m_java_class_name;
//   }
//   catch (...)
//   {
//     throw std::runtime_error("No class for setting " + setting);
//   }
// }


jobject JNICALL FindByName(JNIEnv *env, jobject obj,
    jstring arg0)
{

  jsize len = env->GetStringUTFLength(arg0);
  char buf[len+1];
  env->GetStringUTFRegion(arg0, 0, len, buf);
  std::string name(buf);

  LOG_WARNING << "name: " << name << std::endl;

  // auto& java_class = getJavaClassForSetting(name);

  // LOG_WARNING << "java class: " << java_class << std::endl;

  auto &object = g_objects[name];

  if (!object)
  {
    if (contains(g_int_settings, name))
      object = createCfgInt(name);
    else if (contains(g_flags_settings, name))
      object = createCfgFlags(name);
    else
      throw std::runtime_error("No class for setting " + name);

    // object = createObject(name, java_class);
    // assert(object->getJavaClassName() == java_class);

    // object = (name == "ShadowsFlags" || name == "TexFlags") ?
      // createObject(name, "com/maddox/il2/engine/CfgFlagsEngine") :
      // createObject(name, "com/maddox/il2/engine/CfgIntEngine");
  }

  // object->unref() isn't called, to account for the reference held bythe hash map

  //FIXME: this should NOT create multiple java objects for the same c++ object!!!
  return createJavaObject(object);
}


} // namespace


#include <_generated/il2_jni/il2.engine.CfgGObj_registration>


// std::string il2_jni::getCfgGObjName(jint obj)
// {
//   return getObject(obj)->name;
// }
