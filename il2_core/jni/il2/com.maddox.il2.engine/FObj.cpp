#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "mat.h"
#include <core.h>

#include <unordered_map>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;


namespace
{

#include <_generated/il2_jni/il2.engine.FObj_definitions>


std::unordered_set<std::string> g_ignored_classes =
{
};


jstring JNICALL Name(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
  auto& gobj = getGObj(cpp_obj);
  assert(gobj->isFObj());
  auto& name = static_cast<core::FObj*>(gobj.get())->getPath();
  return env->NewStringUTF(name.c_str());
}


void JNICALL Rename(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jstring name)
{
  jsize len = env->GetStringUTFLength(name);
  char buf[len+1];
  env->GetStringUTFRegion(name, 0, len, buf);

  auto& gobj = getGObj(cpp_obj);
  assert(gobj->isFObj());
  static_cast<core::FObj*>(gobj.get())->rename(buf);
}


jlong JNICALL Hash(JNIEnv *env, jobject obj,
    jint arg0)
{
  UNIMPLEMENTED
}


bool isClassIgnored(std::string name)
{
  return g_ignored_classes.find(name) != g_ignored_classes.end();
}


jint JNICALL GetFObj(JNIEnv *env, jobject obj,
    jstring arg0)
{
  jsize len = env->GetStringUTFLength(arg0);
  char buf[len+1];
  env->GetStringUTFRegion(arg0, 0, len, buf);

  try
  {
    auto object = core::getFObjRef(buf);
    assert(object);

    return core::createObjectHandle(object);
  }
  catch (core::FObj::ReadError& e)
  {
    LOG_ERROR << e.what() << std::endl;
    return 0;
  }
  catch (core::GObj::ClassNotFoundError& e)
  {
    if (isClassIgnored(e.getClassName()))
    {
      LOG_ERROR << e.what() << std::endl;
      return 0;
    }
    throw;
  }
}

jint JNICALL GetFObj(JNIEnv *env, jobject obj,
    jstring arg0,
    jstring arg1)
{
    UNIMPLEMENTED
}

jint JNICALL GetFObj(JNIEnv *env, jobject obj,
    jlong arg0)
{
  UNIMPLEMENTED
}

jboolean JNICALL Exist(JNIEnv *env, jobject obj,
    jstring arg0)
{
  return false;
}

jboolean JNICALL Exist(JNIEnv *env, jobject obj,
    jlong arg0)
{
  return false;
}

jint JNICALL NextFObj(JNIEnv *env, jobject obj,
    jint arg0)
{
  UNIMPLEMENTED
}

jint JNICALL ReLoad(JNIEnv *env, jobject obj,
    jint arg0)
{
  UNIMPLEMENTED
}


} // namespace


#include <_generated/il2_jni/il2.engine.FObj_registration>
