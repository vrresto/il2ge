/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "gobj_factories.h"
#include <il2_jni.h>
#include <core.h>
#include <core_common.h>
#include <core/objects.h>
#include <mutex_locker.h>

#include <iostream>
#include <list>
#include <unordered_set>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;
using namespace std;
using il2ge::core_common::MutexLocker;



namespace il2ge::core
{


extern std::vector<const GObjType*> g_all_types; //FIXME HACK


//FIXME this doesn't belong here


std::vector<const GObjType*> g_all_types;


class GObjType::JavaObjectFactory
{
  JNIEnv* m_env = nullptr;
  jclass m_java_class {};
  jmethodID m_constructor_id {};

public:
  JavaObjectFactory(JNIEnv* env, std::string class_name) :
    m_env(env)
  {
    assert(!class_name.empty());

    auto java_class = m_env->FindClass(class_name.c_str());

    if (!java_class)
    {
      LOG_ERROR << "Java class doesn't exist: " << class_name << std::endl;
      abort();
    }

    m_java_class = static_cast<jclass>(
        m_env->NewGlobalRef(static_cast<jobject>(java_class)) );

    m_constructor_id = m_env->GetMethodID(m_java_class, "<init>", "(I)V");
    assert(m_constructor_id);
  }

  jobject create(const GObjRef& gobj) const
  {
    gobj->onJavaObjectCreate();

    auto java_object = m_env->NewObject(m_java_class, m_constructor_id,
                                        core::createObjectHandle(gobj));
    assert(java_object);
    return java_object;
  }
};


GObjType::GObjType(std::string cpp_class_name, std::string java_class_name,
                   Factory factory) :
  m_cpp_class_name(cpp_class_name),
  m_java_class_name(java_class_name),
  m_factory(factory)
{
  g_all_types.push_back(this);
}


GObjType::GObjType(std::string cpp_class_name, std::string java_class_name) :
  m_cpp_class_name(cpp_class_name), m_java_class_name(java_class_name)
{
  g_all_types.push_back(this);
}


GObjType::GObjType(std::string java_class_name) :
  m_java_class_name(java_class_name)
{
  g_all_types.push_back(this);
}


GObjType::~GObjType()
{
  assert(m_objects == 0);
  remove(*this);
}


const GObjType::JavaObjectFactory& core::GObjType::getJavaObjectFactory() const
{
  if (!m_java_object_factory)
  {
    assert(!getJavaClassName().empty());
    auto env = il2ge::getJNIEnv();
    m_java_object_factory = std::make_unique<JavaObjectFactory>(env, getJavaClassName());
  }
  return *m_java_object_factory;
}


} // namespace il2ge::core


namespace
{

#include <_generated/il2_jni/il2.engine.GObj_definitions>


list<jint> g_main_thread_garbage;

list<jint> g_other_threads_garbage;
CRITICAL_SECTION g_other_threads_garbage_mutex;


void cleanGarbage(list<jint>& garbage)
{
  while (!garbage.empty())
  {
    auto cpp_obj = garbage.front();
    garbage.pop_front();

    getGObj(cpp_obj)->onJavaObjectDestroy();
    core::destroyObjectHandle(cpp_obj);
  }
}


void cleanGarbage()
{
  cleanGarbage(g_main_thread_garbage);

  {
    MutexLocker lock(g_other_threads_garbage_mutex);
    cleanGarbage(g_other_threads_garbage);
  }
}


void finalize(jint cpp_obj)
{
  if (il2ge::core::isMainThread())
  {
    g_main_thread_garbage.push_back(cpp_obj);
  }
  else
  {
    // must have been called from java's garbage collector thread
    MutexLocker lock(g_other_threads_garbage_mutex);
    g_other_threads_garbage.push_back(cpp_obj);
  }
}


void JNICALL Finalize(JNIEnv *env, jobject obj,
    jint arg0)
{
  finalize(arg0);
}


jint JNICALL LinkCount(JNIEnv *env, jobject obj,
    jint arg0)
{
  // return getGObj(arg0)->getRefCount();
  UNIMPLEMENTED
}

void JNICALL Unlink(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
  core::destroyObjectHandle(cpp_obj);
}


jobject JNICALL Clone(JNIEnv *env, jobject obj,
    jint arg0)
{
  auto object = getGObj(arg0);
  if (object)
  {
    return createJavaObject(object->clone());
  }
  else
  {
    assert(0);
    abort();
  }
}


jboolean JNICALL LinkLock(JNIEnv *env, jobject obj,
    jint arg0)
{
  UNIMPLEMENTED
}


void JNICALL SetLinkLock(JNIEnv *env, jobject obj,
    jint arg0,
    jboolean arg1)
{
  UNIMPLEMENTED
}


jstring JNICALL getCppClassName(JNIEnv *env, jobject obj,
    jint arg0)
{
  UNIMPLEMENTED
}


jobject JNICALL getJavaObject(JNIEnv *env, jobject obj,
    jint arg0)
{
  return createJavaObject(getGObj(arg0));
}


void JNICALL DeleteCppObjects(JNIEnv *env, jobject obj)
{
  cleanGarbage();
}


jint JNICALL version(JNIEnv *env, jobject obj)
{
  return 0;
}

void JNICALL setInterf(JNIEnv *env, jobject obj,
    jint arg0)
{
}


} // namespace


#include <_generated/il2_jni/il2.engine.GObj_registration>

namespace il2ge::il2_jni
{


void initGObj()
{
  InitializeCriticalSection(&g_other_threads_garbage_mutex);

  registerGObjTTFont();
  registerGObjMat();
  registerGObjEffectParameters();
}


int getMainThreadGarbageSize()
{
  return g_main_thread_garbage.size();
}


int getOtherThreadsGarbageSize()
{
  MutexLocker lock(g_other_threads_garbage_mutex);
  return g_other_threads_garbage.size();
}


jobject createJavaObject(const core::GObjRef& object)
{
  return object->getType().getJavaObjectFactory().create(object);
}


void finalizeGObj(jint cpp_obj)
{
  return finalize(cpp_obj);
}


} // namespace il2ge::il2_jni
