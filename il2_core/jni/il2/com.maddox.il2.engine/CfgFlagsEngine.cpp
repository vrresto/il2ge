#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "cfg_gobj.h"
#include <log.h>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/il2.engine.CfgFlagsEngine_definitions>



struct CfgFlags : public CfgGObj, public GObjSubClass<CfgFlags>
{
  static auto createType() { return GObj::createType(g_java_class_path); }

  int m_state = 0;
  int m_first_state = 0;
  int m_default_state = 0;
  int m_flags = 100; //FIXME

  // bool isFlagEnabled(int bit)
  // {
    // return m_state & (1 << bit);
  // }

  bool getDefault(int bit)
  {
    return m_default_state & (1 << bit);
  }

  bool get(int bit)
  {
    return m_state & (1 << bit);
  }

  void set(int bit, bool enabled)
  {
    if (enabled)
      m_state |= (1 << bit);
    else
      m_state &= ~(1 << bit);
  }

  CfgFlags(const std::string&  name) : CfgGObj(name, getStaticType()) {}
};


CfgFlags* getObject(jint cpp_obj)
{
  return castGObj<CfgFlags>(cpp_obj);
}


jstring JNICALL Name(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.Name()"<<std::endl;
//   abort();
  // return env->NewStringUTF(getCfgGObjName(cpp_obj).c_str());
  return env->NewStringUTF(getObject(cpp_obj)->m_name.c_str());
}

jboolean JNICALL IsPermanent(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.IsPermanent()"<<std::endl;
//   abort();
  return false;
}

jint JNICALL FirstFlag(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.FirstFlag()"<<std::endl;
//   abort();
  return getObject(cpp_obj)->m_first_state;
}

jint JNICALL CountFlags(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.CountFlags()"<<std::endl;
//   abort();
  return getObject(cpp_obj)->m_flags;
}

jboolean JNICALL GetDefaultFlag(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint flag)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.GetDefaultFlag()"<<std::endl;
//   abort();
  return getObject(cpp_obj)->getDefault(flag);
}

jstring JNICALL NameFlag(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint flag)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.NameFlag()"<<std::endl;
//   abort();
  // return env->NewStringUTF("Dummy");

  auto str = "flag_" + std::to_string(flag);
  return env->NewStringUTF(str.c_str());
}

jboolean JNICALL IsPermanentFlag(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint arg1)
{
  // LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.IsPermanentFlag()"<<std::endl;
  // abort();
  return false;
}

jboolean JNICALL IsEnabledFlag(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint flag)
{
  // LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.IsEnabledFlag()"<<std::endl;
  // abort();
  // return getObject(cpp_obj)->isFlagEnabled(flag);
  return true;
}

jint JNICALL GetFlagStatus(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint arg1)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.GetFlagStatus()"<<std::endl;
  abort();
}

jboolean JNICALL Get(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint flag)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.Get()"<<std::endl;
//   abort();
  return getObject(cpp_obj)->get(flag);
}

jint JNICALL Set(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint flag,
    jboolean value)
{
  // LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CfgFlagsEngine.Set()"<<std::endl;
  // abort();

  getObject(cpp_obj)->set(flag, value);
  return 0; //FIXME
}


} // namespace


core::ObjRef<CfgGObj> il2ge::il2_jni::createCfgFlags(const std::string& name)
{
  return core::GObj::create<CfgFlags>(name);
}


#include <_generated/il2_jni/il2.engine.CfgFlagsEngine_registration>
