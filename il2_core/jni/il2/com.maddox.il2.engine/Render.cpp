/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "mat.h"
#include <core.h>
#include <java_classes.h>
#include <il2ge/renderer.h>

#include <iostream>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;
using namespace std;

namespace
{


#include <_generated/il2_jni/il2.engine.Render_definitions>


struct RenderWrapper;


unordered_map<string, unique_ptr<RenderWrapper>> createWrappers();


unordered_map<string, unique_ptr<RenderWrapper>> g_render_wrappers = createWrappers();


struct RenderWrapper
{
  virtual void prepareStates() = 0;
  virtual void flushBegin() = 0;
  virtual void flushEnd() = 0;

  bool clearStatesOnFlush() { return m_clear_states_on_flush; }

  RenderWrapper(bool clear_states_on_flush) : m_clear_states_on_flush(clear_states_on_flush) {}

private:
  const bool m_clear_states_on_flush = false;
};

#if 0
struct RenderWrapperCockpit: public RenderWrapper
{
  RenderWrapperCockpit() : RenderWrapper(false) {}

  void prepareStates() override
  {
    core::onRenderCockpitBegin();
  }

  void flushBegin() override {}

  void flushEnd() override
  {
    core::onRenderCockpitEnd();
  }
};


struct RenderWrapperSpritesFog: public RenderWrapper
{
  RenderWrapperSpritesFog() : RenderWrapper(false) {}

  void prepareStates() override
  {
    core::onRenderSpritesFogBegin();
  }

  void flushBegin() override {}

  void flushEnd() override
  {
    core::onRenderSpritesFogEnd();
  }
};
#endif

unordered_map<string, unique_ptr<RenderWrapper>> createWrappers()
{
  unordered_map<string, unique_ptr<RenderWrapper>> map;

  // map["renderCockpit"] = make_unique<RenderWrapperCockpit>();
  // map["renderSpritesFog"] = make_unique<RenderWrapperSpritesFog>();

  return map;
}


std::string getCurrentRenderName()
{
  try
  {
    return il2ge::java::getClass<il2ge::java::il2::engine::Renders>().current().getName();
  }
  catch (std::exception& e)
  {
    return "";
  }
}


RenderWrapper* getRenderWrapper(const std::string& name)
{
  auto it = g_render_wrappers.find(name);
  if (it != g_render_wrappers.end())
    return it->second.get();
  else
    return {};
}


void JNICALL prepareStates(JNIEnv *env, jobject obj)
{
  auto wrapper = getRenderWrapper(getCurrentRenderName());
  if (wrapper)
    wrapper->prepareStates();

  core::prepareStates();
}


void JNICALL clearStates(JNIEnv *env, jobject obj)
{
}


void JNICALL flush(JNIEnv *env, jobject obj)
{
  auto wrapper = getRenderWrapper(getCurrentRenderName());

//   if (wrapper && wrapper->clearStatesOnFlush())
//     import.clearStates(env, obj);

  if (wrapper)
    wrapper->flushBegin();

  core::flushRenderList();

//   auto ret = import.flush(env, obj);

  if (wrapper)
  {
//     if (wrapper->clearStatesOnFlush())
//       import.clearStates(env, obj);

    wrapper->flushEnd();
  }
}


void JNICALL shadows(JNIEnv *env, jobject obj)
{
}


void JNICALL enableFog(JNIEnv *env, jobject obj,
    jboolean arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.enableFog()"<<std::endl;
//   abort();
}

void JNICALL transform3f(JNIEnv *env, jobject obj,
    jfloatArray arg0,
    jint arg1)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.transform3f()"<<std::endl;
  abort();
}

void JNICALL transformVirt3f(JNIEnv *env, jobject obj,
    jfloatArray arg0,
    jint arg1)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.transformVirt3f()"<<std::endl;
  abort();
}

void JNICALL transform3fInv(JNIEnv *env, jobject obj,
    jfloatArray arg0,
    jint arg1)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.transform3fInv()"<<std::endl;
  abort();
}

void JNICALL vertexArraysTransformAndLock(JNIEnv *env, jobject obj,
    jfloatArray arg0,
    jint arg1)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.vertexArraysTransformAndLock()"<<std::endl;
  abort();
}

void JNICALL vertexArraysUnLock(JNIEnv *env, jobject obj)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.vertexArraysUnLock()"<<std::endl;
  abort();
}

void JNICALL drawBeginSprites(JNIEnv *env, jobject obj,
    jint arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.drawBeginSprites()"<<std::endl;
//   abort();
}

void JNICALL drawPushSprite(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1,
    jfloat arg2,
    jfloat arg3,
    jint arg4,
    jfloat arg5)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.drawPushSprite__FFFFIF()"<<std::endl;
//   abort();
}

void JNICALL drawPushSprite(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1,
    jfloat arg2,
    jfloat arg3,
    jint arg4,
    jfloat arg5,
    jfloat arg6)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.drawPushSprite__FFFFIFF()"<<std::endl;
//   abort();
}

void JNICALL drawPushSprite(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1,
    jfloat arg2,
    jfloat arg3,
    jfloat arg4,
    jfloat arg5,
    jfloat arg6,
    jfloat arg7,
    jfloat arg8)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.drawPushSprite__FFFFFFFFF()"<<std::endl;
//   abort();
}

// void JNICALL prepareStates(JNIEnv *env, jobject obj)
// {
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.prepareStates()"<<std::endl;
//   abort();
// }

// void JNICALL clearStates(JNIEnv *env, jobject obj)
// {
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.clearStates()"<<std::endl;
//   abort();
// }

void JNICALL drawBeginTriangleLists(JNIEnv *env, jobject obj,
    jint arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.drawBeginTriangleLists()"<<std::endl;
//   abort();
}

void JNICALL drawTriangleList(JNIEnv *env, jobject obj,
    jfloatArray arg0,
    jintArray arg1,
    jint arg2,
    jintArray arg3,
    jint arg4)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.drawTriangleList__FIIII()"<<std::endl;
//   abort();
}

void JNICALL drawTriangleList(JNIEnv *env, jobject obj,
    jfloatArray arg0,
    jint arg1,
    jint arg2,
    jint arg3,
    jintArray arg4,
    jint arg5)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.drawTriangleList__FIIIII()"<<std::endl;
//   abort();
}

void JNICALL drawEnd(JNIEnv *env, jobject obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.drawEnd()"<<std::endl;
//   abort();
}

void JNICALL drawBeginLines(JNIEnv *env, jobject obj,
    jint arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.drawBeginLines()"<<std::endl;
//   abort();
}

void JNICALL drawLines(JNIEnv *env, jobject obj,
    jfloatArray arg0,
    jint arg1,
    jfloat arg2,
    jint arg3,
    jint arg4,
    jint arg5)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.drawLines()"<<std::endl;
//   abort();
}

jboolean JNICALL DrawSetMaterial(JNIEnv *env, jobject obj,
    jint arg0,
    jfloat arg1,
    jfloat arg2,
    jfloat arg3,
    jfloat arg4)
{
  return false;
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.DrawSetMaterial()"<<std::endl;
//   abort();
}

jboolean JNICALL DrawSetMaterial0(JNIEnv *env, jobject obj,
    jint arg0)
{
  return false;
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.DrawSetMaterial0()"<<std::endl;
//   abort();
}


void JNICALL DrawTile(JNIEnv *env, jobject obj,
    jfloat origin_x,
    jfloat origin_y,
    jfloat extent_x,
    jfloat extent_y,
    jfloat arg4,
    jint mat_cpp_obj,
    jint color,
    jfloat texcoord_origin_x,
    jfloat texcoord_origin_y,
    jfloat texcoord_extent_x,
    jfloat texcoord_extent_y)
{
  // assert(getCurrentRenderName() == "renderGUI");

  glm::vec2 origin(origin_x, origin_y);
  glm::vec2 extent(extent_x, extent_y);
  glm::vec2 texcoord_origin(texcoord_origin_x, texcoord_origin_y);
  glm::vec2 texcoord_extent(texcoord_extent_x, texcoord_extent_y);


//   LOG_INFO<<"left: "<<left<<", bottom: "<<bottom
//     <<", width: "<<width<<", height: "<<height
//     <<std::endl;
  
//   LOG_INFO << "texcoord_top_y: " << texcoord_top_y << std::endl;
  
//   LOG_INFO << "texcoord_origin: " << texcoord_origin
//            << ", texcoord_extent: " << texcoord_extent << std::endl;

//   abort();

  // auto &material_path = il2_jni::getMaterialPath(mat_cpp_obj);
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Render.DrawTile()"<<std::endl;
//   abort();

  il2ge::renderer::drawTile(origin, extent, texcoord_origin, texcoord_extent,
                            *getMaterial(mat_cpp_obj));
}


} // namespace

#include <_generated/il2_jni/il2.engine.Render_registration>
