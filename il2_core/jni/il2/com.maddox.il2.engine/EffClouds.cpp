#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include <core/objects.h>
#include <core.h>
#include <log.h>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;


namespace
{

#include <_generated/il2_jni/il2.engine.EffClouds_definitions>


struct EffClouds : public core::GObj, public GObjSubClass<EffClouds>
{
  static auto createType() { return GObj::createType(g_java_class_path); }

  EffClouds() : GObj(getStaticType()) {}
};


jint JNICALL Load(JNIEnv *env, jobject obj,
    jint arg0,
    jfloat arg1)
{
  auto object = core::GObj::create<EffClouds>();
  object->onJavaObjectCreate();
  return core::createObjectHandle(object);
}


void JNICALL Destroy(JNIEnv *env, jobject obj,
    jint arg0)
{
  finalizeGObj(arg0);
}


void JNICALL SetType(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.EffClouds.SetType()"<<std::endl;
//   abort();
}

void JNICALL SetHeight(JNIEnv *env, jobject obj,
    jint arg0,
    jfloat arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.EffClouds.SetHeight()"<<std::endl;
//   abort();
}

jboolean JNICALL GetRandomCloudPos(JNIEnv *env, jobject obj,
    jint arg0,
    jfloatArray arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.EffClouds.GetRandomCloudPos()"<<std::endl;
//   abort();
  return false;
}

jfloat JNICALL GetVisibility(JNIEnv *env, jobject obj,
    jint arg0,
    jfloat arg1,
    jfloat arg2,
    jfloat arg3,
    jfloat arg4,
    jfloat arg5,
    jfloat arg6)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.EffClouds.GetVisibility()"<<std::endl;
//   abort();
  return 0;
}

jint JNICALL PreRender(JNIEnv *env, jobject obj,
    jint arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.EffClouds.PreRender()"<<std::endl;
//   abort();
  return 0;
}

void JNICALL Render(JNIEnv *env, jobject obj,
    jint arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.EffClouds.Render()"<<std::endl;
//   abort();
}


} // namespace


#include <_generated/il2_jni/il2.engine.EffClouds_registration>
