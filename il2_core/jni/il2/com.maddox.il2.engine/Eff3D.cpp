/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "gobj_factories.h"
#include <core.h>
#include <core/context.h>
#include <core/scene.h>
#include <core/objects.h>
#include <sfs.h>
#include <java_util.h>
#include <il2ge/effect3d.h>
#include <il2ge/material.h>
#include <render_util/texture_util.h>
#include <render_util/camera_3d.h>
#include <util.h>


#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

#define ENABLE_PRELOAD 1

using namespace std;
using namespace glm;
using namespace il2ge::il2_jni;
using il2ge::Material;
namespace core = il2ge::core;


namespace
{


#include <_generated/il2_jni/il2.engine.Eff3D_definitions>


struct Eff3D : public core::GObj
{
  static auto createType() { return GObj::createType(g_java_class_path); }

  virtual void finish() = 0;
  virtual float timeLife() = 0;
  virtual float timeFinish() = 0;

  virtual void setPos(glm::vec3) {}
  virtual void setRotationDeg(float, float, float) {}

  virtual void render() {}

  virtual void update(float delta, glm::vec2 wind_speed) {}
  virtual bool cull(const render_util::Camera&) { return true; }

  virtual void setIntensity(float) {}
  virtual float getIntensity() { return 1; }

  virtual void setSize(float) {}

  Eff3D(core::GObjType& type) : core::GObj(type) {}
};



struct EffectParameters : public core::FObj
{
  EffectParameters(core::GObjType& type, const std::string& path) : FObj(type, path)
  {
  }

  virtual core::ObjRef<Eff3D> createEffect() = 0;
};


std::string resolveRelativePath(std::string base_dir, std::string path)
{
  return util::resolveRelativePathComponents(base_dir + '/' + path);
}


std::vector<char> readFile(const std::string &path)
{
    vector<char> content;
    if (!sfs::readFile(path, content))
      throw sfs::ReadError(path);

    return content;
}


struct InitParams
{
  string file_name;
  vec3 pos{0};
  vec3 yaw_pitch_roll_deg {0};
  float size = 1;
  std::string material_name;
  core::ObjRef<EffectParameters> params;
};



// Data g_data;

InitParams g_init_params;


// // Data &getData()
// {
  // auto &effects = core::getEffects();
  // if (!effects.m_jni_wrapper_data)
    // effects.m_jni_wrapper_data = std::make_unique<Data>();

  // return *static_cast<Data*>(effects.m_jni_wrapper_data.get());

  // return g_data;
// }

#if 0
string getMaterialPath(const string &parameter_file_path,
                       ParameterFiles &parameter_files)
{
  auto &params = parameter_files.get(parameter_file_path);
  auto &general = params.getSection("General");

  string path;
  general.get_noexcept("MatName", path);

  if (!path.empty())
  {
    return resolveRelativePath(util::getDirFromPath(parameter_file_path), path);
  }
  else
  {
    auto &class_info = params.getSection("ClassInfo");
    auto &based_on = class_info.at("BasedOn");
    auto path = resolveRelativePath(util::getDirFromPath(parameter_file_path), based_on);
    return getMaterialPath(path, parameter_files);
  }
}


const shared_ptr<const Material> &getMaterial(const string &parameter_file_path)
{
  auto path = getMaterialPath(parameter_file_path, getData().parameter_files);

  auto &mat = getData().material_map[path];
  if (!mat)
  {
    auto new_mat = il2ge::loadMaterial(getData().parameter_files, path);

    auto texture_image = core::Effects::createTexture(*new_mat);

    new_mat->getLayers().front().texture = render_util::createTexture(texture_image);
    new_mat->getLayers().front().texture_is_greyscale = (texture_image->numComponents() == 1);

    mat = std::move(new_mat);
  }
  return mat;
}


const Effect3DParameters &getParams(const string &file_name)
{
  auto &params = getData().param_map[file_name];
  if (!params)
  {
    cout<<"getParams: "<<file_name<<endl;

#if ENABLE_PRELOAD
    if (!getData().preload_out.is_open())
    {
      getData().preload_out.open("il2ge_preload_out", ios_base::trunc | ios_base::binary);
    }
    getData().preload_out << file_name << endl;
#endif

    auto &file = getData().parameter_files.get(file_name);
    auto &class_info = file.getSection("ClassInfo");
    auto class_name = class_info.at("ClassName");

    params = il2ge::createEffect3DParameters(class_name);
    assert(params);

    if (class_info.hasKey("BasedOn"))
    {
      auto based_on = class_info.get("BasedOn");
      auto path = util::getDirFromPath(file_name);
      assert(!path.empty());
      path += '/' + based_on;

      auto &base = getParams(path);
      params->set(base);
    }

    params->loaded_from = file_name;
    // params->loaded_from_content = std::string(content.data(), content.size());
    params->applyFrom(file);
  }
  assert(params);
  return *params;
}


class Factory
{
  JNIEnv *m_env = nullptr;
  jclass m_java_class = nullptr;
  jmethodID m_constructor_id = nullptr;
  const Effect3DParameters &m_params;
  std::shared_ptr<const Material> m_material;

public:
  Factory &operator=(const Factory&) = delete;
  Factory(const Factory&) = delete;
  Factory(const Factory&&) = delete;

  Factory(const string &file_name, JNIEnv *env) :
    m_env(env),
    m_params(getParams(file_name))
  {
    auto java_class = m_env->FindClass(m_params.getJavaClassName());
    assert(java_class);
    m_java_class = (jclass) m_env->NewGlobalRef((jobject)java_class);
    m_constructor_id = m_env->GetMethodID(m_java_class, "<init>", "(I)V");
    assert(m_constructor_id);

    m_material = getMaterial(file_name);
  }

  ~Factory()
  {
    m_env->DeleteGlobalRef((jobject)m_java_class);
  }

  jobject createJavaObject(int cpp_obj) const
  {
    return m_env->NewObject(m_java_class, m_constructor_id, cpp_obj);
  }

  unique_ptr<Effect3D> createEffect() const
  {
    auto e = m_params.createEffect();
    e->material = m_material;
    return e;
  }
};




const Factory &getFactory(string name, JNIEnv *env)
{
  auto &f = getData().factory_map[name];
  if (!f)
    f = make_unique<Factory>(name, env);
  return *f;
}


void preload(JNIEnv *env)
{
#if ENABLE_PRELOAD
  if (getData().preload_done)
    return;
  getData().preload_done = true;

  ifstream in("il2ge_preload", ios_base::binary);

  while (in.good())
  {
    string file_name;
    getline(in, file_name);
    if (!file_name.empty())
      getFactory(file_name, env);
  }
#endif
}
#endif


auto getParamsRef(const std::string& path)
{
  auto fobj = core::getFObjRef(path);
  assert(fobj);
  auto params = std::dynamic_pointer_cast<EffectParameters>(fobj); // FIXME
  assert(params);
  return params;
}


Eff3D& getEffect(jint cpp_obj)
{
  auto& obj = getGObj(cpp_obj);
  auto eff = dynamic_cast<Eff3D*>(obj.get());
  assert(eff);
  return *eff;
}


jobject JNICALL New(JNIEnv *env, jobject obj)
{
#if 1
  auto &params = g_init_params;

  // LOG_INFO << "params.file_name: "  << params.file_name << std::endl;

  if (!params.params)
    return 0;

  assert(params.params);

  auto effect = params.params->createEffect();

  effect->setPos(g_init_params.pos);
  effect->setRotationDeg(g_init_params.yaw_pitch_roll_deg.x,
                         g_init_params.yaw_pitch_roll_deg.y,
                         g_init_params.yaw_pitch_roll_deg.z);
  effect->setSize(g_init_params.size);

  auto java_object = createJavaObject(effect);
  assert(java_object);

  return java_object;

#else
  // preload(env);
  assert(!g_init_params.file_name.empty());

  auto &factory = getFactory(g_init_params.file_name, env);

  auto effect = factory.createEffect();
  effect->setPos(g_init_params.pos);
  effect->setYawPitchRollDeg(g_init_params.yaw_pitch_roll_deg);

  auto cpp_obj = core::addEffect(move(effect));

  auto new_obj = factory.createJavaObject(cpp_obj);
  assert(new_obj);

  return new_obj;
#endif
}


jint JNICALL PreRender(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
  //FIXME: bounding box check
  auto& effect = getEffect(cpp_obj);

  effect.update(core::getFrameDelta(), core::getWindSpeed());

  return effect.cull(*core::getCamera()) ? 0 : 1;
//   return import.PreRender(env, obj, arg0);
}


void JNICALL Render(JNIEnv *env, jobject obj,
                    jint cpp_obj)
{
  getEffect(cpp_obj).render();
}


void JNICALL SetPos(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jfloatArray arg1)
{

  const jint pos_num_elements = env->GetArrayLength(arg1);
  assert(pos_num_elements >= 3);

  float pos[3];
  env->GetFloatArrayRegion(arg1, 0, 3, pos);

  getEffect(cpp_obj).setPos(vec3(pos[0], pos[1], pos[2]));
}


void JNICALL SetAnglesATK(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jfloatArray angles)
{
  UNIMPLEMENTED

//   auto& eff = getEffect(cpp_obj);
}


void JNICALL SetXYZATK(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jfloat arg1,
    jfloat arg2,
    jfloat arg3,
    jfloat arg4,
    jfloat arg5,
    jfloat arg6)
{
  auto& eff = getEffect(cpp_obj);

  eff.setPos(vec3(arg1, arg2, arg3));
  eff.setRotationDeg(arg4, arg5, arg6);
}

void JNICALL Pause(JNIEnv *env, jobject obj,
    jint arg0,
    jboolean arg1)
{
  // core::getEffect(arg0)->pause(arg1);
}

jboolean JNICALL IsPaused(JNIEnv *env, jobject obj,
    jint arg0)
{
  return false;
  // return core::getEffect(arg0)->isPaused();
}

void JNICALL SetIntesity(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jfloat intensity)
{
  getEffect(cpp_obj).setIntensity(intensity);
}

jfloat JNICALL GetIntesity(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
  return getEffect(cpp_obj).getIntensity();
}

void JNICALL Finish(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
  getEffect(cpp_obj).finish();
}

jfloat JNICALL TimeLife(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
  return getEffect(cpp_obj).timeLife();
}

jfloat JNICALL TimeFinish(JNIEnv *env, jobject obj,
    jint cpp_obj)
{
  return getEffect(cpp_obj).timeFinish();
}

jboolean JNICALL IsTimeReal(JNIEnv *env, jobject obj,
    jint arg0)
{
//   cout<<"IsTimeReal: "<<(int)import.IsTimeReal(env, obj, arg0)<<endl;
  return false;
}

void JNICALL initSetProcessTime(JNIEnv *env, jobject obj,
    jfloat arg0)
{
//   cout<<"initSetProcessTime: "<<arg0<<endl;
//   import.initSetProcessTime(env, obj, arg0);
}

void JNICALL initSetTypeTimer(JNIEnv *env, jobject obj,
    jboolean arg0)
{
//   import.initSetTypeTimer(env, obj, arg0);
}

void JNICALL initSetBoundBox(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1,
    jfloat arg2,
    jfloat arg3,
    jfloat arg4,
    jfloat arg5)
{
//   import.initSetBoundBox(env, obj, arg0, arg1, arg2, arg3, arg4, arg5);
}

void JNICALL initSetPos(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1,
    jfloat arg2)
{
  g_init_params.pos = vec3(arg0, arg1, arg2);
}

void JNICALL initSetSize(JNIEnv *env, jobject obj,
    jfloat size)
{
  g_init_params.size = size;
}

void JNICALL initSetAnglesATK(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1,
    jfloat arg2)
{
  g_init_params.yaw_pitch_roll_deg = vec3{arg0, arg1, arg2};
}

void JNICALL initSetSubType(JNIEnv *env, jobject obj,
    jint arg0)
{
//   cout<<"initSetSubType: "<<arg0<<endl;
}

void JNICALL initSetMaterialName(JNIEnv *env, jobject obj,
    jstring arg0)
{
  jsize len = env->GetStringUTFLength(arg0);
  char buf[len+1];
  env->GetStringUTFRegion(arg0, 0, len, buf);

  g_init_params.material_name = buf;
}

void JNICALL initSetParamFileName(JNIEnv *env, jobject obj,
    jstring arg0)
{
  jsize len = env->GetStringUTFLength(arg0);
  char buf[len+1];
  env->GetStringUTFRegion(arg0, 0, len, buf);

  std::string filename = buf;

  if (g_init_params.file_name != filename)
  {
    // LOG_INFO << "setting file name: " << filename << std::endl;
    g_init_params.file_name = filename;

    try
    {
      g_init_params.params = getParamsRef(g_init_params.file_name);
      assert(g_init_params.params);
    }
    catch (std::exception& e)
    {
      g_init_params.params.reset();
      LOG_ERROR << "Failed to load effect parameters: " << filename << std::endl;
      LOG_ERROR << "Reason: " << e.what() << std::endl;
    }
  }
}


///////////////////////////////////////////////////////////


struct ParamsType : public core::GObjType
{
  core::GObjType m_effect_type;

  ParamsType(std::string cpp_class_name, std::string effect_java_class_name);
};


struct DummyParameters : public EffectParameters
{
  ParamsType& m_params_type;
  core::ObjRef<il2ge::Effect3DParameters> m_params;
  core::ObjRef<Material> m_material;

  DummyParameters(ParamsType& type, const std::string& path) :
    EffectParameters(type, path),
    m_params_type(type)
  {
    m_params = il2ge::createEffect3DParameters(m_params_type.getCppClassName());
    assert(m_params);

    auto visitor = [&] (auto& p)
    {
      m_params->applyFrom(p);
    };

    visit(core::getParameterFiles(), path, visitor, true);

    auto material_path = util::getDirFromPath(path) + "/" + m_params->material_name;
    auto material = core::getFObjRef(material_path);
    assert(material);

    m_material = std::dynamic_pointer_cast<Material>(material);
    assert(m_material);
  }

  core::ObjRef<Eff3D> createEffect() override;
};


struct DummyEffect : public Eff3D
{
  // core::ObjRef<DummyEffectParameters> m_parameters;
  core::ObjRef<il2ge::Effect3DParameters> m_params;
  std::shared_ptr<il2ge::Effect3D> m_effect;

  DummyEffect(core::GObjType& type,
              const core::ObjRef<il2ge::Effect3DParameters>& params,
              core::ObjRef<Material> material) :
      Eff3D(type),
      m_params(params)
  {
    m_effect = m_params->createEffect();
    m_effect->material = material;
  }

  ~DummyEffect()
  {
  }

  void finish() override
  {
    m_effect->finish();
  }

  float timeLife() override
  {
    return m_params->FinishTime;
  }

  float timeFinish() override
  {
    return m_params->LiveTime;
  }

  void setPos(glm::vec3 pos) override
  {
    m_effect->setPos(pos);
  }

  void setSize(float size) override
  {
    m_effect->setSize(size);
  }

  void setRotationDeg(float yaw, float pitch, float roll) override
  {
    m_effect->setRotationDeg(yaw, pitch, roll);
  }

  void setIntensity(float intensity) override
  {
    return m_effect->setIntensity(intensity);
  }

  float getIntensity() override
  {
    return m_effect->getIntensity();
  }

  void render() override
  {
    m_effect->addToRenderList(core::getContext().getRenderer(), *core::getCamera());
  }

  void update(float delta, glm::vec2 wind_speed) override
  {
    m_effect->update(delta, wind_speed);
  }

  bool cull(const render_util::Camera& camera) override
  {
    return m_effect->cull(camera);
  }

};

core::ObjRef<Eff3D> DummyParameters::createEffect()
{
  auto material = m_material;
  if (!g_init_params.material_name.empty())
  {
    auto material_path = util::getDirFromPath(getPath()) + "/"
                         + g_init_params.material_name;
    auto fobj = core::getFObjRef(material_path);
    assert(fobj);
    material = std::dynamic_pointer_cast<Material>(material);
    assert(material);
  }

  return core::GObj::create<DummyEffect>(m_params_type.m_effect_type,
                                         m_params, material);
}


ParamsType::ParamsType(std::string cpp_class_name, std::string effect_java_class_name) :
  core::GObjType(cpp_class_name,
                  "com/maddox/il2/engine/FObj",
                  [this] (const std::string& path)
                  {
                    return core::GObj::create<DummyParameters>(*this, path);
                  }),
  m_effect_type(effect_java_class_name)
{
}


std::vector<std::unique_ptr<ParamsType>> g_params_types;


} // namespace


void il2ge::il2_jni::registerGObjEffectParameters()
{
  g_params_types.push_back(std::make_unique<ParamsType>("TParticlesSystemParams",
                                                        "com/maddox/il2/engine/EffParticles"));
  g_params_types.push_back(std::make_unique<ParamsType>("TSmokeSpiralParams",
                                                        "com/maddox/il2/engine/EffSmokeSpiral"));
  g_params_types.push_back(std::make_unique<ParamsType>("TSmokeTrailParams",
                                                        "com/maddox/il2/engine/EffSmokeTrail"));
  g_params_types.push_back(std::make_unique<ParamsType>("TAnimatedSpriteParams",
                                                        "com/maddox/il2/engine/EffAnimatedSprite"));

  for (auto& type : g_params_types)
    core::GObjType::add(*type);
}


#include <_generated/il2_jni/il2.engine.Eff3D_registration>
