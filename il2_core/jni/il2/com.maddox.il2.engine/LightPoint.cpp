/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include <core/objects.h>
#include <core.h>


namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/il2.engine.LightPoint_definitions>


struct LightPoint : public core::GObj, public GObjSubClass<LightPoint>
{
  static auto createType() { return GObj::createType(g_java_class_path); }

  LightPoint() : core::GObj(getStaticType()) {}
};



LightPoint* getObject(jint cpp_obj)
{
  return castGObj<LightPoint>(cpp_obj);
}


void JNICALL setPos(JNIEnv *env, jobject obj,
		jint arg0,
		jfloat arg1,
		jfloat arg2,
		jfloat arg3)
{
// 	return import.setPos(env, obj, arg0, arg1, arg2, arg3);
}

void JNICALL setColor(JNIEnv *env, jobject obj,
		jint arg0,
		jfloat arg1,
		jfloat arg2,
		jfloat arg3)
{
// 	return import.setColor(env, obj, arg0, arg1, arg2, arg3);
}

void JNICALL setEmit(JNIEnv *env, jobject obj,
		jint arg0,
		jfloat arg1,
		jfloat arg2)
{
// 	return import.setEmit(env, obj, arg0, arg1, arg2);
}

void JNICALL getPos(JNIEnv *env, jobject obj,
		jint arg0,
		jfloatArray arg1)
{
// 	return import.getPos(env, obj, arg0, arg1);
}

void JNICALL getColor(JNIEnv *env, jobject obj,
		jint arg0,
		jfloatArray arg1)
{
// 	return import.getColor(env, obj, arg0, arg1);
}

void JNICALL getEmit(JNIEnv *env, jobject obj,
		jint arg0,
		jfloatArray arg1)
{
// 	return import.getEmit(env, obj, arg0, arg1);
}

void JNICALL setOffset(JNIEnv *env, jobject obj,
		jfloat arg0,
		jfloat arg1,
		jfloat arg2)
{
// 	return import.setOffset(env, obj, arg0, arg1, arg2);
}

void JNICALL addToRender(JNIEnv *env, jobject obj,
		jint arg0)
{
// 	return import.addToRender(env, obj, arg0);
}

void JNICALL clearRender(JNIEnv *env, jobject obj)
{
// 	return import.clearRender(env, obj);
}

void JNICALL Finalize(JNIEnv *env, jobject obj,
		jint arg0)
{
  finalizeGObj(arg0);
}

jint JNICALL New(JNIEnv *env, jobject obj)
{
  // this is called from the java object's constructor
  auto object = core::GObj::create<LightPoint>();
  object->onJavaObjectCreate();
  return core::createObjectHandle(object);
}


} // namespace


#include <_generated/il2_jni/il2.engine.LightPoint_registration>
