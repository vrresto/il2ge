/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include <core.h>
#include <render_util/camera_3d.h>
#include <render_util/gl_binding/gl_functions.h>

#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

using namespace std;
namespace core = il2ge::core;
using namespace il2ge::il2_jni;
using namespace render_util::gl_binding;


namespace
{

#include <_generated/il2_jni/il2.engine.Camera_definitions>


void JNICALL SetCameraPos(JNIEnv *env, jobject obj, jdoubleArray arg0)
{
  if (core::getCameraMode() == core::IL2_CAMERA_MODE_3D)
  {
    const jint pos_num_elements = env->GetArrayLength(arg0);
    assert(pos_num_elements > 6);
    double pos[pos_num_elements];
    env->GetDoubleArrayRegion(arg0, 0, pos_num_elements, pos);

    core::getCamera()->setTransform(pos[0], pos[1], pos[2], pos[3] + 360, pos[4], pos[5]);

    gl::MatrixMode(GL_MODELVIEW);
    gl::LoadMatrixf(glm::value_ptr(core::getCamera()->getWorld2ViewMatrix()));
  }
}


void JNICALL SetOrtho2D(JNIEnv *env, jobject obj,
    jfloat left,
    jfloat right,
    jfloat bottom,
    jfloat top,
    jfloat ZNear,
    jfloat ZFar)
{
  core::setCameraMode(core::IL2_CAMERA_MODE_2D);

  auto proj = glm::ortho(left, right, bottom, top, ZNear, ZFar);

  gl::MatrixMode(GL_PROJECTION);
  gl::LoadMatrixf(glm::value_ptr(proj));
  gl::MatrixMode(GL_MODELVIEW);
}


void JNICALL SetOrtho(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1,
    jfloat arg2)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Camera.SetOrtho()"<<std::endl;
  abort();
}


void JNICALL SetZOrder(JNIEnv *env, jobject obj,
    jfloat arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Camera.SetZOrder()"<<std::endl;
//   abort();
}


jboolean JNICALL SetViewportCrop(JNIEnv *env, jobject obj,
                            jfloat arg0,
                            jint fb_w,
                            jint fb_h,
                            jint vp_x,
                            jint vp_y,
                            jint vp_w,
                            jint vp_h,
                            jint arg7,
                            jint arg8,
                            jint arg9,
                            jint arg10)
{
  core::getCamera()->setViewportSize(vp_w, vp_h);
  gl::Viewport(vp_x, vp_y, vp_w, vp_h);

  return true;
}

void JNICALL SetFOV(JNIEnv *env, jobject obj, jfloat arg0, jfloat arg1, jfloat arg2)
{
  core::setCameraMode(core::IL2_CAMERA_MODE_3D);
  core::getCamera()->setProjection(arg0, arg1, arg2);

  gl::MatrixMode(GL_PROJECTION);
  gl::LoadMatrixf(glm::value_ptr(core::getCamera()->getProjectionMatrixFar()));
  gl::MatrixMode(GL_MODELVIEW);
}

jboolean JNICALL isSphereVisibleF(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1,
    jfloat arg2,
    jfloat arg3)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Camera.isSphereVisibleF()"<<std::endl;
//   abort();
  return false;
}

jboolean JNICALL isSphereVisibleD(JNIEnv *env, jobject obj,
    jdouble arg0,
    jdouble arg1,
    jdouble arg2,
    jfloat arg3)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Camera.isSphereVisibleD()"<<std::endl;
//   abort();
  return false;
}

void JNICALL SetTargetSpeed(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1,
    jfloat arg2)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Camera.SetTargetSpeed()"<<std::endl;
//   abort();
}

void JNICALL GetUniformDistParams(JNIEnv *env, jobject obj,
    jfloatArray arg0)
{
  std::array<jfloat, 2> values = { 0, 0 };

  env->SetFloatArrayRegion(arg0, 0, values.size(), values.data());
}

void JNICALL ActivateWorldMode(JNIEnv *env, jobject obj,
    jint arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Camera.ActivateWorldMode()"<<std::endl;
//   abort();
}

void JNICALL DeactivateWorldMode(JNIEnv *env, jobject obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.Camera.DeactivateWorldMode()"<<std::endl;
//   abort();
}


void JNICALL GetVirtOrigin(JNIEnv *env, jobject obj,
    jfloatArray arg0)
{
  std::array<jfloat, 2> values;
  values.at(0) = 0;
  values.at(1) = 0;

  env->SetFloatArrayRegion(arg0, 0, values.size(), values.data());
}


} // namespace


#include <_generated/il2_jni/il2.engine.Camera_registration>
