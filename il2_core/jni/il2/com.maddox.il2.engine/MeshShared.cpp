#include "il2_jni_p.h"
#include "mesh.h"
#include "meta_class_registrators.h"

namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/il2.engine.MeshShared_definitions>


void JNICALL RenderArray(JNIEnv *env, jobject obj,
    jboolean arg0,
    jint arg1,
    jintArray arg2,
    jdoubleArray arg3,
    jfloatArray arg4)
{
  static_assert(sizeof(glm::vec3) == sizeof(jfloat) * 3);
  static_assert(sizeof(glm::dvec3) == sizeof(jdouble) * 3);

  auto count = arg1;

  std::vector<jint> objs(count);
  std::vector<glm::dvec3> positions(count);
  std::vector<glm::vec3> angles(count);

  env->GetIntArrayRegion(arg2, 0, count, objs.data());
  env->GetDoubleArrayRegion(arg3, 0, count * 3, reinterpret_cast<jdouble*>(positions.data()));
  env->GetFloatArrayRegion(arg4, 0, count * 3, reinterpret_cast<jfloat*>(angles.data()));

  renderMeshes(objs.data(), positions.data(), angles.data(), count);
}


void JNICALL RenderArrayShadowProjective(JNIEnv *env, jobject obj,
    jint arg0,
    jintArray arg1,
    jdoubleArray arg2,
    jfloatArray arg3)
{
//   return import.RenderArrayShadowProjective(env, obj, arg0, arg1, arg2, arg3);
}

void JNICALL RenderArrayShadowVolume(JNIEnv *env, jobject obj,
    jint arg0,
    jintArray arg1,
    jdoubleArray arg2,
    jfloatArray arg3)
{
//   return import.RenderArrayShadowVolume(env, obj, arg0, arg1, arg2, arg3);
}

void JNICALL RenderArrayShadowVolumeHQ(JNIEnv *env, jobject obj,
    jint arg0,
    jintArray arg1,
    jdoubleArray arg2,
    jfloatArray arg3)
{
//   return import.RenderArrayShadowVolumeHQ(env, obj, arg0, arg1, arg2, arg3);
}


} // namespace


#include <_generated/il2_jni/il2.engine.MeshShared_registration>
