#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include <log.h>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/il2.engine.BmpUtils_definitions>

jint JNICALL squareSizeBMP8Pal(JNIEnv *env, jobject obj,
    jstring arg0)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.BmpUtils.squareSizeBMP8Pal()"<<std::endl;
  abort();
//   return true;
}

jint JNICALL RectSizeBMP8Pal(JNIEnv *env, jobject obj,
    jstring arg0)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.BmpUtils.RectSizeBMP8Pal()"<<std::endl;
  abort();
}

jboolean JNICALL BMP8Scale05(JNIEnv *env, jobject obj,
    jstring arg0,
    jstring arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.BmpUtils.BMP8Scale05()"<<std::endl;
//   abort();
  return true;
}

jboolean JNICALL BMP8PalToTGA3(JNIEnv *env, jobject obj,
    jstring arg0,
    jstring arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.BmpUtils.BMP8PalToTGA3()"<<std::endl;
//   abort();
  return true;
}

jboolean JNICALL BMP8Pal192x256ToTGA3(JNIEnv *env, jobject obj,
    jstring arg0,
    jstring arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.BmpUtils.BMP8Pal192x256ToTGA3()"<<std::endl;
//   abort();
  return true;
}

jboolean JNICALL BMP8PalToTGA4(JNIEnv *env, jobject obj,
    jstring arg0,
    jstring arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.BmpUtils.BMP8PalToTGA4()"<<std::endl;
//   abort();
    return true;
}

jboolean JNICALL BMP8PalTo2TGA4(JNIEnv *env, jobject obj,
    jstring arg0,
    jstring arg1,
    jstring arg2)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.BmpUtils.BMP8PalTo2TGA4()"<<std::endl;
//   abort();
  return true;
}

jboolean JNICALL BMP8PalTo4TGA4(JNIEnv *env, jobject obj,
    jstring arg0,
    jstring arg1,
    jstring arg2)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.BmpUtils.BMP8PalTo4TGA4()"<<std::endl;
//   abort();
  return true;
}


} // namespace


#include <_generated/il2_jni/il2.engine.BmpUtils_registration>
