#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "gobj_factories.h"
#include "mat.h"
#include <sfs.h>
#include <core.h>
#include <core/objects.h>
#include <core/context.h>
#include <java_util.h>
#include <il2ge/image_loader.h>
#include <il2ge/renderer.h>
#include <render_util/texture_util.h>
#include <render_util/gl_binding/gl_functions.h>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;
using namespace render_util::gl_binding;

namespace
{


#include <_generated/il2_jni/il2.engine.Mat_definitions>

// using byte = unsigned char;
using byte = jbyte;

constexpr jint BIT(int shift)
{
  return 1 << shift;
}


constexpr jint WRAPU = BIT(0);
constexpr jint WRAPV = BIT(1);
constexpr jint WRAPUV = BIT(0) | BIT(1);
constexpr jint MINLINEAR = BIT(2);
constexpr jint MAGLINEAR = BIT(3);
constexpr jint MIPMAP = BIT(4);
constexpr jint BLEND = BIT(5);
constexpr jint BLENDADD = BIT(6);
constexpr jint TESTA = BIT(7);
constexpr jint TRANSPBORDER = BIT(8);
constexpr jint TESTZ = BIT(9);
constexpr jint TESTZEQUAL = BIT(10);
constexpr jint NOWRITEZ = BIT(11);
constexpr jint IMAGERESIDENT = BIT(13);
constexpr jint MODULATE = BIT(14);
constexpr jint NOTEXTURE = BIT(15);
constexpr jint ANIMATEPALETTE = BIT(16);
constexpr jint ANIMATESKIPPEDFRAMES = BIT(17);
constexpr jint NODEGRADATION = BIT(22);
constexpr jint DEPTHOFFSET = BIT(12);
constexpr jint DOUBLESIDE = BIT(18);
constexpr jint SHOULDSORT = BIT(19);
constexpr jint DROPSHADOW = BIT(20);
constexpr jint NATIVETIMER = BIT(21);

constexpr jint TEXTURE_NOIMAGE = 0;
constexpr jint TEXTURE_ALPHA = 1;
constexpr jint TEXTURE_ALPHALM = 2;
constexpr jint TEXTURE_RGB = 3;
constexpr jint TEXTURE_RGBA = 4;
constexpr jint TEXTURE_PALETTE = 5;
constexpr jint TEXTURE_MIPMAP = 0x10000;
constexpr jint TEXTURE_TRANSPBORDERA = 0x20000;
constexpr jint TEXTURE_NODEGRADATION = 0x80000;
constexpr jint TEXTURE_NOCOMPRESS16BIT = 0x100000;
constexpr jint TEXTURE_NOCOMPRESSARB = 0x200000;
constexpr jint TEXTURE_COMPRESSMAJORALPHA = 0x400000;
constexpr jint TEXTURE_GRAYSCALEMODE = 0x800000;
constexpr jint TEXTURE_ANIMATED = 256;
constexpr jint TEXTURE_ANIMATEDPROCEDURE = 768;
constexpr jint TEXTURE_ANIMATEDLIST = 1280;

constexpr jshort fEnable = 0;

constexpr jint fFlags = 0;
constexpr jint fType = 1;
constexpr jint fFrames = 2;

constexpr byte fVisDistanceNear = 0;
constexpr byte fVisDistanceFar = 1;
constexpr byte fSX = 2;
constexpr byte fSY = 3;
constexpr byte fFrame = 4;
constexpr byte fTime = 5;
constexpr byte fFrameInc = 6;
constexpr byte fScaleRed = 7;
constexpr byte fScaleGreen = 8;
constexpr byte fScaleBlue = 9;
constexpr byte fScaleAlpha = 10;
constexpr byte fUAdd = 11;
constexpr byte fVAdd = 12;
constexpr byte fUMul = 13;
constexpr byte fVMul = 14;
constexpr byte fAmbient = 20;
constexpr byte fDiffuse = 21;
constexpr byte fSpecular = 22;
constexpr byte fSpecularPow = 23;
constexpr byte fShine = 24;
constexpr byte Ka = 20;
constexpr byte Kd = 21;
constexpr byte Ks = 22;
constexpr byte pow = 23;
constexpr byte Ke = 24;

constexpr jchar fTextureName = 0;


std::shared_ptr<il2ge::Material::TextureAndSize> loadTexture(std::string texture_path)
{
  LOG_INFO << "Loading texture: " << texture_path << std::endl;

  // FIXME HACK
  auto dir = util::getDirFromPath(texture_path);
  auto name = util::basename(texture_path);
  if (util::isPrefix("skin1", name))
  {
    // texture_path = dir + "/summer/" + name;
  }

  std::vector<char> data;
  if (!sfs::readFile(texture_path, data))
  {
    LOG_ERROR << "Failed to read " << texture_path << ": SFS error" << std::endl;
    return nullptr;
  }

  auto img = il2ge::loadImageFromMemory(data, texture_path.c_str());
  if (!img)
  {
    LOG_ERROR << "texture path: " << texture_path << std::endl;
    return nullptr;
  }
  else
  {
    // FIXME HACK
    if (img->numComponents() == 1)
    {
      auto rgba_image = std::make_shared<render_util::GenericImage>(img->size(), 4);
      for (int y = 0; y < rgba_image->h(); y++)
      {
        for (int x = 0; x < rgba_image->w(); x++)
        {
          rgba_image->at(x,y,0) = 255;
          rgba_image->at(x,y,1) = 255;
          rgba_image->at(x,y,2) = 255;
          rgba_image->at(x,y,3) = img->get(x,y,0);
        }
      }
      img = rgba_image;
    }

    auto ret = std::make_shared<il2ge::Material::TextureAndSize>(texture_path);

    ret->size = img->getSize();
    ret->texture = render_util::createTexture(img);

    return ret;
  }
};


auto getTexture(std::string path)
{
  return core::getContext().getTextures().get(path, loadTexture);
}


struct Mat : public core::FObj, public GObjSubClass<Mat>, public il2ge::Material
{
  // static auto createType()
  // {
  //   auto factory = [] (const std::string& path)
  //   {
  //     return new Mat(path);
  //   };
  //   return GObj::createType("TMaterial", g_java_class_path, factory);
  // }

  static auto createType()
  {
    return GObj::createType<Mat>("TMaterial", g_java_class_path);
  }

  // std::string m_path;

  int m_layer =  -1;

  Mat(const std::string& path) :
    FObj(getStaticType(), path),
    Material(core::getParameterFiles(), path, getTexture)
  {
  }

  Mat(const Mat& other) : FObj(getStaticType(), other.getPath()), Material(other)
  {
  }

  int getLayersCount()
  {
    return 1;
  }

  void setLayer(int l)
  {
    assert(l >= 0);
    assert(l < getLayersCount());
    m_layer = l;
  }

  glm::ivec2 getTextureSize()
  {
    assert(getRendererData().layers.at(m_layer).texture_and_size);
    return getRendererData().layers.at(m_layer).texture_and_size->size;
  }

  jboolean getBool(jshort)
  {
    UNIMPLEMENTED
  }

  jint getInt(jint)
  {
    UNIMPLEMENTED
  }

  jfloat getFloat(jbyte param)
  {
    switch (param)
    {
      case fSX:
        return getTextureSize().x;
      case fSY:
        return getTextureSize().y;
      case fScaleRed:
      case fScaleGreen:
      case fScaleBlue:
      case fScaleAlpha:
        return 1;
      default:
        return 0;
    }
  }

  const std::string& getString(jchar param)
  {
    switch (param)
    {
      case fTextureName:
        return getLayers().at(m_layer).texture_path;
      default:
        assert(0);
        abort();
    }
  }

  void setString(jchar param, const std::string& value)
  {
    switch (param)
    {
      case fTextureName:
        getLayers().at(m_layer).texture_path = value;
        break;
      default:
        LOG_INFO << "param: " << (int)param << std::endl;
        LOG_INFO << "value: " << value << std::endl;
        assert(0);
        abort();
    }

    refreshRendererData(getTexture);
  }

  // std::string getJavaClassName() override
  // {
    // return "com/maddox/il2/engine/Mat";
  // }

//   void *createJavaObject() override
//   {
//     auto env = il2ge::getJNIEnv();
// 
//     auto java_class = env->FindClass("com/maddox/il2/engine/Mat");
//     assert(java_class);
//     auto constructor_id = env->GetMethodID(java_class, "<init>", "(I)V");
//     assert(constructor_id);
// 
//     auto java_obj = env->NewObject(java_class, constructor_id, cpp_obj);
//     assert(java_obj);
// 
//     return java_obj;
//   }

  core::GObjRef clone() override
  {
    return core::GObj::create<Mat>(*this);
  }
};


Mat* getMat(int cpp_obj)
{
  return castGObj<Mat>(cpp_obj);
}


jboolean JNICALL tgaInfo(JNIEnv *env, jobject obj,
    jstring arg0,
    jintArray arg1)
{
//   return import.tgaInfo(env, obj, arg0, arg1);
//   UNIMPLEMENTED
  return false;
}

jintArray JNICALL LoadTextureAsArrayFromTga(JNIEnv *env, jobject obj,
    jstring arg0,
    jint flags,
    jint chunk_width,
    jint chunk_height,
    jint border,
    jintArray array_size_out)
{
//   jsize len = env->GetStringUTFLength(arg0);
//   char buf[len+1];
//   env->GetStringUTFRegion(arg0, 0, len, buf);
//   LOG_INFO << "name: " << std::string(buf) << std::endl;

  auto width = 2;
  auto height = 2;

  jint array[width][height];

  for (int x = 0; x < width; x++)
  {
    for (int y = 0; y < height; y++)
    {
      GLuint texture = 0;
      gl::GenTextures(1, &texture);
      assert(texture != 0);
      array[x][y] = texture;
    }
  }

  jint array_size_out_data[2] = { width, height };
  env->SetIntArrayRegion(array_size_out, 0, 2, array_size_out_data);

  auto array_size = width * height;
  auto ret = env->NewIntArray(array_size);
  env->SetIntArrayRegion(ret, 0, array_size, &array[0][0]);
  return ret;
}

void JNICALL enableDeleteTextureID(JNIEnv *env, jobject obj,
    jboolean arg0)
{
//   return import.enableDeleteTextureID(env, obj, arg0);
}

void JNICALL setGrayScaleLoading(JNIEnv *env, jobject obj,
    jboolean arg0)
{
//   return import.setGrayScaleLoading(env, obj, arg0);
}

jboolean JNICALL isGrayScaleLoading(JNIEnv *env, jobject obj)
{
//   return import.isGrayScaleLoading(env, obj);
  UNIMPLEMENTED
}

jboolean JNICALL IsValidLayer(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  assert(arg1 >= 0);
  return arg1 < getMat(arg0)->getLayersCount();
}

void JNICALL SetLayer(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  getMat(arg0)->setLayer(arg1);
}

jboolean JNICALL Get(JNIEnv *env, jobject obj,
    jint arg0,
    jshort arg1)
{
  return getMat(arg0)->getBool(arg1);
}

jint JNICALL Get(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  return getMat(arg0)->getInt(arg1);
}

jfloat JNICALL Get(JNIEnv *env, jobject obj,
    jint arg0,
    jbyte arg1)
{
  return getMat(arg0)->getFloat(arg1);
}

jstring JNICALL Get(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jchar param)
{
  return env->NewStringUTF(getMat(cpp_obj)->getString(param).c_str());
}

jboolean JNICALL Set(JNIEnv *env, jobject obj,
    jint arg0,
    jshort arg1,
    jboolean arg2)
{
//   if (getMat(arg0))
  {
    return 0;
  }

//   return import.Set__ISZ(env, obj, arg0, arg1, arg2);
}

jint JNICALL Set(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2)
{
//   if (getMat(arg0))
  {
    return 0;
  }

//   return import.Set__III(env, obj, arg0, arg1, arg2);
}

jfloat JNICALL Set(JNIEnv *env, jobject obj,
    jint arg0,
    jbyte arg1,
    jfloat arg2)
{
//   if (getMat(arg0))
  {
    return 0;
  }

//   return import.Set__IBF(env, obj, arg0, arg1, arg2);
}

jint JNICALL Set(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jchar param,
    jstring value)
{
  jsize len = env->GetStringUTFLength(value);
  char buf[len+1];
  env->GetStringUTFRegion(value, 0, len, buf);

  getMat(cpp_obj)->setString(param, buf);

  return 0;
}

jint JNICALL PreRender(JNIEnv *env, jobject obj,
    jint arg0,
    jfloat arg1,
    jfloat arg2,
    jfloat arg3,
    jfloat arg4)
{
//   if (getMat(arg0))
    UNIMPLEMENTED

//   return import.PreRender__IFFFF(env, obj, arg0, arg1, arg2, arg3, arg4);
}

jint JNICALL PreRender(JNIEnv *env, jobject obj,
    jint arg0)
{
//   if (getMat(arg0))
//     UNIMPLEMENTED
  return 1;

//   return import.PreRender__I(env, obj, arg0);
}

void JNICALL UpdateImage(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jint arg3,
    jbyteArray arg4)
{
  if (getMat(arg0))
    UNIMPLEMENTED

//   return import.UpdateImage(env, obj, arg0, arg1, arg2, arg3, arg4);
}

void JNICALL GrabFromScreen(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jint arg3,
    jint arg4)
{
//   if (getMat(arg0))
    UNIMPLEMENTED

//   return import.GrabFromScreen(env, obj, arg0, arg1, arg2, arg3, arg4);
}


} // namespace


namespace il2ge::il2_jni
{


void* getDummyMaterial()
{
  auto fobj = core::getFObjRef("3DO/TEXTURES/clear.mat");
  assert(fobj);

  auto java_object = createJavaObject(fobj);
  assert(java_object);
  return java_object;
}


// int createMaterial(std::string path)
// {
//   auto mat = std::make_unique<Mat>();
//   mat->m_path = path;
//   mat->m_material = core::getMaterial(path);
//   auto cpp_obj = core::addGObj(std::move(mat));
//
//   return cpp_obj;
// }

// const std::string &getMaterialPath(int cpp_obj)
// {
//   auto mat = getMat(cpp_obj);
//   assert(!mat->getPath().empty());
//   return mat->getPath();
// }


void registerGObjMat()
{
  core::GObjType::add<Mat>();
}


const core::ObjRef<il2ge::Material> getMaterial(jint cpp_obj)
{
  auto& gobj = getGObj(cpp_obj);
  auto mat = std::dynamic_pointer_cast<il2ge::Material>(gobj);
  assert(mat);
  return mat;
}


} // namespace il2ge::il2_jni


#include <_generated/il2_jni/il2.engine.Mat_registration>
