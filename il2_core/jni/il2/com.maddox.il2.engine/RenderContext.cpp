/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include <core.h>

#include <array>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;


namespace
{

#include <_generated/il2_jni/il2.engine.RenderContext_definitions>

// Interface import;

void JNICALL Begin(JNIEnv *env, jobject obj,
		jint arg0,
		jint arg1)
{
// 	import.Begin(env, obj, arg0, arg1);
  // core::initJavaClasses();
}

void JNICALL End(JNIEnv *env, jobject obj)
{
// 	return import.End(env, obj);
}

void JNICALL SetDrawMesh(JNIEnv *env, jobject obj,
    jboolean arg0)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.RenderContext.SetDrawMesh()"<<std::endl;
  abort();
}

jint JNICALL TexGetFlags(JNIEnv *env, jobject obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.RenderContext.TexGetFlags()"<<std::endl;
//   abort();
  return 0;
}

void JNICALL SetPolygonOffset(JNIEnv *env, jobject obj,
    jfloat arg0,
    jfloat arg1)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.RenderContext.SetPolygonOffset()"<<std::endl;
//   abort();
}

void JNICALL GetPolygonOffset(JNIEnv *env, jobject obj,
    jfloatArray arg0)
{
  std::array<jfloat, 2> values;
  values.at(0) = 0;
  values.at(1) = 0;

  env->SetFloatArrayRegion(arg0, 0, values.size(), values.data());
}


} // namespace

#include <_generated/il2_jni/il2.engine.RenderContext_registration>
