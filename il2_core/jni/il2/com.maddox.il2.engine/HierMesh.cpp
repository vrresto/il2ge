#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "mat.h"
#include "mesh.h"
#include <core.h>
#include <core/objects.h>
#include <il2ge/mesh/mesh_base.h>
#include <render_util/camera_3d.h>
#include <log.h>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/il2.engine.HierMesh_definitions>


void applyRotation(const glm::vec3 &yaw_pitch_roll, glm::mat4 &mat)
{
  mat = glm::rotate(mat, glm::radians(yaw_pitch_roll.x), glm::vec3(0, 0, 1));
  mat = glm::rotate(mat, glm::radians(yaw_pitch_roll.y), glm::vec3(0, 1, 0));
  mat = glm::rotate(mat, glm::radians(yaw_pitch_roll.z), glm::vec3(1, 0, 0));
}


void matrixToArray(const glm::mat4 &mat, std::array<double, 16> &array)
{
  size_t index = 0;
  for (int col = 0; col < 4; col++)
  {
    for (int row = 0; row < 4; row++)
    {
      array[index] = mat[row][col];
      index++;
    }
  }
}


struct HierMesh : public core::GObj, public GObjSubClass<HierMesh>
{
  static auto createType() { return GObj::createType(g_java_class_path); }

  std::unique_ptr<il2ge::mesh::HierMeshBase> m_mesh;
  glm::dvec3 m_pos {};
  glm::vec3 m_yaw_pitch_roll {};
  int m_current_chunk = -1;

  HierMesh() : core::GObj(getStaticType()) {}

  const std::string &getHookName(int index)
  {
    return m_mesh->getHookName(index);
  }

  const glm::mat4 getHookMatrix(int index)
  {
    return m_mesh->getHookMatrix(index);
  }

  std::string getChunkName()
  {
    assert(m_current_chunk >= 0);

    if (m_current_chunk < 0)
      return {};
    else
      return m_mesh->getNodeName(m_current_chunk);
  }

  glm::mat4 getChunkTransform()
  {
    assert(m_current_chunk >= 0);

    if (m_current_chunk < 0)
      return glm::mat4(1);
    else
      return m_mesh->getNodeTransform(m_current_chunk);
  }

  void setChunkRotation(const glm::vec3 &yaw_pitch_roll)
  {
    assert(m_current_chunk >= 0);

    if (m_current_chunk < 0)
      return;

    glm::mat4 rotation(1);
    applyRotation(yaw_pitch_roll, rotation);

    m_mesh->setNodeRotation(m_current_chunk, rotation);
  }

  void setChunkTransform(const glm::vec3 &pos, const glm::vec3 &yaw_pitch_roll)
  {
    assert(m_current_chunk >= 0);

    if (m_current_chunk < 0)
      return;

    glm::mat4 rotation(1);
    applyRotation(yaw_pitch_roll, rotation);

    m_mesh->setNodeTransform(m_current_chunk, pos, rotation);
  }

  std::unique_ptr<il2ge::mesh::MeshInstance> getChunk()
  {
    assert(m_current_chunk >= 0);
    return m_mesh->getNodeMesh(m_current_chunk);
  }

  int findMaterialInChunk(const std::string& name, int chunk)
  {
    return m_mesh->findMaterial(name);
  }

  void replaceMaterial(const std::string& name,
                       const core::ObjRef<il2ge::Material>& mat)
  {
    m_mesh->replaceMaterial(name, mat);
  }

  void render()
  {
    glm::mat4 rotation(1);
    applyRotation(m_yaw_pitch_roll, rotation);

    auto model_world = glm::translate(glm::dmat4(1.0), m_pos);
    model_world = model_world * glm::dmat4(rotation);

    auto model_view = core::getCamera()->getWorld2ViewMatrixD() * model_world;

    m_mesh->draw(model_view,
                 glm::distance(glm::vec3(m_pos), core::getCamera()->getPos()),
                 core::getRenderer());
  }
};



HierMesh* getMesh(int cpp_obj)
{
  return castGObj<HierMesh>(cpp_obj);
}


void JNICALL GetChunkCurVisBoundBox(JNIEnv *env, jobject obj,
    jint arg0,
    jfloatArray arg1)
{
  UNIMPLEMENTED
//   return import.GetChunkCurVisBoundBox(env, obj, arg0, arg1);
}

jint JNICALL Load(JNIEnv *env, jobject obj,
    jstring arg0)
{
  jsize len = env->GetStringUTFLength(arg0);
  char buf[len+1];
  env->GetStringUTFRegion(arg0, 0, len, buf);

  auto mesh = core::GObj::create<HierMesh>();

  mesh->m_mesh = core::loadHierMesh(buf);

  mesh->onJavaObjectCreate();

  return core::createObjectHandle(mesh);
}

jint JNICALL LoadSubtree(JNIEnv *env, jobject obj,
    jint arg0)
{
  UNIMPLEMENTED
//   return import.LoadSubtree(env, obj, arg0);
}

jint JNICALL LoadSubtreeLoc(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1)
{
  UNIMPLEMENTED
//   return import.LoadSubtreeLoc(env, obj, arg0, arg1);
}

jintArray JNICALL HideChunksInSubtrees(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1)
{
  auto mesh = getMesh(arg0);

  jsize len = env->GetStringUTFLength(arg1);
  char buf[len+1];
  env->GetStringUTFRegion(arg1, 0, len, buf);

  std::vector<jint> hidden_indices;

  auto node_index = mesh->m_mesh->findNode(buf);
  if (node_index >= 0)
  {
    mesh->m_mesh->getSubTree(node_index, true, hidden_indices);

    auto ret = env->NewIntArray(hidden_indices.size());
    env->SetIntArrayRegion(ret, 0, hidden_indices.size(), hidden_indices.data());

    return ret;
  }
  else
  {
    return nullptr;
  }
}

jintArray JNICALL GetChunksInSubtrees(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1)
{
  auto mesh = getMesh(arg0);

  jsize len = env->GetStringUTFLength(arg1);
  char buf[len+1];
  env->GetStringUTFRegion(arg1, 0, len, buf);

  std::vector<jint> indices;

  auto node_index = mesh->m_mesh->findNode(buf);
  if (node_index >= 0)
    mesh->m_mesh->getSubTree(node_index, false, indices);

  auto ret = env->NewIntArray(indices.size());
  env->SetIntArrayRegion(ret, 0, indices.size(), indices.data());

  return ret;
}

jintArray JNICALL GetChunksInSubtreesSpec(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1)
{
  UNIMPLEMENTED
//   return import.GetChunksInSubtreesSpec(env, obj, arg0, arg1);
}

void JNICALL SetPosXYZATK(JNIEnv *env, jobject obj,
    jint arg0,
    jdouble arg1,
    jdouble arg2,
    jdouble arg3,
    jfloat arg4,
    jfloat arg5,
    jfloat arg6)
{
  auto mesh = getMesh(arg0);

  mesh->m_pos = glm::dvec3(arg1, arg2, arg3);
  mesh->m_yaw_pitch_roll = glm::vec3(-arg4, -arg5, arg6);

//   return import.SetPosXYZATK(env, obj, arg0, arg1, arg2, arg3, arg4, arg5, arg6);
}

jint JNICALL PreRender(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 1;
//   return import.PreRender(env, obj, arg0);
}

void JNICALL Render(JNIEnv *env, jobject obj,
    jint arg0)
{
  getMesh(arg0)->render();
//   return import.Render(env, obj, arg0);
}

void JNICALL RenderShadowProjective(JNIEnv *env, jobject obj,
    jint arg0)
{
//   return import.RenderShadowProjective(env, obj, arg0);
}

void JNICALL RenderShadowVolume(JNIEnv *env, jobject obj,
    jint arg0)
{
//   return import.RenderShadowVolume(env, obj, arg0);
}

void JNICALL RenderShadowVolumeHQ(JNIEnv *env, jobject obj,
    jint arg0)
{
//   return import.RenderShadowVolumeHQ(env, obj, arg0);
}

jint JNICALL DetectCollision(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jint arg2,
    jdoubleArray arg3)
{
  return 0;
//   return import.DetectCollision(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL DetectCollisionMesh(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jint arg2,
    jdoubleArray arg3)
{
  return 0;
//   return import.DetectCollisionMesh(env, obj, arg0, arg1, arg2, arg3);
}

jfloat JNICALL DetectCollisionLine(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2,
    jdoubleArray arg3)
{
  return -1;
//   return import.DetectCollisionLine(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL DetectCollisionLineMulti(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2,
    jdoubleArray arg3)
{
  UNIMPLEMENTED
//   return import.DetectCollisionLineMulti(env, obj, arg0, arg1, arg2, arg3);
}

jfloat JNICALL DetectCollisionQuad(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2)
{
  UNIMPLEMENTED
//   return import.DetectCollisionQuad(env, obj, arg0, arg1, arg2);
}

jint JNICALL DetectCollisionQuadMultiH(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2)
{
  UNIMPLEMENTED
//   return import.DetectCollisionQuadMultiH(env, obj, arg0, arg1, arg2);
}

jint JNICALL DetectCollisionPoint(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2)
{
  UNIMPLEMENTED
//   return import.DetectCollisionPoint(env, obj, arg0, arg1, arg2);
}

jfloat JNICALL DetectCollisionPlane(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2)
{
  UNIMPLEMENTED
//   return import.DetectCollisionPlane(env, obj, arg0, arg1, arg2);
}

jfloat JNICALL VisibilityR(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 0;
//   return import.VisibilityR(env, obj, arg0);
}

jfloat JNICALL CollisionR(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 0;
//   return import.CollisionR(env, obj, arg0);
}

jfloat JNICALL GetUniformMaxDist(JNIEnv *env, jobject obj,
    jint arg0)
{
  UNIMPLEMENTED
//   return import.GetUniformMaxDist(env, obj, arg0);
}

jint JNICALL Chunks(JNIEnv *env, jobject obj,
    jint arg0)
{
  return getMesh(arg0)->m_mesh->getNodeCount();
}

void JNICALL SetCurChunk(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  getMesh(arg0)->m_current_chunk = arg1;

//   return import.SetCurChunk(env, obj, arg0, arg1);
}

jint JNICALL SetCurChunkByName(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1)
{
  auto mesh = getMesh(arg0);

  jsize len = env->GetStringUTFLength(arg1);
  char buf[len+1];
  env->GetStringUTFRegion(arg1, 0, len, buf);

  mesh->m_current_chunk = mesh->m_mesh->findNode(buf);

  if (mesh->m_current_chunk < 0)
  {
    LOG_ERROR << "Chunk " << std::string(buf) << " not found." << std::endl;
    abort();
//     env->ThrowNew(env->FindClass("java/lang/Exception"), "Chunk not found.");
  }

  return mesh->m_current_chunk;
}

jint JNICALL ChunkFind(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1)
{
  jsize len = env->GetStringUTFLength(arg1);
  char buf[len+1];
  env->GetStringUTFRegion(arg1, 0, len, buf);

  auto index = getMesh(arg0)->m_mesh->findNode(buf);
  if (index >= 0)
  {
    return index;
  }
  else
  {
    env->ThrowNew(env->FindClass("java/lang/Exception"), "Chunk not found.");
    return -1;
  }

//   return import.ChunkFind(env, obj, arg0, arg1);
}

jint JNICALL ChunkFindCheck(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1)
{
  jsize len = env->GetStringUTFLength(arg1);
  char buf[len+1];
  env->GetStringUTFRegion(arg1, 0, len, buf);

  return getMesh(arg0)->m_mesh->findNode(buf);

//   return import.ChunkFindCheck(env, obj, arg0, arg1);
}

jstring JNICALL ChunkName(JNIEnv *env, jobject obj,
    jint arg0)
{
  auto name = getMesh(arg0)->getChunkName();
  return env->NewStringUTF(name.c_str());

//   return import.ChunkName(env, obj, arg0);
}

jfloat JNICALL GetChunkVisibilityR(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 0;
//   return import.GetChunkVisibilityR(env, obj, arg0);
}

jint JNICALL GetChunkVisibility(JNIEnv *env, jobject obj,
    jint arg0)
{
  auto mesh = getMesh(arg0);

  assert(mesh->m_current_chunk >= 0);

  if (mesh->m_current_chunk >= 0)
    return mesh->m_mesh->isNodeVisible(mesh->m_current_chunk);
  else
    return true;

//   return import.GetChunkVisibility(env, obj, arg0);
}

void JNICALL SetChunkVisibility(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  auto mesh = getMesh(arg0);

  assert(mesh->m_current_chunk >= 0);

  if (mesh->m_current_chunk >= 0)
    mesh->m_mesh->setNodeVisible(mesh->m_current_chunk, arg1 != 0);

//   return import.SetChunkVisibility(env, obj, arg0, arg1);
}

void JNICALL SetChunkAngles(JNIEnv *env, jobject obj,
    jint arg0,
    jfloatArray arg1)
{
    const auto rotation_num_elements = env->GetArrayLength(arg1);
    assert(rotation_num_elements == 3);
    float rotation[rotation_num_elements];
    env->GetFloatArrayRegion(arg1, 0, rotation_num_elements, rotation);

    getMesh(arg0)->setChunkRotation(glm::vec3(rotation[0], -rotation[1], -rotation[2]));

//   return import.SetChunkAngles(env, obj, arg0, arg1);
}

void JNICALL SetChunkLocate(JNIEnv *env, jobject obj,
    jint arg0,
    jfloatArray arg1,
    jfloatArray arg2)
{
    const auto pos_num_elements = env->GetArrayLength(arg1);
    assert(pos_num_elements == 3);
    float pos[pos_num_elements];
    env->GetFloatArrayRegion(arg1, 0, pos_num_elements, pos);

    const auto rotation_num_elements = env->GetArrayLength(arg2);
    assert(rotation_num_elements == 3);
    float rotation[rotation_num_elements];
    env->GetFloatArrayRegion(arg2, 0, rotation_num_elements, rotation);

    getMesh(arg0)->setChunkTransform(glm::vec3(pos[0], pos[1], pos[2]),
                                     glm::vec3(rotation[0], -rotation[1], -rotation[2]));

//   return import.SetChunkLocate(env, obj, arg0, arg1, arg2);
}

jint JNICALL Frames(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 1;
//   return import.Frames(env, obj, arg0);
}

void JNICALL SetFrame(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
//   return import.SetFrame__II(env, obj, arg0, arg1);
}

void JNICALL SetFrame(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jfloat arg3)
{
//   return import.SetFrame__IIIF(env, obj, arg0, arg1, arg2, arg3);
}

void JNICALL SetFrameFromRange(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jfloat arg3)
{
//   return import.SetFrameFromRange(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL Hooks(JNIEnv *env, jobject obj,
    jint arg0)
{
  // number of hooks?
//   return 0;
  UNIMPLEMENTED
//   return import.Hooks(env, obj, arg0);
}

jint JNICALL HookFind(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1)
{
  jsize len = env->GetStringUTFLength(arg1);
  char buf[len+1];
  env->GetStringUTFRegion(arg1, 0, len, buf);

  return getMesh(arg0)->m_mesh->findHook(buf);

//   return import.HookFind(env, obj, arg0, arg1);
}

jstring JNICALL HookName(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  return env->NewStringUTF(getMesh(arg0)->getHookName(arg1).c_str());

//   return import.HookName(env, obj, arg0, arg1);
}

void JNICALL HookMatrix(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jdoubleArray arg2)
{
  std::array<double, 16> data;
  matrixToArray(getMesh(arg0)->getHookMatrix(arg1), data);

  env->SetDoubleArrayRegion(arg2, 0, data.size(), data.data());

//   return import.HookMatrix(env, obj, arg0, arg1, arg2);
}

void JNICALL GetChunkLTM(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1)
{
  std::array<double, 16> data;
  matrixToArray(getMesh(arg0)->getChunkTransform(), data);

  env->SetDoubleArrayRegion(arg1, 0, data.size(), data.data());

//   return import.GetChunkLTM(env, obj, arg0, arg1);
}

jint JNICALL ChunkByHookNamed(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  return getMesh(arg0)->m_mesh->getHookNode(arg1);

//   return import.ChunkByHookNamed(env, obj, arg0, arg1);
}

jint JNICALL HookFaceCollisFind(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jintArray arg2,
    jdoubleArray arg3)
{
  UNIMPLEMENTED
//   return import.HookFaceCollisFind(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL HookFaceFind(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jintArray arg2,
    jdoubleArray arg3)
{
  UNIMPLEMENTED
//   return import.HookFaceFind(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL HookChunkFaceFind(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2)
{
  UNIMPLEMENTED
//   return import.HookChunkFaceFind(env, obj, arg0, arg1, arg2);
}

void JNICALL HookFaceMatrix(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2,
    jdoubleArray arg3)
{
  UNIMPLEMENTED
//   return import.HookFaceMatrix(env, obj, arg0, arg1, arg2, arg3);
}

jint JNICALL Materials(JNIEnv *env, jobject obj,
    jint arg0)
{
  return 0;
//   return import.Materials(env, obj, arg0);
}

jint JNICALL MaterialFind(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jstring name,
    jint arg2,
    jint arg3)
{
  auto mesh = getMesh(cpp_obj);

  jsize len = env->GetStringUTFLength(name);
  char buf[len+1];
  env->GetStringUTFRegion(name, 0, len, buf);

  return mesh->m_mesh->findMaterial(buf);
}

jint JNICALL MaterialFindInChunk(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jstring name,
    jint chunk)
{
  jsize len = env->GetStringUTFLength(name);
  char buf[len+1];
  env->GetStringUTFRegion(name, 0, len, buf);

  return getMesh(cpp_obj)->findMaterialInChunk(buf, chunk);
}

jobject JNICALL Material(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jint index)
{
  // returns a material object

  auto mat = getMesh(cpp_obj)->m_mesh->getMaterial(index);
  assert(mat);

  auto gobj = std::dynamic_pointer_cast<core::GObj>(mat);
  assert(gobj);

  return createJavaObject(gobj);
}

void JNICALL MaterialReplace(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jint arg2)
{
  UNIMPLEMENTED
}

void JNICALL MaterialReplace(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jstring arg2)
{
  UNIMPLEMENTED
}

void JNICALL MaterialReplace(JNIEnv *env, jobject obj,
    jint cpp_obj,
    jstring name,
    jint mat_cpp_obj)
{
  jsize len = env->GetStringUTFLength(name);
  char buf[len+1];
  env->GetStringUTFRegion(name, 0, len, buf);

  getMesh(cpp_obj)->replaceMaterial(buf, getMaterial(mat_cpp_obj));
}

void JNICALL MaterialReplace(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1,
    jstring arg2)
{
  UNIMPLEMENTED
}

jfloat JNICALL PoseCRC(JNIEnv *env, jobject obj,
    jint arg0)
{
  UNIMPLEMENTED
//   return import.PoseCRC(env, obj, arg0);
}

jint JNICALL isVisualRayHit(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdouble arg2,
    jdouble arg3,
    jdoubleArray arg4)
{
  return 0;
//   return import.isVisualRayHit(env, obj, arg0, arg1, arg2, arg3, arg4);
}

jint JNICALL ApplyDecal(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jfloat arg2,
    jfloat arg3,
    jint arg4,
    jdoubleArray arg5,
    jfloatArray arg6,
    jint arg7,
    jintArray arg8,
    jint arg9)
{
  UNIMPLEMENTED
//   return import.ApplyDecal(env, obj, arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
}

jint JNICALL GrabDecalsFromChunk(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  UNIMPLEMENTED
//   return import.GrabDecalsFromChunk(env, obj, arg0, arg1);
}

jint JNICALL ApplyGrabbedDecalsToChunk(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  UNIMPLEMENTED
//   return import.ApplyGrabbedDecalsToChunk(env, obj, arg0, arg1);
}

void JNICALL RenderChunkMirror(JNIEnv *env, jobject obj,
    jint arg0,
    jdoubleArray arg1,
    jdoubleArray arg2,
    jdoubleArray arg3)
{
//   return import.RenderChunkMirror(env, obj, arg0, arg1, arg2, arg3);
}

void JNICALL RenderShadowPairs(JNIEnv *env, jobject obj,
    jint arg0,
    jintArray arg1)
{
//   return import.RenderShadowPairs(env, obj, arg0, arg1);
}


} // namespace


#include <_generated/il2_jni/il2.engine.HierMesh_registration>

namespace il2ge::il2_jni
{


std::unique_ptr<il2ge::mesh::MeshInstance> getHierMeshChunk(jint cpp_obj)
{
  return getMesh(cpp_obj)->getChunk();
}


} // namespace il2ge::il2_jni
