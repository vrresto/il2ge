#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include "gobj_factories.h"
// #include "fobj.h"
#include <core.h>
#include <java_util.h>
#include <render_util/state.h>
#include <text_renderer/text_renderer.h>
#include <render_util/gl_binding/gl_functions.h>
#include <log.h>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;
using namespace render_util::gl_binding;

namespace
{

#include <_generated/il2_jni/il2.engine.TTFont_definitions>

//FIXME
static constexpr int CHAR_WIDTH = 9;
static constexpr int CHAR_HEIGHT = 15;

  
  
struct TTFont : public core::FObj, public GObjSubClass<TTFont>
{
  static auto createType()
  {
    return GObj::createType<TTFont>("TTTextureFont", g_java_class_path);
  }

  TTFont(const std::string& path) : FObj(getStaticType(), path) {}
};


jobject JNICALL get(JNIEnv *env, jobject obj,
    jstring arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.get()"<<std::endl;
//   abort();
//   return nullptr;

  jsize len = env->GetStringUTFLength(arg0);
  char buf[len+1];
  env->GetStringUTFRegion(arg0, 0, len, buf);

  std::string name(buf);
  assert(!name.empty());

  auto path = "font/" + name + ".font";

  auto object = core::getFObjRef(path);
  assert(castGObj<TTFont>(*object));

  return createJavaObject(object);

#if 0
  //FIXME HACK
  static jobject dummy = nullptr;

  if (!dummy)
  {
    auto font = std::make_unique<TTFont>();
    auto cpp_obj = core::addGObj(std::move(font));

    auto env = il2ge::java::getEnv();

    auto java_class = env->FindClass("com/maddox/il2/engine/TTFont");
    assert(java_class);
    auto constructor_id = env->GetMethodID(java_class, "<init>", "(I)V");
    assert(constructor_id);

    auto java_obj = env->NewObject(java_class, constructor_id, cpp_obj);
    assert(java_obj);

    dummy = env->NewGlobalRef(java_obj);
  }

  return dummy;
#endif
}

void JNICALL setContextWidth(JNIEnv *env, jobject obj,
    jint arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.setContextWidth()"<<std::endl;
//   abort();
}

void JNICALL reloadAllOnResize(JNIEnv *env, jobject obj)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.reloadAllOnResize()"<<std::endl;
//   abort();
}

void JNICALL Output(JNIEnv *env, jobject obj, jint arg0,
    jint color,
    jfloat pos_x,
    jfloat pos_y,
    jfloat arg4,
    jstring text,
    jint arg6,
    jint arg7)
{
  auto chars = env->GetStringUTFChars(text, nullptr);

  int viewport[4];
  gl::GetIntegerv(GL_VIEWPORT, viewport);
  float viewport_h = viewport[3];

  static_assert(sizeof(jint) == 4);
  auto color_unsigned = *reinterpret_cast<uint32_t*>(&color);

  glm::vec4 color_rgba(0xFF & ((0x000000FF & color_unsigned) >> 0),
                      0xFF & ((0x0000FF00 & color_unsigned) >> 8),
                      0xFF & ((0x00FF0000 & color_unsigned) >> 16),
                      0xFF & ((0xFF000000 & color_unsigned) >> 24));

  color_rgba /= glm::vec4(255);

  render_util::StateModifier mod;
  mod.setDefaults();
  mod.enableBlend(true);

  auto &r = core::getTextRenderer();
  r.SetColor(color_rgba.r, color_rgba.g, color_rgba.b, color_rgba.a);
  r.DrawText(chars, pos_x, viewport_h - pos_y - CHAR_HEIGHT);

  env->ReleaseStringUTFChars(text, chars);
}

void JNICALL Output(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jfloat arg2,
    jfloat arg3,
    jfloat arg4,
    jcharArray arg5,
    jint arg6,
    jint arg7)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.Output__IIFFFCII()"<<std::endl;
//   abort();
}

void JNICALL OutputClip(JNIEnv *env, jobject obj, jint arg0,
    jint color,
    jfloat pos_x,
    jfloat pos_y,
    jfloat arg4,
    jstring text,
    jint arg6,
    jint arg7,
    jfloat clip_x,
    jfloat clip_y,
    jfloat clip_dx,
    jfloat clip_dy)
{
  auto chars = env->GetStringUTFChars(text, nullptr);

  int viewport[4];
  gl::GetIntegerv(GL_VIEWPORT, viewport);
  float viewport_h = viewport[3];

  render_util::StateModifier mod;
  mod.setDefaults();
  mod.enableBlend(true);

  static_assert(sizeof(jint) == 4);
  auto color_unsigned = *reinterpret_cast<uint32_t*>(&color);

  glm::vec4 color_rgba(0xFF & ((0x000000FF & color_unsigned) >> 0),
                      0xFF & ((0x0000FF00 & color_unsigned) >> 8),
                      0xFF & ((0x00FF0000 & color_unsigned) >> 16),
                      0xFF & ((0xFF000000 & color_unsigned) >> 24));

  color_rgba /= glm::vec4(255);

  auto &r = core::getTextRenderer();
  r.SetColor(color_rgba.r, color_rgba.g, color_rgba.b, color_rgba.a);
  r.DrawText(chars, pos_x, viewport_h - pos_y - CHAR_HEIGHT);

  env->ReleaseStringUTFChars(text, chars);
}

void JNICALL OutputClipArr(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1,
    jfloat arg2,
    jfloat arg3,
    jfloat arg4,
    jcharArray arg5,
    jint arg6,
    jint arg7,
    jfloat arg8,
    jfloat arg9,
    jfloat arg10,
    jfloat arg11)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.OutputClipArr()"<<std::endl;
//   abort();
}

void JNICALL Transform(JNIEnv *env, jobject obj,
    jint arg0,
    jobject arg1,
    jint arg2,
    jstring arg3,
    jint arg4,
    jint arg5)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.Transform__ILjava_lang_Object_2ILjava_lang_String_2II()"<<std::endl;
//   abort();
}

void JNICALL Transform(JNIEnv *env, jobject obj,
    jint arg0,
    jobject arg1,
    jint arg2,
    jcharArray arg3,
    jint arg4,
    jint arg5)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.Transform__ILjava_lang_Object_2ICII()"<<std::endl;
//   abort();
}

jfloat JNICALL Width(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1,
    jint arg2,
    jint arg3)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.Width__ILjava_lang_String_2II()"<<std::endl;
//   abort();
  
  
  
  return env->GetStringLength(arg1) * CHAR_WIDTH;
}

jfloat JNICALL Width(JNIEnv *env, jobject obj,
    jint arg0,
    jcharArray arg1,
    jint arg2,
    jint arg3)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.Width__ICII()"<<std::endl;
//   abort();
//   return 10;
  return env->GetArrayLength(arg1) * CHAR_WIDTH;
}

jint JNICALL Len(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1,
    jint arg2,
    jint arg3,
    jfloat arg4,
    jint arg5)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.Len__ILjava_lang_String_2IIFI()"<<std::endl;
//   abort();
//   return 10;
  return env->GetStringLength(arg1) * CHAR_WIDTH;
}

jint JNICALL Len(JNIEnv *env, jobject obj,
    jint arg0,
    jcharArray arg1,
    jint arg2,
    jint arg3,
    jfloat arg4,
    jint arg5)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.Len__ICIIFI()"<<std::endl;
//   abort();
//   return 10;
  return env->GetArrayLength(arg1) * CHAR_WIDTH;
}

jint JNICALL LenEnd(JNIEnv *env, jobject obj,
    jint arg0,
    jstring arg1,
    jint arg2,
    jint arg3,
    jfloat arg4,
    jint arg5)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.LenEnd__ILjava_lang_String_2IIFI()"<<std::endl;
  abort();
//   return 10;
}

jint JNICALL LenEnd(JNIEnv *env, jobject obj,
    jint arg0,
    jcharArray arg1,
    jint arg2,
    jint arg3,
    jfloat arg4,
    jint arg5)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.LenEnd__ICIIFI()"<<std::endl;
  abort();
//   return 10;
}

jint JNICALL Height(JNIEnv *env, jobject obj,
    jint arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.Height()"<<std::endl;
//   abort();
  return CHAR_HEIGHT;
}

jint JNICALL Descender(JNIEnv *env, jobject obj,
    jint arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.Descender()"<<std::endl;
//   abort();
  return 0;
}

void JNICALL ReloadOnResize(JNIEnv *env, jobject obj,
    jint arg0)
{
//   LOG_ERROR<<"UNIMPLEMENTED: il2.engine.TTFont.ReloadOnResize()"<<std::endl;
//   abort();
}


} // namespace


void il2ge::il2_jni::registerGObjTTFont()
{
  core::GObjType::add<TTFont>();
}


#include <_generated/il2_jni/il2.engine.TTFont_registration>
