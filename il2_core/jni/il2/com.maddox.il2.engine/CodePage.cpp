#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include <log.h>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/il2.engine.CodePage_definitions>

jobject JNICALL get(JNIEnv *env, jobject obj,
    jstring arg0)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CodePage.get()"<<std::endl;
  abort();
}

jobject JNICALL getApp(JNIEnv *env, jobject obj)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CodePage.getApp()"<<std::endl;
  abort();
}

void JNICALL setApp(JNIEnv *env, jobject obj,
    jstring arg0)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CodePage.setApp()"<<std::endl;
  abort();
}

jobject JNICALL getSystemAnsi(JNIEnv *env, jobject obj)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CodePage.getSystemAnsi()"<<std::endl;
  abort();
}

jobject JNICALL getSystemOem(JNIEnv *env, jobject obj)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CodePage.getSystemOem()"<<std::endl;
  abort();
}

jint JNICALL Translate(JNIEnv *env, jobject obj,
    jint arg0,
    jint arg1)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CodePage.Translate()"<<std::endl;
  abort();
}

jint JNICALL Size(JNIEnv *env, jobject obj,
    jint arg0)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CodePage.Size()"<<std::endl;
  abort();
}

jstring JNICALL NameCP(JNIEnv *env, jobject obj,
    jint arg0)
{
  LOG_ERROR<<"UNIMPLEMENTED: il2.engine.CodePage.NameCP()"<<std::endl;
  abort();
}


} // namespace


#include <_generated/il2_jni/il2.engine.CodePage_registration>
