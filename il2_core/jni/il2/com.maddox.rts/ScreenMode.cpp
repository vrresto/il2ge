#include "il2_jni_p.h"
#include "meta_class_registrators.h"
#include <core.h>
#include <log.h>

#include <array>

namespace core = il2ge::core;
using namespace il2ge::il2_jni;

namespace
{

#include <_generated/il2_jni/rts.ScreenMode_definitions>


std::array<jint, 3> getConfiguredScreenMode()
{
  auto screen_mode = core::getConfiguredScreenMode();

  std::array<jint, 3> values =
  {
    screen_mode.x,
    screen_mode.y,
    screen_mode.z,
  };

  return values;
}


jboolean JNICALL EGet(JNIEnv *env, jobject obj,
    jint arg0,
    jintArray arg1)
{
  auto values = getConfiguredScreenMode();
  env->SetIntArrayRegion(arg1, 0, values.size(), values.data());

  return arg0 == 0;
}

jboolean JNICALL ESet(JNIEnv *env, jobject obj,
    jintArray arg0)
{
  std::array<jint, 3> values;
  env->GetIntArrayRegion(arg0, 0, values.size(), values.data());

  auto configured = getConfiguredScreenMode();
  assert(configured == values);

  return true;
}

void JNICALL EGetCurrent(JNIEnv *env, jobject obj,
    jintArray arg0)
{
  auto values = getConfiguredScreenMode();
  env->SetIntArrayRegion(arg0, 0, values.size(), values.data());
}


} // namespace


#include <_generated/il2_jni/rts.ScreenMode_registration>
