/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CORE_H
#define CORE_H


#include <core/objects.h>
#include <core/forwards.h>
#include <il2ge/effect3d.h>
#include <il2ge/parameter_file.h>
#include <render_util/shader.h>
#include <render_util/terrain_util.h>

#include <glm/glm.hpp>


class TextRenderer;


namespace render_util
{
  class CirrusClouds;
  class Camera3D;
}


namespace il2ge::mesh
{
  class MeshBase;
  class HierMeshBase;
  class MeshInstance;
}


namespace il2ge::renderer
{
  class Renderer;
}


namespace il2ge::core
{
  enum Il2RenderPhase
  {
    #define IL2_DECLARE_RENDER_PHASE(name) IL2_##name,
    #include "il2_render_phase.inc"
    #undef IL2_DECLARE_RENDER_PHASE
    IL2_RENDER_PHASE_MAX
  };

  enum Il2CameraMode
  {
    IL2_CAMERA_MODE_UNKNOWN,
    IL2_CAMERA_MODE_2D,
    IL2_CAMERA_MODE_3D
  };

  struct Il2RenderState
  {
    Il2CameraMode camera_mode = IL2_CAMERA_MODE_UNKNOWN;
    Il2RenderPhase render_phase = IL2_RENDER_PHASE_UNKNOWN;
  };

  [[ noreturn ]] void fatalError(const std::string &message);

  bool isFMBActive();
  bool isMapLoaded();

  Il2CameraMode getCameraMode();
  void setCameraMode(Il2CameraMode);

  void getRenderState(Il2RenderState *state);

  void updateUniforms(render_util::ShaderProgram&);

  void loadMap(const char *path, void*);
  void unloadMap();

  const glm::vec3 &getSunDir();
  void setSunDir(const glm::vec3 &dir);

  render_util::Camera3D *getCamera();
  render_util::TerrainBase &getTerrain();
  render_util::ShaderParameters getShaderParameters();
  render_util::CirrusClouds *getCirrusClouds();
  TextRenderer &getTextRenderer();

  void setTime(uint64_t);
  void setWindSpeed(const glm::vec2&);

  glm::vec2 getWindSpeed();
  float getFrameDelta();

  render_util::ImageGreyScale::ConstPtr getPixelMapH();

  void onPrePreRenders();
  void onPostPreRenders();
  void onPostRenders();
  void onRenderCockpitBegin();
  void onRenderCockpitEnd();
  void onRenderSpritesFogBegin();
  void onRenderSpritesFogEnd();

  void showMenu(bool);
  bool isMenuShown();
  void handleKey(int key, bool ctrl, bool alt, bool shift);

  void init();
  void initJavaClasses();
  bool checkHardwareShaders();

  il2ge::renderer::Renderer &getRenderer();

  void prepareStates();
  void drawTerrain(bool is_mirror);
  void flushRenderList();

  // void preloadEffects();

  std::unique_ptr<il2ge::mesh::HierMeshBase> loadHierMesh(std::string path);
  std::unique_ptr<il2ge::mesh::MeshInstance> createMeshInstance(std::string path);
  std::shared_ptr<const il2ge::mesh::MeshBase> getMeshFromGObj(int cpp_object);

  // int addGObj(std::unique_ptr<GObj>);
  // GObj *getGObj(int id);
  // bool handleGObjFinalize(int id);

  // use GObj::unref() to release the reference
  GObjRef getFObjRef(const std::string& path);

  const std::shared_ptr<il2ge::Material>& getMaterial(GObj*);

  // std::unique_ptr<il2ge::MaterialWrapper> createMaterialWrapper(std::string path);

  // std::unique_ptr<il2ge::mesh::MeshInstance>
  // createMeshInstance(std::shared_ptr<const il2ge::mesh::MeshBase> mesh);

  void mapClearDebugFrameData();
  void mapRenderDebug();

  const il2ge::ParameterFile &getParameterFile(const std::string &path);
  il2ge::ParameterFiles& getParameterFiles();

  glm::ivec3 getConfiguredScreenMode();
}

#endif
