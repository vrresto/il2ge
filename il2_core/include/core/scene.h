/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CORE_SCENE_H
#define CORE_SCENE_H

#include <scene_base.h>
#include <core/forwards.h>
#include <core/objects.h>
#include <render_util/camera.h>
#include <render_util/parameter_wrapper.h>
#include <render_util/cirrus_clouds.h>
#include <render_util/elevation_map.h>

#include <memory>
#include <vector>
#include <cassert>


namespace render_util
{
  class TextDisplay;
  class Atmosphere;
  class CirrusClouds;
  class TerrainBase;
}


namespace il2ge::core
{
  class Scene : public SceneBase
  {
    std::unique_ptr<Map> map;
    std::unique_ptr<render_util::TextDisplay> m_overlay;
    std::unique_ptr<SkyBox> m_skybox;

  public:
    Scene(TextRenderer&);
    ~Scene();

    void unloadMap();
    void loadMap(const char *path, ProgressReporter*);
    void update(float delta, const glm::vec2 &wind_speed);
    void updateUniforms(render_util::ShaderProgram& program);
    render_util::TerrainBase &getTerrain();

    render_util::CirrusClouds *getCirrusClouds();

    render_util::ImageGreyScale::ConstPtr getPixelMapH();

    bool isMapLoaded() { return map != nullptr; }

    const render_util::ElevationMap& getHeightMap();

    render_util::TextDisplay& getOverlay()
    {
      assert(m_overlay);
      return *m_overlay;
    }

    void drawSkyBox(const render_util::Camera&);
  };
};

#endif
