/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_CORE_WRAPPER_CORE_CONTEXT_H
#define IL2GE_CORE_WRAPPER_CORE_CONTEXT_H

#include <core/forwards.h>
#include <il2ge/object_map.h>
#include <il2ge/material.h>

#include <memory>
#include <cassert>


class TextRenderer;


namespace il2ge::renderer
{
  class Renderer;
}


namespace il2ge::core
{


class Context
{
  std::unique_ptr<TextRenderer> m_text_renderer;
  std::unique_ptr<il2ge::renderer::Renderer> m_renderer;
  il2ge::AutoAddingWeakMap<il2ge::Material::TextureAndSize> m_textures;
  std::unique_ptr<Meshes> m_meshes;
  std::unique_ptr<FObjects> m_fobjects;
  std::unique_ptr<Scene> m_scene;
  std::unique_ptr<ObjectHandleManager> m_object_handle_manager;

  static Context *s_current;

public:
  Context();
  ~Context();

  Scene& getScene()
  {
    assert(m_scene);
    return *m_scene;
  }

  FObjects& getFObjects()
  {
    assert(m_fobjects);
    return *m_fobjects;
  }

  Meshes& getMeshes()
  {
    assert(m_meshes);
    return *m_meshes;
  }

  auto& getTextures()
  {
    return m_textures;
  }

  il2ge::renderer::Renderer &getRenderer()
  {
    assert(m_renderer);
    return *m_renderer;
  }

  auto& getTextRenderer()
  {
    assert(m_text_renderer);
    return *m_text_renderer;
  }

  ObjectHandleManager& getObjectHandleManager()
  {
    assert(m_object_handle_manager);
    return *m_object_handle_manager;
  }

  static void setCurrent(Context *c) { s_current  = c; }
  static Context& getCurrent() { return *s_current; }
};


inline Context& getContext()
{
  return Context::getCurrent();
}


} // namespace il2ge::core

#endif
