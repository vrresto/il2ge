#pragma once

#include <core_config.h>

#define IL2GE_CORE_USE_HANDLE_TABLE 1
#define IL2GE_CORE_DEBUG_GOBJ_HANDLE 1
#define IL2GE_CORE_DEBUG_GOBJ_HANDLE_EXTRA 1


namespace il2ge::core_common {}


namespace il2ge::core
{

using namespace core_common;


class GObj;
class FObj;
class Context;
class Scene;
class Map;
class Menu;
class FObjects;
class Meshes;
class ProgressReporter;
class SkyBox;

class ObjectHandleManagerWithTable;
class DebugObjectHandleManager;
class SimpleObjectHandleManager;


#if IL2GE_CORE_USE_HANDLE_TABLE
using ObjectHandleManager = ObjectHandleManagerWithTable;
#elif IL2GE_CORE_DEBUG_GOBJ_HANDLE
using ObjectHandleManager = DebugObjectHandleManager;
#else
using ObjectHandleManager = SimpleObjectHandleManager;
#endif


} // namespace il2ge::core
