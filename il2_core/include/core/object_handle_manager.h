#pragma once

#include <core/objects.h>
#include <main_thread.h>

#include <queue>
#include <unordered_set>


namespace il2ge::core
{


using CppObjectHandle = long;


class SimpleObjectHandleManager
{
  static_assert(sizeof(CppObjectHandle) == sizeof(GObjRef*));

public:
  static void printStats(std::ostream& out) {}

  static CppObjectHandle create(const GObjRef& obj)
  {
    return reinterpret_cast<CppObjectHandle>(new GObjRef(obj));
  }

  static void destroy(CppObjectHandle handle)
  {
    delete reinterpret_cast<GObjRef*>(handle);
  }

  static const GObjRef& getObject(CppObjectHandle handle)
  {
    return *reinterpret_cast<GObjRef*>(handle);
  }
};


class DebugObjectHandleManager
{
  struct DebugRef : public GObjRef
  {
    uint64_t magic = 0;

    DebugRef(const GObjRef& other, uint64_t magic) : GObjRef(other), magic(magic) {}
  };

  static_assert(sizeof(CppObjectHandle) == sizeof(DebugRef*));

  static uint64_t s_magic;

  static uint64_t getMagic();

#if IL2GE_CORE_DEBUG_GOBJ_HANDLE_EXTRA
  static std::unordered_set<CppObjectHandle> s_all_handles;
#endif

  static DebugRef* getRef(CppObjectHandle handle)
  {
    assert(isMainThread());

    assert(handle);

#if IL2GE_CORE_DEBUG_GOBJ_HANDLE_EXTRA
    assert(s_all_handles.find(handle) != s_all_handles.end());
#endif

    auto ref = reinterpret_cast<DebugRef*>(handle);
    assert(ref->magic == getMagic());
    return ref;
  }

public:
  static void printStats(std::ostream& out) {}

  static CppObjectHandle create(const GObjRef& obj)
  {
    assert(isMainThread());

    auto handle = reinterpret_cast<CppObjectHandle>(new DebugRef(obj, getMagic()));

#if IL2GE_CORE_DEBUG_GOBJ_HANDLE_EXTRA
    assert(s_all_handles.find(handle) == s_all_handles.end());
    s_all_handles.insert(handle);
#endif

    return handle;
  }

  static void destroy(CppObjectHandle handle)
  {
    assert(isMainThread());

    auto ref = getRef(handle);

#if IL2GE_CORE_DEBUG_GOBJ_HANDLE_EXTRA
    auto node = s_all_handles.extract(handle);
    assert(node);
    // s_deleted_handles[handle] = &get()->getType();
#endif

    ref->magic = 0;
    delete ref;
  }

  static const GObjRef& getObject(CppObjectHandle handle)
  {
    return *getRef(handle);
  }
};


class ObjectHandleManagerWithTable
{
  static constexpr size_t MIN_FREE_HANDLE_COUNT = 1000;

  std::vector<GObjRef> m_table;
  std::queue<CppObjectHandle> m_free_handles;
  size_t m_handle_count = 0;

  void resizeTable();

public:
  ~ObjectHandleManagerWithTable();

  void printStats(std::ostream& out);

  CppObjectHandle create(const GObjRef& obj);

  void destroy(CppObjectHandle handle)
  {
    assert(isMainThread());

    assert(m_table.at(handle));
    m_table.at(handle).reset();

    m_free_handles.push(handle);

    assert(m_handle_count >= 1);
    m_handle_count--;
  }

  const GObjRef& getObject(CppObjectHandle handle)
  {
    assert(isMainThread());

    assert(m_table.at(handle));
    return m_table.at(handle);
  }
};


} // namespace il2ge::core
