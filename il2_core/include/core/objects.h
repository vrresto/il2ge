/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CORE_OBJECTS_H
#define CORE_OBJECTS_H

#include <core/forwards.h>
#include <il2ge/named.h>
#include <il2ge/object_map.h>
#include <il2ge/exceptions.h>
#include <log.h>
#include <util/non_copyable.h>

#include <unordered_map>
#include <memory>
#include <optional>
#include <functional>


namespace il2ge
{
  class Material;
}


namespace il2ge::core
{


template <class T>
// using ObjRef = std::__shared_ptr<T, __gnu_cxx::_S_single>; // no thread safety needed
using ObjRef = std::shared_ptr<T>;

template <class T>
// using WeakObjRef = std::__weak_ptr<T, __gnu_cxx::_S_single>; // no thread safety needed
using WeakObjRef = std::weak_ptr<T>;

using GObjRef = ObjRef<GObj>;


class GObjType : public util::NonCopyable
{
public:
  class JavaObjectFactory;
  using Factory = std::function<GObjRef(const std::string& path)>;

  GObjType(GObjType&&)=default;
  GObjType(std::string cpp_class_name, std::string java_class_name, Factory);
  GObjType(std::string cpp_class_name, std::string java_class_name);
  GObjType(std::string java_class_name);
  virtual ~GObjType();

  const Factory& getFactory() const;
  const JavaObjectFactory& getJavaObjectFactory() const;
  const std::string& getJavaClassName() const { return m_java_class_name; }
  const std::string& getCppClassName() const { return m_cpp_class_name; }

  bool operator==(const GObjType& other) const
  {
    return this == &other;
  }

  void objectCreated() const
  {
    m_objects++;
  }

  void objectDestroyed() const
  {
    m_objects--;
  }

  int getObjectCount() const
  {
    return m_objects;
  }

  static const GObjType& get(const std::string& class_name);
  static void add(const GObjType&);

  template <class T>
  static void add()
  {
    add(T::getStaticType());
  }

private:
  static void remove(const GObjType&);

  std::string m_cpp_class_name;
  std::string m_java_class_name;
  Factory m_factory;
  mutable std::unique_ptr<JavaObjectFactory> m_java_object_factory;
  mutable int m_objects = 0;
};


class GObj
{
public:
  class ClassNotFoundError : public std::runtime_error
  {
    std::string m_name;

  public:
    ClassNotFoundError(const std::string& name) :
      std::runtime_error("Class not found: " + name), m_name(name) {}

    const std::string& getClassName() const { return m_name; }
  };


  virtual GObjRef clone()
  {
    LOG_ERROR << "Cloning not implemented for type " << getType().getCppClassName() << std::endl;
    abort();
  }

  void onJavaObjectCreate();
  void onJavaObjectDestroy();
  void onObjectHandleTableDestroy();
  const GObjType& getType() { return m_type; }
  bool isFObj() { return m_is_fobj; }

  static void printStats(std::ostream&);

  template <class T, typename... ArgTypes>
  static auto create(ArgTypes&... args)
  {
    return ObjRef<T>(new T(args...));
  }

protected:
  template <typename... ArgTypes>
  static auto createType(ArgTypes... args)
  {
    return std::make_unique<GObjType>(args...);
  }

  template <class T, typename... ArgTypes>
  static auto createType(ArgTypes... args)
  {
    auto factory = [] (const std::string& path)
    {
      return GObj::create<T>(path);
    };
    return std::make_unique<GObjType>(args..., factory);
  }

  GObj(const GObjType& type, bool is_fobj = false);
  virtual ~GObj();

private:
  const GObjType& m_type;
  bool m_is_fobj = false;
  int m_java_objects = 0;
};


class FObj : public GObj, public il2ge::Named
{
  std::string m_path;

protected:
  FObj(const GObjType&, const std::string& path);
  ~FObj();

public:
  using ReadError = il2ge::FileReadError;

  const std::string& getPath() const { return m_path; }
  void rename(const std::string& name) { m_path = name; }
};


using FObjRef = ObjRef<FObj>;


class FObjects
{
  il2ge::CachedAutoAddingWeakMap<FObj> m_objects;

public:
  FObjects();

  GObjRef getRef(const std::string& path);
};


} // namespace il2ge::core

#endif
