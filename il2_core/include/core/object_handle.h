#pragma once

#include <core/object_handle_manager.h>
#include <core/context.h>


namespace il2ge::core
{


#if IL2GE_CORE_USE_HANDLE_TABLE


inline CppObjectHandle createObjectHandle(const GObjRef& obj)
{
  return getContext().getObjectHandleManager().create(obj);
}


inline void destroyObjectHandle(CppObjectHandle handle)
{
  getContext().getObjectHandleManager().destroy(handle);
}


inline const GObjRef& objectFromHandle(CppObjectHandle handle)
{
  return getContext().getObjectHandleManager().getObject(handle);
}


#else


inline CppObjectHandle createObjectHandle(const GObjRef& obj)
{
  return ObjectHandleManager::create(obj);
}


inline void destroyObjectHandle(CppObjectHandle handle)
{
  ObjectHandleManager::destroy(handle);
}


inline const GObjRef& objectFromHandle(CppObjectHandle handle)
{
  return ObjectHandleManager::getObject(handle);
}


#endif

} // namespace il2ge::core
