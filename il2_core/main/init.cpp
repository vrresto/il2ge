#include "gl_wrapper.h"
#include <log.h>
#include <sfs.h>
#include "wgl_wrapper.h"
#include <core.h>
#include <core_common.h>
#include <core_interface.h>
#include <il2_jni.h>
#include <util.h>


namespace
{


void initCore()
{
  LOG_INFO << "Initializing core ..." << std::endl;

  sfs::init();
  il2ge::core::wgl_wrapper::init();
  il2ge::core::gl_wrapper::init();
  il2ge::core::init();

  LOG_INFO << "Initializing core ... done." << std::endl;
}


struct Interface : public il2ge::core_common::CoreInterface
{
  HMODULE m_il2ge_module {};
  bool m_is_loaded = false;
  bool m_is_initialized = false;

  Interface(HMODULE il2ge_module) : m_il2ge_module(il2ge_module)
  {
    il2ge::il2_jni::init();
  }

  void load(const char*) override
  {
    assert(!m_is_loaded);
    m_is_loaded = true;
  }

  bool isLoaded() override
  {
    return m_is_loaded;
  }

  void init() override
  {
    assert(!m_is_initialized);
    assert(isLoaded());
    initCore();
    m_is_initialized = true;
  }

  bool isInitialized() override
  {
    return m_is_initialized;
  }

  HMODULE getModuleHandle() override
  {
    assert(m_is_loaded);
    return m_il2ge_module;
  }

  void* getExeProcAddress(const char* name) override
  {
    return nullptr;
  }

  void resolveImports(HMODULE mod) override
  {
    il2ge::il2_jni::resolveImports(mod);
  }

  void* getJNIProcAddress(const char* name) override
  {
    return il2ge::il2_jni::getExport(name);
  }

  HMODULE getGLModuleHandle() override
  {
    return il2ge::core::wgl_wrapper::getGLModule();
  }

  void* getGLProcAddress(const char* name) override
  {
    return il2ge::core::wgl_wrapper::getProcAddress(name);
  }
};


} // namespace


namespace il2ge::core
{


il2ge::core_common::CoreInterface* createCoreInterface(HMODULE il2ge_module)
{
  return new Interface(il2ge_module);
}


} // namespace il2ge::core
