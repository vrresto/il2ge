#include "gl_wrapper.h"
#include "wgl_wrapper.h"
#include <render_util/state.h>
#include <render_util/gl_binding/gl_functions.h>
#include <util.h>

#include <unordered_map>


using namespace render_util::gl_binding;

namespace wgl_wrapper = il2ge::core::wgl_wrapper;


namespace
{


class Context
{
  std::stack<std::unique_ptr<render_util::StateModifier>> m_state_stack;

public:
  Context()
  {
    auto state = std::make_unique<render_util::StateModifier>();
    state->setFrontFace(GL_CCW);
    state->setCullFace(GL_BACK);
    state->setDepthFunc(GL_LESS);
    state->setDepthMask(GL_TRUE);
    state->setBlendFunc(GL_ONE, GL_ZERO);
    m_state_stack.push(std::move(state));
  }

  render_util::StateModifier& getStateModifier()
  {
    assert(!m_state_stack.empty());
    return *m_state_stack.top();
  }

  void pushState()
  {
    m_state_stack.push(std::make_unique<render_util::StateModifier>());
  }

  void popState()
  {
    m_state_stack.pop();
  }
};


std::unordered_map<std::string, void*> g_procs;
Context* g_main_context {};
bool g_initialized = false;
// bool g_is_main_context_current = false;


Context& getContext()
{
  // assert(g_is_main_context_current);
  assert(wgl_wrapper::isMainContextCurrent());
  assert(g_main_context);
  return *g_main_context;
}


void enableCap(GLenum cap, bool enable)
{
  auto index = render_util::State::getIndexFromCap(cap);

  if (wgl_wrapper::isMainContextCurrent() && index != -1)
  {
    getContext().getStateModifier().enable(index, enable);
  }
  else
  {
    if (enable)
      state::Enable(cap);
    else
      state::Disable(cap);
  }
}


void GLAPIENTRY wrap_glEnable(GLenum cap)
{
  enableCap(cap, true);
}


void GLAPIENTRY wrap_glDisable(GLenum cap)
{
  // if (!wgl_wrapper::isShuttingDown())
    enableCap(cap, false);
}


void GLAPIENTRY wrap_glAlphaFunc(GLenum func, GLclampf ref)
{
  if (wgl_wrapper::isMainContextCurrent())
    getContext().getStateModifier().setAlphaFunc(func, ref);
  else
    state::AlphaFunc(func, ref);
}


void GLAPIENTRY wrap_glBlendFunc(GLenum sfactor, GLenum dfactor)
{
  if (wgl_wrapper::isMainContextCurrent())
    getContext().getStateModifier().setBlendFunc(sfactor, dfactor);
  else
    state::BlendFunc(sfactor, dfactor);
}


void GLAPIENTRY wrap_glCullFace(GLenum mode)
{
  if (wgl_wrapper::isMainContextCurrent())
    getContext().getStateModifier().setCullFace(mode);
  else
    state::CullFace(mode);
}


void GLAPIENTRY wrap_glDepthFunc(GLenum func)
{
  if (wgl_wrapper::isMainContextCurrent())
    getContext().getStateModifier().setDepthFunc(func);
  else
    state::DepthFunc(func);
}


void GLAPIENTRY wrap_glDepthMask(GLboolean flag)
{
  if (wgl_wrapper::isMainContextCurrent())
    getContext().getStateModifier().setDepthMask(flag);
  else
    state::DepthMask(flag);
}


void GLAPIENTRY wrap_glFrontFace()
{
  UNIMPLEMENTED
}


void GLAPIENTRY wrap_glUseProgram()
{
  UNIMPLEMENTED
}


void GLAPIENTRY wrap_glPolygonMode(GLenum face, GLenum mode)
{
  assert(face == GL_FRONT_AND_BACK);

  if (wgl_wrapper::isMainContextCurrent())
    getContext().getStateModifier().setPolygonMode(mode);
  else
    state::PolygonMode(face, mode);
}


void GLAPIENTRY wrap_glPolygonOffset(GLfloat factor, GLfloat units)
{
  if (wgl_wrapper::isMainContextCurrent())
    getContext().getStateModifier().setPolygonOffset(factor, units);
  else
    state::PolygonOffset(factor, units);
}


void GLAPIENTRY wrap_glPushAttrib(GLbitfield mask)
{
  if (wgl_wrapper::isMainContextCurrent())
    getContext().pushState();

  gl::PushAttrib(mask);
}


void GLAPIENTRY wrap_glPopAttrib()
{
  gl::PopAttrib();

  if (wgl_wrapper::isMainContextCurrent())
    getContext().popState();
}


void GLAPIENTRY wrap_glPixelStoref(GLenum pname, GLfloat param)
{
  UNIMPLEMENTED
}


void GLAPIENTRY wrap_glPixelStorei(GLenum pname, GLint param)
{
  UNIMPLEMENTED
}


void registerProc(const char* name, void* func)
{
  LOG_DEBUG << "Register proc: " << name << std::endl;

  auto& proc = g_procs[name];
  assert(!proc);
  proc = func;

  // assert(gl_wrapper::getProc(name) == func);
}


} // namespace


namespace il2ge::core::gl_wrapper
{


void* getProc(const char* name)
{
  assert(g_initialized);

  void* proc {};
  auto it = g_procs.find(name);
  if (it != g_procs.end())
    proc = it->second;

  assert(proc || !render_util::gl_binding::isStateProc(name));
  return proc;
}


void init()
{
  assert(!g_initialized);

#define REGISTER_PROC(name) registerProc(#name, (void*) &wrap_##name)
  REGISTER_PROC(glEnable);
  REGISTER_PROC(glDisable);
  REGISTER_PROC(glPixelStorei);
  REGISTER_PROC(glPixelStoref);
  REGISTER_PROC(glAlphaFunc);
  REGISTER_PROC(glBlendFunc);
  REGISTER_PROC(glCullFace);
  REGISTER_PROC(glDepthFunc);
  REGISTER_PROC(glDepthMask);
  REGISTER_PROC(glFrontFace);
  REGISTER_PROC(glUseProgram);
  REGISTER_PROC(glPolygonMode);
  REGISTER_PROC(glPolygonOffset);
  REGISTER_PROC(glPushAttrib);
  REGISTER_PROC(glPopAttrib);
#undef REGISTER_PROC

  g_initialized = true;
}


void createContext()
{
  assert(g_initialized);
  assert(!g_main_context);

  if (!g_main_context)
    g_main_context = new Context();
}


void destroyContext()
{
  assert(g_main_context);
  delete g_main_context;
  g_main_context = nullptr;
}


} // namespace il2ge::core::gl_wrapper
