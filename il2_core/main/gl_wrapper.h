#pragma once

namespace il2ge::core::gl_wrapper
{


void init();
void createContext();
void destroyContext();
void* getProc(const char* name);


} // namespace il2ge::core::gl_wrapper
