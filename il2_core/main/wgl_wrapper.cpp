/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#undef NOGDI

#include "wgl_wrapper.h"
#include "gl_wrapper.h"
#include "gl_version_check.h"
#include <core/context.h>
#include <render_util/context.h>
#include <render_util/state.h>
#include <log.h>
#include <render_util/gl_binding/gl_interface.h>

#include <cassert>
#include <cstdio>
#include <iostream>
#include <windows.h>

#include <wgl_interface.h>


using namespace std;
using namespace il2ge::core::wgl_wrapper;

namespace gl_wrapper = il2ge::core::gl_wrapper;


namespace
{


using GLInterface = render_util::gl_binding::GL_Interface;
using WglInterface = il2ge::core_common::WglInterface;


struct GlobalData
{
  std::unique_ptr<WglInterface> wgl_interface;
  std::unordered_map<HGLRC, std::unique_ptr<GLInterface>> interface_for_context;
  std::unique_ptr<render_util::Context> render_util_context;
  std::unique_ptr<il2ge::core::Context> core_context;
  bool in_shutdown = false;
  HMODULE gl_module = 0;
  HGLRC main_context_handle {};
  HGLRC current_context_handle {};
};


GlobalData* g_data = nullptr;


GlobalData& gData()
{
  assert(g_data);
  return *g_data;
}


const WglInterface& realWGL()
{
  assert(gData().wgl_interface);
  return *gData().wgl_interface;
}


void *getProcAddress_ext(const char *name)
{
  void *func = (void*) GetProcAddress(gData().gl_module, name);
  if (!func)
    func = (void*) realWGL().GetProcAddress(name);

  return func;
}


// ContextData* getMainContext()
// {
//   assert(gData().main_context);
//   return gData().main_context.get();
// }


bool isMainContext(HGLRC handle)
{
  assert(handle);
  assert(gData().main_context_handle);
  return handle == gData().main_context_handle;
}


BOOL WINAPI wrap_wglMakeContextCurrentARB(HDC hDrawDC, HDC hReadDC, HGLRC hglrc)
{
  UNIMPLEMENTED
}


PROC WINAPI wrap_wglGetProcAddress(LPCSTR name)
{
  LOG_TRACE<<"wrap_wglGetProcAddress: "<<name<<endl;

  if (isMainContextCurrent())
  {
    if (strcmp(name, "wglMakeContextCurrentARB") == 0)
    {
      return (PROC) &wrap_wglMakeContextCurrentARB;
    }

    auto proc = realWGL().GetProcAddress(name);

    auto wrap_proc = gl_wrapper::getProc(name);
    if (wrap_proc && proc)
      return (PROC) wrap_proc;

    return proc;
  }
  else
  {
    return realWGL().GetProcAddress(name);
  }
}


BOOL WINAPI wrap_wglMakeCurrent(HDC hdc, HGLRC hglrc)
{
  assert(!gData().in_shutdown);

  if (!hglrc)
  {
    // assumtion: this is called by the game prior to deleting the main context (and only then)
    // so we use this last chance to free our ressources while the the context is still current

    LOG_INFO << "Shutting down ..." << std::endl;

    gData().in_shutdown = true;

    assert(isMainContextCurrent());

    LOG_INFO << "Shutting down core::Context ..." << std::endl;
    il2ge::core::Context::setCurrent(nullptr);
    gData().core_context.reset();

    LOG_INFO << "Shutting gl wrapper context ..." << std::endl;
    gl_wrapper::destroyContext();

    LOG_INFO << "Shutting down render_util::Context ..." << std::endl;
    render_util::Context::setCurrent(nullptr);
    gData().render_util_context.reset();

    LOG_INFO << "Calling GLInterface::setCurrent(nullptr) ..." << std::endl;
    GLInterface::setCurrent(nullptr);

    LOG_INFO << "Calling wglMakeCurrent ..." << std::endl;
    bool res = realWGL().MakeCurrent(hdc, hglrc);
    assert(res);

    gData().current_context_handle = 0;

    LOG_INFO << "Shutdown complete." << std::endl;

    return true;
  }

  if (realWGL().MakeCurrent(hdc, hglrc))
  {
    assert(gData().main_context_handle);

    gData().current_context_handle = hglrc;

    bool is_new_context = false;

    auto& iface = gData().interface_for_context[hglrc];
    if (!iface)
    {
      is_new_context = true;
      il2ge::checkGLVersion(getProcAddress_ext);
      iface = std::make_unique<GLInterface>(&getProcAddress_ext);
    }

    GLInterface::setCurrent(iface.get());

    if (isMainContext(hglrc))
    {
      assert(isMainContextCurrent());

      if (!gData().render_util_context)
        gData().render_util_context = std::make_unique<render_util::Context>();
      render_util::Context::setCurrent(gData().render_util_context.get());

      if (is_new_context)
      {
        render_util::StateModifier::init();
        gl_wrapper::createContext();
      }

      if (!gData().core_context)
        gData().core_context = std::make_unique<il2ge::core::Context>();
      il2ge::core::Context::setCurrent(gData().core_context.get());
    }
    else
    {
      il2ge::core::Context::setCurrent(nullptr);
      render_util::Context::setCurrent(nullptr);
    }

    return true;
  }
  else
    return false;
}


HGLRC WINAPI wrap_wglCreateContext(HDC dc)
{
  auto handle = realWGL().CreateContext(dc);

  if (!gData().main_context_handle)
    gData().main_context_handle = handle;

  return handle;
}


BOOL WINAPI wrap_wglDeleteContext(HGLRC hglrc)
{
  assert(hglrc);
  assert(gData().current_context_handle != hglrc);

  if (isMainContext(hglrc))
  {
    assert(gData().in_shutdown);
    // should have been deleted already in wrap_wglMakeCurrent()
    // assert(!gData().main_context);
  }

  {
    auto iface = gData().interface_for_context.extract(hglrc);
    assert(iface);
  }

  bool res =  realWGL().DeleteContext(hglrc);
  assert(res);

  return res;
}


} // namespace


namespace il2ge::core::wgl_wrapper
{


void init()
{
  assert(!g_data);
  g_data = new GlobalData();

  gData().gl_module = LoadLibrary("opengl32.dll");
  assert(gData().gl_module);

  gData().wgl_interface = std::make_unique<WglInterface>(gData().gl_module);
}


HMODULE getGLModule()
{
  return gData().gl_module;
}


bool isMainContextCurrent()
{
  if (!gData().current_context_handle)
    return false;

  if (!gData().main_context_handle)
    return false;

  assert(gData().current_context_handle);
  assert(gData().main_context_handle);
  // assert(gData().main_context);

  return gData().current_context_handle == gData().main_context_handle;
}


void *getProcAddress(const char* name)
{
  if (strcmp(name, "wglGetProcAddress") == 0)
  {
    return (void*) &wrap_wglGetProcAddress;
  }

  if (strcmp(name, "wglMakeCurrent") == 0)
  {
    return (void*) &wrap_wglMakeCurrent;
  }

  if (strcmp(name, "wglCreateContext") == 0)
  {
    return (void*) &wrap_wglCreateContext;
  }

  if (strcmp(name, "wglDeleteContext") == 0)
  {
    return (void*) &wrap_wglDeleteContext;
  }

  assert(!isMainContextCurrent());
  assert(!gData().current_context_handle);

  void *proc = (void*) GetProcAddress(getGLModule(), name);

  void *wrap_proc = gl_wrapper::getProc(name);
  if (wrap_proc && proc)
    return wrap_proc;

  return proc;
}


// ContextData *getContext()
// {
//   assert(!gData().in_shutdown);
//   assert(isMainContextCurrent());
//   return getMainContext();
// }


// bool isShuttingDown()
// {
//   return gData().in_shutdown;
// }


} // namespace il2ge::core::wgl_wrapper
