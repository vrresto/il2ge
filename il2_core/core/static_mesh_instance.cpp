#include <core.h>
#include <core/objects.h>
#include <il2ge/mesh/mesh_base.h>
#include <render_util/camera_3d.h>
#include <log.h>
#include <il2ge_java_interface/interface.h>

#include <list>


namespace
{


struct StaticMeshInstance;
struct Node;

void removeInstanceFromTree(StaticMeshInstance *instance);


//FIXME - put this into a header
void applyRotation(const glm::vec3 &yaw_pitch_roll, glm::mat4 &mat)
{
  mat = glm::rotate(mat, glm::radians(yaw_pitch_roll.x), glm::vec3(0, 0, 1));
  mat = glm::rotate(mat, glm::radians(yaw_pitch_roll.y), glm::vec3(0, 1, 0));
  mat = glm::rotate(mat, glm::radians(yaw_pitch_roll.z), glm::vec3(1, 0, 0));
}


struct StaticMeshInstance : public core::GObj
{
  std::shared_ptr<const il2ge::mesh::MeshBase> m_mesh;
  glm::dvec3 m_pos = glm::dvec3(0);
  glm::dmat4 m_model_world = glm::dmat4(1);
  Node *m_node = nullptr;

  StaticMeshInstance() : GObj(GObj::Type::STATIC_MESH_INSTANCE) {}

  ~StaticMeshInstance()
  {
    removeInstanceFromTree(this);
  }

  // void *clone() override
  // {
  //   abort();
  // }
};


constexpr int MAX_TREE_DEPTH = 8;
constexpr double ROOT_NODE_SIZE_M = 1000000;


struct Node
{
  enum class ChildIndex : size_t
  {
    TOP_LEFT = 0,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT,
  };


  Node *parent = nullptr;
  const int tree_depth = 0;

  std::array<std::unique_ptr<Node>, 4> children;
  int num_children = 0;

  std::list<StaticMeshInstance*> instances;

  render_util::Box bounding_box;
  glm::dvec2 origin {};
  double extent = 0;
  glm::dvec2 center {};


  Node(int tree_depth, glm::dvec2 origin, double extent, Node *parent = nullptr) :
    parent(parent),
    tree_depth(tree_depth),
    origin(origin),
    extent(extent)
  {
    assert(tree_depth <= MAX_TREE_DEPTH);
    center = origin + glm::dvec2(extent / 2.0);

    {
      //FIXME calculate bounding_box from contents

      glm::vec3 bb_origin =
      {
        origin.x,
        origin.y,
        0,
      };

      glm::vec3 bb_extent =
      {
        extent,
        extent,
        4000, //FIXME
      };

      bounding_box.set(bb_origin, bb_extent);
    }
  }


  bool isEmpty() { return instances.empty() && !num_children; }

  bool isLeaf() const { return tree_depth == MAX_TREE_DEPTH; }

  Node *getParent() { return parent; }

  void destroyChild(Node *child)
  {
    for (auto &c : children)
    {
      if (c.get() == child)
      {
        c.reset();
        num_children--;
        assert(num_children >= 0);
        return;
      }
    }
    abort();
  }

  void remove(StaticMeshInstance *instance)
  {
    assert(isLeaf());
    assert(instance->m_node == this);

    instance->m_node = nullptr;

    for (auto it = instances.begin(); it != instances.end(); it++)
    {
      if (*it == instance)
      {
        instances.erase(it);
        return;
      }
    }

    LOG_ERROR<<"instances.size(): "<<instances.size()<<std::endl;
    abort();
  }

  void insert(StaticMeshInstance *instance, ChildIndex child_index)
  {
    assert(!isLeaf());
    size_t index = (size_t)child_index;
    auto &child = children.at(index);

    if (!child)
    {
      glm::dvec2 child_origin {};

      switch(child_index)
      {
        case ChildIndex::TOP_LEFT:
          child_origin = glm::dvec2(origin.x, center.y);
          break;
        case ChildIndex::TOP_RIGHT:
          child_origin = glm::dvec2(center.x, center.y);
          break;
        case ChildIndex::BOTTOM_LEFT:
          child_origin = glm::dvec2(origin.x, origin.y);
          break;
        case ChildIndex::BOTTOM_RIGHT:
          child_origin = glm::dvec2(center.x, origin.y);
          break;
      }

      child = std::make_unique<Node>(tree_depth+1, child_origin, extent / 2.0, this);

      num_children++;
      assert(num_children <= 4);
    }

    child->insert(instance);
  }

  void insert(StaticMeshInstance *instance)
  {
    assert(!instance->m_node);

    if (isLeaf())
    {
      instances.push_back(instance);
      instance->m_node = this;
    }
    else
    {
      auto pos_2d = glm::dvec2(instance->m_pos);

      if (pos_2d.x < center.x)
      {
        if (pos_2d.y < center.y)
          insert(instance, ChildIndex::BOTTOM_LEFT);
        else
          insert(instance, ChildIndex::TOP_LEFT);
      }
      else
      {
        if (pos_2d.y < center.y)
          insert(instance, ChildIndex::BOTTOM_RIGHT);
        else
          insert(instance, ChildIndex::TOP_RIGHT);
      }
    }
  }

};


void removeInstanceFromTree(StaticMeshInstance *instance)
{
  auto node = instance->m_node;
  if (!node)
    return;

  node->remove(instance);

  while (node->isEmpty() && node->getParent())
  {
    auto parent = node->getParent();
    parent->destroyChild(node);
    node = parent;
  }
}


bool cullNode(const Node &node, const render_util::Camera3D &camera)
{
  return camera.cull(node.bounding_box);
}


void traverseNode(const Node &node, const render_util::Camera3D &camera)
{
  if (cullNode(node, camera))
    return;

  for (auto &child : node.children)
  {
    if (child)
      traverseNode(*child, camera);
  }

  for (auto instance : node.instances)
  {
    if (!instance->m_mesh)
      continue;

    auto pos = instance->m_pos;

    auto dist = glm::distance(glm::vec3(pos), camera.getPos());

    if (dist > instance->m_mesh->getMaxVisibleDistance() * 4)
      continue;

    // if (camera.cull(pos, 500)) //FIXME use actual bounding sphere
      // continue;

    auto model_view = camera.getWorld2ViewMatrixD() * instance->m_model_world;

    instance->m_mesh->draw(model_view, dist, 0);
  }
}


Node g_root(0, glm::dvec2(0), ROOT_NODE_SIZE_M);


// StaticMeshInstance *get(int cpp_obj)
// {
//   auto obj = core::getGObj(cpp_obj);
//   assert(obj);
//
//   if (obj->getType() == core::GObj::Type::STATIC_MESH_INSTANCE)
//   {
//     return static_cast<StaticMeshInstance*>(obj);
//   }
//   else
//   {
//     abort();
//   }
// }




} // namespace


namespace il2ge::java_interface
{


CppObjectID StaticMeshInstance_New()
{
  UNIMPLEMENTED
  // auto mi = std::make_unique<StaticMeshInstance>();
  // mi->is_owned_by_java_object = true;
  // return core::addGObj(std::move(mi));
}


void StaticMeshInstance_Finalize(CppObjectID cpp_object)
{
  UNIMPLEMENTED
  // il2_jni::finalizeGObj(cpp_object);
}


void StaticMeshInstance_SetMesh(CppObjectID cpp_object, CppObjectID mesh_cpp_object)
{
  UNIMPLEMENTED
  // auto instance = get(cpp_object);
  //
  // if (mesh_cpp_object)
  // {
  //   instance->m_mesh = core::getMeshFromGObj(mesh_cpp_object);
  // }
  // else
  // {
  //   instance->m_mesh.reset();
  // }
}


void StaticMeshInstance_SetPos(CppObjectID cpp_object,
                               double x, double y, double z,
                               float pitch, float yaw, float roll)
{
  UNIMPLEMENTED
  // auto instance = get(cpp_object);
  //
  // auto pos = glm::dvec3(x,y,z);
  //
  // glm::mat4 rotation(1);
  // applyRotation(glm::vec3(yaw, -pitch, -roll), rotation);
  //
  // instance->m_pos = pos;
  // instance->m_model_world = glm::translate(glm::dmat4(1.0), pos) * glm::dmat4(rotation);
  //
  // removeInstanceFromTree(instance);
  // g_root.insert(instance);
}


} // namespace il2ge::java_interface


namespace il2ge::core
{
  void renderStatics()
  {
    traverseNode(g_root, *core::getCamera());
  }
}
