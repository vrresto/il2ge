/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core.h"
#include "core_p.h"
#include "il2_state.h"
#include "meshes.h"
#include <wgl_wrapper.h>
#include <il2_jni.h>
#include <core/scene.h>
#include <core/context.h>
#include <core/object_handle_manager.h>
#include <il2ge/renderer.h>
#include <render_util/state.h>
#include <render_util/render_util.h>
#include <render_util/text_display.h>
#include <render_util/gl_binding/gl_functions.h>

#include <iostream>

using namespace glm;
using namespace il2ge;
using namespace il2ge::core;
using namespace std;

using namespace render_util::gl_binding;

using Clock = std::chrono::steady_clock;


namespace
{


IL2State g_il2_state;


void drawTerrain(render_util::TerrainBase &terrain,
                   const render_util::Camera &camera,
                   bool is_far_camera)
{
  auto update_uniforms = [&] (auto& program)
  {
    core::updateUniforms(program);
    render_util::updateUniforms(program, camera);
  };

  terrain.update(camera, is_far_camera);
  terrain.draw(camera, update_uniforms);
}


void drawTerrain(render_util::TerrainBase &terrain)
{
  terrain.setDrawDistance(0);

  auto z_far = core::getCamera()->getZFar();

  auto far_camera = *core::getCamera();
  far_camera.setProjection(far_camera.getFov(), z_far - 4000, 1500000);

  auto far_camera_cirrus = *core::getCamera();
  far_camera_cirrus.setProjection(far_camera.getFov(), 20000, 1500000);

  drawTerrain(terrain, far_camera, true);
  // drawCirrus(ctx, state, far_camera_cirrus, true);

  gl::Clear(GL_DEPTH_BUFFER_BIT);

  drawTerrain(terrain, *core::getCamera(), false);
}


void drawTerrain()
{
  render_util::StateModifier state;
  state.setDefaults();
  state.enableCullFace(true);
  state.enableDepthTest(true);
  state.setFrontFace(GL_CCW);

  assert(gl::IsEnabled(GL_COLOR_LOGIC_OP) == GL_FALSE);

  gl::Clear(GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

  // getContext().getScene().drawSkyBox(*core::getCamera());

  gl::Clear(GL_DEPTH_BUFFER_BIT);

  drawTerrain(core::getTerrain());

  state.enableDepthTest(false);
  state.enableCullFace(false);

  // mapRenderDebug();
}


void setRenderPhase(core::Il2RenderPhase phase, bool is_mirror = false)
{
  assert(!is_mirror);
  assert(wgl_wrapper::isMainContextCurrent());

  if (g_il2_state.render_state.render_phase != core::IL2_PostRenders)
  {
    if (!(phase > g_il2_state.render_state.render_phase))
    {
      LOG_ERROR << "current phase: " << g_il2_state.render_state.render_phase
                << " - new phase: " << phase << std::endl;
    }
    assert(phase > g_il2_state.render_state.render_phase);
  }
  else
  {
    assert(phase == core::IL2_PrePreRenders);
  }

  switch (phase)
  {
    case core::IL2_PrePreRenders:
      il2ge::renderer::clearPerFrameStats();
      mapClearDebugFrameData();
      break;
    case core::IL2_PostPreRenders:
      gl::ClearColor(0.5, 0.5, 0.5, 1.0);
      gl::Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |
                GL_ACCUM_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
      break;
    default:
      break;
  }

  g_il2_state.render_state.render_phase = phase;
}


} // namespace


namespace il2ge::core
{
  glm::vec2 getWindSpeed()
  {
    return g_il2_state.wind_speed;
  }

  float getFrameDelta()
  {
    return g_il2_state.frame_delta;
  }

  void setFMBActive(bool active)
  {
    g_il2_state.is_fmb_active = active;
  }

  bool isFMBActive()
  {
    return g_il2_state.is_fmb_active;
  }

  void getRenderState(Il2RenderState *state)
  {
    *state = g_il2_state.render_state;
  }

  render_util::Camera3D *getCamera()
  {
    return &g_il2_state.camera;
  }

  void onPrePreRenders()
  {
    if (!g_il2_state.last_frame_time)
      g_il2_state.last_frame_time = g_il2_state.current_time;

    auto delta = g_il2_state.current_time - g_il2_state.last_frame_time;

    g_il2_state.last_frame_time = g_il2_state.current_time;
    g_il2_state.frame_delta = (float)delta / 1000.f;

    // core::getRenderer().resetFlushCount();

    setRenderPhase(IL2_PrePreRenders);

    getScene()->update(g_il2_state.frame_delta, g_il2_state.wind_speed);

    getContext().getMeshes().createSharedVAO();
  }

  void onPostPreRenders()
  {
    setRenderPhase(IL2_PostPreRenders);
  }

  void onPostRenders()
  {
    setRenderPhase(IL2_PostRenders);

    // if (isMenuShown())
    // {
    //   getMenu().draw();
    // }
    // else
    {
      auto& overlay = getScene()->getOverlay();
      overlay.clear();

      overlay.addLine("Main thread garbage size: " +
                      std::to_string(il2_jni::getMainThreadGarbageSize()));
      overlay.addLine("Other threads garbage size: " +
                      std::to_string(il2_jni::getOtherThreadsGarbageSize()));

      render_util::StateModifier state;
      state.setDefaults();

      std::ostringstream stats;

      getRenderer().printStats(stats);

      getContext().getObjectHandleManager().printStats(stats);

      GObj::printStats(stats);

      overlay.addText(stats.str());
      overlay.draw(getTextRenderer(), 0, 0);
    }
	}

  // void onRenderSpritesFogBegin()
  // {
  //   setRenderPhase(IL2_SpritesFog);
  // }
  //
  // void onRenderSpritesFogEnd()
  // {
  //   setRenderPhase(IL2_SpritesFog_Finished);
  // }
  //
  // void onRenderCockpitBegin()
  // {
  //   setRenderPhase(IL2_Cockpit);
  // }
  //
  // void onRenderCockpitEnd()
  // {
  //   setRenderPhase(IL2_Cockpit_Finished);
  // }

  const vec3 &getSunDir()
  {
    return g_il2_state.sun_dir;
  }

  void setSunDir(const vec3 &dir)
  {
    g_il2_state.sun_dir = dir;
  }

  void setCameraMode(Il2CameraMode mode)
  {
    g_il2_state.render_state.camera_mode = mode;
  }

  Il2CameraMode getCameraMode()
  {
    return g_il2_state.render_state.camera_mode;
  }

  Il2RenderPhase getRenderPhase()
  {
    return g_il2_state.render_state.render_phase;
  }

  void setTime(uint64_t time)
  {
    g_il2_state.current_time = time;
  }

  void setWindSpeed(const glm::vec2 &speed)
  {
    g_il2_state.wind_speed = speed;
  }

  void drawTerrain(bool is_mirror)
  {
    assert(!is_mirror);
    ::drawTerrain();
  }
}
