#include "meshes.h"
#include <sfs.h>
#include <core/objects.h>
#include <il2ge/material.h>
#include <il2ge/renderer.h>


using namespace il2ge::core;
using namespace il2ge;


namespace
{


std::unique_ptr<il2ge::MaterialList>
createMaterialList(const std::string& dir,
                   const std::vector<std::string>& names,
                   FObjects& fobjects)
{
  auto list = std::make_unique<MaterialList>();
  list->m_materials.resize(names.size());

  for (int i = 0; i < list->size(); i++)
  {
    auto path = dir + '/' + names.at(i) + ".mat";
    try
    {
      auto fobj = fobjects.getRef(path);
      assert(fobj);
      auto material = std::dynamic_pointer_cast<il2ge::Material>(fobj);
      assert(material);
      list->m_materials.at(i) =  material;
    }
    catch (std::exception& e)
    {
      LOG_ERROR << "Exception while getting material " << path
                << ": " << e.what() << std::endl;
    }
  }
  return list;
}


std::shared_ptr<const il2ge::mesh::MeshBase> loadMesh(const std::string& path)
{
  std::vector<char> file_content;
  if (!sfs::readFile(path, file_content))
    throw sfs::ReadError(path);

  auto mesh = il2ge::mesh::loadMesh(path, file_content.data(), file_content.size());
  assert(mesh);
  mesh->createRendererData();

  return mesh;
}


} // namespace


namespace il2ge::core
{


const il2ge::mesh::MeshInstance* MeshInstanceCache::get(const std::string& path)
{
  //TODO
  return nullptr;
}


void MeshInstanceCache::insert(std::unique_ptr<il2ge::mesh::MeshInstance>&&, const std::string& path)
{
  //TODO
}


Meshes::Meshes(FObjects& fobjs) :
  m_fobjects(fobjs),
  m_meshes([this] (const std::string& path)
              {
                auto mesh = loadMesh(path);
                m_new_meshes.push_back(mesh);
                return mesh;
              },
           20)
{
}


std::unique_ptr<mesh::MeshInstance> Meshes::createInstance(const std::string& path)
{
  auto mesh = m_meshes.get(path);

  auto materials = createMaterialList(util::getDirFromPath(path),
                                      mesh->getMaterialNames(),
                                      m_fobjects);

  auto instance = std::make_unique<mesh::MeshInstance>(mesh, *materials);

  return instance;
#if 0
  std::unique_ptr<mesh::MeshInstance> instance;

  auto cached = m_instance_cache.get(path);

  if (!cached)
  {
    auto mesh = core::getMesh(path);
    assert(mesh);

    auto new_cached = ::createMeshInstance(mesh, m_fobjects);
    assert(new_cached);

    instance = std::make_unique<mesh::MeshInstance>(*new_cached);

    m_instance_cache.insert(std::move(new_cached), path);
  }
  else
  {
    instance = std::make_unique<mesh::MeshInstance>(*cached);
  }

  assert(instance);
  return instance;
#endif
}


std::unique_ptr<mesh::HierMeshBase> Meshes::createHierMesh(const std::string& path)
{
  LOG_INFO << "Loading HierMesh: " << path << std::endl;

  auto read_file = [] (std::string path)
  {
    std::vector<char> data;
    if (sfs::readFile(path, data))
      return data;
    else
      throw sfs::ReadError(path);
  };

  auto file_content = read_file(path);

  auto get_mesh = [this] (auto path)
  {
    return m_meshes.get(path);
  };

  auto mesh = mesh::loadHierMesh(path, get_mesh,
                                 file_content.data(), file_content.size());

  mesh->setMaterials(createMaterialList(util::getDirFromPath(path),
                                        mesh->getMaterialNames(),
                                        m_fobjects));

  return mesh;
}


void Meshes::createSharedVAO()
{
  std::vector<renderer::MeshRendererData*> all_meshes;

  for (auto& it : m_new_meshes)
  {
    auto mesh = it.lock();
    if (!mesh)
      continue;

    for (int lod = 0; lod < mesh->getLodCount(); lod++)
    {
      auto& lod_mesh = mesh->getLod(lod);

      auto rdata = lod_mesh.getRendererData();
      assert(rdata);

      if (!rdata->getSharedVAO())
        all_meshes.push_back(rdata);
    }
  }

  if (!all_meshes.empty())
  {
    auto shared_vao = renderer::createSharedVAO();

    LOG_INFO << "Merging meshes ..." << std::endl;
    renderer::addToSharedVAO(shared_vao, all_meshes);
  }

  m_new_meshes.clear();
}


} // namespace il2ge::core
