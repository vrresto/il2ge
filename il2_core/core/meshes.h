#ifndef CORE_MESHES_H
#define CORE_MESHES_H

#include <il2ge/mesh/mesh_base.h>
#include <il2ge/object_map.h>

namespace il2ge::core
{


class FObjects;


class MeshInstanceCache
{
public:
  const il2ge::mesh::MeshInstance* get(const std::string& path);
  void insert(std::unique_ptr<il2ge::mesh::MeshInstance>&&, const std::string& path);
};


class Meshes
{
  FObjects& m_fobjects;
  il2ge::CachedAutoAddingWeakMapWithFactory<const il2ge::mesh::MeshBase> m_meshes;
  std::vector<std::weak_ptr<const il2ge::mesh::MeshBase>> m_new_meshes;

  // std::unordered_map<std::string, std::weak_ptr<il2ge::mesh::MeshBase>> meshes;
  // MeshInstanceCache m_instance_cache;

public:
  Meshes(FObjects&);

  // std::shared_ptr<const il2ge::mesh::MeshBase> getMesh(std::string path);

  std::unique_ptr<il2ge::mesh::MeshInstance> createInstance(const std::string& path);
  std::unique_ptr<il2ge::mesh::HierMeshBase> createHierMesh(const std::string& path);

  void createSharedVAO();
};


}


#endif
