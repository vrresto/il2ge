#include <core.h>
#include <core/objects.h>
#include <main_thread.h>
#include <util.h>

#include <cassert>
#include <queue>


using namespace il2ge::core;


namespace il2ge::core
{
  extern std::vector<const GObjType*> g_all_types; //FIXME HACK
}


namespace
{


uint64_t g_magic = 0;
std::unordered_map<std::string, const GObjType*> g_types;
int g_total_objects = 0;


void printGObjInfo(const GObjType& type)
{
  LOG_ERROR << "Class: " << type.getCppClassName()
            << ", Java class: " << type.getJavaClassName() << std::endl;
}


FObjRef createFObj(const std::string& path)
{
  LOG_DEBUG << "path: " << path << std::endl;

  std::string class_name;

  const auto &params = getParameterFile(path);
  auto &class_info = params.getSection("ClassInfo");
  class_name = class_info.get("ClassName");

  LOG_DEBUG << "class_name: " << class_name << std::endl;

  auto& type = GObjType::get(class_name);

  auto object = type.getFactory()(path);
  assert(object->getType() == type);
  assert(object->isFObj());
  return std::static_pointer_cast<FObj>(object);
}


} // namespace


namespace il2ge::core
{


const GObjType& GObjType::get(const std::string& class_name)
{
  assert(!class_name.empty());

  LOG_TRACE << "class_name: " << class_name << std::endl;

  try
  {
    return *g_types.at(class_name);
  }
  catch (...)
  {
    throw GObj::ClassNotFoundError(class_name);
  }
}


void GObjType::add(const GObjType& type)
{
  LOG_INFO << "java class: " << type.getJavaClassName() << std::endl;
  LOG_INFO << "c++ class: " << type.getCppClassName() << std::endl;

  assert(!type.getCppClassName().empty());

  auto& entry = g_types[type.getCppClassName()];
  assert(!entry);

  entry = &type;
}


void GObjType::remove(const GObjType& type)
{
  if (!type.getCppClassName().empty())
  {
    auto it = g_types.find(type.getCppClassName());
    if (it != g_types.end())
    {
      assert(it->second == &type);
      g_types.erase(it);
    }
  }

  bool was_erased = false;
  for (auto it = g_all_types.begin(); it != g_all_types.end(); it++)
  {
    if (*it == &type)
    {
      g_all_types.erase(it);
      was_erased = true;
      break;
    }
  }
  assert(was_erased);
}


const GObjType::Factory& GObjType::getFactory() const
{
  assert(m_factory);
  return m_factory;
}


GObj::GObj(const GObjType& type, bool is_fobj) : m_type(type), m_is_fobj(is_fobj)
{
  assert(isMainThread());
  g_total_objects++;
  getType().objectCreated();
}


GObj::~GObj()
{
  getType().objectDestroyed();
  assert(m_java_objects == 0);
  g_total_objects--;
}


void GObj::onJavaObjectCreate()
{
  assert(m_java_objects >= 0);
  m_java_objects++;
}


void GObj::onJavaObjectDestroy()
{
  assert(m_java_objects >= 1);
  m_java_objects--;
}


void GObj::onObjectHandleTableDestroy()
{
  if (m_java_objects)
  {
    LOG_WARNING << m_java_objects
                << " java objects are still referencing this object of type "
                << getType().getJavaClassName()
                << " (" << getType().getCppClassName() << ")"
                << std::endl;
  }

  m_java_objects = 0;
}


void GObj::printStats(std::ostream& out)
{
  out << "Total objects: " << g_total_objects << std::endl;
  for (auto& it : g_all_types)
  {
    auto& type = *it;
    out << type.getJavaClassName();
    if (!type.getCppClassName().empty())
      out << " (" << type.getCppClassName() << ")";
    out << ": " << type.getObjectCount() << std::endl;
  }
}


// ---- FObjects ----


FObjects::FObjects() : m_objects(2000)
{
}


GObjRef FObjects::getRef(const std::string& path_)
{
  auto path = util::resolveRelativePathComponents(path_);

  auto factory = [&] (auto& name)
  {
    assert(name == path);

    auto object = createFObj(path);
    assert(object->getPath() == path);

    return object;
  };

  return m_objects.get(path, factory);
}


FObj::FObj(const GObjType& type, const std::string& path) :
  GObj(type, true),
  Named(path),
  m_path(path)
{
}


FObj::~FObj()
{
}


} // namespace il2ge::core
