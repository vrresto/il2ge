/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2021 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "meshes.h"
#include <core/context.h>
#include <core/scene.h>
#include <core/object_handle_manager.h>
#include <il2ge/renderer.h>
#include <render_util/text_display.h>
#include <render_util/shader_util.h>


namespace il2ge::core
{


Context* Context::s_current = nullptr;


Context::Context()
{
  m_text_renderer = std::make_unique<TextRenderer>();
  m_fobjects = std::make_unique<core::FObjects>();
  m_meshes = std::make_unique<core::Meshes>(*m_fobjects);
  m_scene = std::make_unique<core::Scene>(getTextRenderer());
  m_object_handle_manager = std::make_unique<ObjectHandleManager>();

  auto create_shader_program = [&] (auto name, auto params)
  {
    auto merged_params = m_scene->getShaderParameters();
    merged_params.add(params);
    return render_util::createShaderProgram(name,
                                            m_scene->getShaderSearchPath(),
                                            merged_params);
  };

  m_renderer = std::make_unique<il2ge::renderer::Renderer>(create_shader_program);
}


Context::~Context() {}


} // namespace il2ge::core
