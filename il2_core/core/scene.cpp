/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core_p.h"
#include "map.h"
#include <sfs.h>
#include <configuration.h>
#include <core_config.h>
#include <il2_jni.h>
#include <core/scene.h>
#include <il2ge/mesh/mesh_base.h>
#include <il2ge/renderer.h>
#include <render_util/texture_util.h>
#include <render_util/text_display.h>
#include <render_util/render_util.h>
#include <render_util/texunits.h>
#include <render_util/shader_util.h>
#include <render_util/state.h>

#include <string>
#include <memory>
#include <GL/gl.h>

#include <render_util/gl_binding/gl_functions.h>

using namespace render_util::gl_binding;
using namespace render_util;
using namespace std;

#include <render_util/skybox.h>


namespace il2ge::core
{

  class SkyBox
  {
  public:
    SkyBox(render_util::ShaderSearchPath shader_search_path,
           render_util::ShaderParameters shader_parameters)
    {
      shader_parameters.set("enable_atmosphere", true); //FIXME

      m_program =
        render_util::createShaderProgram("sky",
                                         shader_search_path,
                                         shader_parameters);
    }

    template <class T>
    void draw(T update_uniforms)
    {
      update_uniforms(*m_program);

      render_util::StateModifier state;
      state.setProgram(m_program->getID());

      render_util::drawSkyBox();
    }

  private:
    render_util::ShaderProgramPtr m_program;
  };


  Scene::Scene(TextRenderer& text_renderer)
  {
    m_overlay = std::make_unique<render_util::TextDisplay>(m_shader_search_path);
    m_skybox = std::make_unique<SkyBox>(m_shader_search_path, m_shader_parameters);
  }


  Scene::~Scene()
  {
    unloadMap();
  }


  void Scene::unloadMap()
  {
    map.reset();
    gl::Finish();
  }

  void Scene::loadMap(const char *path, ProgressReporter *progress)
  {
    unloadMap();

    LOG_INFO << "Loading map: " << path << std::endl;

    map = make_unique<Map>(path, progress, m_shader_search_path, m_shader_parameters,
                           getMaxCirrusOpacity());
  }


  void Scene::update(float delta, const glm::vec2 &wind_speed)
  {
    if (map)
      map->update(delta);
  }


  void Scene::updateUniforms(render_util::ShaderProgram& program)
  {
    m_atmosphere->setUniforms(program);
    if (map)
      map->setUniforms(program);
  }


  render_util::TerrainBase &Scene::getTerrain()
  {
    assert(map);
    return map->getTerrain();
  }


  render_util::ImageGreyScale::ConstPtr Scene::getPixelMapH()
  {
    assert(map);
    return map->getPixelMapH();
  }


  render_util::CirrusClouds *Scene::getCirrusClouds()
  {
    assert(map);
    return map->getCirrusClouds();
  }


  const render_util::ElevationMap& Scene::getHeightMap()
  {
    assert(map);
    return map->getHeightMap();
  }


  void Scene::drawSkyBox(const render_util::Camera& camera)
  {
    m_skybox->draw([&] (auto& program)
      {
        core::updateUniforms(program); //FIXME
        render_util::updateUniforms(program, camera);
      });
  }

} // namespace il2ge::core
