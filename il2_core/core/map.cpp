#include "map.h"
#include "core_p.h"


namespace il2ge::core
{


Map::Map(const char* path, ProgressReporter* progress,
         const render_util::ShaderSearchPath& shader_search_path,
         const render_util::ShaderParameters& shader_params,
         float max_cirrus_opacity) :
  MapBase(shader_params)
{
  progress->report(0, "Creating terrain");

  auto ctx = createMapLoaderContext(path);

  m_heightmap = ctx.terrain_resources->getDetailLayer().getHeightMap();

  createTerrain(ctx, shader_search_path);

  progress->report(10, "task.Load_landscape", false);
}


Map::~Map()
{
}


} // namespace il2ge::core

