#include <core/object_handle_manager.h>
#include <util/random_integer_generator.h>


namespace il2ge::core
{


uint64_t DebugObjectHandleManager::s_magic = 0;
std::unordered_set<CppObjectHandle> DebugObjectHandleManager::s_all_handles;


uint64_t DebugObjectHandleManager::getMagic()
{
  if (s_magic == 0)
  {
    util::RandomIntegerGenerator<uint64_t> gen;
    s_magic = gen();
    assert(s_magic != 0);
  }

  return s_magic;
}


ObjectHandleManagerWithTable::~ObjectHandleManagerWithTable()
{
  LOG_DEBUG << "Handle count: " << m_handle_count << std::endl;

  for (auto& obj : m_table)
  {
    if (obj)
      obj->onObjectHandleTableDestroy();
  }
}


CppObjectHandle ObjectHandleManagerWithTable::create(const GObjRef& obj)
{
  assert(isMainThread());
  assert(m_free_handles.size() + m_handle_count == m_table.size());

  if (m_free_handles.size() < MIN_FREE_HANDLE_COUNT)
    resizeTable();

  assert(!m_free_handles.empty());
  auto handle = m_free_handles.front();
  m_free_handles.pop();

  assert(!m_table.at(handle));
  m_table.at(handle) = obj;

  m_handle_count++;

  return handle;
}


void ObjectHandleManagerWithTable::resizeTable()
{
  size_t new_table_size = 1024;
  while (true)
  {
    if (new_table_size >= m_table.size())
    {
      assert(m_handle_count < new_table_size);
      auto new_free_handle_count = new_table_size - m_handle_count;
      if (new_free_handle_count > MIN_FREE_HANDLE_COUNT)
        break;
    }
    new_table_size *= 2;
  }

  if (new_table_size > m_table.size())
  {
    for (auto i = m_table.size(); i < new_table_size; i++)
      m_free_handles.push(i);
    m_table.resize(new_table_size);
  }

  assert(m_free_handles.size() + m_handle_count == m_table.size());
}


void ObjectHandleManagerWithTable::printStats(std::ostream& out)
{
  out << "Handle table size: " << m_table.size() << std::endl;
  out << "Object handles: " << m_handle_count << std::endl;
}


} // namespace il2ge::core
