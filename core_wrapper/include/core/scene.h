/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CORE_SCENE_H
#define CORE_SCENE_H

#include <scene_base.h>
#include <misc.h>
#include <core.h>
#include <render_util/camera.h>
#include <text_renderer/text_renderer.h>
#include <render_util/cirrus_clouds.h>
#include <render_util/viewer/parameter_base.h>
#include <render_util/viewer/parameters_base.h>

#include <memory>
#include <vector>
#include <cassert>


namespace render_util
{
  class CirrusClouds;
}


namespace core
{
  class ProgressReporter;
  class Menu;
  class Map;
  class Overlay;


  class Scene : public il2ge::core_common::SceneBase
  {
  private:
    std::unique_ptr<Map> map;
    std::unique_ptr<TextRenderer> text_renderer; //FIXME move to core::Context
    std::unique_ptr<Overlay> m_overlay;

    void refreshPrameters();

  public:
    Scene();
    ~Scene();

    void unloadMap();
    void loadMap(const char *path, ProgressReporter*);
    void update(float delta);
    void updateUniforms(render_util::ShaderProgram& program);
    render_util::TerrainBase &getTerrain();

    render_util::CirrusClouds *getCirrusClouds();

    render_util::ImageGreyScale::ConstPtr getPixelMapH();

    TextRenderer &getTextRenderer() { return *text_renderer; }

    Overlay& getOverlay() { assert(m_overlay); return *m_overlay; }

    bool isMapLoaded() { return map != nullptr; }
  };
};

#endif
