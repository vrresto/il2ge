/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_CORE_WRAPPER_CORE_CONTEXT_H
#define IL2GE_CORE_WRAPPER_CORE_CONTEXT_H

#include <memory>
#include <cassert>


namespace core
{


class Scene;


class Context
{
  std::unique_ptr<core::Scene> m_scene;

  static Context *s_current;

public:
  Context();
  ~Context();

  core::Scene& getScene()
  {
    assert(m_scene);
    return *m_scene;
  }

  static void setCurrent(Context *c) { s_current  = c; }
  static Context& getCurrent() { return *s_current; }
};


inline Context& getContext()
{
  return Context::getCurrent();
}


} // namespace core

#endif
