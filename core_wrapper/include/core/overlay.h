/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_CORE_WRAPPER_CORE_OVERLAY_H
#define IL2GE_CORE_WRAPPER_CORE_OVERLAY_H

#include <render_util/text_display.h>
#include <render_util/state.h>
#include <text_renderer/text_renderer.h>

#include <type_traits>
#include <string>


namespace core
{


class Overlay
{
  TextRenderer& m_text_renderer;
  render_util::TextDisplay m_display;
  bool m_is_shown = false;

public:

  Overlay(TextRenderer& text_renderer,
          render_util::ShaderSearchPath shader_seach_path) :
    m_text_renderer(text_renderer),
    m_display(shader_seach_path)
  {
  }

  void clear()
  {
    m_display.clear();
  }

  void draw()
  {
    render_util::StateModifier state;
    state.setDefaults();
    state.enableBlend(true);
    m_display.draw(m_text_renderer, 0, 0);
  }

  void toggle()
  {
    m_is_shown = !m_is_shown;
  }

  bool isShown() { return m_is_shown; }

  void addText(const std::string& text)
  {
    m_display.addText(text);
  }

  template <typename T>
  void addLine(const std::string& title, const T& value)
  {
    if constexpr (std::is_same<typename std::decay<T>::type, std::string>::value)
      m_display.addLine(title + ": " + value);
    else
      m_display.addLine(title + ": " + std::to_string(value));
  }
};


} // namespace core

#endif
