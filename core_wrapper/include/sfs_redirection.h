#pragma once

#include <string>
#include <vector>


namespace sfs
{


bool readFile(const std::string &filename, std::vector<char> &out);
void redirect(__int64 hash, __int64 hash_redirection);
void clearRedirections();
void *get_openf_wrapper();


} // namespace sfs

