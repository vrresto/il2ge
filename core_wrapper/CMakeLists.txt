include_directories(
  include
  ${CMAKE_CURRENT_BINARY_DIR}/include
  ${CMAKE_CURRENT_BINARY_DIR}
)


set(library_name "il2ge")


set(SRCS
  main/init.cpp
  main/core_wrapper.cpp
  main/wgl_wrapper.cpp
  sfs/sfs_wrapper.cpp
  core/java_interface.cpp
  core/context.cpp
  core/core.cpp
  core/map.cpp
  core/render_state.cpp
  core/scene.cpp
  jni_wrapper/jni_wrapper.cpp
  gl_wrapper/gl_wrapper_main.cpp
  gl_wrapper/texture_state.cpp
  gl_wrapper/arb_program.cpp
)

set(JNI_WRAPPER_CLASSES
  rts.Time
  il2.engine.RenderContext
  il2.engine.Camera
  il2.engine.Landscape
  il2.engine.Render
  il2.engine.Renders
)


add_executable(jni_generator jni_wrapper/generate_meta_code.cpp)
target_link_libraries(jni_generator render_util_util)

set(generator_cmd $<TARGET_FILE:jni_generator>)


if(platform_mingw)
  if(NOT CMAKE_HOST_WIN32)
    set(generator_cmd wine ${generator_cmd})
  endif(NOT CMAKE_HOST_WIN32)
endif(platform_mingw)

set(jni_generator_output_dir ${CMAKE_CURRENT_BINARY_DIR}/_generated/jni_wrapper)

foreach(class_name ${JNI_WRAPPER_CLASSES})
  set(SRCS ${SRCS} jni_wrapper/wrap_${class_name}.cpp)
  set(jni_wrapper_classes_string "${class_name} ${jni_wrapper_classes_string}")

  foreach(cmd definitions registration)
    set(output ${jni_generator_output_dir}/${class_name}_${cmd})

    add_custom_command(
        OUTPUT ${output}
        COMMAND mkdir -p ${jni_generator_output_dir}
        COMMAND ${generator_cmd} ${cmd} ${class_name}
          < ${CMAKE_CURRENT_SOURCE_DIR}/jni_wrapper/signatures
          > ${output}

        DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/jni_wrapper/signatures
        DEPENDS jni_generator
    )
    set(generated_output ${generated_output} ${output})
  endforeach(cmd)
endforeach(class_name)

foreach(name table definitions)
  set(output ${jni_generator_output_dir}/registrator_${name})
  add_custom_command(
      OUTPUT ${output}
      COMMAND mkdir -p ${jni_generator_output_dir}
      COMMAND ${generator_cmd} registrator-${name} ${jni_wrapper_classes_string} > ${output}
      DEPENDS jni_generator
  )
  set(generated_output ${generated_output} ${output})
endforeach(name)

add_custom_target(class-wrappers
  COMMAND mkdir -p ${jni_generator_output_dir}/class_wrappers
  COMMAND ${generator_cmd} class-wrappers ${jni_generator_output_dir}/class_wrappers
    < ${CMAKE_CURRENT_SOURCE_DIR}/jni_wrapper/signatures
  DEPENDS jni_generator
)

add_custom_target(class-wrappers-all
  COMMAND mkdir -p ${jni_generator_output_dir}/class_wrappers
  COMMAND ${generator_cmd} class-wrappers ${jni_generator_output_dir}/class_wrappers
    < ${CMAKE_CURRENT_SOURCE_DIR}/jni_wrapper/all_signatures
  DEPENDS jni_generator
)

add_custom_target(class-stubs
  COMMAND mkdir -p ${jni_generator_output_dir}/class_stubs
  COMMAND ${generator_cmd} class-stubs ${jni_generator_output_dir}/class_stubs
    < ${CMAKE_CURRENT_SOURCE_DIR}/jni_wrapper/all_signatures
  DEPENDS jni_generator
)

add_custom_target(core_wrapper_generated DEPENDS ${generated_output})

add_library(${library_name}_static ${SRCS})

add_dependencies(${library_name}_static core_wrapper_generated)

target_link_libraries(${library_name}_static
  il2_core_common
  common
  render_util
)
