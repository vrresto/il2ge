/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <sfs_redirection.h>
#include <util.h>

#include <iostream>
#include <string>
#include <unordered_map>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <windef.h>
#include <winbase.h>

using std::cout;
using std::endl;
using std::unordered_map;

// #define SFS_API __cdecl
#define SFS_API __stdcall


namespace
{


unordered_map<__int64, __int64> g_redirections;


#if ENABLE_SFS_OPENF_WRAPPER
int __cdecl wrap_SFS_openf(unsigned __int64 hash, int flags)
{
//   auto it = g_redirections.find(hash);
//   if (it != g_redirections.end())
//     hash = it->second;

  if (g_sas_openf_func)
    return g_sas_openf_func(hash, flags); // call new "routing" method __SAS_openf if available

  assert(g_openf_func);

  return g_openf_func(hash, flags); // fallback in case old wrapper.dll was loaded
}
#endif


} // namespace


namespace sfs
{


void redirect(__int64 hash, __int64 hash_redirection)
{
  g_redirections[hash] = hash_redirection;
}

void clearRedirections()
{
  cout<<"SFS wrapper: clearing "<<g_redirections.size()<<" redirections."<<endl;
  g_redirections.clear();
}


#if ENABLE_SFS_OPENF_WRAPPER
void *get_openf_wrapper()
{
  sfs::init();
  return (void*) &wrap_SFS_openf;
}
#endif


} // namespace sfs
