/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "core_p.h"
#include "map.h"
#include <sfs_redirection.h>
#include <configuration.h>
#include <core_config.h>
#include <core/scene.h>
#include <core/overlay.h>
#include <render_util/terrain_base.h>
#include <render_util/texture_util.h>
#include <render_util/render_util.h>
#include <render_util/texunits.h>

#include <string>
#include <memory>
#include <GL/gl.h>

#include <render_util/gl_binding/gl_functions.h>

using namespace render_util::gl_binding;
using namespace render_util;
using namespace std;


namespace core
{

  Scene::Scene()
  {
    text_renderer = make_unique<TextRenderer>();
    m_overlay = std::make_unique<Overlay>(*text_renderer, m_shader_search_path);
    refreshPrameters();
  }


  Scene::~Scene()
  {
    unloadMap();
  }


  void Scene::unloadMap()
  {
    m_parameters.clear();
    map.reset();
    gl::Finish();
    sfs::clearRedirections();

    refreshPrameters();
  }


  void Scene::loadMap(const char *path, ProgressReporter *progress)
  {
    printf("load map: %s\n", path);

    unloadMap();

    map = make_unique<Map>(path, progress, m_shader_search_path, m_shader_parameters,
                           getMaxCirrusOpacity());

    refreshPrameters();
  }


  void Scene::refreshPrameters()
  {
    m_parameters.clear();
    addParameters();
    if (map)
      map->addParameters(m_parameters);
  }


  void Scene::update(float delta)
  {
    if (map)
      map->update(delta);
  }


  void Scene::updateUniforms(render_util::ShaderProgram& program)
  {
    m_atmosphere->setUniforms(program);
    if (map)
      map->setUniforms(program);
  }


  render_util::TerrainBase &Scene::getTerrain()
  {
    assert(map);
    return map->getTerrain();
  }


  render_util::ImageGreyScale::ConstPtr Scene::getPixelMapH()
  {
    assert(map);
    return map->getPixelMapH();
  }


  render_util::CirrusClouds *Scene::getCirrusClouds()
  {
    assert(map);
    return map->getCirrusClouds();
  }

}
