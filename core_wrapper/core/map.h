/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2023 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <map_base.h>

#include <il2ge/map_loader.h>
#include <il2ge/map_resources.h>
#include <render_util/image.h>
#include <render_util/shader.h>


#include <glm/glm.hpp>
#include <memory>


namespace core
{


class ProgressReporter;



class Map : public il2ge::core_common::MapBase
{
public:
  Map(const char *path, ProgressReporter*,
      const render_util::ShaderSearchPath&,
      const render_util::ShaderParameters&,
      float max_cirrus_opacity);
  ~Map();
};


} // namespace core
