
#include "core_wrapper.h"
#include "wgl_wrapper.h"
#include <misc.h>
#include <il2ge/exception_handler.h>
#include <log.h>

#include <windows.h>

#include <core_interface.h>
#include "wgl_wrapper_p.h"


namespace
{


HMODULE loadCoreWrapper(const char *core_library_filename)
{
  HMODULE core_module = LoadLibraryA(core_library_filename);
  if (!core_module)
  {
    LOG_ERROR << "Loading " << core_library_filename << " failed with error "
              << GetLastError() << std::endl;
    LOG_FLUSH;
    abort();
  }

  il2ge::exception_handler::blacklistModule(core_module);

  il2ge::core_common::installIATPatches(core_module);
  jni_wrapper::resolveImports(reinterpret_cast<void*>(core_module));

  return core_module;
}


struct Interface : public il2ge::core_common::CoreInterface
{
  HMODULE m_il2ge_module {};
  HMODULE m_mod {};
  bool m_is_initialized = false;

  Interface(HMODULE il2ge_module) : m_il2ge_module(il2ge_module)
  {
    jni_wrapper::init();
  }

  void load(const char* name) override
  {
    assert(!m_mod);
    m_mod = loadCoreWrapper(name);
  }

  bool isLoaded() override
  {
    return m_mod;
  }

  void init() override
  {
    assert(!m_is_initialized);
    assert(isLoaded());
    il2ge::core_wrapper::init();
    m_is_initialized = true;
  }

  bool isInitialized() override
  {
    return m_is_initialized;
  }

  HMODULE getModuleHandle() override
  {
    assert(isLoaded());
    return m_mod;
  }

  void* getExeProcAddress(const char* name) override
  {
#if ENABLE_SFS_OPENF_WRAPPER
    if (_stricmp(name, "__SFS_openf") == 0)
      return sfs::get_openf_wrapper();
#endif
    return nullptr;
  }

  void resolveImports(HMODULE mod) override
  {
    jni_wrapper::resolveImports(mod);
  }

  void* getJNIProcAddress(const char* name) override
  {
    return jni_wrapper::getExport(name);
  }

  HMODULE getGLModuleHandle() override
  {
    return il2ge::core_wrapper::wgl_wrapper::getGLModule();
  }

  void* getGLProcAddress(const char* name) override
  {
    return il2ge::core_wrapper::wgl_wrapper::getProcAddress(name);
  }
};


} // namespace


namespace il2ge::core_wrapper
{


il2ge::core_common::CoreInterface* createCoreInterface(HMODULE il2ge_module)
{
  return new Interface(il2ge_module);
}


} // namespace il2ge::core_wrapper
