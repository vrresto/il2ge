# Changelog

## [Unreleased]

### Fixed

- Terrain LOD gaps (#10)
- CTD at '3% creating textures' on certain maps (#18)
- No ice cover on water in some maps (#22)

## [0.3.0]

### Fixed
- Installer unnecessarily requests admin privileges (#50)
- Crash with game version 4.15 (thanks to SAS_Storebror) (#44)
- Terrain outside of the map borders is always covered by water.  
  The new behaviour is to repeat the textures at the map border like IL-2 does.

## [0.2.0]

### Fixed
- Bombsight has wrong color (#20)
- Shadows on water have wrong color (#25)
- Lines are displayed on water and land (#14)

## [0.1.0]

- First release
