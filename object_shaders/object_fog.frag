#version 330

vec3 fogAndToneMap(vec3 color)
{
  return color;
}


void fogAndToneMap(in vec3 in_color0, in vec3 in_color1,
                   out vec3 out_color0, out vec3 out_color1)
{
  out_color0 = in_color0;
  out_color1 = in_color1;
}

