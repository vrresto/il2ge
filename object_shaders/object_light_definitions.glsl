// void getReflectedObjectLight(in LightParams light, vec3 normal, out vec3 direct, out vec3 ambient);
void getReflectedObjectLight(vec3 pos, vec3 normal, out vec3 direct, out vec3 ambient);
