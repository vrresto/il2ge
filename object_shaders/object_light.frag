#version 330

void getReflectedObjectLight(vec3 pos, vec3 normal, out vec3 direct, out vec3 ambient)
{
  direct = vec3(1);
  ambient = vec3(0.3);
}
