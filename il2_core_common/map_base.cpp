#include "map_base.h"
#include <sfs.h>
#include <il2ge/map_loader.h>
#include <render_util/terrain_util.h>
#include <render_util/viewer/parameterizable.h>
#include <render_util/viewer/value_wrapper.h>
#include <render_util/viewer/boolean_parameter.h>
#include <render_util/viewer/parameters_base.h>


using namespace render_util;


namespace
{
  // const bool g_terrain_use_lod = true;
  // const string dump_base_dir = "il2ge_dump/"; //HACK


class SFSResource : public il2ge::SFSResourceBase
{
public:
  SFSResource(const std::string& path) : SFSResourceBase(path) {}

  std::unique_ptr<util::File> open() const override
  {
    return std::make_unique<sfs::File>(getPath());
  }
};


} // namespace


namespace il2ge::core_common
{


MapLoaderContext MapBase::createMapLoaderContext(const char* path)
{
  MapLoaderContext ctx;

  // const bool enable_base_map = false;//il2ge::core::getConfig().enable_base_map;
  // const bool enable_normal_maps = il2ge::core::getConfig().enable_bumph_maps;

  std::string ini_path = path;

  auto map_dir = util::getDirFromPath(ini_path);
  assert(!map_dir.empty());
  map_dir += '/';

  const std::string dump_base_dir = "il2ge_dump/"; //FIXME

  auto dump_dir = dump_base_dir + util::makeLowercase(ini_path);
  auto dump_map_dir = dump_base_dir + map_dir;

  if (il2ge::map_loader::isDumpEnabled())
  {
    util::mkdir(dump_map_dir.c_str(), true);
    util::mkdir(dump_dir.c_str(), true);
  }

  map_dir = std::string("maps/") + map_dir;
  ini_path = std::string("maps/") + path;

  auto get_resource = [] (std::string path)
  {
    return std::make_unique<SFSResource>(path);
  };

  ctx.map_resources = getSfsMapResources(map_dir, ini_path, dump_dir, get_resource);

  // progress->report(0, "Creating terrain");

  ctx.terrain_resources = map_loader::getTerrainResources(*ctx.map_resources);

  return ctx;
}


MapBase::MapBase(const render_util::ShaderParameters& shader_params) :
  m_shader_params(shader_params)
{
}


//FIXME - duplicated in TerrainViewerSceneBase
render_util::ShaderParameters MapBase::makeShaderParameters()
{
  auto params = m_shader_params;

  for (auto& it : m_shader_options)
    params.set<bool>(it.name, it.value);

  params.set("enable_atmosphere", false);
  params.set("enable_curvature", false);
  params.set("terrain_enable_detail_layer", true);

  return params;
}


void MapBase::update(float delta)
{
  getTerrain().updateAnimation(delta);
}


void MapBase::setUniforms(render_util::ShaderProgram&)
{
  //FIXME - remove
  // program.setUniform("map_size", p->size);
  // program.setUniform("terrain_height_offset", 0.f);
}


void MapBase::addParameters(render_util::viewer::ParametersBase& params)
{
  auto iface = dynamic_cast<render_util::viewer::IParameterizable*>(m_terrain.get());
  if (iface)
    iface->addParameters(params);

  //FIXME - duplicated in render_util::viewer::TerrainViewerSceneBase
  auto add_shader_option = [&] (std::string name, bool& option)
  {
    viewer::ValueWrapper<bool> wrapper =
    {
      .get = [&option] ()
      {
        return option;
      },
      .set = [this, &option] (bool value)
      {
        if (value != option)
        {
          option = value;

          m_terrain->setShaderParameters(makeShaderParameters());
          m_terrain->reloadShaders();
        }
      },
    };
    using Param = viewer::BooleanParameter<viewer::ValueWrapper<bool>>;
    params.add(std::make_unique<Param>(name, wrapper, option));
  };

  //FIXME - duplicated in render_util::viewer::TerrainViewerSceneBase
  for (auto& option : m_shader_options)
    add_shader_option(option.display_name, option.value);

  //FIXME - duplicated in render_util::viewer::TerrainViewerSceneBase
  auto add_terrain_module = [&] (std::string name, int mask)
  {
    viewer::ValueWrapper<bool> wrapper =
    {
      .get = [this, mask] ()
      {
        return m_terrain->getEnabledModules() & mask;
      },
      .set = [this, mask] (bool enable)
      {
        auto modules = m_terrain->getEnabledModules();
        if (enable)
          modules |= mask;
        else
          modules &= ~mask;

        if (m_terrain->getEnabledModules() != modules)
        {
          m_terrain->setEnabledModules(modules);
          m_terrain->reloadShaders();
        }
      },
    };
    using Param = viewer::BooleanParameter<viewer::ValueWrapper<bool>>;
    params.add(std::make_unique<Param>(name,
                                       wrapper,
                                       m_terrain->getEnabledModules() & mask));
  };

  //FIXME - duplicated in render_util::viewer::TerrainViewerSceneBase
  add_terrain_module("Land", TerrainBase::ModuleMask::LAND);
  add_terrain_module("Water", TerrainBase::ModuleMask::WATER);
  add_terrain_module("Forest", TerrainBase::ModuleMask::FOREST);
}


void MapBase::createTerrain(const MapLoaderContext& ctx,
                            const render_util::ShaderSearchPath& shader_search_path)
{
  m_pixel_map_h = map_loader::createPixelMapH(ctx.map_resources.get());
  assert(m_pixel_map_h);

  m_terrain = render_util::createTerrain(shader_search_path);

  render_util::TerrainBase::BuildParameters params =
  {
    .resources = *ctx.terrain_resources,
    .shader_parameters = makeShaderParameters(),
  };

  params.load_modules |= render_util::TerrainBase::ModuleMask::RENDERER;

  params.load_modules |= render_util::TerrainBase::ModuleMask::LAND;
  params.load_modules |= render_util::TerrainBase::ModuleMask::WATER;
  params.load_modules |= render_util::TerrainBase::ModuleMask::FOREST;

  params.enabled_modules |= render_util::TerrainBase::ModuleMask::RENDERER;

  params.enabled_modules |= render_util::TerrainBase::ModuleMask::LAND;
  // params.enabled_modules |= render_util::TerrainBase::ModuleMask::WATER;
  params.enabled_modules |= render_util::TerrainBase::ModuleMask::FOREST;

  // progress->report(5, "Creating terrain");

  m_terrain->build(params);

  // if (il2ge::core::getConfig().enable_cirrus_clouds)
  // {
  //   auto cirrus_texture = map_loader::createCirrusTexture(res_loader);
  //   if (cirrus_texture)
  //   {
  //     cirrus_clouds = std::make_unique<render_util::CirrusClouds>(max_cirrus_opacity,
  //         core::textureManager(), shader_search_path, shader_params, 7000, cirrus_texture);
  //   }
  // }
}


render_util::CirrusClouds* MapBase::getCirrusClouds()
{
  UNIMPLEMENTED
}


} // namespace il2ge::core_common
