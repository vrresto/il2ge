#pragma once

#include <il2ge/map_resources.h>
#include <render_util/image.h>
#include <render_util/shader.h>
#include <render_util/terrain_resources.h>

#include <memory>


namespace render_util
{
  class TerrainBase;
  class CirrusClouds;
}


namespace render_util::viewer
{
  struct ParametersBase;
}


namespace il2ge::core_common
{


struct MapLoaderContext
{
  std::unique_ptr<MapResources> map_resources;
  std::unique_ptr<render_util::TerrainResourcesBase> terrain_resources;
};


class MapBase
{
  //FIXME - duplicated in render_util::viewer::TerrainViewerSceneBase
  struct ShaderOption
  {
    std::string display_name;
    std::string name;
    bool value {};
  };

  std::shared_ptr<render_util::TerrainBase> m_terrain; // FIXME - make unique_ptr
  render_util::ImageGreyScale::ConstPtr m_pixel_map_h;
  render_util::ShaderParameters m_shader_params;

  //FIXME - duplicated in render_util::viewer::TerrainViewerSceneBase
  std::vector<ShaderOption> m_shader_options  =
  {
    { "Far texture", "terrain_enable_far_texture", true },
    { "Near texture", "terrain_enable_near_texture", true},
    { "Base layer type map", "terrain_enable_base_layer_type_map", false },
    { "Altitudinal zone textures", "terrain_enable_altitudinal_zone_texture", false },
    { "Debug LOD", "terrain_enable_lod_debug", false },
    { "Enable terrain layer blending", "enable_terrain_layer_blend", false },
    { "Enable detail map clip", "enable_detail_map_clip", false },
    { "Use pass_normal", "terrain_use_pass_normal", false },
    { "TexelFetch height map", "terrain_texelfetch_heightmap", false },
    { "Land texture offset", "terrain_enable_land_texture_offset", false },
    { "Forest layers", "terrain_enable_forest_pass", false },
    { "Forest alpha map workaround", "terrain_forest_enable_alpha_map_precision_workaround", false },
    { "Forest layer separate alpha", "terrain_forest_layers_separate_alpha", false },
    { "Water map pass", "terrain_enable_water_map_pass", false },
    { "Wave parallax mapping", "terrain_water_enable_parallax", false },
    { "Water map smooth sampling", "terrain_water_map_smooth_sampling", false },
    { "Shore waves", "terrain_enable_shore_waves", false },
  };


protected:
  MapBase(const render_util::ShaderParameters&);

  // void createTerrain(const render_util::ShaderSearchPath&);
  void createTerrain(const MapLoaderContext&, const render_util::ShaderSearchPath&);
  render_util::ShaderParameters makeShaderParameters();

  static MapLoaderContext createMapLoaderContext(const char* path);

public:
  render_util::TerrainBase& getTerrain()
  {
    assert(m_terrain);
    return *m_terrain;
  }

  render_util::ImageGreyScale::ConstPtr getPixelMapH()
  {
    assert(m_pixel_map_h);
    return m_pixel_map_h;
  }

  // glm::vec2 getSize();
  // glm::ivec2 getTypeMapSize();
  render_util::CirrusClouds* getCirrusClouds();

  void setUniforms(render_util::ShaderProgram&);
  void update(float delta);
  void addParameters(render_util::viewer::ParametersBase&);
};


} // namespace il2ge::core_common
