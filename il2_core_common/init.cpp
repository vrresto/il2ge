#include <core_common_log.h>
#include <core_common.h>
#include <configuration_p.h>
#include <configuration.h>
#include <core_interface.h>
#include <main_thread.h>
#include <compiler_version.h>
#include <iat.h>
#include <il2ge/version.h>
#include <il2ge/exception_handler.h>
#include <log.h>
#include <util.h>

#include <jni.h>
#include <atomic>
#include <windows.h>


using namespace il2ge::core_common;


namespace
{


constexpr jint JNI_VERSION = JNI_VERSION_1_2;


decltype(JNI_OnLoad) wrap_il2_core_JNI_OnLoad;
decltype(JNI_OnUnload) wrap_il2_core_JNI_OnUnload;
decltype(LoadLibraryA) wrap_LoadLibraryA_jgl;
decltype(GetProcAddress) wrap_GetProcAddress_jgl;
decltype(LoadLibraryA) wrap_LoadLibraryA;
decltype(GetProcAddress) wrap_GetProcAddress;


bool g_initialized = false;
DWORD g_main_thread = 0;
HMODULE g_il2ge_module = 0;
CoreInterface* g_core_interface {};

std::atomic<bool> g_fatal_error = false;

std::atomic<JavaVM*> g_java_vm = nullptr;
JNIEnv_* g_jni_env = nullptr;


CoreInterface& getCoreInterface()
{
  assert(g_core_interface);
  return *g_core_interface;
}


jint JNICALL wrap_il2_core_JNI_OnLoad(JavaVM* vm, void *reserved)
{
  assert(isMainThread());

  assert(vm);

  g_java_vm = vm;
  (*g_java_vm).GetEnv(reinterpret_cast<void**>(&g_jni_env), JNI_VERSION);
  assert(g_jni_env);

  getCoreInterface().init();

  auto real_proc = reinterpret_cast<decltype(JNI_OnLoad)*>
    (GetProcAddress(getCoreInterface().getModuleHandle(), "JNI_OnLoad"));

  if (real_proc)
  {
    auto ret = real_proc(vm, reserved);
    assert(ret == JNI_VERSION);
    return ret;
  }
  else
  {
    return JNI_VERSION;
  }
}


void wrap_il2_core_JNI_OnUnload(JavaVM *vm, void *reserved)
{
  auto real_proc = reinterpret_cast<decltype(JNI_OnUnload)*>
    (GetProcAddress(getCoreInterface().getModuleHandle(), "JNI_OnUnload"));

  if (real_proc)
    real_proc(vm, reserved);

  g_jni_env = nullptr;
  g_java_vm = nullptr;

  assert(0);
}


HMODULE WINAPI wrap_LoadLibraryA_jgl(LPCSTR libFileName)
{
  LOG_DEBUG << "jgl => LoadLibrary: " << libFileName << std::endl;
  assert(getCoreInterface().isInitialized());

  auto mod = GetModuleHandle(libFileName);

  if (!mod || mod != getCoreInterface().getGLModuleHandle())
  {
    throw std::runtime_error("DirectX mode is not supported.");
  }

  return mod;
}


FARPROC WINAPI wrap_GetProcAddress_jgl(HMODULE mod, LPCSTR name)
{
  LOG_TRACE << "jgl => GetProcAddress: " << name << std::endl;
  assert(getCoreInterface().isInitialized());
  assert(mod == getCoreInterface().getGLModuleHandle());

  return reinterpret_cast<FARPROC>(getCoreInterface().getGLProcAddress(name));
}


HMODULE WINAPI wrap_LoadLibraryA(LPCSTR libFileName)
{
  LOG_INFO << "LoadLibrary: " << libFileName << std::endl;
  LOG_FLUSH;

  auto& core = getCoreInterface();

  auto module_name = util::makeLowercase(util::basename(libFileName, true));

  if (module_name == "il2_core" ||
      module_name == "il2_corep4")
  {
    assert(!GetModuleHandle("jgl.dll"));
    auto jgl_module = LoadLibraryA("jgl.dll");
    assert(jgl_module);

    patchIAT("LoadLibraryA", "kernel32.dll",
             reinterpret_cast<void*>(&wrap_LoadLibraryA_jgl), nullptr, jgl_module);
    patchIAT("GetProcAddress", "kernel32.dll",
             reinterpret_cast<void*>(&wrap_GetProcAddress_jgl), nullptr, jgl_module);

    core.load(libFileName);
    return core.getModuleHandle();
  }

  auto mod = GetModuleHandle(libFileName);

  if (mod)
    return mod;

  assert(!GetModuleHandle((module_name + ".dll").c_str()));

  // these should have been loaded already
  assert(module_name != "jgl");
  assert(module_name != "jvm");

  mod = LoadLibraryA(libFileName);
  assert(mod);

  if (module_name == "dt" ||
      module_name == "hpi" ||
      module_name == "wrapper")
  {
    installIATPatches(mod);
  }

  if (module_name == "dt")
  {
    core.resolveImports(mod);
  }

  return mod;
}


FARPROC WINAPI wrap_GetProcAddress(HMODULE hModule, LPCSTR lpProcName)
{
  auto& core = getCoreInterface();

  std::string proc_name(lpProcName);

  FARPROC addr {};

  if (proc_name == "LoadLibraryA")
  {
    addr = reinterpret_cast<FARPROC>(&wrap_LoadLibraryA);
  }
  else if (proc_name == "GetProcAddress")
  {
    addr = reinterpret_cast<FARPROC>(&wrap_GetProcAddress);
  }
  else if (util::isPrefix("Java_", proc_name) || util::isPrefix("_Java_", proc_name))
  {
    addr = reinterpret_cast<FARPROC>(core.getJNIProcAddress(lpProcName));

    if (!addr)
    {
      addr = GetProcAddress(g_il2ge_module, lpProcName);
    }

    if (!addr && util::isPrefix("_Java_", proc_name))
    {
      addr = GetProcAddress(g_il2ge_module,
                             proc_name.substr(1, strlen(lpProcName) - 1).c_str());
    }

    assert(!addr || core.isInitialized());
  }
  else if (hModule == GetModuleHandle(0))
  {
    addr = reinterpret_cast<FARPROC>(core.getExeProcAddress(lpProcName));
  }
  else if ((core.isLoaded() && hModule == core.getModuleHandle()))
  {
    if (proc_name == "JNI_OnLoad")
      addr = reinterpret_cast<FARPROC>(&wrap_il2_core_JNI_OnLoad);
    else if (proc_name == "JNI_OnUnload")
      addr = reinterpret_cast<FARPROC>(&wrap_il2_core_JNI_OnUnload);
  }

  if (!addr)
    addr = GetProcAddress(hModule, lpProcName);

  return addr;
}


void fatalErrorHandler(const char *msg)
{
  g_fatal_error = true;
  auto env = il2ge::getJNIEnv();
  assert(env);
  env->FatalError(msg);
}


} // namespace


void il2ge::core_common::installIATPatches(HMODULE mod)
{
  patchIAT("LoadLibraryA", "kernel32.dll",
           reinterpret_cast<void*>(&wrap_LoadLibraryA),
           nullptr, mod);

  patchIAT("GetProcAddress", "kernel32.dll",
           reinterpret_cast<void*>(&wrap_GetProcAddress),
           nullptr, mod);

  il2ge::core_common::installLoggerIATPatches(mod);
}


bool il2ge::core_common::isMainThread()
{
  assert(g_main_thread);
  return GetCurrentThreadId() == g_main_thread;
}


bool il2ge::hasFatalError()
{
  return g_fatal_error;
}


JNIEnv_ *il2ge::getJNIEnv()
{
  if (il2ge::core_common::isMainThread())
  {
    return g_jni_env;
  }
  else
  {
    JNIEnv_* env {};
    if (g_java_vm)
      (*g_java_vm).GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION);
    return env;
  }
}


void il2ge::core_common::il2geCommonInit(HMODULE il2ge_module,
                                         CoreInterfaceFactory interface_factory)
{
  assert(!g_initialized);

  g_il2ge_module = il2ge_module;

  try
  {
    il2ge::core_common::initLog();
  }
  catch (std::exception& e)
  {
    auto message = std::string(e.what()) + "\n\rTerminating program.";
    MessageBoxA(nullptr, message.c_str(), "IL-2 Graphics Extender", MB_OK | MB_ICONERROR);
    _Exit(EXIT_FAILURE);
  }

  LOG_SEPARATOR;
  LOG_INFO << "*** il2ge.dll initialization ***" << std::endl;
  LOG_INFO << "Version: " << il2ge::version::getVersion() << std::endl;
  LOG_INFO << "Commit: " << il2ge::version::getCommitSHA() << std::endl;
  LOG_INFO << "Build: " << il2ge::version::getBuildJobID() << std::endl;
  LOG_INFO << "Build configuration: " << BUILD_CONFIG_NAME << std::endl;

  LOG_INFO << "Compiler: " << COMPILER_VERSION_STRING << std::endl;

  for (auto& s : util::tokenize(COMPILER_VERSION_STRING_VERBOSE, ';'))
    LOG_INFO << "Compiler info: " << s << std::endl;

  LOG_FLUSH;

  readConfig();
  writeConfig();

  if (!getConfig().enable_graphics_extender)
  {
    LOG_WARNING << "IL2GE is disabled in config." << std::endl;
    LOG_FLUSH;
    return;
  }

  g_main_thread = GetCurrentThreadId();

  il2ge::exception_handler::install("il2ge_crash.log", &fatalErrorHandler);
  il2ge::exception_handler::watchModule(g_il2ge_module);

  auto jvm_module = GetModuleHandle("jvm.dll");
  assert(jvm_module);

  installIATPatches(GetModuleHandle(0));
  installIATPatches(jvm_module);

  g_core_interface = interface_factory(g_il2ge_module);

  getCoreInterface().resolveImports(GetModuleHandle(0));

  g_initialized = true;
}
