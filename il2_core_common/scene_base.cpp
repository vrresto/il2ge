#include <scene_base.h>
#include <menu.h>
#include <render_util/atmosphere.h>
#include <render_util/viewer/value_wrapper.h>
#include <render_util/viewer/simple_parameter.h>


using namespace render_util;


namespace
{
  constexpr auto MAX_CIRRUS_OPACITY = 0.7;
  constexpr auto MAX_CIRRUS_ALBEDO = 0.4;
  constexpr auto MAX_MIE_ARMSTRONG_BETA_FACTOR = 10.f;
  // const std::string g_shader_path_old = IL2GE_DATA_DIR "/shaders_new";
  // const std::string g_shader_path = IL2GE_DATA_DIR "/shaders_new";
}


namespace il2ge::core_common
{


SceneBase::SceneBase()
{
  {
    AtmosphereCreationParameters params;
    params.max_cirrus_albedo = MAX_CIRRUS_ALBEDO;
#if ENABLE_CONFIGURABLE_ATMOSPHERE
    params.precomputation.cache_mode = AtmospherePrecomputationParameters::CacheMode::USE_OR_CREATE;
    params.precomputation.options.precomputed_luminance =
      config.atmosphere_precomputed.precomputed_luminance;
    params.precomputation.atmosphere_properties.mie_angstrom_beta_factor =
      glm::mix(1.f, MAX_MIE_ARMSTRONG_BETA_FACTOR, config.atmosphere_precomputed.haziness.get());
    params.precomputation.options.single_mie_horizon_hack =
      config.atmosphere_precomputed.single_mie_horizon_hack;
    m_atmosphere = createAtmosphere(config.atmosphere.get(),
                                  IL2GE_DATA_DIR "/shaders_new", params);
#else
    m_atmosphere = createAtmosphere(Atmosphere::DEFAULT,
                                    IL2GE_DATA_DIR "/shaders_new", params);
#endif
  }

  m_shader_search_path.push_back(IL2GE_DATA_DIR "/shaders_new/"
                               + m_atmosphere->getShaderPath());
  m_shader_search_path.push_back(IL2GE_DATA_DIR "/shaders_new");
  m_shader_search_path.push_back(IL2GE_DATA_DIR "/shaders");

#if ENABLE_CONFIGURABLE_SHADOWS
    m_shader_parameters.set("enable_unlit_output", config.better_shadows);
#endif

  // m_curvature_map = render_util::createCurvatureTexture(IL2GE_CACHE_DIR);

  m_menu = std::make_unique<Menu>(m_parameters, m_shader_search_path);
}


SceneBase::~SceneBase()
{
}


float SceneBase::getMaxCirrusOpacity()
{
  return MAX_CIRRUS_OPACITY;
}


void SceneBase::addParameters()
{
  assert(m_parameters.empty());
  assert(m_atmosphere);

  auto addAtmosphereParameter = [this] (std::string name, Atmosphere::Parameter p)
  {
    if (m_atmosphere->hasParameter(p))
    {
      viewer::ValueWrapper<double> wrapper =
      {
        .get = [this, p] () { return m_atmosphere->getParameter(p); },
        .set = [this, p] (auto value) { m_atmosphere->setParameter(p, value); },
      };
      using Param = viewer::SimpleParameter<double, viewer::ValueWrapper<double>>;
      m_parameters.add(std::make_unique<Param>(name, 1.f, wrapper));
    }
  };

  addAtmosphereParameter("exposure", Atmosphere::Parameter::EXPOSURE);
  addAtmosphereParameter("brightness_curve_exponent",
                          Atmosphere::Parameter::BRIGHTNESS_CURVE_EXPONENT);
  addAtmosphereParameter("saturation", Atmosphere::Parameter::SATURATION);
  addAtmosphereParameter("texture_brightness", Atmosphere::Parameter::TEXTURE_BRIGHTNESS);
  addAtmosphereParameter("texture_brightness_curve_exponent",
                          Atmosphere::Parameter::TEXTURE_BRIGHTNESS_CURVE_EXPONENT);
  addAtmosphereParameter("texture_saturation", Atmosphere::Parameter::TEXTURE_SATURATION);
  addAtmosphereParameter("blue_saturation", Atmosphere::Parameter::BLUE_SATURATION);

  addAtmosphereParameter("uncharted2_a", Atmosphere::Parameter::UNCHARTED2_A);
  addAtmosphereParameter("uncharted2_b", Atmosphere::Parameter::UNCHARTED2_B);
  addAtmosphereParameter("uncharted2_c", Atmosphere::Parameter::UNCHARTED2_C);
  addAtmosphereParameter("uncharted2_d", Atmosphere::Parameter::UNCHARTED2_D);
  addAtmosphereParameter("uncharted2_e", Atmosphere::Parameter::UNCHARTED2_E);
  addAtmosphereParameter("uncharted2_f", Atmosphere::Parameter::UNCHARTED2_F);
  addAtmosphereParameter("uncharted2_w", Atmosphere::Parameter::UNCHARTED2_W);
}


} // namespace il2ge::core_common
