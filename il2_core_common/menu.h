/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CORE_MENU_H
#define CORE_MENU_H

#include <render_util/viewer/parameter_base.h>
#include <render_util/text_display.h>


namespace il2ge::core_common
{


class Menu
{
  using Parameters = render_util::viewer::ParameterGroup;
  using Parameter = render_util::viewer::Parameter;

  bool m_is_shown = false;
  int m_active_param = 0;
  render_util::TextDisplay m_display;
  Parameters& m_params;

  void nextEntry();
  void prevEntry();
  void changeValue(double increment);
  void resetValue();
  void rebuild();
  bool hasActiveParameter();
  Parameter& getActiveParameter();
  void setActiveParameter(int index);

  void increase(float step_factor);
  void decrease(float step_factor);

public:
  Menu(Parameters&, render_util::ShaderSearchPath);

  bool isShown() { return m_is_shown; }
  void show(bool show)
  {
    m_is_shown = show;
    rebuild();
  }

  void draw(TextRenderer&);

  void handleKey(int key, bool ctrl, bool alt, bool shift);
};


} // namespace il2ge::core_common

#endif
