#pragma once


struct JNIEnv_;


namespace il2ge
{


JNIEnv_* getJNIEnv();
bool hasFatalError();


} // namespace il2ge
