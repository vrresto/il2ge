/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <il2ge_java_interface/interface.h>
#include <java_interface_impl.h>
#include <core_common.h>
#include <log.h>

#include <jni.h>


using namespace il2ge::core_common;


namespace
{


JavaInterfaceImpl* g_impl {};


JavaInterfaceImpl& getImpl()
{
  assert(g_impl);
  return *g_impl;
}


} // namespace


namespace il2ge::core_common
{


void initJavaClasses(JavaInterfaceImpl* interface_impl)
{
  if (g_impl)
  {
    assert(g_impl == interface_impl);
    return;
  }

  g_impl = interface_impl;

  LOG_INFO << "Loading class com/maddox/il2ge/HotKeys ..." << std::endl;
  il2ge::getJNIEnv()->FindClass("com/maddox/il2ge/HotKeys");

  if (il2ge::getJNIEnv()->ExceptionCheck())
  {
    LOG_WARNING  << "Exception encountered when loading class "
                  << "com/maddox/il2ge/HotKeys." << std::endl;
    il2ge::getJNIEnv()->ExceptionDescribe();
  }
  else
  {
    LOG_INFO << "Loading class com/maddox/il2ge/HotKeys ... done." << std::endl;
  }

  il2ge::getJNIEnv()->ExceptionClear();
}


} // namespace il2ge::core_common



namespace il2ge::java_interface
{


void showMenu(bool show)
{
  getImpl().showMenu(show);
}


void handleKey(int key, bool ctrl, bool alt, bool shift)
{
  getImpl().handleKey(key, ctrl, alt, shift);
}


int getNumCommandNames()
{

  return getImpl().getNumCommandNames();
}


std::string getCommandName(int index)
{
  return getImpl().getCommandName(index);
}


std::string getCommandDisplayText(std::string command_name)
{
  return getImpl().getCommandDisplayText(command_name);
}


bool isDebugCommand(std::string name)
{
  return getImpl().isDebugCommand(name);
}


void executeCommand(std::string name)
{
  getImpl().executeCommand(name);
}


} // namespace il2ge::java_interface
