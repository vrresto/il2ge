#undef NOGDI

#include <type_traits>
#include <cassert>
#include <windows.h>

#include <wingdi.h>

#include <wgl_interface.h>


namespace il2ge::core_common
{


WglInterface::WglInterface(HMODULE gl_module)
{
  auto get_proc = [gl_module] (auto& var, auto name)
  {
    using T = std::remove_reference_t<decltype(var)>;
    var = reinterpret_cast<T>(::GetProcAddress(gl_module, name));
    assert(var);
  };

  get_proc(this->GetProcAddress, "wglGetProcAddress");
  get_proc(this->MakeCurrent, "wglMakeCurrent");
  get_proc(this->CreateContext, "wglCreateContext");
  get_proc(this->DeleteContext, "wglDeleteContext");
}


} // namespace il2ge::core_common
