#pragma once


#include <string>


namespace il2ge::core_common
{


struct JavaInterfaceImpl
{
  virtual int getNumCommandNames() = 0;
  virtual std::string getCommandName(int index) = 0;

  virtual std::string getCommandDisplayText(std::string command_name) = 0;
  virtual bool isDebugCommand(std::string command_name) = 0;
  virtual void executeCommand(std::string command_name) = 0;

  virtual void showMenu(bool show) = 0;
  virtual void handleKey(int key, bool ctrl, bool alt, bool shift) = 0;
};


void initJavaClasses(JavaInterfaceImpl*);


} // namespace il2ge::core_common
