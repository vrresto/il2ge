#pragma once

#include <render_util/viewer/parameter_base.h>
#include <render_util/viewer/parameters_base.h>
#include <render_util/shader.h>


namespace render_util
{
  class Atmosphere;
}


namespace il2ge::core_common
{


class Menu;


class SceneBase
{
public:
  using Parameter = render_util::viewer::Parameter;

  class Parameters : public render_util::viewer::ParametersBase
  {
  public:
    Parameters() : render_util::viewer::ParametersBase("Rendering") {}

    void clear()
    {
      m_parameters.clear();
    }
  };

protected:
  Parameters m_parameters;
  std::unique_ptr<Menu> m_menu;
  render_util::ShaderParameters m_shader_parameters;
  render_util::ShaderSearchPath m_shader_search_path;
  std::unique_ptr<render_util::Atmosphere> m_atmosphere;

  void addParameters();

public:
  SceneBase();
  ~SceneBase();

  int getNumParameters() { return m_parameters.size(); }
  Parameter& getParameter(int i) { return m_parameters.get(i); }

  const render_util::ShaderSearchPath &getShaderSearchPath()
  {
    return m_shader_search_path;
  }

  const render_util::ShaderParameters& getShaderParameters()
  {
    return m_shader_parameters;
  }

  Menu &getMenu()
  {
    assert(m_menu);
    return *m_menu;
  }

  float getMaxCirrusOpacity();
};


} // namespace il2ge::core_common
