#include <configuration.h>
#include <il2ge/map_loader.h>


namespace il2ge::map_loader
{
  bool isDumpEnabled()
  {
#if ENABLE_MAP_VIEWER
    return il2ge::core_common::getConfig().enable_dump;
#else
    return false;
#endif
  }
}
