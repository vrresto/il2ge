#include <core_common_log.h>
#include <core_common.h>
#include <iat.h>
#include <log.h>
// #include <render_util/config.h>
#include <log/file_appender.h>
#include <log/console_appender.h>
#include <log/txt_formatter.h>
#include <log/message_only_formatter.h>

#include <sstream>
#include <iostream>
#include <fstream>
#include <cassert>
#include <unistd.h>


#define LOG_TO_CONSOLE 1


namespace
{


constexpr auto LOG_FILE_NAME = "il2ge.log";
constexpr auto LOG_FULL_FILE_NAME = "il2ge_full.log";


#if !USE_PLOG
class LogBuf : public std::stringbuf
{
protected:
  int sync() override
  {
    fwrite(str().data(), sizeof(char), str().size(), stdout);
    fflush(stdout);

    str({});

    return 0;
  }
};
#endif


#if !USE_PLOG
LogBuf g_cout_buf;
LogBuf g_cerr_buf;
#endif


#if USE_PLOG
int wrap_write(int fd, const void *buffer, unsigned int count)
{
  if (fd <= 2)
  {
    std::string str((char*)buffer, count);

    plog::Record record(il2ge::hasFatalError() ? plog::error : plog::verbose);
    record << str;
    *plog::get<PLOG_DEFAULT_INSTANCE>() += record;

    return count;
  }
  else
  {
    return _write(fd, buffer, count);
  }
}
#endif


} // namespace



namespace il2ge::core_common
{


void initLog()
{
#if USE_PLOG

  constexpr bool ADD_NEW_LINE = false;

  using namespace util::log;
  using FileSink = FileAppender<TxtFormatter<ADD_NEW_LINE>>;
  using ConsoleSink = ConsoleAppender<MessageOnlyFormatter<ADD_NEW_LINE>>;

  auto &logger_default = plog::init(plog::verbose);

  #if LOG_TO_CONSOLE
  {
    auto sink = new ConsoleSink();
    auto &logger = plog::init<LOGGER_INFO>(plog::info, sink);
    logger_default.addAppender(&logger);
  }
  #endif

  {
    auto sink = new FileSink(LOG_FILE_NAME);
    auto &logger = plog::init<LOGGER_DEBUG>(plog::debug, sink);
    logger_default.addAppender(&logger);
  }

  {
    auto sink = new FileSink(LOG_FULL_FILE_NAME);
    auto &logger = plog::init<LOGGER_TRACE>(plog::verbose, sink);
    logger_default.addAppender(&logger);
  }


#else

  {
    std::ofstream log(LOG_FILE_NAME);
    log << std::endl;
  }

  freopen(LOG_FILE_NAME, "a", stdout);
  freopen(LOG_FILE_NAME, "a", stderr);

  auto out_fd = _fileno(stdout);

  auto res = _dup2(out_fd, 1);
  assert(res != -1);
  res = _dup2(out_fd, 2);
  assert(res != -1);

  std::cout.rdbuf(&g_cout_buf);
  std::cerr.rdbuf(&g_cerr_buf);

#endif
}


void installLoggerIATPatches(HMODULE mod)
{
#if USE_PLOG
  patchIAT("_write", "msvcrt.dll", (void*) &wrap_write, nullptr, mod);
#endif
}


} // namespace il2ge::core_common
