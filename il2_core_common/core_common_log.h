#pragma once


#include <windef.h>


namespace il2ge::core_common
{
  void initLog();
  void installLoggerIATPatches(HMODULE);
}
