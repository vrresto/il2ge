#pragma once

// undef NOGDI and include gl header before including this!

namespace il2ge::core_common
{


struct WglInterface
{
  decltype(wglMakeCurrent)* MakeCurrent = nullptr;
  decltype(wglCreateContext)* CreateContext = nullptr;
  decltype(wglDeleteContext)* DeleteContext = nullptr;
  decltype(wglGetProcAddress)* GetProcAddress = nullptr;

  WglInterface(HMODULE gl_module);
};


} // namespace il2ge::core_common
