#pragma once


#include <functional>


namespace il2ge::core_common
{


struct CoreInterface
{
  virtual ~CoreInterface() {}

  virtual void load(const char*) = 0;
  virtual bool isLoaded() = 0;
  virtual void init() = 0;
  virtual bool isInitialized() = 0;
  virtual HMODULE getModuleHandle() = 0;

  virtual void* getExeProcAddress(const char*) = 0;

  virtual void resolveImports(HMODULE) = 0;
  virtual void* getJNIProcAddress(const char*) = 0;

  virtual HMODULE getGLModuleHandle() = 0;
  virtual void* getGLProcAddress(const char*) = 0;
};


using CoreInterfaceFactory =
  std::function<CoreInterface*(HMODULE il2ge_module)>;

void il2geCommonInit(HMODULE il2ge_module, CoreInterfaceFactory);
void installIATPatches(HMODULE);


} // namespace il2ge::core_common


namespace il2ge::core
{
  il2ge::core_common::CoreInterface* createCoreInterface(HMODULE il2ge_module);
}


namespace il2ge::core_wrapper
{
  il2ge::core_common::CoreInterface* createCoreInterface(HMODULE il2ge_module);
}
