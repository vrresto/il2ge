/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sfs.h"
#include <il2ge/sfs_hash.h>
#include <util.h>

#include <iostream>
#include <string>
#include <unordered_map>
#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <windef.h>
#include <winbase.h>

using std::cout;
using std::endl;
using std::unordered_map;

// #define SFS_API __cdecl
#define SFS_API __stdcall


namespace
{


typedef int __cdecl SAS_SFS_openf_T(unsigned __int64 hash, int flags);
typedef int SFS_API SAS_openf_T(unsigned __int64 hash, int flags);
typedef int __cdecl SFS_open_T(char *filename, int flags);
typedef int SFS_API SFS_close_T(int fd);
typedef int SFS_API SFS_read_T(int fd, void *buffer, unsigned int numberOfBytesToRead);
typedef long SFS_API SFS_lseek_T(int fd, long offset, int moveMethod);


bool g_initialized = false;
SAS_SFS_openf_T *g_openf_func = nullptr;
SAS_openf_T *g_sas_openf_func = nullptr;
SFS_open_T *g_sfs_open_func = nullptr;
SFS_close_T *g_close_func = nullptr;
SFS_read_T *g_read_func = nullptr;
SFS_lseek_T *g_lseek_func = nullptr;


int openf(__int64 hash)
{
  if (g_sas_openf_func)
    return g_sas_openf_func(hash, 0); // call new "routing" method __SAS_openf if available
  else
    return g_openf_func(hash, 0); // fallback in case old wrapper.dll was loaded
}


int open(std::string filename)
{
  auto ret = openf(sfs::getHash(filename));

  if (ret != -1)
    return ret;
  else
    return g_sfs_open_func(filename.data(), 0);
}


} // namespace


namespace sfs
{

void init()
{
  if (!g_initialized)
  {
    HMODULE m = GetModuleHandle(0);
    assert(m);

    g_sfs_open_func = (SFS_open_T*) GetProcAddress(m, "SFS_open");
    assert(g_sfs_open_func);

    g_close_func = (SFS_close_T*) GetProcAddress(m, "SFS_close");
    assert(g_close_func);

    g_read_func = (SFS_read_T*) GetProcAddress(m, "SFS_read");
    assert(g_read_func);

    g_lseek_func = (SFS_lseek_T*) GetProcAddress(m, "SFS_lseek");
    assert(g_lseek_func);

    m = GetModuleHandle("wrapper.dll");
    assert(m);

    g_openf_func = (SAS_SFS_openf_T*) GetProcAddress(m, "__SFS_openf");
    assert(g_openf_func);

    // no asserts here, method might not exist in old wrapper.dll versions!
    g_sas_openf_func = (SAS_openf_T*) GetProcAddress(m, "__SAS_openf@12");

    g_initialized = true;
  }
}


bool readFile(const std::string &filename, std::vector<char> &out)
{
  auto path = util::resolveRelativePathComponents(filename);

  auto fd = open(path);

  if (fd == -1)
    return false;

  assert(fd >= 0);

  auto size = g_lseek_func(fd, 0, SEEK_END);
  g_lseek_func(fd, 0, SEEK_SET);

  out.resize(size);

  auto ret = g_read_func(fd, out.data(), size);

  g_close_func(fd);

  return ret > 0 ? ((unsigned int)ret) == size : false;
}


ReadError::ReadError(const std::string& path) :
  il2ge::FileReadError(path, "SFS error")
{
}


File::File(std::string path) : m_path(path)
{
  m_fd = open(path);
  if (m_fd == -1)
    throw std::runtime_error("Failed to open " + path); //FIXME - throw ReadError?

  assert(m_fd >= 0);

  m_size = g_lseek_func(m_fd, 0, SEEK_END);
  g_lseek_func(m_fd, 0, SEEK_SET);
}


File::~File()
{
  g_close_func(m_fd);
}


int File::read(char *out, int bytes)
{
  assert(m_pos < m_size);

  auto ret = g_lseek_func(m_fd, m_pos, SEEK_SET);
  assert(ret == m_pos);

  ret = g_read_func(m_fd, out, bytes);
  assert(ret != -1);
  assert(ret >= 0);

  m_pos += ret;
  assert(m_pos <= m_size);

  return ret;
}


void File::skip(int bytes)
{
  m_pos += bytes;
  assert(m_pos >= 0);

  auto ret = g_lseek_func(m_fd, m_pos, SEEK_SET);
  assert(ret == m_pos);
}


void File::rewind()
{
  m_pos = 0;
  g_lseek_func(m_fd, m_pos, SEEK_SET);
}


bool File::eof()
{
  return m_pos >= m_size;
}


void File::readAll(std::vector<char> &out)
{
  assert(m_pos == 0);
  assert(!eof());

  out.resize(m_size);

  auto ret = read(out.data(), out.size());
  assert(ret == m_size);

  rewind();
}


int File::getSize()
{
  return m_size;
}


} // namespace sfs
