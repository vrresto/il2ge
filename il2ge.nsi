!define APP_NAME "IL-2 Graphics Extender"
!define INSTALLER_NAME "il2ge-installer"
!define CONFIG_FILE "il2ge.ini"
!define README_URL "https://gitlab.com/vrresto/il2ge/-/blob/master/README.md"

!define VERSION "$%VERSION%"
!define SRC_DIR "$%SRC_DIR%"
!define DEST_DIR "$%DEST_DIR%"

!define CONFIG_FILE_PATH "$INSTDIR\${CONFIG_FILE}"


#!define MUI_ABORTWARNING # This will warn the user if they exit from the installer.


; *** welcome page configuration ***

; !define MUI_WELCOMEPAGE_TITLE_3LINES
; !define MUI_WELCOMEPAGE_TEXT "Welcome"


; *** install directory page configuration ***

!define MUI_DIRECTORYPAGE_TEXT_TOP "Select the folder of your IL-2 installation."
!define MUI_DIRECTORYPAGE_TEXT_DESTINATION "IL-2 game folder"


; *** finish page configuration ***

; !define MUI_FINISHPAGE_TEXT_LARGE
!define MUI_FINISHPAGE_TEXT 'Installation finnished successfully.$\r$\n\
                             ${APP_NAME} may be configured by editing ${CONFIG_FILE} (optional).'
!define MUI_FINISHPAGE_LINK "View README"
!define MUI_FINISHPAGE_LINK_LOCATION "${README_URL}"

!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_RUN_TEXT "Edit configuration file (${CONFIG_FILE})"
!define MUI_FINISHPAGE_RUN_FUNCTION OpenConfig


!macro BackupFile FILE BACKUP_DIR
  ClearErrors
  IfFileExists "$INSTDIR\${FILE}" 0 +2
    Rename "$INSTDIR\${FILE}" "${BACKUP_DIR}\${FILE}"
  IfErrors 0 +3
    DetailPrint "Failed to backup file: ${FILE}"
    Abort
!macroend


Function OpenConfig
  ExecShell "open" "${CONFIG_FILE_PATH}"
FunctionEnd


SetCompressor /SOLID /FINAL lzma


!include "FileFunc.nsh"
!insertmacro GetTime
!include "MUI2.nsh"

; !define MUI_PAGE_HEADER_TEXT
!insertmacro MUI_PAGE_WELCOME # Welcome to the installer page.
!insertmacro MUI_PAGE_LICENSE "LICENSE"
!insertmacro MUI_PAGE_DIRECTORY # In which folder install page.
!insertmacro MUI_PAGE_INSTFILES # Installing page.
!insertmacro MUI_PAGE_FINISH # Finished installation page.

!insertmacro MUI_LANGUAGE "English"


Name "${APP_NAME} ${VERSION}" # Name of the installer (usually the name of the application to install).
OutFile "${DEST_DIR}/${INSTALLER_NAME}.exe" # Name of the installer's file.
InstallDir "" # Default installing folder ($PROGRAMFILES is Program Files folder).
; ShowInstDetails show # This will always show the installation details.
RequestExecutionLevel user


Section "il2ge" # In this section add your files or your folders.

  ${GetTime} "" "l" $0 $1 $2 $3 $4 $5 $6
  !define TIMESTAMP "$2.$1.$0-$4.$5.$6"

  !define BACKUP_DIR "$INSTDIR\il2ge-installer-backup\${TIMESTAMP}"

  CreateDirectory ${BACKUP_DIR}

  IfErrors 0 create_backup_dir_success
  DetailPrint "Failed to create folder: ${BACKUP_DIR}"
  Abort

create_backup_dir_success:
  !insertmacro BackupFile "il2ge.dll" "${BACKUP_DIR}"
  !insertmacro BackupFile "il2ge" "${BACKUP_DIR}"

  SetOutPath "$INSTDIR\il2ge"
  File /r "${SRC_DIR}/il2ge/*"

  SetOutPath "$INSTDIR"
  ExecWait 'rundll32 "$INSTDIR\il2ge\lib\il2ge.dll,UpdateConfig"'

SectionEnd
