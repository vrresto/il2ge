#include <windef.h>

#include "common.h"

extern "C"
{
  void WINAPI il2geInit();
  BOOL WINAPI DllMain(HINSTANCE instance, DWORD reason, void *reserved);
}


namespace
{


HMODULE g_il2ge_module = 0;


} // namespace


extern "C"
{


void WINAPI il2geInit()
{
  il2ge::core_common::il2geCommonInit(g_il2ge_module, g_core_interface_factory);
}


BOOL WINAPI DllMain(HINSTANCE instance, DWORD reason, void *reserved)
{
  switch (reason)
  {
    case DLL_PROCESS_ATTACH:
      g_il2ge_module = instance;
      break;
    case DLL_PROCESS_DETACH:
      break;
  }

  return true;
}


} // extern "C"
