if(NOT CMAKE_RC_COMPILER)
  message(FATAL_ERROR "CMAKE_RC_COMPILER is not set")
endif()

add_custom_command(
    OUTPUT ${resource_obj}
    COMMAND ${CMAKE_RC_COMPILER} ${resource_script} ${resource_obj}
    DEPENDS ${resource_script}
)

add_library(${library_name} SHARED
  ${resource_obj}
  ${def_file}
  factory.cpp
  ../main.cpp
)

target_link_libraries(${library_name}
  java_interface_common
)

install(TARGETS ${library_name}
  RUNTIME DESTINATION ${il2ge_lib_dir}
)
