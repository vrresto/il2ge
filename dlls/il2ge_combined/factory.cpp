#include "../common.h"

#include <configuration.h>
#include <core_config.h>


const il2ge::core_common::CoreInterfaceFactory g_core_interface_factory =
[] (auto il2ge_module)
{
#if ENABLE_CORE_WRAPPER && ENABLE_IL2_CORE
  if (il2ge::core_common::getConfig().enable_il2_core)
    return il2ge::core::createCoreInterface(il2ge_module);
  else
    return il2ge::core_wrapper::createCoreInterface(il2ge_module);
#elif ENABLE_CORE_WRAPPER
  return il2ge::core_wrapper::createCoreInterface(il2ge_module);
#elif ENABLE_IL2_CORE
  return il2ge::core::createCoreInterface(il2ge_module);
#else
  #error No core interface enabled
#endif
};
