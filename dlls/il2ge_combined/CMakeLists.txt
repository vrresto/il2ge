set(library_name "il2ge_combined")

include(../common.cmake)

if(ENABLE_CORE_WRAPPER)
  target_link_libraries(${library_name} il2ge_static)
endif()

if(ENABLE_IL2_CORE)
  target_link_libraries(${library_name} il2ge_core_static)
endif()
