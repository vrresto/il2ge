/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAP_LOADER_SFS_DUMP_H
#define MAP_LOADER_SFS_DUMP_H

#include <il2ge/map_resources.h>
#include <il2ge/map_loader.h>
#include <render_util/viewer/map_loader.h>
#include <log.h>

#include <unordered_map>
#include <filesystem>


namespace il2ge
{


class MapLoaderSFSDump : public render_util::viewer::MapLoaderBase
{
  class ResourceProvider ;

  std::unique_ptr<ResourceProvider> m_provider;
  std::unique_ptr<il2ge::MapResources> m_map_resources;
  std::unique_ptr<render_util::TerrainResourcesBase> m_terrain_resources;

public:
  MapLoaderSFSDump(std::filesystem::path ini_path, std::filesystem::path sfs_base_dir);
  ~MapLoaderSFSDump();

  render_util::TerrainResourcesBase& getTerrainResources() override;
  const std::shared_ptr<render_util::GenericImage> getCirrusTexture() const override;
};


} // namespace il2ge

#endif
