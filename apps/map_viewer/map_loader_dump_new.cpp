/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "map_loader_dump.h"
#include <render_util/image.h>
#include <render_util/image_loader.h>
#include <render_util/image_util.h>
#include <render_util/texture_util.h>
#include <render_util/texunits.h>
#include <il2ge/map_loader.h>
#include <il2ge/map_resources.h>
#include <util.h>
#include <log.h>

#include <INIReader.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>


using namespace std;
using namespace render_util;
using namespace il2ge;
using namespace glm;


class FileResource : public il2ge::Resource
{
  std::string m_path;

public:
  FileResource(std::string path) : m_path(path) {}

  const std::string& getName() const override
  {
    return m_path;
  }

  std::unique_ptr<util::File> open() const override
  {
    try
    {
      return std::make_unique<util::NormalFile>(m_path);
    }
    catch (std::exception &e)
    {
      throw std::runtime_error("Failed to open " + m_path + ": " + e.what());
    }
  }

  virtual const std::string& getUniqueID() const
  {
    throw std::runtime_error("Not implemented");
  }
};


class MapLoaderDump::Resources : public il2ge::MapResources
{
  std::string map_dir;

  std::string getFilePath(const char *section,
                          const char *name,
                          bool from_map_dir,
                          bool is_bumpmap) const
  {
    string path = map_dir + '/' + section + '_' + name + ".tga";
    if (is_bumpmap)
      path = map_dir + '/' + section + '_' + name + "_nm.tga";
    return path;
  }

  float getScale(const char *section, const char *name) const
  {
    string scale_path = map_dir + '/' + section + '_' + name + "_scale";
    vector<char> scale_content;
    if (util::readFile(scale_path, scale_content, false))
    {
      string scale_str(scale_content.data(), scale_content.size());
      try
      {
        return stof(scale_str);
      }
      catch (...)
      {
        LOG_ERROR<<"can't convert '"<<scale_str<<"' to float"<<endl;
        LOG_ERROR<<"section: "<<section<<endl;
        LOG_ERROR<<"key: "<<name<<endl;
        return 1;
      }
    }

    return 1;
  }

public:
  Resources(const char *path)
  {
    map_dir = path;
    assert(!map_dir.empty());
  }

#if 0
  glm::vec3 getBaseMapOrigin() const override
  {
    auto cfg_file_path = map_dir + "/il2ge.ini";
    INIReader ini(cfg_file_path);
    if (!ini.ParseError())
    {
      glm::vec3 origin =
      {
        ini.GetReal("base_map", "origin_x", 0),
        ini.GetReal("base_map", "origin_y", 0),
        ini.GetReal("base_map", "origin_z", 0),
      };
      return origin;
    }
    else
    {
      throw std::runtime_error("Error parsing " + cfg_file_path
                                + " at line " + std::to_string(ini.ParseError()));
    }
  }


  render_util::ElevationMap::Ptr createBaseElevationMapRAW() const
  {
    constexpr auto BYTES_PER_PIXEL = 2;

    ivec2 size(4096);

    auto num_pixels = size.x * size.y;
    auto data_size = num_pixels * BYTES_PER_PIXEL;

    try
    {
      auto data = util::readFile<unsigned char>(map_dir + "/base_map_hm.raw");
      cout<<"data.size(): "<<data.size()<<endl;
      assert(data.size() == data_size);
      auto image = make_shared<render_util::Image<int16_t>>(size, std::move(data));
      return render_util::image::convert<float>(image);
    }
    catch(std::exception &e)
    {
      LOG_WARNING << e.what() << endl;
      return {};
    }
  }
#endif

  std::string getDumpDir() const override
  {
    static string dir = "/dev/null/";
    return dir;
  }

  glm::vec3 getWaterColor(const glm::vec3 &default_value) const
  {
    return default_value;
  }

  // std::unique_ptr<Resource>
  // getWaterAnimationFrame(int index, WaterTextureType type) const
  // {
  //   std::string suffix;
  //   switch (type)
  //   {
  //     case WaterTextureType::HEIGHT_MAP:
  //       suffix = "height_map";
  //       break;
  //     case WaterTextureType::NORMAL_MAP:
  //       suffix = "normal_map";
  //       break;
  //     case WaterTextureType::FOAM_MASK:
  //       suffix = "foam_mask";
  //       break;
  //   }
  //   auto path = map_dir + '/' + "water_animation_" + suffix + '_' + std::to_string(index) + ".tga";
  //
  //   try
  //   {
  //     return std::make_unique<FileResource>(path);
  //     }
  //   catch (...)
  //   {
  //     LOG_DEBUG<<"Failed to open " << path << std::endl;
  //     throw;
  //   }
  // }

  std::unique_ptr<Resource> getTexture(const char *section,
                          const char *name,
                          const char *default_path,
                          bool from_map_dir,
                          bool redirect,
                          float *scale = nullptr,
                          bool is_bumpmap = false) const
  {
    if (scale)
      *scale = getScale(section, name);

    auto path = getFilePath(section, name, from_map_dir, is_bumpmap);
    return std::make_unique<FileResource>(path);
  }

  std::unique_ptr<Resource> getFile(const char *section,
           const char *name,
           const char *default_path,
           bool from_map_dir,
           bool redirect,
           const char* suffix) const
  {
    auto path = map_dir + '/' + section + '_' + name + ".tga";
    if (suffix)
      path += suffix;
//     LOG_INFO<<"path: "<<path<<endl;
    return std::make_unique<FileResource>(path);
  }
};


render_util::TerrainResourcesBase &MapLoaderDump::getTerrainResources()
{
  assert(m_terrain_resources);
  return *m_terrain_resources;
}


const std::shared_ptr<render_util::GenericImage> MapLoaderDump::getCirrusTexture() const
{
  return il2ge::map_loader::createCirrusTexture(*m_resources);
}


MapLoaderDump::MapLoaderDump(const std::string &path) :
  m_path(path),
  m_resources(std::make_unique<Resources>(m_path.c_str())),
  m_terrain_resources(il2ge::map_loader::getTerrainResources(*m_resources))
{
}


MapLoaderDump::~MapLoaderDump()
{
}


#if 0

glm::vec3 MapLoaderDump::getBaseMapOrigin() const
{
  auto cfg_file_path = m_path + "/il2ge.ini";
  INIReader ini(cfg_file_path);
  if (!ini.ParseError())
  {
    glm::vec3 origin =
    {
      ini.GetReal("base_map", "origin_x", 0),
      ini.GetReal("base_map", "origin_y", 0),
      ini.GetReal("base_map", "origin_z", 0),
    };
    return origin;
  }
  else
  {
    throw std::runtime_error("Error parsing " + cfg_file_path
                              + " at line " + std::to_string(ini.ParseError()));
  }
}


unsigned int MapLoaderDump::getBaseElevationMapMetersPerPixel() const
{
  return 200;
}

// ImageGreyScale::Ptr MapLoaderDump::createBaseLandMap() const
// {
//   auto land_map_path = m_path + '/' + il2ge::map_generator::getBaseLandMapFileName();
// 
//   LOG_INFO << "creating land map from: " << land_map_path <<endl;
// 
//   auto land_map = render_util::loadImageFromFile<ImageGreyScale>(land_map_path);
// 
//   if (land_map)
//     land_map = image::flipY(land_map);
// 
//   return land_map;
// }

#endif
