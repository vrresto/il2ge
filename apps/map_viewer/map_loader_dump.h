/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAP_LOADER_DUMP_H
#define MAP_LOADER_DUMP_H

#include <render_util/viewer/map_loader.h>
#include <render_util/terrain_resources.h>

#include <memory>


namespace il2ge::map_loader
{
  class TerrainLoader;
}


class MapLoaderDump : public render_util::viewer::MapLoaderBase
{
  class Resources;

  std::string m_path;
  std::unique_ptr<Resources> m_resources;
  render_util::ImageGreyScale::Ptr m_type_map;
  render_util::ImageGreyScale::Ptr m_base_type_map;

  std::unique_ptr<render_util::TerrainResourcesBase> m_terrain_resources;

public:
  MapLoaderDump(const std::string &path);
  ~MapLoaderDump();

  render_util::TerrainResourcesBase& getTerrainResources() override;
  const std::shared_ptr<render_util::GenericImage> getCirrusTexture() const override;

//   render_util::ElevationMap::Ptr createElevationMap() const override;
//   int getHeightMapMetersPerPixel() const override;
// 
//   render_util::ElevationMap::Ptr createBaseElevationMap() const override;
//   glm::vec3 getBaseMapOrigin() const override;
//   virtual unsigned int getBaseElevationMapMetersPerPixel() const override;
// 
//   void createLandTextures(render_util::TerrainBase::Textures&) const override;
//   void createMapTextures(render_util::MapBase *map, render_util::TerrainBase::Textures&) const override;
// 
//   std::shared_ptr<const render_util::TerrainBase::TypeMap> getBaseTypeMap() const override;
//   void generateBaseTypeMap(render_util::ElevationMap::ConstPtr map) override;
};

#endif
