/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "map_loader_sfs_dump.h"
#include <il2ge/sfs_hash.h>
#include <render_util/altitudinal_zone.h>


using namespace il2ge;


namespace
{


struct SFSDumpResource : public il2ge::SFSResourceBase
{
  std::filesystem::path m_real_path;

  SFSDumpResource(std::string sfs_path, std::filesystem::path real_path) :
    SFSResourceBase(sfs_path),
    m_real_path(real_path)
  {
  }

  std::unique_ptr<util::File> open() const override
  {
    assert(!m_real_path.empty());
    return std::make_unique<util::NormalFile>(m_real_path);
  }
};


} // namespace


namespace il2ge
{


class MapLoaderSFSDump::ResourceProvider
{
  std::unordered_map<int64_t, std::filesystem::path> m_real_path;
  std::filesystem::path m_base_dir;

  void readDir(std::filesystem::path path)
  {
    std::filesystem::directory_iterator it(path);
    for (auto &entry : it)
    {
      if (entry.is_directory())
      {
        readDir(entry.path());
      }
      else if (entry.is_regular_file())
      {
        auto sfs_path = std::filesystem::relative(entry.path(), m_base_dir);

        LOG_DEBUG << "adding sfs path: " << sfs_path.u8string()
                  << " real path: " << entry.path().u8string() << std::endl;

        auto hash = sfs::getHash(sfs_path.u8string().c_str());
        m_real_path[hash] = entry.path();
      }
    }
  }

public:
  void setBaseDir(std::filesystem::path dir)
  {
    LOG_INFO << "SFS base dir: " << dir.u8string() << std::endl;
    m_base_dir = dir;
    m_real_path.clear();
    LOG_INFO << "Indexing files ..." << std::endl;
    readDir(dir);
    LOG_INFO << "Indexing files ... done." << std::endl;
  }

  std::unique_ptr<il2ge::Resource> get(std::string sfs_path)
  {
    auto hash = sfs::getHash(sfs_path.c_str());

    std::filesystem::path real_path;

    auto it = m_real_path.find(hash);

    if (it != m_real_path.end())
    {
      real_path = it->second;
    }

    if (real_path.empty())
    {
      throw std::runtime_error(sfs_path + " not found");
    }

    return std::make_unique<SFSDumpResource>(sfs_path, real_path);
  }
};


MapLoaderSFSDump::MapLoaderSFSDump(std::filesystem::path ini_path,
                                   std::filesystem::path sfs_base_dir)
{
  m_provider = std::make_unique<ResourceProvider>();
  m_provider->setBaseDir(sfs_base_dir);

  auto get_resource = [this] (std::string path)
  {
    return m_provider->get(path);
  };

  auto ini_path_sfs = std::filesystem::relative(ini_path, sfs_base_dir);
  assert(!ini_path_sfs.empty());
  assert(ini_path_sfs.has_filename());

  auto map_dir_sfs = ini_path_sfs.parent_path() / "";
  assert(!map_dir_sfs.empty());
  assert(!map_dir_sfs.has_filename());

  LOG_INFO << "map_dir_sfs: " << map_dir_sfs.u8string() << std::endl;
  LOG_INFO << "ini_path_sfs: " << ini_path_sfs.u8string() << std::endl;

  std::string dump_path;

  m_map_resources = getSfsMapResources(map_dir_sfs.u8string(),
                                       ini_path_sfs.u8string(),
                                       dump_path,
                                       get_resource);

  m_terrain_resources = il2ge::map_loader::getTerrainResources(*m_map_resources);
}


MapLoaderSFSDump::~MapLoaderSFSDump() {}


render_util::TerrainResourcesBase& MapLoaderSFSDump::getTerrainResources()
{
  assert(m_terrain_resources);
  return *m_terrain_resources;
}


const std::shared_ptr<render_util::GenericImage> MapLoaderSFSDump::getCirrusTexture() const
{
  abort();
}


} // namespace il2ge
