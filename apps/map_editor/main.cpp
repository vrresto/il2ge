/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#define GLM_ENABLE_EXPERIMENTAL

#include <map_loader_sfs_dump.h>
#include <il2ge/map_properties.h>
#include <il2ge/map_loader.h>
#include <il2ge/exception_handler.h>
#include <render_util/viewer/editor_backend.h>
#include <render_util/viewer/editor.h>
#include <render_util/image_loader.h>
#include <render_util/physics.h>
#include <util.h>
#include <log.h>

#include <INIReader.h>

#include <glm/glm.hpp>
#include <glm/gtx/vec_swizzle.hpp>
#include <iostream>

#ifdef _WIN32
  #include <windows.h>
  #include <shlobj.h>
  #include <direct.h>
#else
  #include <portable-file-dialogs.h>
#endif

using namespace std;
using namespace glm;
using namespace render_util;
using namespace il2ge;


bool il2ge::map_loader::isDumpEnabled()
{
  return false;
}


namespace
{


const std::string APP_NAME = "il2ge_map_editor";
const std::string SFS_DUMP_DIR = "dump";


#ifdef _WIN32

std::string getExeFilePath()
{
  char module_file_name[MAX_PATH];

  if (!GetModuleFileNameA(0,
      module_file_name,
      sizeof(module_file_name)))
  {
    abort();
  }

  return module_file_name;
}


int showOkCancelDialog(std::string text, std::string title)
{
  return MessageBoxA(nullptr, text.c_str(), title.c_str(), MB_OKCANCEL | MB_ICONQUESTION)
      == IDOK;
}

#else

bool showOkCancelDialog(std::string text, std::string title)
{
  return pfd::message(title, text, pfd::choice::ok_cancel,
                      pfd::icon::question).result()
      == pfd::button::ok;
}

#endif


struct Backend final : public render_util::viewer::EditorBackend
{
  struct Layer final : public EditorBackend::Layer
  {
    std::unique_ptr<render_util::MapGeography> map_geography;
    unsigned resolution_m = 200;
    glm::ivec2 map_size_px {};
    ElevationMap::Ptr m_elevation_map;


    unsigned getResolutionM() override
    {
      return resolution_m;
    }

    glm::ivec2 getSizePx() override
    {
      assert(map_size_px != glm::ivec2(0));
      return map_size_px;
    }

    ElevationMap::Ptr createElevationMap() override
    {
      assert(m_elevation_map);
      return m_elevation_map;
    }

    const render_util::MapGeography& getMapGeography() override
    {
      assert(map_geography);
      return *map_geography;
    }
  };

  std::string m_map_path;
  std::string m_load_ini_path;
  std::string m_load_il2ge_ini_path;
  std::string m_base_map_path;
  std::string m_base_map_data_path;
  std::string m_base_map_properties_path;
  std::string m_water_map_chunks_path;
  std::string m_water_map_table_path;
  std::string m_base_water_map_texture_path;
  bool m_has_base_map = false;
  std::vector<AltitudinalZone> m_altitudinal_zones;
  std::map<std::string, int> m_land_cover_type_mapping;
  std::vector<LandCoverType> m_land_cover_types;
  std::unique_ptr<render_util::viewer::MapLoaderBase> m_map_loader;

  std::vector<Layer*> m_layers;

  Layer m_detail_layer;
  Layer m_base_layer;

  Backend(std::string map_path, std::string base_map_path)
  {
    LOG_INFO << "Initializing backend ..." << std::endl;

    for (auto& type : BaseMapResources::getLandCoverTypes())
      m_land_cover_types.push_back(type);

    m_layers.push_back(&m_detail_layer);

    auto map_dir = util::getDirFromPath(map_path);

    bool map_path_is_ini = util::makeLowercase(util::getFileExtensionFromPath(map_path)) == "ini";

    if (map_path_is_ini)
    {
      m_load_ini_path = map_path;
      m_load_il2ge_ini_path = map_dir + '/' + util::basename(map_path, true) + ".il2ge.ini";

      INIReader ini(map_path);
      if (!ini.ParseError())
      {
        auto heightmap = ini.Get("MAP", "HeightMap", "");
        assert(!heightmap.empty());
        m_map_path = map_dir + '/' + heightmap;

        auto map_c = ini.Get("MAP", "ColorMap", "");
        assert(!map_c.empty());
        m_water_map_chunks_path = map_dir + '/' + map_c;
        m_water_map_table_path = m_water_map_chunks_path + "_table";

        if (util::isPrefix(SFS_DUMP_DIR, map_path))
        {
          m_map_loader =
            std::make_unique<MapLoaderSFSDump>(std::filesystem::u8path(map_path),
                                               std::filesystem::u8path(SFS_DUMP_DIR));
        }
      }
      else
      {
        throw std::runtime_error("Error parsing " + map_path
                                  + " at line " + std::to_string(ini.ParseError()));
      }
    }
    else
    {
      m_map_path = map_path;
    }

    assert(base_map_path.empty());
//     m_base_map_path = base_map_path;

    if (!m_load_il2ge_ini_path.empty())
    {
      if (util::fileExists(m_load_il2ge_ini_path))
      {
        INIReader ini(m_load_il2ge_ini_path);
        if (!ini.ParseError())
        {
//           if (m_base_map_path.empty())
//             m_base_map_path = ini.Get("Geography", "BaseMap", "");
          m_detail_layer.map_geography = loadMapGeography(ini, m_load_il2ge_ini_path);
          m_altitudinal_zones = loadAltitudinalZones(ini, m_load_il2ge_ini_path);
          m_land_cover_type_mapping = loadLandCoverTypes(ini, m_load_il2ge_ini_path);
        }
        else
        {
          throw std::runtime_error("Error parsing " + m_load_il2ge_ini_path
                                    + " at line " + std::to_string(ini.ParseError()));
        }
      }
    }

    assert(!m_map_path.empty());

    if (m_base_map_path.empty())
    {
      m_base_map_path = "il2ge_base_map/" + util::basename(map_dir);
      m_base_map_data_path = m_base_map_path + ".data";
      m_base_map_properties_path = m_base_map_path + ".properties";
      m_base_water_map_texture_path = m_base_map_path + ".water.tga";
    }

    if (!m_base_map_path.empty())
    {
      if (util::fileExists(m_base_map_properties_path) && m_detail_layer.map_geography)
      {
        m_has_base_map = true;
        INIReader ini(m_base_map_properties_path);
        if (!ini.ParseError())
        {
          m_base_layer.map_geography = loadMapGeography(ini, m_base_map_properties_path);
          m_base_layer.map_size_px.x = ini.GetInteger("", "Width", 0);
          m_base_layer.map_size_px.y = ini.GetInteger("", "Height", 0);
        }
        else
        {
          throw std::runtime_error("Error parsing " + m_base_map_properties_path
                                    + " at line " + std::to_string(ini.ParseError()));
        }

        if (!m_detail_layer.map_geography && m_base_layer.map_geography)
        {
          m_detail_layer.map_geography =
            std::make_unique<render_util::MapGeography>(*m_base_layer.map_geography);
        }

        m_layers.push_back(&m_base_layer);
      }
    }

    if (!m_detail_layer.map_geography)
    {
      m_detail_layer.map_geography = createDefaultMapGeography();
    }

    assert(m_detail_layer.map_geography);

    LOG_INFO << "Loading base height map ..." << std::endl;
    m_detail_layer.m_elevation_map = createElevationMap();
    m_detail_layer.map_size_px = m_detail_layer.m_elevation_map->getSize();

    m_base_layer.m_elevation_map = createBaseElevationMap();
    LOG_INFO << "Loading base height map ... done." << std::endl;


    assert(!m_map_path.empty());
    assert(util::fileExists(m_map_path));

    int num_components = 0;
    getImageInfo(m_map_path, m_detail_layer.map_size_px, num_components);

    LOG_INFO << "Initializing backend ... done." << std::endl;
  }

  virtual bool hasBaseLayer() override
  {
    return m_has_base_map;
  }

  Layer& getLayer(int number) override
  {
    return *m_layers.at(number);
  }

  std::vector<AltitudinalZone> getAltitudinalZones() override
  {
    return m_altitudinal_zones;
  }

  render_util::ElevationMap::Ptr createElevationMap()
  {
    auto hm_image =
      render_util::loadImageFromFile<render_util::ImageGreyScale>(m_map_path);

    if (hm_image)
    {
      m_detail_layer.map_size_px = hm_image->getSize();
      return il2ge::map_loader::createElevationMap(hm_image);
    }
    else
      throw std::runtime_error("Failed to load height map: " + m_map_path);
  }

  render_util::ElevationMap::Ptr createBaseElevationMapBin() const
  {
    LOG_INFO << "Opening " << m_base_map_data_path << std::endl;
    try
    {
      util::NormalFile file(m_base_map_data_path);
      return il2ge::map_loader::createBaseElevationMap(file, m_base_layer.map_size_px);
    }
    catch (std::exception &e)
    {
      LOG_ERROR << e.what() << std::endl;
      return {};
    }
  }

  render_util::ElevationMap::Ptr createBaseElevationMapTGA() const
  {
      auto hm_image =
        render_util::loadImageFromFile<render_util::ImageGreyScale>(m_base_map_path);

      if (hm_image)
        return il2ge::map_loader::createElevationMap(hm_image);
      else
        exit(1);
  }

  render_util::ElevationMap::Ptr createBaseElevationMap() const
  {
    if (!m_base_map_path.empty() && m_has_base_map)
    {
      auto ext = util::makeLowercase(util::getFileExtensionFromPath(m_base_map_path));

      if (ext == "tga")
      {
        return createBaseElevationMapTGA();
      }
      else if(ext.empty())
      {
        return createBaseElevationMapBin();
      }
      else
      {
        cerr<<"Unhandled extension: "<<ext<<endl;
        exit(1);
      }
    }
    else
      return {};
  }

//   double getBaseMapMetersPerDegreeLongitude() const
//   {
//     assert(m_has_base_map);
//     assert(m_base_layer.map_geography.getMetersPerDegreeLongitude() > 0);
//     return m_base_layer.map_geography.getMetersPerDegreeLongitude();
//   }
//
//   double getDetailMapMetersPerDegreeLongitude() const
//   {
//     assert(m_detail_layer.map_geography.getMetersPerDegreeLongitude() > 0);
//     return m_detail_layer.map_geography.getMetersPerDegreeLongitude();
//   }
//
//   glm::dvec2 getBaseElevationMapOrigin() const
//   {
//     assert(m_has_base_map);
//     return m_base_layer.map_geography.getOrigin();
//   }

  std::string getBaseMapPath() override
  {
    return m_base_map_path;
  }

  void saveMapAttributes(const glm::dvec2 &origin,
                         double meters_per_degree_longitude,
                         string map_projection,
                         const std::vector<AltitudinalZone>& altitudinal_zones,
                         const std::map<std::string, int>& base_map_types) override
  {
#if 1
    std::string ini_path;

    if (!m_load_il2ge_ini_path.empty())
      ini_path = m_load_il2ge_ini_path;
    else
      ini_path = m_map_path + ".ini";

    if (util::fileExists(ini_path))
    {
      if (!showOkCancelDialog("Overwrite " + ini_path + "?", "Confirm overwrite"))
        return;
    }

    std::ofstream out(ini_path);
    MapGeography g(origin, meters_per_degree_longitude, map_projection);
    saveMapGeography(g, out);
    out << "BaseMap = " << m_base_map_path << std::endl;

    saveAltitudinalZones(altitudinal_zones, out);
    saveLandCoverTypes(base_map_types, out);
#endif
  }

  void reloadBaseMap() override
  {
#if 0
    INIReader ini(m_base_map_properties_path);
    if (!ini.ParseError())
    {
      m_has_base_map = true;
      m_base_layer.map_geography = MapGeography(ini, m_base_map_properties_path);
    }
    else
    {
      throw std::runtime_error("Error parsing " + m_base_map_properties_path
                                + " at line " + std::to_string(ini.ParseError()));
    }
#endif
  }

  std::string getMapProjection() override
  {
    return m_detail_layer.getMapGeography().getMapProjection();
  }

  render_util::TerrainResourcesBase::WaterMap loadWaterMap() override
  {
    abort();

    auto chunks = render_util::loadImageFromFile<render_util::ImageGreyScale>(
      m_water_map_chunks_path);
    assert(chunks);

    auto table = util::readFile<char>(m_water_map_table_path);
    assert(!table.empty());

    return il2ge::map_loader::createWaterMap(m_detail_layer.map_size_px, chunks, table);
  }

  render_util::ImageGreyScale::Ptr loadBaseWaterMap() override
  {
    return render_util::loadImageFromFile<render_util::ImageGreyScale>(
      m_base_water_map_texture_path);
  }

  std::string getBaseWaterMapPath() override
  {
    return m_base_water_map_texture_path;
  }

  render_util::viewer::MapLoaderBase* getMapLoader() override
  {
    return m_map_loader.get();
  }

  const std::vector<std::string>& getLandCoverTypeNames() override
  {
    return BaseMapResources::getLandCoverTypeNames();
  }

  std::map<std::string, int> getLandCoverTypeMapping() override
  {
    return m_land_cover_type_mapping;
  }

  const std::vector<LandCoverType>& getLandCoverTypes() override
  {
    return m_land_cover_types;
  }
};


} // namespace


int main(int argc, char **argv)
{
#ifdef _WIN32
  il2ge::exception_handler::install(APP_NAME + "_crash.log");
  il2ge::exception_handler::watchModule(GetModuleHandle(0));
#endif

  string map_path;
  string base_map_path;

  if (argc == 2)
  {
    map_path = argv[1];
  }
  else if (argc == 3)
  {
    map_path = argv[1];
    base_map_path = argv[2];
  }
  else
  {
    cerr << "Wrong number of arguments: " << argc << endl;
    return 1;
  }

  assert(!map_path.empty());

  render_util::viewer::CreateEditorBackendFunc create_map_loader_func =
    [map_path, base_map_path] ()
  {
    return make_shared<Backend>(map_path, base_map_path);
  };

  viewer::runMapEditor(create_map_loader_func, APP_NAME);
}
