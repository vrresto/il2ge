/**
 *    Rendering utilities
 *    Copyright (C) 2019  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDER_UTIL_VIEWER_SCENEBASE_H
#define RENDER_UTIL_VIEWER_SCENEBASE_H

#include "camera.h"
#include <il2ge/renderer.h>
#include <il2ge/mesh/mesh_base.h>

#include <render_util/viewer/parameter.h>
#include <render_util/render_util.h>
#include <render_util/shader.h>
#include <render_util/terrain_util.h>
#include <render_util/atmosphere.h>
#include <render_util/parameter_wrapper.h>
#include <render_util/gl_binding/gl_functions.h>



namespace il2ge::renderer
{
  class Mesh;
  class Renderer;
}


namespace il2ge::mesh_viewer
{


struct MeshViewerScene;


class MeshViewer
{
public:
  Camera camera;
  il2ge::renderer::Renderer &m_renderer;
  std::vector<std::string> m_paths;
  std::string m_path;
  glm::ivec3 m_instances = glm::ivec3(5, 5, 1);
  Camera::Unit m_instance_spacing = 100;
  bool m_single_instance = true;

  MeshViewer(const std::vector<std::string>& paths, il2ge::renderer::Renderer&);
  ~MeshViewer();

  // render_util::TextureManager &getTextureManager() { return m_renderer.getTextureManager(); }

  glm::vec3 getSunDir() { return glm::normalize(glm::vec3(0,0,1)); }
  void cursorPos(glm::dvec2) {}

  const std::vector<std::unique_ptr<render_util::viewer::Parameter>> &getParameters()
  {
    return m_parameters.getParameters();
  }
  render_util::viewer::Parameter &getActiveParameter() { return m_parameters.getActive(); }
  void setActiveParameter(int index) { m_parameters.setActive(index); }
  int getActiveParameterIndex() { return m_parameters.getActiveIndex(); }
  bool hasParameters() { return !m_parameters.getParameters().empty(); }

  void render(float frame_delta);
  void setup();
  void updateUniforms(render_util::ShaderProgramPtr);

  void printStats(std::ostream &out);

private:
  void enableLod(bool);
  bool isLodEnabled();
  void enableInstancedDrawing(bool);

  std::unique_ptr<MeshViewerScene> m_scene;
  render_util::viewer::Parameters m_parameters;
  int m_max_transparent_visibility = 1000;
  bool m_is_instanced_drawing_enabled = true;
};


} // namespace il2ge::mesh_viewer


#endif
