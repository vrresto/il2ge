/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define GLM_ENABLE_EXPERIMENTAL

#include "scene.h"
#include <il2ge/renderer.h>
#include <render_util/render_util.h>
#include <render_util/water.h>
#include <render_util/shader.h>
#include <render_util/shader_util.h>
#include <render_util/texture_manager.h>
#include <render_util/map_textures.h>
#include <render_util/texture_util.h>
#include <render_util/texunits.h>
#include <render_util/image_loader.h>
#include <render_util/image_util.h>
#include <render_util/elevation_map.h>
#include <render_util/camera.h>
#include <render_util/terrain_util.h>
#include <render_util/image_util.h>
#include <render_util/state.h>
#include <render_util/gl_binding/gl_binding.h>
#include <log.h>

#include <glm/gtx/vec_swizzle.hpp>
#include <memory>


#define USE_RENDERER 1


using namespace il2ge::mesh_viewer;
using namespace render_util::gl_binding;


struct il2ge::mesh_viewer::SceneContent
{
  Scene &m_scene;

  SceneContent(Scene &scene) : m_scene(scene) {}
  virtual ~SceneContent() {}

  virtual void render() = 0;
  virtual void enableLod(bool) {}
  virtual void printStats(std::ostream &out) {}
};


namespace
{


constexpr auto INSTANCE_SPACING = 100;


Camera::Vec3 getWorldCenter()
{
//   return Camera::Vec3(1000000);
  return Camera::Vec3(0);
}


struct SimpleMeshSceneContent : public il2ge::mesh_viewer::SceneContent
{
  std::unique_ptr<il2ge::mesh::MeshBase> m_mesh;
  size_t m_frame = 0;

  SimpleMeshSceneContent(Scene &scene) : SceneContent(scene)
  {
    m_mesh = il2ge::mesh::loadMesh(m_scene.m_path);
    m_mesh->attachToRenderer(&m_scene.m_renderer);
  }

  ~SimpleMeshSceneContent()
  {
  }

  void printStats(std::ostream &out) override
  {
    out << "frames: " << m_mesh->getFrameCount() << std::endl;
    out << "current frame: " << m_frame << std::endl;
  }

  void render() override
  {
    // static bool first_frame = true;
    
    m_frame++;
    if (m_frame >= m_mesh->getFrameCount())
      m_frame = 0;
    
#if USE_RENDERER
    for (int x = 0; x < m_scene.m_instances.x; x++)
    {
      for (int y = 0; y < m_scene.m_instances.y; y++)
      {
        for (int z = 0; z < m_scene.m_instances.z; z++)
        {
          Camera::Vec3 pos(x * INSTANCE_SPACING, y * INSTANCE_SPACING, z * INSTANCE_SPACING);
          auto model_world = glm::translate(Camera::Mat4(1.0), pos);
          auto model_view = m_scene.camera.getWorld2ViewMatrixD() * model_world;
          m_mesh->draw(model_view, glm::distance(m_scene.camera.getPosD(), pos), m_frame);
        }
      }
    }

#else

    auto &lod = m_mesh->getBaseLod();

    const auto &verts = lod.getVertices();
    const auto &indices = lod.getIndices();

    gl::LoadMatrixf(glm::value_ptr(m_scene.camera.getVP_s()));
    
    auto start_vertex = lod.getVertexCount() * m_frame;

    gl::PolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    gl::Begin(GL_TRIANGLES);
    for (const auto &fg : lod.getFaceGroups())
    {
      for (int i = 0; i < fg.FaceCount * 3; i++)
      {
        auto index = start_vertex + fg.StartVertex + indices[fg.StartFace * 3 + i];

        const auto &v = verts[index].Position;
        
//         if (first_frame)
//           LOG_INFO << "vertex: " << v << std::endl;
        gl::Vertex3f(v.x, v.y, v.z);
      }
    }
    gl::End();

    gl::PolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#endif
    
    // first_frame = false;
  }
};


struct HierMeshMeshSceneContent : public SceneContent
{
  std::unordered_map<il2ge::mesh::MeshBase*, std::string> m_submesh_paths;
  std::unordered_map<std::string, std::weak_ptr<il2ge::mesh::MeshBase>> m_submeshes;
  std::unique_ptr<il2ge::mesh::HierMeshBase> m_mesh;


  HierMeshMeshSceneContent(Scene &scene) : SceneContent(scene)
  {
    auto delete_mesh = [this] (il2ge::mesh::MeshBase *mesh)
    {
      auto path = m_submesh_paths[mesh];
      m_submesh_paths.erase(mesh);

      assert(!path.empty());
      m_submeshes.erase(path);

      delete mesh;
    };

    auto get_mesh = [this, delete_mesh] (std::string path)
    {
      auto &mesh = m_submeshes[path];

      if (mesh.expired())
      {
        std::shared_ptr<il2ge::mesh::MeshBase> new_mesh = il2ge::mesh::loadMesh(path, delete_mesh);
        m_submesh_paths[new_mesh.get()] = path;
        mesh = new_mesh;
        return new_mesh;
      }
      else
      {
        return mesh.lock();
      }
    };

    m_mesh = il2ge::mesh::loadHierMesh(m_scene.m_path, get_mesh);

    LOG_INFO << "Number of submeshes: " << m_submeshes.size() << std::endl;

    for (auto &m : m_submeshes)
    {
      if (!m.second.expired())
        m.second.lock()->attachToRenderer(&m_scene.m_renderer);
    }
  }


  void enableLod(bool value) override
  {
    for (auto &m : m_submeshes)
    {
      if (!m.second.expired())
        m.second.lock()->enableLod(value);
    }
  }


  void render() override
  {
    if (m_scene.m_single_instance)
    {
      auto pos = getWorldCenter();
      auto model_world = glm::translate(Camera::Mat4(1.0), pos);
      auto model_view = m_scene.camera.getWorld2ViewMatrixD() * model_world;
      m_mesh->draw(model_view, glm::distance(pos, m_scene.camera.getPosD()));
    }
    else
    {
      for (int x = 0; x < m_scene.m_instances.x; x++)
      {
        for (int y = 0; y < m_scene.m_instances.y; y++)
        {
          for (int z = 0; z < m_scene.m_instances.z; z++)
          {
            auto pos = getWorldCenter() + Camera::Vec3(x * INSTANCE_SPACING,
                                                       y * INSTANCE_SPACING,
                                                       z * INSTANCE_SPACING);
            auto model_world = glm::translate(Camera::Mat4(1.0), pos);
            auto model_view = m_scene.camera.getWorld2ViewMatrixD() * model_world;
            m_mesh->draw(model_view, glm::distance(pos, m_scene.camera.getPosD()));
          }
        }
      }
    }
  }
};


}


namespace il2ge::mesh_viewer
{


Scene::Scene(std::string path, il2ge::renderer::Renderer &r) : m_renderer(r), m_path(path)
{
}


Scene::~Scene()
{
}


void Scene::setup()
{
  getTextureManager().setActive(true);

  if (util::isSuffix(".msh", m_path))
    m_content = std::make_unique<SimpleMeshSceneContent>(*this);
  else if (util::isSuffix(".him", m_path))
    m_content = std::make_unique<HierMeshMeshSceneContent>(*this);
  else
    throw std::runtime_error("Unknown file extension.");

  camera.x = getWorldCenter().x;
  camera.y = getWorldCenter().y;
  camera.z = getWorldCenter().z;
  camera.updateTransform();

  camera.setProjection(90, 1.0, 20000.0);

  m_parameters.add("max_transparent_visibility", m_max_transparent_visibility);
  m_parameters.addBool("single_instance", m_single_instance);

  enableLod(true);
  m_parameters.addBool("enable_lod",
                       [this] (bool value) { enableLod(value); },
                       true);

  m_parameters.add("instances_x", m_instances.x);
  m_parameters.add("instances_y", m_instances.y);
  m_parameters.add("instances_z", m_instances.z);
}


void Scene::enableLod(bool value)
{
  m_content->enableLod(value);
}


void Scene::render(float frame_delta)
{
  m_renderer.setMaxTransparentVisibility(m_max_transparent_visibility);
  m_renderer.applyCamera(camera);
  m_content->render();

  auto set_uniforms = [this] (render_util::ShaderProgramPtr program)
  {
    updateUniforms(program);
  };

  m_renderer.flushRenderList(set_uniforms);

}


void Scene::updateUniforms(render_util::ShaderProgramPtr program)
{
  glm::vec3 sun_dir_view = camera.getWorld2ViewMatrix() * glm::vec4(getSunDir(), 0);

  program->setUniform("sunDir", getSunDir());
  program->setUniform("sun_dir_world", getSunDir());
  program->setUniform("sun_dir_view", sun_dir_view);
}


void Scene::printStats(std::ostream &out)
{
  out << "object instances: " << m_instances.x * m_instances.y * m_instances.z << std::endl;
  m_content->printStats(out);
  m_renderer.printStats(out);
}


}
