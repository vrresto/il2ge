/**
 *    Rendering utilities
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define GLM_ENABLE_EXPERIMENTAL

#include "viewer.h"
#include <il2ge/renderer.h>
#include <il2ge/material.h>
#include <il2ge/effect3d.h>
#include <il2ge/image_loader.h>
#include <il2ge/object_map.h>
#include <render_util/render_util.h>
#include <render_util/shader.h>
#include <render_util/shader_util.h>
#include <render_util/texture_util.h>
#include <render_util/texunits.h>
#include <render_util/image_loader.h>
#include <render_util/image_util.h>
#include <render_util/elevation_map.h>
#include <render_util/camera.h>
#include <render_util/terrain_util.h>
#include <render_util/image_util.h>
#include <render_util/state.h>
#include <render_util/gl_binding/gl_binding.h>
#include <log.h>

#include <glm/gtx/vec_swizzle.hpp>
#include <memory>
#include <variant>


#define USE_RENDERER 1


using namespace il2ge;
using namespace il2ge::mesh_viewer;
using namespace render_util::gl_binding;


namespace
{


using Drawable = std::variant<std::shared_ptr<mesh::HierMeshBase>,
                              std::shared_ptr<mesh::MeshInstance>,
                              std::shared_ptr<Effect3D>>;


struct Actor
{
  Camera::Vec3 pos {};
  Drawable drawable;
  float frame {};
};


template <class T>
int getFrameCount(T&)
{
  return 0;
}


template <>
int getFrameCount(mesh::MeshInstance& inst)
{
  return inst.getMesh().getFrameCount();
}


int getFrameCount(Drawable& d)
{
  int count = 0;
  auto visitor = [&] (auto& obj)
  {
    count = getFrameCount(*obj);
  };
  std::visit(visitor, d);
  return count;
}


std::shared_ptr<Material::TextureAndSize> createTexture(const std::string texture_path_)
{
  auto texture_path = texture_path_;
  LOG_INFO << "Loading texture: " << texture_path << std::endl;

  // HACK
  auto dir = util::getDirFromPath(texture_path);
  auto name = util::basename(texture_path);
  if (util::isPrefix("skin1", name))
  {
    if (util::fileExists(dir + "/summer/" + name))
      texture_path = dir + "/summer/" + name;
    else if (util::fileExists(dir + "/winter/" + name))
      texture_path = dir + "/winter/" + name;
  }

  auto image_data = util::readFile<char>(texture_path);
  auto image = il2ge::loadImageFromMemory(image_data, texture_path.c_str());
  assert(image);

  if (image->numComponents() == 1)
  {
    auto rgba_image = std::make_shared<render_util::GenericImage>(image->size(), 4);
    for (int y = 0; y < rgba_image->h(); y++)
    {
      for (int x = 0; x < rgba_image->w(); x++)
      {
        rgba_image->at(x,y,0) = 255;
        rgba_image->at(x,y,1) = 255;
        rgba_image->at(x,y,2) = 255;
        rgba_image->at(x,y,3) = image->get(x,y,0);
      }
    }
    image = rgba_image;
  }

  auto ret = std::make_shared<Material::TextureAndSize>(texture_path_);

  ret->size = image->getSize();
  ret->texture = render_util::createTexture(image);

  return ret;
}


} // namespace


struct il2ge::mesh_viewer::MeshViewerScene
{
  MeshViewer& m_viewer;
  CachedAutoAddingWeakMapWithFactory<Material::TextureAndSize> m_textures;
  std::unordered_map<std::string, std::shared_ptr<il2ge::Material>> m_materials;

  MeshViewerScene(MeshViewer& viewer) :
    m_viewer(viewer),
    m_textures(&createTexture, 1000) {}

  virtual ~MeshViewerScene() {}

  const std::shared_ptr<il2ge::Material> getMaterial(std::string path)
  {
    auto& material = m_materials[path];
    if (!material)
    {
      try
      {
        LOG_INFO << "Loading material: " << path << std::endl;
        material = std::make_unique<Material>(path,
                            [&] (auto& path) { return m_textures.get(path); });

        material->path = path;
      }
      catch (std::exception& e)
      {
        LOG_ERROR << e.what() << std::endl;
      }
    }
    return material;
  }

  virtual void render(float frame_delta) = 0;
  virtual void enableLod(bool) {}
  virtual bool isLodEnabled() { return true; }
  virtual void printStats(std::ostream &out) {}
};


namespace
{


constexpr int ANIMATION_FPS = 30;


struct SimpleMaterialList : public il2ge::MaterialList
{
  void add(std::shared_ptr<il2ge::Material> mat)
  {
    m_materials.push_back(mat);
  }
};


Camera::Vec3 getWorldCenter()
{
//   return Camera::Vec3(1000000);
  return Camera::Vec3(0);
}


std::string resolveRelativePath(std::string base_dir, std::string path)
{
  return util::resolveRelativePathComponents(base_dir + '/' + path);
}


struct BasicScene : public MeshViewerScene
{
  AutoAddingWeakMapWithFactory<const il2ge::mesh::MeshBase> m_submeshes;
  std::shared_ptr<renderer::SharedVAO> m_shared_vao;
  std::vector<std::unique_ptr<Effect3DParameters>> m_effect_params; //FIXME

  BasicScene(MeshViewer& viewer) : MeshViewerScene(viewer),
    m_submeshes([this] (std::string path)
    {
      auto mesh = il2ge::mesh::loadMesh(path);
      mesh->createRendererData();
      return mesh;
    })
  {};

  void mergeMeshes()
  {
    std::vector<renderer::MeshRendererData*> all_meshes;

    for (auto& it : m_submeshes.getObjects())
    {
      auto mesh = it.second.lock();
      if (!mesh)
        continue;

      for (int lod = 0; lod < mesh->getLodCount(); lod++)
      {
        auto& lod_mesh = mesh->getLod(lod);
        all_meshes.push_back(lod_mesh.getRendererData());
      }
    }

    if (!m_shared_vao)
      m_shared_vao = renderer::createSharedVAO();

    LOG_INFO << "Merging meshes ..." << std::endl;
    addToSharedVAO(m_shared_vao, all_meshes);
  }

  auto loadHierMesh(std::string path)
  {
    auto dir = util::getDirFromPath(path);

    auto get_mesh = [this] (std::string path)
    {
      return m_submeshes.get(path);
    };

    auto hiermesh = il2ge::mesh::loadHierMesh(path, get_mesh);

    auto materials = std::make_unique<SimpleMaterialList>();
    for (auto& name : hiermesh->getMaterialNames())
    {
      materials->add(getMaterial(dir + '/' + name + ".mat"));
    }
    hiermesh->setMaterials(std::move(materials));

    return hiermesh;
  }

  auto loadMesh(std::string path) -> std::shared_ptr<mesh::MeshInstance>
  {
    auto dir = util::getDirFromPath(path);
    auto mesh = m_submeshes.get(path);

    auto materials = std::make_unique<SimpleMaterialList>();
    for (auto& name : mesh->getMaterialNames())
      materials->add(getMaterial(dir + '/' + name + ".mat"));

    return std::make_unique<mesh::MeshInstance>(mesh, *materials);
  }

  auto loadEffect(std::string path) -> std::shared_ptr<Effect3D>
  {
    auto read_file = [] (std::string path)
    {
      return util::readFile<char>(path);
    };

    ParameterFiles files(read_file);

    std::string class_name;

    auto class_name_visitor = [&] (auto& p)
    {
      auto& class_info = p.getSection("ClassInfo");
      std::string tmp;
      class_info.get("ClassName", tmp);
      if (!tmp.empty())
        class_name = tmp;
    };

    visit(files, path, class_name_visitor, true);

    assert(!class_name.empty());

    auto effect_params = createEffect3DParameters(class_name);
    assert(effect_params);

    auto visitor = [&] (auto& p)
    {
      effect_params->applyFrom(p);
    };

    visit(files, path, visitor, true);

    auto material_name = effect_params->material_name;
    assert(!material_name.empty());

    auto material_path = resolveRelativePath(util::getDirFromPath(path), material_name);
    auto material = getMaterial(material_path);
    assert(material);

    auto effect = effect_params->createEffect();
    assert(effect);

    effect->setRotationDeg(90, 0, 0);
    effect->setIntensity(1);
    effect->setPos(glm::vec3(0));

    m_effect_params.push_back(std::move(effect_params)); //FIXME

    effect->material = material;

    return effect;
  }

  Drawable load(std::string path)
  {
    if (util::isSuffix(".him", util::makeLowercase(path)))
      return loadHierMesh(path);
    else if (util::isSuffix(".msh", util::makeLowercase(path)))
      return loadMesh(path);
    else if (util::isSuffix(".sim", util::makeLowercase(path)))
      return loadMesh(path);
    else if (util::isSuffix(".eff", util::makeLowercase(path)))
      return loadEffect(path);
    else
      throw std::runtime_error("Unknown file extension.");
  }

  void update(mesh::HierMeshBase& mesh, float frame_delta) {}

  void render(mesh::HierMeshBase& mesh, Camera::Vec3 pos, int frame)
  {
    pos += getWorldCenter();
    auto model_world = glm::translate(Camera::Mat4(1.0), pos);
    auto model_view = m_viewer.camera.getWorld2ViewMatrixD() * model_world;
    mesh.draw(model_view, glm::distance(pos, m_viewer.camera.getPosD()),
              m_viewer.m_renderer);
  }

  void update(mesh::MeshInstance& mesh, float frame_delta) {}

  void render(mesh::MeshInstance& mesh, Camera::Vec3 pos, int frame)
  {
    pos += getWorldCenter();
    auto model_world = glm::translate(Camera::Mat4(1.0), pos);
    auto model_view = m_viewer.camera.getWorld2ViewMatrixD() * model_world;
    auto dist = glm::distance(m_viewer.camera.getPosD(), pos);
    mesh.draw(model_view, dist, m_viewer.m_renderer, frame);
  }

  void update(Effect3D& effect, float frame_delta)
  {
    // assert(0);
    effect.update(frame_delta, glm::vec2(0));
  }

  void render(Effect3D& effect, Camera::Vec3 pos, int frame)
  {
    render_util::StateModifier state;
    state.setDefaults();

    state.enableCullFace(false);
    state.enableDepthTest(false);

    // effect.render();
    effect.addToRenderList(m_viewer.m_renderer, m_viewer.camera);
  }

  void renderDrawable(Drawable& d, Camera::Vec3 pos, int frame)
  {
    auto visitor = [&] (auto& obj)
    {
      render(*obj, pos, frame);
    };
    std::visit(visitor, d);
  }

  void renderActor(Actor& actor)
  {
    renderDrawable(actor.drawable, actor.pos, actor.frame);
  };

  void updateDrawable(Drawable d, float frame_delta)
  {
    auto visitor = [&] (auto& obj)
    {
      update(*obj, frame_delta);
    };

    std::visit(visitor, d);
  }

  void updateActor(Actor& actor, float frame_delta)
  {
    actor.frame += frame_delta * ANIMATION_FPS;
    if (actor.frame >= getFrameCount(actor.drawable))
      actor.frame = 0;

    updateDrawable(actor.drawable, frame_delta);
  }
};


struct SimpleScene : public BasicScene
{
  Drawable m_drawable;
  float m_frame = 0;

  SimpleScene(MeshViewer &v) : BasicScene(v)
  {
    m_drawable = load(m_viewer.m_path);
    mergeMeshes();
  }

  void printStats(std::ostream &out) override
  {
    out << "frames: " << getFrameCount(m_drawable) << std::endl;
    out << "current frame: " << m_frame << std::endl;
  }

  void render(float frame_delta) override
  {
    m_frame += frame_delta * ANIMATION_FPS;
    if (m_frame >= getFrameCount(m_drawable))
      m_frame = 0;

    if (m_viewer.m_single_instance)
    {
      updateDrawable(m_drawable, frame_delta);
      renderDrawable(m_drawable, Camera::Vec3(0), m_frame);
    }
    else
    {
      for (int x = 0; x < m_viewer.m_instances.x; x++)
      {
        for (int y = 0; y < m_viewer.m_instances.y; y++)
        {
          for (int z = 0; z < m_viewer.m_instances.z; z++)
          {
            updateDrawable(m_drawable, frame_delta);
            renderDrawable(m_drawable, Camera::Vec3(x,y,z) * m_viewer.m_instance_spacing,
                           m_frame);
          }
        }
      }
    }
  }
};


struct MultiObjectScene : public BasicScene
{
  static constexpr int MAX_OBJECTS = 100;

  std::vector<Drawable> m_objects;
  std::vector<std::unique_ptr<Actor>> m_actors;
  int m_instance_spacing = {};
  glm::ivec3 m_instances = {};

  void addActor(Drawable d, Camera::Vec3 pos)
  {
    auto actor = std::make_unique<Actor>();
    actor->drawable = d;
    actor->pos = pos;
    m_actors.push_back(std::move(actor));
  }

  void createActors()
  {
    m_actors.clear();

    int i_obj = 0;
    for (int x = 0; x < m_viewer.m_instances.x; x++)
    {
      for (int y = 0; y < m_viewer.m_instances.y; y++)
      {
        for (int z = 0; z < m_viewer.m_instances.z; z++)
        {
          addActor(m_objects.at(i_obj), Camera::Vec3(x,y,z) * m_viewer.m_instance_spacing);
          i_obj = (i_obj + 1) % m_objects.size();
        }
      }
    }
  }

  MultiObjectScene(MeshViewer &v) : BasicScene(v)
  {
    m_instance_spacing = m_viewer.m_instance_spacing;
    m_instances = m_viewer.m_instances;

    for (auto& path : m_viewer.m_paths)
    {
      if (m_objects.size() == MAX_OBJECTS)
        break;
      m_objects.push_back(load(path));
    }

    mergeMeshes();
    createActors();
  }

  void render(float frame_delta) override
  {
    if (m_instance_spacing != m_viewer.m_instance_spacing ||
        m_instances != m_viewer.m_instances)
    {
      createActors();
    }
    m_instance_spacing = m_viewer.m_instance_spacing;
    m_instances = m_viewer.m_instances;

    for (auto& actor : m_actors)
    {
      updateActor(*actor, frame_delta);
      renderActor(*actor);
    }
  }
};


} // namespace


namespace il2ge::mesh_viewer
{


MeshViewer::MeshViewer(const std::vector<std::string>& paths,
                       il2ge::renderer::Renderer& r) :
  m_renderer(r), m_paths(paths), m_parameters("Parameters")
{
  if (m_paths.size() == 1)
    m_path = m_paths.front();
}


MeshViewer::~MeshViewer()
{
}


void MeshViewer::setup()
{
  if (m_paths.size() > 1)
    m_scene = std::make_unique<MultiObjectScene>(*this);
  else
    m_scene = std::make_unique<SimpleScene>(*this);

  camera.x = getWorldCenter().x;
  camera.y = getWorldCenter().y;
  camera.z = getWorldCenter().z;

  camera.x -= 100;

  camera.updateTransform();

  camera.setProjection(90, 1.0, 20000.0);

  enableLod(true);
  enableInstancedDrawing(true);

  m_parameters.add("max_transparent_visibility", m_max_transparent_visibility);
  m_parameters.addBool("single_instance", m_single_instance);

  m_parameters.addBool("enable_lod",
                       [this] () { return isLodEnabled(); },
                       [this] (bool value) { enableLod(value); });

  m_parameters.addBool("enable_instaned_drawing",
                       [this] () { return m_is_instanced_drawing_enabled; },
                       [this] (bool value) { enableInstancedDrawing(value); });

  m_parameters.add("instances_x", m_instances.x);
  m_parameters.add("instances_y", m_instances.y);
  m_parameters.add("instances_z", m_instances.z);
  m_parameters.add("instance spacing", m_instance_spacing);

  camera.setProjection(70, 0.01, 10000.0);
}


void MeshViewer::enableLod(bool value)
{
  m_scene->enableLod(value);
}


bool MeshViewer::isLodEnabled()
{
  return m_scene->isLodEnabled();
}


void MeshViewer::enableInstancedDrawing(bool value)
{
  m_is_instanced_drawing_enabled = value;
  m_renderer.enableInstancedDrawing(value);
}


void MeshViewer::render(float frame_delta)
{
  m_renderer.setMaxTransparentVisibility(m_max_transparent_visibility);
  m_renderer.applyCamera(camera);
  m_scene->render(frame_delta);

  auto set_uniforms = [this] (render_util::ShaderProgramPtr program)
  {
    updateUniforms(program);
  };

  m_renderer.flushRenderList(set_uniforms);
}


void MeshViewer::updateUniforms(render_util::ShaderProgramPtr program)
{
  glm::vec3 sun_dir_view = camera.getWorld2ViewMatrix() * glm::vec4(getSunDir(), 0);

  program->setUniform("sunDir", getSunDir());
  program->setUniform("sun_dir_world", getSunDir());
  program->setUniform("sun_dir_view", sun_dir_view);
}


void MeshViewer::printStats(std::ostream &out)
{
  out << "object instances: " << m_instances.x * m_instances.y * m_instances.z << std::endl;
  m_scene->printStats(out);
  m_renderer.printStats(out);
}


} // namespace il2ge::mesh_viewer
