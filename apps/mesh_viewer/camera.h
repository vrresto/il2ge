/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CAMERA_H
#define CAMERA_H

#include <render_util/camera_3d.h>

#include <glm/gtc/matrix_transform.hpp>


namespace il2ge::mesh_viewer
{


struct Camera : public render_util::Camera3D
{
  Unit x = 0;
  Unit y = 0;
  Unit z = 0;
  Unit yaw = 0;
  Unit pitch = 0;
  Unit roll = 0;

  Camera() {
    updateTransform();
  }

  Vec3 getDirection()
  {
    Mat4 rot_yaw = glm::rotate(Mat4(1), glm::radians(yaw), Vec3(0,0,1));
    glm::dvec4 pitch_axis(0,1,0,0);
    pitch_axis = rot_yaw * pitch_axis;
    Mat4 rot_pitch =
      glm::rotate(Mat4(1), glm::radians(-pitch), glm::dvec3(pitch_axis.x, pitch_axis.y, pitch_axis.z));

    Vec4 direction(1,0,0,0);
    direction = rot_pitch * rot_yaw * direction;

    return Vec3(direction);
  }

  void moveForward(Unit dist)
  {
    Vec3 pos(x,y,z);

    pos += getDirection() * dist;

    x = pos.x;
    y = pos.y;
    z = pos.z;
  }

  void updateTransform()
  {
//       cout<<"yaw: "<<yaw<<"\tpitch: "<<pitch<<endl;
    setTransform(x, y, z, yaw, pitch, roll);
  }

};


} // namespace il2ge::mesh_viewer


#endif
