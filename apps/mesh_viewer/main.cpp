/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#define GLM_ENABLE_EXPERIMENTAL

#include "application.h"
#include <il2ge/exception_handler.h>
#include <util.h>
#include <log.h>

#include <INIReader.h>

#include <glm/glm.hpp>
#include <glm/gtx/vec_swizzle.hpp>
#include <iostream>

#ifdef _WIN32
#include <windows.h>
#include <shlobj.h>
#include <direct.h>
#endif

using namespace std;
using namespace glm;


namespace
{


const char* const g_crash_log_file_name = "il2ge_map_editor_crash.log";


#ifdef _WIN32
std::string getExeFilePath()
{
  char module_file_name[MAX_PATH];

  if (!GetModuleFileNameA(0,
      module_file_name,
      sizeof(module_file_name)))
  {
    abort();
  }

  return module_file_name;
}


int showOkCancelDialog(std::string text, std::string title)
{
  return MessageBoxA(nullptr, text.c_str(), title.c_str(), MB_OKCANCEL | MB_ICONQUESTION);
}
#endif


} // namespace


int main(int argc, char **argv)
{
  vector<string> paths;

  if (argc >= 2)
  {
    for (int i = 1; i < argc; i++)
      paths.emplace_back(argv[i]);
  }
  else
  {
    cerr << "Wrong number of arguments: " << argc << endl;
    return 1;
  }

  assert(!paths.empty());

#ifdef _WIN32
  il2ge::exception_handler::install(g_crash_log_file_name);
  il2ge::exception_handler::watchModule(GetModuleHandle(0));

  string il2_dir = util::getDirFromPath(getExeFilePath());
  _chdir(il2_dir.c_str());
#endif

  il2ge::mesh_viewer::runMeshViewerApplication(paths, "il2ge_mesh_viewer");
}
