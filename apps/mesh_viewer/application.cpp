/**
 *    Rendering utilities
 *    Copyright (C) 2018  Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "application.h"
#include "viewer.h"
#include <il2ge/renderer.h>
#include <text_renderer/text_renderer.h>
#include <render_util/render_util.h>
#include <render_util/terrain_base.h>
#include <render_util/texture_util.h>
#include <render_util/texunits.h>
#include <render_util/image_loader.h>
#include <render_util/camera.h>
#include <render_util/state.h>
#include <render_util/text_display.h>
#include <render_util/shader_util.h>
#include <render_util/context.h>
#include <render_util/gl_binding/gl_binding.h>
#include <log/file_appender.h>
#include <log/console_appender.h>
#include <log/txt_formatter.h>
#include <log/message_only_formatter.h>
#include <log.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>
#include <memory>

using namespace glm;
using namespace std;
using namespace render_util;
using namespace il2ge::mesh_viewer;
using namespace render_util::gl_binding;
using render_util::ShaderProgram;
using Clock = std::chrono::steady_clock;


namespace
{
  const float camera_move_speed_default = 1.0;
  const float camera_move_speed_min = 1.0;
  const float camera_move_speed_max = 1000.0;
  const float camera_rotation_speed = 45.0;
  const double mouse_rotation_speed = 0.2;

  std::unique_ptr<il2ge::renderer::Renderer> g_renderer;
  std::shared_ptr<MeshViewer> g_viewer;

  float camera_move_speed = camera_move_speed_default;
  dvec2 last_cursor_pos(-1, -1);
  bool g_shift_active = false;
  Clock::time_point g_last_frame_time {};
  std::unique_ptr<TextRenderer> g_text_renderer;
  std::unique_ptr<TextDisplay> g_text_display;


  void initLog(string app_name)
  {
  #if USE_PLOG
    constexpr bool ADD_NEW_LINE = false;

    using namespace util::log;
    using FileSink = FileAppender<TxtFormatter<ADD_NEW_LINE>>;
    using ConsoleSink = ConsoleAppender<MessageOnlyFormatter<ADD_NEW_LINE>>;

    static FileSink file_sink_warn(app_name + "_warnings.log");
    static FileSink file_sink_info(app_name + "_info.log");
    static FileSink file_sink_debug(app_name + "_debug.log");
    static FileSink file_sink_trace(app_name + "_trace.log");

    static ConsoleSink console_sink;

    auto &logger_default = plog::init(plog::verbose);

    auto &warn_sink = plog::init<LOGGER_WARNING>(plog::warning, &file_sink_warn);
    auto &info_sink = plog::init<LOGGER_INFO>(plog::info, &file_sink_info);
    auto &debug_sink = plog::init<LOGGER_DEBUG>(plog::debug, &file_sink_debug);
    auto &trace_sink = plog::init<LOGGER_TRACE>(plog::verbose, &file_sink_trace);

    info_sink.addAppender(&console_sink);

    logger_default.addAppender(&warn_sink);
    logger_default.addAppender(&info_sink);
    logger_default.addAppender(&debug_sink);
    logger_default.addAppender(&trace_sink);
  #endif
  }


  void printDistance (float distance, const char *suffix, ostream &out)
  {
    out.width(8);
    out << distance << suffix;
  }


  void printStats(int frame_delta_ms, ostream &out)
  {
    out.precision(3);
    out.setf(ios_base::fixed);
    out.setf(ios_base::right);

    float fps = 1000.0 / frame_delta_ms;

    out << "fps: ";
    out.width(8);
    out << fps << "   |   ";

    out<<"pos: ";
    printDistance(g_viewer->camera.x/1000, " ", out);
    printDistance(g_viewer->camera.y/1000, " ", out);
    printDistance(g_viewer->camera.z/1000, " ", out);

    out<<"    |    speed: ";
    printDistance(camera_move_speed / 1000 * 60 * 60, " kph", out);

    out<<"   |  yaw: ";
    printDistance(g_viewer->camera.yaw, " ", out);

    out<<"    |    fov: " << g_viewer->camera.getFov();
  }


  void mouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
  {
    if (action == GLFW_PRESS)
    {
      if (mods == GLFW_MOD_SHIFT)
        g_shift_active = true;
      else
        g_shift_active = false;
    }
    else if (action == GLFW_RELEASE)
    {
      g_shift_active = false;
    }

    if (button == 1)
    {
      if (action == GLFW_PRESS) {
        glfwGetCursorPos(window, &last_cursor_pos.x, &last_cursor_pos.y);
      }
      else if (action == GLFW_RELEASE) {
        last_cursor_pos = dvec2(-1, -1);
      }
    }
  }


  void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    if (action == GLFW_PRESS)
    {
      float fov_step = mods & GLFW_MOD_SHIFT ? 1 : 10;

      switch (key)
      {
        case GLFW_KEY_ESCAPE:
          glfwSetWindowShouldClose(window, true);
          break;
        case GLFW_KEY_F2:
          {
            auto fov = glm::clamp(g_viewer->camera.getFov() - fov_step, 1.0, 170.0);
            g_viewer->camera.setFov(fov);
          }
          break;
        case GLFW_KEY_F3:
          {
            auto fov = glm::clamp(g_viewer->camera.getFov() + fov_step, 1.0, 170.0);
            g_viewer->camera.setFov(fov);
          }
          break;
        case GLFW_KEY_F12:
          {
            constexpr int num_channels = 3;

            int width = 0, height = 0;
            glfwGetFramebufferSize(window, &width, &height);

            vector<unsigned char> data(width * height * num_channels);
            gl::ReadnPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, data.size(), data.data());

            auto image = make_shared<ImageRGB>(ivec2(width, height), std::move(data));
            image = image::flipY(image);

            try
            {
              util::mkdir("il2ge_mesh_viewer");
              string path = "il2ge_mesh_viewer/screenshot_" + util::makeTimeStampString() + ".png";
              saveImageToFile(path, image.get(), ImageType::PNG);
            }
            catch (std::exception& e)
            {
              LOG_ERROR << e.what() << std::endl;
            }
          }
          break;
        case GLFW_KEY_KP_ADD:
          camera_move_speed *= 2;
          break;
        case GLFW_KEY_KP_SUBTRACT:
          camera_move_speed /= 2;
          break;
      }
    }

    if (g_viewer->hasParameters() && (action == GLFW_PRESS || action == GLFW_REPEAT))
    {
      float step_factor = mods & GLFW_MOD_CONTROL ? 10 : 1;
      auto &p = g_viewer->getActiveParameter();

      switch (key)
      {
        case GLFW_KEY_PAGE_UP :
//           g_viewer->setActiveParameter(g_viewer->getActiveParameterIndex() - 1);
          break;
        case GLFW_KEY_PAGE_DOWN:
//           g_viewer->setActiveParameter(g_viewer->getActiveParameterIndex() + 1);
          break;
        case GLFW_KEY_R:
          p.reset();
          break;
        case GLFW_KEY_LEFT:
          p.decrease(step_factor);
          break;
        case GLFW_KEY_RIGHT:
          p.increase(step_factor);
          break;
        case GLFW_KEY_UP:
          g_viewer->setActiveParameter(g_viewer->getActiveParameterIndex() - 1);
          break;
        case GLFW_KEY_DOWN:
          g_viewer->setActiveParameter(g_viewer->getActiveParameterIndex() + 1);
          break;
      }
    }
  }


  void processInput(GLFWwindow *window, float frame_delta)
  {
    if (glfwGetMouseButton(window, 1) == GLFW_PRESS)
    {
      dvec2 cursor_pos;
      glfwGetCursorPos(window, &cursor_pos.x, &cursor_pos.y);

      if (last_cursor_pos != dvec2(-1, -1)) {
        dvec2 cursor_delta = last_cursor_pos - cursor_pos;
        last_cursor_pos = cursor_pos;

        dvec2 rotation = cursor_delta * mouse_rotation_speed;

        g_viewer->camera.yaw += rotation.x;
        g_viewer->camera.pitch += rotation.y;
      }
    }
    else if (glfwGetMouseButton(window, 0) == GLFW_PRESS)
    {
      dvec2 cursor_pos;
      glfwGetCursorPos(window, &cursor_pos.x, &cursor_pos.y);
      g_viewer->cursorPos(cursor_pos);
    }
    else
    {
      dvec2 cursor_pos;
      glfwGetCursorPos(window, &cursor_pos.x, &cursor_pos.y);

      assert(g_viewer);
      g_viewer->cursorPos(cursor_pos);
    }

    float move_speed = camera_move_speed;

    float rotation_speed = 50;

    if (glfwGetKey(window, GLFW_KEY_KP_4) == GLFW_PRESS) {
      g_viewer->camera.yaw += frame_delta * rotation_speed;
    }
    if (glfwGetKey(window, GLFW_KEY_KP_6) == GLFW_PRESS) {
      g_viewer->camera.yaw -= frame_delta * rotation_speed;
    }
    if (glfwGetKey(window, GLFW_KEY_KP_8) == GLFW_PRESS) {
      g_viewer->camera.pitch -= frame_delta * rotation_speed;
    }
    if (glfwGetKey(window, GLFW_KEY_KP_2) == GLFW_PRESS) {
      g_viewer->camera.pitch += frame_delta * rotation_speed;
    }

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
      g_viewer->camera.moveForward(frame_delta * move_speed);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
      g_viewer->camera.moveForward(frame_delta * -move_speed);
    }

    g_viewer->camera.updateTransform();
  }


  void *getGLProcAddress(const char *name)
  {
    return (void*) glfwGetProcAddress(name);
  }


  void errorCallback(int error, const char* description)
  {
    fprintf(stderr, "Error: %s\n", description);
    exit(1);
  }


  std::chrono::milliseconds frame()
  {
    Clock::time_point current_frame_time = Clock::now();
    std::chrono::milliseconds frame_delta =
      std::chrono::duration_cast<std::chrono::milliseconds>(current_frame_time - g_last_frame_time);
    g_last_frame_time = current_frame_time;
    return frame_delta;
  }


  void refresh(GLFWwindow* window, std::chrono::milliseconds frame_delta)
  {
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    gl::Viewport(0, 0, width, height);
    gl::ClearColor(0.5, 0.5, 1.0, 1.0);
    // gl::ClearColor(0.0, 0.0, 0.0, 1.0);
    gl::Clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    g_viewer->camera.setViewportSize(width, height);

    g_viewer->render((float)frame_delta.count() / 1000.0);

    render_util::StateModifier modifier;
    modifier.setDefaults();
    modifier.enableBlend(true);

    float text_pos_y = 0;
    float text_pos_x = 300;

    ostringstream stats;
    printStats(frame_delta.count(), stats);

    g_text_renderer->SetColor(0,0,0);
    g_text_renderer->DrawText(stats.str(), text_pos_x+1, text_pos_y+1);
    g_text_renderer->SetColor(1.0, 1.0, 1.0);
    g_text_renderer->DrawText(stats.str(), text_pos_x, text_pos_y);

    text_pos_y += 30;

    ostringstream scene_stats;
    g_viewer->printStats(scene_stats);

    g_text_display->setText(scene_stats.str(), glm::vec3(1, 1, 1));
    g_text_display->addLine();

    // for (auto& m : g_renderer->getMaterials())
    // {
    //   g_text_display->addLine(m.first, m.second);
    // }

    for (int i = 0; i < g_viewer->getParameters().size(); i++)
    {
      auto &parameter = *g_viewer->getParameters().at(i);
      auto parameter_text = parameter.name + ": " + parameter.getValueString();

      glm::vec3 color = (i == g_viewer->getActiveParameterIndex()) ?
        glm::vec3(0.6, 1.0, 0.6) : glm::vec3(0.6, 0.6, 0.6);

      g_text_display->addLine(parameter_text, color);
    }

    g_text_display->draw(*g_text_renderer, text_pos_x, text_pos_y);

    glfwSwapBuffers(window);

    il2ge::renderer::clearPerFrameStats();
  }


  void refresh(GLFWwindow* window)
  {
    refresh(window, frame());
  }


  std::vector<char> readFile(const std::string &path)
  {
    std::cout << "reading file: " << path << std::endl;
    return util::readFile<char>(path);
  }


} // namespace


void il2ge::mesh_viewer::runMeshViewerApplication(const vector<string>& paths,
                                                  string app_name)
{
  initLog(app_name);

  glfwSetErrorCallback(errorCallback);

  if (!glfwInit())
    exit(EXIT_FAILURE);

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

  glfwWindowHint(GLFW_VISIBLE, 0);

  GLFWwindow* window = glfwCreateWindow(1024, 768, "Test", NULL, NULL);
  if (!window)
  {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);

  glfwSetKeyCallback(window, keyCallback);
  glfwSetMouseButtonCallback(window, mouseButtonCallback);
  glfwSetWindowRefreshCallback(window, refresh);

  gl_binding::GL_Interface *gl_interface = new gl_binding::GL_Interface(&getGLProcAddress);
  gl_binding::GL_Interface::setCurrent(gl_interface);

  auto context = std::make_unique<render_util::Context>();
  render_util::Context::setCurrent(context.get());

  StateModifier::init();

  render_util::ShaderSearchPath shader_search_path;
  shader_search_path.push_back("./il2ge/shaders");
  shader_search_path.push_back("./il2ge/shaders_new");

  render_util::ShaderParameters shader_params;

  auto create_shader = [&] (auto name, auto params)
  {
    auto merged_params = shader_params;
    merged_params.add(params);
    return render_util::createShaderProgram(name,
                                            shader_search_path,
                                            merged_params);
  };

  g_renderer = make_unique<il2ge::renderer::Renderer>(create_shader);

  g_viewer = std::make_unique<MeshViewer>(paths, *g_renderer);
  assert(g_viewer);

  LOG_INFO << "setting up scene ..." << endl;
  g_viewer->setup();
  LOG_INFO << "setting up scene ... done" << endl;

  CHECK_GL_ERROR();


  g_text_display = std::make_unique<TextDisplay>(shader_search_path);
  g_text_display->setBackground(glm::vec4(0,0,0,0.5));

  glfwShowWindow(window);

#if GLFW_VERSION_MINOR >= 2
  glfwMaximizeWindow(window);
#endif

  g_text_renderer = make_unique<TextRenderer>();

  g_last_frame_time = Clock::now();

  while (!glfwWindowShouldClose(window))
  {
    CHECK_GL_ERROR();

    auto frame_delta = frame();

    glfwPollEvents();
    processInput(window, (float)frame_delta.count() / 1000.0);

    if (glfwWindowShouldClose(window))
    {
      CHECK_GL_ERROR();
      break;
    }

    refresh(window, frame_delta);
  }

  cout<<endl;

  CHECK_GL_ERROR();

  g_text_renderer.reset();
  g_text_display.reset();

  g_viewer.reset();
  g_renderer.reset();

  render_util::Context::setCurrent(nullptr);
  context.reset();

  gl::Finish();

  CHECK_GL_ERROR();

  gl_binding::GL_Interface::setCurrent(nullptr);

  glfwMakeContextCurrent(0);

  glfwDestroyWindow(window);
  glfwTerminate();
}
