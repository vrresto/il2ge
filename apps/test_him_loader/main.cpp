/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include <il2ge/exception_handler.h>
#include <il2ge/mesh/mesh_base.h>
#include <util.h>
#include <log/file_appender.h>
#include <log/console_appender.h>
#include <log/txt_formatter.h>
#include <log/message_only_formatter.h>
#include <log.h>

#if _WIN32
#include <windows.h>
#endif

#include <iostream>

using namespace std;
using namespace glm;


namespace
{


void initLog(string log_file_name)
{
#if USE_PLOG
  constexpr bool ADD_NEW_LINE = false;

  using namespace util::log;
  using FileSink = FileAppender<TxtFormatter<ADD_NEW_LINE>>;
  using ConsoleSink = ConsoleAppender<TxtFormatter<ADD_NEW_LINE>>;

  static FileSink file_sink_trace(log_file_name);
  static ConsoleSink console_sink;

  auto &logger_default = plog::init(plog::verbose);
  auto &file_sink = plog::init<LOGGER_TRACE>(plog::verbose, &file_sink_trace);

  logger_default.addAppender(&console_sink);
  logger_default.addAppender(&file_sink);
#endif
}


} // namespace


int main(int argc, char **argv)
{
#if _WIN32
  il2ge::exception_handler::install("il2ge_test_him_loader_crash.log");
  il2ge::exception_handler::watchModule(GetModuleHandle(0));
#endif

  string mesh_path;

  if (argc == 2)
  {
    mesh_path = argv[1];
  }
  else
  {
    cerr << "Wrong number of arguments: " << argc << endl;
    return 1;
  }

  assert(!mesh_path.empty());

  initLog("il2ge_test_him_loader.log");

  try
  {
    il2ge::mesh::loadHierMesh(mesh_path);
  }
  catch (std::exception &e)
  {
    LOG_ERROR << "Failed to load mesh: " << mesh_path << endl;
    LOG_ERROR << "Exception was: " << e.what() << endl;
    LOG_ERROR << "---------------------------------------------" << endl;
    return 1;
  }
}
