set(SRCS
  main.cpp
)

if (platform_mingw)
  set(SRCS ${SRCS} ${PROJECT_SOURCE_DIR}/common/exception_handler_win32.cpp)
endif()

add_executable(test_him_loader ${SRCS})

target_link_libraries(test_him_loader
  common
)

install(TARGETS test_him_loader
  DESTINATION .
)
