#pragma once

#include <random>


namespace util
{


template <typename T>
class RandomIntegerGenerator
{
  std::random_device m_rd;
  std::default_random_engine m_engine = std::default_random_engine(m_rd());

public:
  T operator()()
  {
    std::uniform_int_distribution<T> dist(std::numeric_limits<T>::min(),
                                          std::numeric_limits<T>::max());
    return dist(m_engine);
  }

  T operator()(T min_, T max_)
  {
    std::uniform_int_distribution<T> dist(min_, max_);
    return dist(m_engine);
  }
};


} // namespace util
