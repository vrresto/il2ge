/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_SECTION_FILE_H
#define IL2GE_SECTION_FILE_H

#include <util.h>

#include <string>
#include <unordered_map>
#include <vector>
#include <memory>

namespace il2ge
{


struct SectionBase
{
  struct Entry
  {
    std::vector<std::string> values;
    int line_in_file = 0;
  };
};


template <class T>
class SectionFileBase
{
public:
  using Section = T;
  using Entry = typename Section::Entry;


  SectionFileBase(bool case_sensitive) : m_case_sensitive(case_sensitive) {}

  const bool isCaseSensitive() const { return m_case_sensitive; }

  const std::unordered_map<std::string, Section> &getSections() const { return m_sections; }

  const bool hasSection(const std::string &name) const
  {
    return getSectionPtr(name) != nullptr;
  }

  const Section &getSection(const std::string &name) const
  {
    auto s = getSectionPtr(name);
    if (s)
      return *s;
    else
      throw std::runtime_error(std::string("Section not found: ") + name);
  }

  Section &getOrCreateSection(const std::string &name_)
  {
    auto name = m_case_sensitive ? name_ : util::makeLowercase(name_);
    auto it = m_sections.find(name);
    if (it != m_sections.end())
    {
      return it->second;
    }
    else
    {
      return m_sections.emplace(name, m_case_sensitive).first->second;
    }
  }

protected:
  const Section *getSectionPtr(const std::string &name) const
  {
    auto it = m_sections.find(m_case_sensitive ? name : util::makeLowercase(name));
    return (it != m_sections.end()) ? &it->second : nullptr;
  }

private:
  const bool m_case_sensitive = false;
  std::unordered_map<std::string, Section> m_sections;
};


struct SimpleSection : public SectionBase
{
  std::vector<Entry> entries;

  SimpleSection(bool case_sensitive) {}
};


class SectionFile : public SectionFileBase<SimpleSection>
{
  using Base = SectionFileBase<SimpleSection>;

public:
  SectionFile(std::istream&, const std::vector<std::string> &comment_prefixes,
              char eof = 0, bool case_insensitive = false);
};


}

#endif
