#pragma once

#include <string>
#include <functional>
#include <vector>
#include <cassert>


namespace il2ge
{


class Named
{
public:
  using DestroyCallBack = std::function<void(const Named&)>;

  Named(const std::string& name) : m_name(name) {}
  ~Named()
  {
    if (m_destroy_callback)
      m_destroy_callback(*this);
  }

  void setDestroyCallback(const DestroyCallBack& cb) const
  {
    assert(!m_destroy_callback);
    m_destroy_callback = cb;
  }

  void clearDestroyCallback() const
  {
    assert(m_destroy_callback);
    m_destroy_callback = {};
  }

  const std::string& getName() const { return m_name; }

private:
  std::string m_name;
  mutable DestroyCallBack m_destroy_callback;
};


} // namespace il2ge
