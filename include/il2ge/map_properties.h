/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MAP_PROPERTIES_H
#define IL2GE_MAP_PROPERTIES_H

#include <il2ge/base_map_resources.h>
#include <render_util/map_geography.h>
#include <render_util/altitudinal_zone.h>

#include <string>
#include <ostream>

#include <INIReader.h>


namespace il2ge
{


std::unique_ptr<render_util::MapGeography> createDefaultMapGeography();
std::unique_ptr<render_util::MapGeography> loadMapGeography(INIReader &ini, std::string ini_path);
void saveMapGeography(const render_util::MapGeography& p, std::ostream &out);

std::vector<render_util::AltitudinalZone>
loadAltitudinalZones(INIReader &ini, std::string ini_path);

void saveAltitudinalZones(const std::vector<render_util::AltitudinalZone>& zones,
                          std::ostream& out);

std::map<std::string, int> loadLandCoverTypes(INIReader &ini, std::string ini_path);
void saveLandCoverTypes(const std::map<std::string, int>& map, std::ostream& out);



} // namespace il2ge

#endif
