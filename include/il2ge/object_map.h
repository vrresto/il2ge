#pragma once

#include <il2ge/cache.h>

#include <memory>
#include <functional>
#include <unordered_map>
#include <string>
#include <cassert>


namespace il2ge
{


template<typename T>
class AutoAddingWeakMap
{
public:
  using Ptr = std::shared_ptr<T>;
  using WeakPtr = std::weak_ptr<T>;

  ~AutoAddingWeakMap()
  {
    for (auto& e : m_objects)
    {
      auto obj = e.second.lock();
      if (obj)
        obj->clearDestroyCallback();
    }
  }

  template <class Factory>
  Ptr get(const std::string& name, const Factory& factory)
  {
    auto& weak = m_objects[name];

    auto obj = weak.lock();

    if (!obj)
    {
      obj = factory(name);
      if (obj)
      {
        assert(obj->getName() == name);
        obj->setDestroyCallback([this] (auto& obj) { m_objects.erase(obj.getName()); });
      }
      weak = obj;
    }

    return obj;
  }

  const std::unordered_map<std::string, WeakPtr>& getObjects()
  {
    return m_objects;
  }

private:
  std::unordered_map<std::string, WeakPtr> m_objects;
};


template<class Base>
class BasicCachedAutoAddingMap : public Base
{
public:
  BasicCachedAutoAddingMap(int cache_size) : m_cache(cache_size) {}

  template <class Factory>
  auto get(const std::string& name, const Factory& factory)
  {
    return Base::get(name,
                    [&] (auto& name)
                      {
                        auto cached = m_cache.get(name);
                        if (cached)
                          return cached;

                        auto new_obj = factory(name);
                        m_cache.set(name, new_obj);

                        return new_obj;
                      });
  }

private:
  Cache<typename Base::Ptr> m_cache;
};


template <class T>
using CachedAutoAddingWeakMap = BasicCachedAutoAddingMap<AutoAddingWeakMap<T>>;


template<class Base>
class BasicAutoAddingMapWithFactory : public Base
{
public:
  using Ptr = typename Base::Ptr;
  using Factory = std::function<Ptr(const std::string&)>;

  template <typename... Args>
  BasicAutoAddingMapWithFactory(Factory factory, Args... args) :
    Base(args...),
    m_factory(factory) {}

  auto get(const std::string& name)
  {
    return Base::get(name, m_factory);
  }

private:
  Factory m_factory;
};


template <class T>
using AutoAddingWeakMapWithFactory =
  BasicAutoAddingMapWithFactory<AutoAddingWeakMap<T>>;

template <class T>
using CachedAutoAddingWeakMapWithFactory =
  BasicAutoAddingMapWithFactory<CachedAutoAddingWeakMap<T>>;


} // namespace il2ge
