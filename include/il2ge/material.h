/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MATERIAL_H
#define IL2GE_MATERIAL_H

#include <il2ge/named.h>
#include <il2ge/parameter_file.h>
#include <render_util/texture.h>
#include <render_util/image.h>

// #include <memory>
#include <functional>
#include <vector>
#include <string>

namespace il2ge
{


namespace renderer
{
  class MaterialRendererData;
}


class MaterialBase
{
public:
  struct Layer
  {
    std::string texture_path;
    bool texture_is_greyscale = false;
    bool tfTestZ = true;
    bool tfTestZEqual = false;
    bool tfBlend = false;
    bool tfBlendAdd = false;
    bool tfNoTexture = false;
    bool tfNoWriteZ = false;
    bool tfTestA = false;
    float AlphaTestVal = 0.5;
    bool tfWrapX = false;
    bool tfWrapY = false;
    glm::vec4 ColorScale = glm::vec4(1);
    bool tfDepthOffset = false;
    glm::vec4 TextureCoordScale = glm::vec4(0,0,1,1);
    float VisibleDistanceNear = 0;
    float VisibleDistanceFar = 100;
  };

  struct LightParams
  {
    float Ambient = 1;
    float Diffuse = 1;
    float Specular = 0;
    float SpecularPow = 1;
    float Shine = 0;
  };

  std::vector<Layer> &getLayers() { return m_layers; }
  const std::vector<Layer> &getLayers() const { return m_layers; }
  void applyParameters(ParameterFiles &files, std::string path);

  bool tfDepthOffset = false;
  bool tfDoubleSide = false;
  bool tfShouldSort = false;
  LightParams light_params;
  std::string path;
  std::string name;

private:
  std::vector<Layer> m_layers;
};


class Material : public MaterialBase
{
  Material() = delete;
  Material& operator=(const Material&) = delete;

  std::unique_ptr<renderer::MaterialRendererData> m_renderer_data;

public:
  struct TextureAndSize : public Named
  {
    TextureAndSize(std::string name) : Named(name) {}

    render_util::TexturePtr texture;
    glm::ivec2 size;
  };

  using GetTexture =
    std::function<std::shared_ptr<TextureAndSize>(const std::string&)>;

  Material(const Material&);
  Material(ParameterFiles &files, std::string path, const GetTexture&);
  Material(std::string path, const GetTexture&);
  virtual ~Material();

  const renderer::MaterialRendererData& getRendererData() const
  {
    assert(m_renderer_data);
    return *m_renderer_data;
  }

  renderer::MaterialRendererData& getRendererData()
  {
    assert(m_renderer_data);
    return *m_renderer_data;
  }

  void refreshRendererData(const GetTexture&);
};


class MaterialList
{
public:
  std::vector<std::shared_ptr<Material>> m_materials;

  // MaterialList(const MaterialList&) = delete;
  // MaterialList& operator=(const MaterialList&) = delete;
  // MaterialList() {}
  // virtual ~MaterialList() {}

  // virtual std::unique_ptr<MaterialList> clone()
  // {
  //   UNIMPLEMENTED
  // }

  bool has(int index) const
  {
    return m_materials.at(index) != nullptr;
  }

  const std::shared_ptr<const Material> at(size_t index) const
  {
    assert(m_materials.at(index));
    return m_materials.at(index);
  }

  std::shared_ptr<Material>& at(size_t index)
  {
    // assert(m_materials.at(index));
    return m_materials.at(index);
  }

  size_t size() const
  {
    return m_materials.size();
  }
};


} // namespace il2ge

#endif
