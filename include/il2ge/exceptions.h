#ifndef IL2GE_EXCEPTIONS_H
#define IL2GE_EXCEPTIONS_H

#include <stdexcept>
#include <string>


namespace il2ge
{


class FileReadError : public std::runtime_error
{
  std::string m_path;
  std::string m_description;

public:
  FileReadError(const std::string& path) :
    std::runtime_error("Error reading file " + path), m_path(path) {}

  FileReadError(const std::string& path, const std::string& description) :
    std::runtime_error("Error reading file " + path + ": " + description),
    m_path(path), m_description(description) {}

  const std::string& getPath() const { return m_path; }
  const std::string& getDescription() const { return m_description; }
};


} // namespace il2ge

#endif
