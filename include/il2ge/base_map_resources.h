/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2022 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_TERRAIN_BASE_LAYER_RESOURCES
#define IL2GE_TERRAIN_BASE_LAYER_RESOURCES

#include <render_util/map_geography.h>
#include <render_util/elevation_map.h>
#include <render_util/land_cover.h>
#include <render_util/image.h>

#include <INIReader.h>
#include <glm/glm.hpp>

#include <string>


namespace il2ge
{


class BaseMapResources
{
  std::string m_name;
  std::string m_ini_path;
  std::unique_ptr<INIReader> m_ini;
  glm::ivec2 m_map_size_px;

public:
  struct LandCoverType : public render_util::LandCoverType
  {
    int variants = 1;
  };

  BaseMapResources(std::string name);

  std::unique_ptr<render_util::MapGeography> getMapGeography();
  std::shared_ptr<render_util::ElevationMap> getElevationMap();
  std::shared_ptr<render_util::ImageGreyScale> getMap(std::string name);
  glm::ivec2 getMapSizePx() { return m_map_size_px; }

  static const std::vector<std::string>& getLandCoverTypeNames();
  static const std::vector<LandCoverType>& getLandCoverTypes();
  static bool exists(std::string name);
};


} // namespace il2ge

#endif
