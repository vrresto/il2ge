/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_RENDERER_H
#define IL2GE_RENDERER_H

#if ENABLE_WIP_FEATURES

#include <il2ge/parameter_file.h>
#include <render_util/texture_manager.h>
#include <render_util/shader.h>
#include <render_util/camera_3d.h>

#include <glm/glm.hpp>
#include <vector>
#include <memory>
#include <map>
#include <functional>


namespace render_util
{
  class StateModifier;
  class VertexArrayObject;
}


namespace il2ge
{
  class Material;
}


namespace il2ge::mesh
{
  struct LodBase;
}


namespace il2ge::renderer
{

using Mat4 = glm::mat4;


class Mesh;


class Renderer
{
public:
  using ReadFileFunc = std::function<std::vector<char>(std::string)>;

  Renderer(render_util::ShaderSearchPath, render_util::ShaderParameters, ReadFileFunc);
  ~Renderer();

  void setMaxTransparentVisibility(float v)
  {
    m_max_transparent_visibility = v;
  }

  void applyCamera(const render_util::Camera3D&);
  void addToRenderList(Mesh*, const glm::mat4& model_view, float distance, size_t animation_frame);
  void flushRenderList(std::function<void(render_util::ShaderProgramPtr)> set_uniforms);
  void releaseMesh(Mesh*);
  void enableAtmosphere(bool enable)
  {
    m_is_atmosphere_enabled = enable;
  }
  void drawTile(glm::vec2 origin,
                glm::vec2 extent,
                glm::vec2 texcoord_origin,
                glm::vec2 texcoord_extent,
                const std::string &material);

  // Creates a renderable mesh that can be passed to addToRenderList().
  // This object may be used by a different thread than the calling one.
  // Mustn't be deleted! When not needed anymore it shound be passed to releaseMesh().
  Mesh* createMesh(il2ge::mesh::LodBase&);


  //FIXME remove this (before implementing multi-threading)
  render_util::TextureManager &getTextureManager() { return m_txmgr; }

  void printStats(std::ostream &out);

  int getFlushCount() { return m_flush_count; }
  void resetFlushCount() { m_flush_count = 0; }

private:
  class RenderListBase;
  class RenderList;
  class ZSortingMeshRenderList;
  class ZSortingFaceRenderList;

  struct TextureWrapper
  {
    render_util::TexturePtr texture;
    int num_channels = 0;
    glm::ivec2 size = glm::ivec2(0);
  };

  render_util::TextureManager m_txmgr = render_util::TextureManager(0);

  std::unique_ptr<RenderList> m_render_list_solid;
  std::unique_ptr<RenderList> m_render_list_transparent_unsorted;
  std::unique_ptr<ZSortingFaceRenderList> m_render_list_z_sorted_faces;
  std::unique_ptr<ZSortingMeshRenderList> m_render_list_z_sorted_meshes;

  std::vector<RenderList*> m_instancing_render_lists;

  unsigned m_instance_buffer_id = 0;
  size_t m_max_instance_buffer_size = 0;
  size_t m_num_instances = 0;

  std::map<std::string, std::shared_ptr<Material>> m_materials;
  std::unordered_map<std::string, TextureWrapper> m_textures;
  ParameterFiles m_parameter_files;
  ReadFileFunc m_read_file;
  render_util::ShaderSearchPath m_shader_search_path;
  render_util::Camera3D m_camera;
  float m_max_transparent_visibility = 200;
  bool m_is_atmosphere_enabled = false;

  int m_last_render_list_solid_size = 0;
  int m_last_render_list_z_sorted_meshes_size = 0;
  int m_last_render_list_z_sorted_faces_size = 0;
  int m_last_animated_meshes_count = 0;

  int m_flush_count = 0;

  void updateInstanceBuffer();
  void bindVertexAttribBuffers(render_util::VertexArrayObject&);
  std::shared_ptr<Material> getMaterial(const std::string path);
  std::shared_ptr<Material> loadMaterial(const std::string path);
  const TextureWrapper &getTexture(const std::string path);
  TextureWrapper loadTexture(const std::string path);
  size_t processRenderList(RenderListBase&, render_util::StateModifier&,
                           std::function<void(render_util::ShaderProgramPtr)> set_uniforms,
                           size_t base_instance);
  static void applyMaterial(const Material*, render_util::StateModifier&, render_util::ShaderProgram&);
};


}

#endif // ENABLE_WIP_FEATURES

#endif
