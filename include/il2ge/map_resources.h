/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2018 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_RESSOURCE_LOADER_H
#define IL2GE_RESSOURCE_LOADER_H

#include <il2ge/base_map_resources.h>
#include <render_util/image.h>
#include <render_util/base_map_config.h>
#include <render_util/altitudinal_zone.h>
#include <file.h>
#include <util.h>

#include <string>
#include <vector>
#include <memory>
#include <optional>

#include <glm/glm.hpp>


namespace render_util
{
  class MapGeography;
}


namespace il2ge
{


using MapProperty = std::variant<int, glm::vec3, std::string>;


struct Resource
{
  virtual ~Resource() {}
  virtual const std::string& getName() const = 0;
  virtual const std::string& getUniqueID() const = 0;
  virtual std::unique_ptr<util::File> open() const = 0;

  std::vector<char> readAll()
  {
    std::vector<char> data;
    open()->readAll(data);
    return data;
  }
};


class SFSResourceBase : public Resource
{
  std::string m_unique_id;
  std::string m_sfs_path;

public:
  SFSResourceBase(std::string sfs_path);

  const std::string& getPath() const { return m_sfs_path; }
  const std::string& getName() const override { return m_sfs_path; }
  const std::string& getUniqueID() const override { return m_unique_id; }

  static std::string getUniqueID(int64_t hash);
  static std::string getUniqueID(std::string sfs_path);
};


enum class MapResourceID
{
  FIELD_TEXTURE,
  FIELD_NORMAL_MAP,
  FOAM_TEXTURE,
  WATER_ANIMATION_HEIGHT_MAP,
  WATER_ANIMATION_NORMAL_MAP,
  WATER_TEXTURE,
  WATER_NOISE_TEXTURE,
  HEIGHT_MAP,
  TYPE_MAP,
  LAND_MAP,
  LAND_MAP_TABLE,
  HIGH_CLOUDS,
  NOISE_TEXTURE,
  FAR_NOISE_TEXTURE,
  FOREST_LAYER
};


enum class MapPropertyID
{
  WATER_COLOR,
};


constexpr unsigned MAP_RESOURCES_NUM_FIELDS = 32;


const std::string& getMapPropertyName(MapPropertyID);
const char* getMapResourceName(MapResourceID);
const char* getMapFieldName(unsigned index);


std::string getDumpName(MapResourceID, std::optional<unsigned> index);


class MapResources
{
public:
  class NoEntryException : public std::runtime_error
  {
  public:
    NoEntryException(const std::string& section, const std::string& name) :
      std::runtime_error("No entry for " + section + "." + name) {}
  };

  virtual ~MapResources() {}

  virtual std::vector<render_util::AltitudinalZone> getAltitudinalZones() const
  {
    return {};
  }

//     virtual glm::vec3 getBaseMapOrigin() const = 0;
  virtual bool hasBaseLayer() const { return false; }

#if ENABLE_BASE_MAP
  virtual std::unique_ptr<BaseMapResources> getBaseLayerResources() const
  {
    UNIMPLEMENTED;
  }
#endif

  virtual std::unique_ptr<render_util::MapGeography> getMapGeography() const
  {
    UNIMPLEMENTED;
  }

  virtual std::string getDumpDir() const = 0;

  virtual std::unique_ptr<Resource>
  getResource(MapResourceID, std::optional<unsigned> index = {},
              float* scale = nullptr) const { UNIMPLEMENTED }

  virtual MapProperty getProperty(MapPropertyID) const
  {
    UNIMPLEMENTED
  }

  template <typename T>
  T getProperty(MapPropertyID id) const
  {
    return std::get<T>(getProperty(id));
  }
};


using GetSfsResourceFunc = std::function<std::unique_ptr<Resource>(std::string path)>;


std::unique_ptr<MapResources>
getSfsMapResources(const std::string &map_dir,
                   const std::string &ini_path,
                   const std::string &dump_path,
                   GetSfsResourceFunc);


} // namespace il2ge


#endif
