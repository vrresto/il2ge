/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_RENDERER_H
#define IL2GE_RENDERER_H

#include <il2ge/parameter_file.h>
#include <il2ge/material.h>
#include <il2ge/mesh/mesh_base.h>
#include <render_util/image.h>
#include <render_util/shader.h>
#include <render_util/camera_3d.h>

#include <glm/glm.hpp>
#include <vector>
#include <memory>
#include <map>
#include <set>
#include <functional>


namespace render_util
{
  class StateModifier;
  class IndexedVAOBase;
  class VertexArrayObject;
}


namespace il2ge
{

  class Material;
  class MaterialList;
}


namespace il2ge::renderer
{


class UnsortedRenderList;
class ZSortedFaceRenderList;
class ShaderManager;
class SharedVAO;
class RenderGroup;


using Mat4 = glm::mat4;
using MaterialMapping = std::vector<int>;
using ShaderProgramFactory =
  std::function<render_util::ShaderProgramPtr(std::string,
                                              render_util::ShaderParameters)> ;


class MeshRendererData
{
  const mesh::MeshBase& m_mesh; // FIXME - not needed?
  std::unique_ptr<render_util::IndexedVAOBase> m_vao;
  int m_lod = 0;
  std::vector<mesh::FaceGroup> m_face_groups;
  int m_ref_count = 0;

  std::shared_ptr<SharedVAO> m_shared_vao;
  int m_shared_vao_start_vertex = -1;
  int m_shared_vao_start_index = -1;

public:
  MeshRendererData(const mesh::MeshBase&, int lod);
  ~MeshRendererData();

  const std::vector<il2ge::mesh::FaceGroup>& getFaceGroups();

  const mesh::LodBase& getLodMesh() const
  {
    return m_mesh.getLod(m_lod);
  }

  const int getLod() const
  {
    return m_lod;
  }

  render_util::IndexedVAOBase& getVAO()
  {
    assert(m_vao);
    return *m_vao;
  }

  bool isAnimated()
  {
    return m_mesh.isAnimated();
  }

  void addToSharedVAO(const std::shared_ptr<SharedVAO>&);

  const std::shared_ptr<SharedVAO>& getSharedVAO()
  {
    return m_shared_vao;
  }

  int getSharedVAOStartVertex()
  {
    return m_shared_vao_start_vertex;
  }

  int getSharedVAOStartIndex()
  {
    return m_shared_vao_start_index;
  }

  void ref();
  void unref();
};


struct MaterialRendererData
{
  struct Layer
  {
    unsigned sampler_id = 0;
    std::shared_ptr<Material::TextureAndSize> texture_and_size;

    Layer();
    ~Layer();
  };

  std::vector<Layer> layers;

  MaterialRendererData(const Material&, const Material::GetTexture&);
  MaterialRendererData(const Material&, const MaterialRendererData&);
  ~MaterialRendererData();
};


#if USE_MESH_INSTANCE_RENDER_LIST_CACHE
class MeshInstanceRenderListCache
{
public:
  struct MaterialProperties
  {
    bool is_null = true;
    // bool no_texture = false;
    // bool needs_sorting = false;
    std::shared_ptr<UnsortedRenderList> render_list; // when no sorting is needed
  };

  MeshInstanceRenderListCache(const MaterialList& materials);

  void clear();
  void materialsChanged(const MaterialList&);
  void materialChanged(size_t index);
  MaterialProperties& getMaterialProperties(int index);

private:
  std::vector<MaterialProperties> m_material_properties;
};
#endif


struct ParticleInstance
{
  glm::vec3 pos;
  float size;
  glm::vec4 color;
};


enum class RenderGroupID : int
{
  DEFAULT,
  RETICLE_MASK,
  RETICLE,
  Count
};


class Renderer
{
  render_util::Camera3D m_camera;
  std::vector<std::unique_ptr<RenderGroup>> m_groups;
  std::unique_ptr<ShaderManager> m_shader_manager;

  RenderGroup& getRenderGroup(RenderGroupID);
  void renderGroup(RenderGroup&);

public:
  Renderer(ShaderProgramFactory shader_program_factory = {});
  ~Renderer();

  void applyCamera(const render_util::Camera3D&);
  void setMaxTransparentVisibility(float);
  void enableAtmosphere(bool);
  void enableInstancedDrawing(bool);
  void flushRenderList(std::function<void(render_util::ShaderProgramPtr)> set_uniforms);

  void addToRenderList(std::vector<ParticleInstance>&&,
                       const std::shared_ptr<const Material>&,
                       float distance = 0);

  void addToRenderList(MeshRendererData* mesh,
                       const MaterialMapping& material_mapping,
                       const MaterialList& materials,
#if USE_MESH_INSTANCE_RENDER_LIST_CACHE
                       MeshInstanceRenderListCache& cache,
#endif
                       const glm::mat4& model_view,
                       float distance,
                       size_t animation_frame,
                       RenderGroupID group = RenderGroupID::DEFAULT);

  void addToRenderList(const MaterialMapping& material_mapping,
                       const MaterialList& materials,
                       const std::vector<mesh::Face>& faces,
                       const glm::mat4& model_view,
                       bool z_sort,
                       RenderGroupID group = RenderGroupID::DEFAULT,
                       float distance = 0);

  void printStats(std::ostream &out);
  std::vector<std::pair<std::string, glm::vec3>> getMaterialDebugColors();
};


void drawTile(glm::vec2 origin, glm::vec2 extent,
              glm::vec2 texcoord_origin, glm::vec2 texcoord_extent,
              const Material&);


void clearPerFrameStats(); //FIXME


std::shared_ptr<SharedVAO> createSharedVAO();


void addToSharedVAO(const std::shared_ptr<SharedVAO>&,
                    const std::vector<MeshRendererData*>&);


} // namespace il2ge::renderer

#endif
