#include <cstdint>
#include <string>

namespace sfs
{
  int64_t getHash(std::string filename);
}
