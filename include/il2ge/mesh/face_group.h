﻿/**
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MESH_FACE_GROUP_H
#define IL2GE_MESH_FACE_GROUP_H

namespace il2ge::mesh
{

  struct FaceGroup
  {
    int StartVertex = 0;
    int VertexCount = 0;
    int StartFace = 0;
    int FaceCount = 0;
    int Material = 0;

//         void Write(BinaryWriter b)
//         {
//             b.Write(StartVertex);
//             b.Write(VertexCount);
//             b.Write(StartFace);
//             b.Write(FaceCount);
//             b.Write(Material);
//         }
  };

}

#endif
