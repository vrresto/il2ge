/**
 *    IL-2 Graphics Extender
 *
 *    Copyright (C) 2015 Stainless <http://stainlessbeer.weebly.com>
 *    Copyright (C) 2020 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_MESH_BASE
#define IL2GE_MESH_BASE

#include <il2ge/mesh/face_group.h>
#include <il2ge/named.h>

#include <glm/glm.hpp>
#include <memory>
#include <vector>
#include <functional>


#define IL2GE_MESH_DEBUG 1


namespace il2ge
{
  class Material;
  class MaterialWrapper;
  class MaterialList;
}


namespace il2ge::renderer
{
  class Renderer;
  class MeshRendererData;
  class MeshInstanceRenderListCache;

  using MaterialMapping = std::vector<int>;
}


namespace il2ge::mesh
{


struct MeshInstance;


template <typename T>
class Vector : public std::vector<T>
{
public:
#if IL2GE_MESH_DEBUG
  __attribute__ ((noinline)) T& operator[] (size_t index)
  {
    assert(index < this->size());
    return this->at(index);
  }

  __attribute__ ((noinline)) const T& operator[] (size_t index) const
  {
    assert(index < this->size());
    return this->at(index);
  }
#endif
};


using Index = uint16_t;
using Mat4 = glm::mat4;
using Vec3 = glm::vec3;


struct VertexPositionNormalTexture
{
  glm::vec3 Position {};
  glm::vec3 Normal {};
  glm::vec2 TextureCoordinate {};
};


struct Face
{
  using Vertex = VertexPositionNormalTexture;

  glm::vec4 center {};
  std::array<Vertex, 3> vertices {};
  int material {};
};


struct VertexPositionColor
{
  glm::vec3 Position {};
  glm::vec3 Color {};
};


struct LodBase
{
  virtual renderer::MeshRendererData* getRendererData() const = 0;
  virtual const Vector<VertexPositionNormalTexture>& getVertices() const = 0;
  virtual const Vector<Index>& getIndices() const = 0;
  virtual const Vector<FaceGroup>& getFaceGroups() const = 0;
  virtual const size_t getVertexCount() const = 0;
  virtual const size_t getFrameCount() const = 0;
};


struct MeshBase : public Named
{
  MeshBase(std::string name) : Named(name) {}
  virtual ~MeshBase() {}
  virtual bool isAnimated() const = 0;
  virtual void enableLod(bool) = 0;
  virtual size_t getFrameCount() const = 0;
  virtual const LodBase& getBaseLod() const = 0;
  virtual int getLodNumAtDistance(float) const = 0;
  virtual const LodBase& getLodAtDistance(float) const = 0;
  virtual const LodBase& getLod(int) const = 0;
  virtual size_t getLodCount() const = 0;
  virtual int findHook(const std::string &name) const = 0;
  virtual const std::string &getHookName(int index) const = 0;
  virtual const Mat4 &getHookMatrix(int index, int frame) const = 0;
  virtual float getMaxVisibleDistance() const = 0;
  virtual const Vector<std::string>& getMaterialNames() const = 0;
  virtual size_t getMaterialsCount() const = 0;
  virtual void createRendererData() = 0;
};


class MeshInstanceBase
{
  struct Lod
  {
    std::vector<Face> z_sorted_faces;
  };

  std::vector<Lod> m_lods;

public:
  bool hasZSortedFaces(int lod);
  const std::vector<Face>& getZSortedFaces(int lod);

  void materialsChanged(const MeshBase& mesh,
                        const MaterialList& materials,
                        const renderer::MaterialMapping& mapping);
};


struct MeshInstance : public MeshInstanceBase
{
  std::shared_ptr<const mesh::MeshBase> m_mesh;
  std::unique_ptr<MaterialList> m_materials;
#if USE_MESH_INSTANCE_RENDER_LIST_CACHE
  std::unique_ptr<renderer::MeshInstanceRenderListCache> m_render_list_cache;
#endif

public:
  MeshInstance(std::shared_ptr<const mesh::MeshBase>, MaterialList&);
  MeshInstance(const MeshInstance&);
  ~MeshInstance();

  void materialChanged(int index);
  std::unique_ptr<MeshInstance> clone() const;
  const mesh::MeshBase& getMesh() const;
#if USE_MESH_INSTANCE_RENDER_LIST_CACHE
  renderer::MeshInstanceRenderListCache& getRenderListCache();
#endif
  const std::shared_ptr<const Material>& getMaterial(int index) const;
  const bool hasMaterial(int index) const;
  const int getMaterialsCount() const;

  void draw(const Mat4 &model_view, float distance, renderer::Renderer&, int frame);
};


struct HierMeshBase
{
  virtual ~HierMeshBase() {}
  virtual void draw(const Mat4 &model_view, float distance, renderer::Renderer&) = 0;
  virtual const std::vector<std::string>& getMaterialNames() const = 0;
  virtual void setMaterials(std::unique_ptr<MaterialList>&&) = 0;
  virtual int findMaterial(const std::string &name) = 0;
  virtual const std::shared_ptr<Material>& getMaterial(int index) = 0;
  virtual void replaceMaterial(const std::string& name,
                               const std::shared_ptr<il2ge::Material>&) = 0;
  virtual int getNodeCount() const = 0;
  virtual int findNode(const std::string &name) = 0;
  virtual void setNodeVisible(int index, bool visible) = 0;
  virtual bool isNodeVisible(int index) = 0;
  virtual void setNodeTransform(int index, const Vec3 &pos, const Mat4 &rotation) = 0;
  virtual void setNodeRotation(int index, const Mat4&) = 0;
  virtual glm::mat4 getNodeTransform(int index) = 0;
  virtual const std::string &getNodeName(int index) = 0;
  virtual std::unique_ptr<il2ge::mesh::MeshInstance> getNodeMesh(int index) = 0;
  virtual void getSubTree(int node_index, bool hide_nodes, std::vector<long int> &indices) = 0;
  virtual int findHook(const std::string &name) = 0;
  virtual const std::string &getHookName(int index) = 0;
  virtual int getHookNode(int index) = 0;
  virtual const Mat4 getHookMatrix(int index) = 0;
};



template <class T>
void createFaces(const T& mesh,
                 const il2ge::mesh::FaceGroup& fg,
                 int material,
                 std::vector<mesh::Face>& faces)
{
  auto& vertices_in = mesh.getVertices();
  auto& indices_in = mesh.getIndices();

  auto fg_start_index = fg.StartFace * 3;

  for (size_t i = 0; i < fg.FaceCount; i++)
  {
    auto face_start_index = fg_start_index + i * 3;

    mesh::Face face;
    face.material = material;

    for (size_t i = 0; i < face.vertices.size(); i++)
    {
      auto &v = vertices_in.at(fg.StartVertex + indices_in.at(face_start_index+i));
      face.vertices[i] = v;
    }

    face.center = glm::vec4((face.vertices[0].Position +
                             face.vertices[1].Position +
                             face.vertices[2].Position) / glm::vec3(3),
                            1.f);

    faces.push_back(face);
  }
}


inline void createFaces(const mesh::LodBase& mesh,
                 const mesh::FaceGroup& fg,
                 int material,
                 std::vector<mesh::Face>& faces)
{
  createFaces<mesh::LodBase>(mesh, fg, material, faces);
}


using MeshDeleter = std::function<void(MeshBase*)>;
using GetMeshFunc = std::function<std::shared_ptr<const MeshBase>(std::string)>;


std::unique_ptr<MeshBase> loadMesh(std::string path);
std::unique_ptr<MeshBase> loadMesh(std::string path, const char *data, size_t data_size);
std::shared_ptr<MeshBase> loadMesh(std::string path, MeshDeleter);
std::unique_ptr<HierMeshBase> loadHierMesh(std::string path);
std::unique_ptr<HierMeshBase> loadHierMesh(std::string path, GetMeshFunc);
std::unique_ptr<HierMeshBase> loadHierMesh(std::string path, GetMeshFunc,
                                           const char *data, size_t data_size);


}

#endif
