/**
 *    IL-2 Graphics Extender
 *    Copyright (C) 2019 Jan Lepper
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IL2GE_PARAMETER_FILE_H
#define IL2GE_PARAMETER_FILE_H

#include <il2ge/section_file.h>
#include <il2ge/exceptions.h>

#include <glm/glm.hpp>
#include <string>
#include <unordered_map>
#include <vector>
#include <iostream>
#include <functional>
#include <memory>
#include <optional>

namespace il2ge
{


class ParameterSection :  public SectionBase
{
public:
  struct EntryNotFoundException : public std::runtime_error
  {
    EntryNotFoundException(std::string name) :
      std::runtime_error(std::string("Entry not found: ") + name) {}
  };


  ParameterSection(bool case_sensitive) : m_case_sensitive(case_sensitive) {}

  const std::string &at(const char *param) const;

  template <typename T>
  void get(const char *name, T &value) const
  {
    try
    {
      getImp(name, value);
    }
    catch(std::exception &e)
    {
      LOG_ERROR << "Failed to get value of parameter "<< name<< " : " << e.what() << std::endl;
      throw;
    }
  }

  template <typename T>
  bool get_noexcept(const char *name, T &value) const
  {
    try
    {
      getImp(name, value);
      return true;
    }
    catch(std::exception &e)
    {
      return false;
    }
  }

  std::string get(const char *param) const
  {
    std::string s;
    get(param, s);
    return s;
  }

  Entry &getOrCreateEntry(const std::string &key)
  {
    return m_entries[m_case_sensitive ? key : util::makeLowercase(key)];
  }

  const Entry &getEntry(const std::string &key) const
  {
    auto e = getEntryPtr(key);
    if (e)
      return *e;
    else
      throw EntryNotFoundException(key);
  }

  bool hasKey(const char *param) const
  {
    return getEntryPtr(param) != nullptr;
  }

private:
  const Entry *getEntryPtr(const std::string &key) const
  {
    auto it = m_entries.find(m_case_sensitive ? key : util::makeLowercase(key));
    return (it != m_entries.end()) ? &it->second : nullptr;
  }

  void getImp(const char *name, std::string &value) const
  {
    value = getEntry(name).values.at(0);
  }

  void getImp(const char *name, int &value) const
  {
    value = std::stoi(getEntry(name).values.at(0));
  }

  void getImp(const char *name, bool &value) const
  {
    value = std::stoi(getEntry(name).values.at(0));
  }

  void getImp(const char *name, float &value) const
  {
    value = std::stof(getEntry(name).values.at(0));
  }

  void getImp(const char *name, glm::vec4 &value) const;
  void getImp(const char *name, glm::vec3 &value) const;
  void getImp(const char *name, glm::vec2 &value) const;
  void getImp(const char *name, std::vector<std::string> &values) const;

  const bool m_case_sensitive = false;
  std::unordered_map<std::string, Entry> m_entries;
};


class ParameterFile : public SectionFileBase<ParameterSection>
{
public:
  using Base = SectionFileBase<ParameterSection>;

  // struct ReadError : public std::runtime_error
  // {
  //   ReadError(const std::string& path, const std::string& description) :
  //     std::runtime_error("Error reading file " + path + ": " + description) {}
  // };

  ParameterFile(const char *content, size_t size, bool case_insensitive = false);
};


class ParameterFiles
{
public:
  using ReadFileFunc = std::function<std::vector<char>(std::string)>;

  ParameterFiles(ReadFileFunc f) : m_read_file(f) {}

  const ParameterFile &get(const std::string &file_path);

private:
  struct Entry
  {
    std::unique_ptr<ParameterFile> file;
    std::optional<FileReadError> error;
  };

  ReadFileFunc m_read_file;

  std::unordered_map<std::string, Entry> m_file_map;
};


template <class T>
void visit(ParameterFiles& files, const std::string& path, T visitor, bool include_bases)
{
  auto& file = files.get(path);

  if (include_bases)
  {
    auto& class_info = file.getSection("ClassInfo");
    std::string based_on;
    class_info.get_noexcept("BasedOn", based_on);

    if (!based_on.empty())
    {
      auto base_path = util::getDirFromPath(path) + "/" + based_on;
      visit<T>(files, base_path, visitor, true);
    }
  }

  visitor(file);
}


} // namespace il2ge

#endif
