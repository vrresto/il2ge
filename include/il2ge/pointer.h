#pragma once

#include <memory>

namespace il2ge
{

#if 1

template <typename T>
using Shared = std::shared_ptr<T>;

template <typename T>
using Weak = std::weak_ptr<T>;

template <typename T, typename... Args>
Shared<T> makeShared(Args... args)
{
  return std::make_shared<T>(args...);
}

#else

template <typename T>
using Shared = std::__shared_ptr<T, __gnu_cxx::_S_single>; // no thread safety needed

template <typename T>
using Weak = std::__weak_ptr<T, __gnu_cxx::_S_single>; // no thread safety needed

template <typename T, typename... Args>
Shared<T> makeShared(Args... args)
{
  return Shared<T>(new T(args...));
}

#endif

} // namespace il2ge
