#pragma once

#include <unordered_map>
#include <string>
#include <list>


namespace il2ge
{


template<typename T>
class Cache
{
public:
  using Entry = std::pair<std::string, T>;

  Cache(int max_size) : m_max_size(max_size) {}

  void set(const std::string& key, T& value)
  {
    if (m_map.size() >= m_max_size)
    {
      evict();
    }
    auto it = m_map.find(key);
    if (it != m_map.end())
    {
      auto& it_entry = it->second;
      m_entries.splice(m_entries.begin(), m_entries, it_entry);
      it_entry->second = value;
    }
    else
    {
      m_entries.emplace_front(key, value);
      m_map[key] = m_entries.begin();
    }
  }

  T get(const std::string& key)
  {
    auto it = m_map.find(key);
    if (it == m_map.end())
    {
      return {};
    }
    auto& it_entry = it->second;
    m_entries.splice(m_entries.begin(), m_entries, it_entry);
    return it_entry->second;
  }

private:
  std::list<Entry> m_entries;
  std::unordered_map<std::string, typename std::list<Entry>::iterator> m_map;
  size_t m_max_size;

  void evict()
  {
    auto last = m_entries.end();
    last--;
    m_map.erase(last->first);
    m_entries.pop_back();
  }
};


} // namespace il2ge
