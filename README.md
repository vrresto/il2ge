# IL-2 Graphics Extender (IL2GE)


## Requirements

- IL-2 Selector (at least version 3.4.2)
- IL-2 "Perfect Mode" needs to be enabled
- [OpenGL version 4.5](https://en.wikipedia.org/wiki/OpenGL#OpenGL_4.5) (should be supported by any non-ancient GPU)


## Download

[![Latest release](https://gitlab.com/vrresto/il2ge/-/badges/release.svg)](https://gitlab.com/vrresto/il2ge/-/releases/permalink/latest)

[All releases](https://gitlab.com/vrresto/il2ge/-/releases)


## Installation

Run `il2ge-installer.exe` and follow the instructions.
Previous IL2GE installations will be backed up into the folder `il2ge-installer-backup` in the game folder.


## Uninstallation

To uninstall IL2GE simply delete the folder `il2ge` in the game folder.


## Configuration

IL2GE can be configured by editing `il2ge.ini` in the game folder.

The following options need to be set in `conf.ini`:
```
[window]
StencilBits=8
```
```
[GLPROVIDER]
GL=Opengl32.dll
```
```
[Render_OpenGL]  
TexFlags.UseVertexArrays=1
HardwareShaders=1  
Water=0
DynamicalLights=0
```


## Running

1. Make sure the following prerequisites are met:

   - The latest graphics drivers are installed
   - IL-2 Selector is at least version 3.4.2
   - The selected game type in IL-2 Selector is **not** "Stock Game"
   - The required `conf.ini` options are set

2. That's it. When IL-2 is lauched IL2GE will be active.


## Random cirrus cloud textures

IL2GE has the option of using random cirrus cloud textures.
To use this feature, create the folder`il2ge_random_cirrus_textures` inside the game folder and copy textures into it.
IL2GE will randomly select a texure from this folder each time a map is loaded.


## Current limitations

- dynamic lights cause artifacts - disabling them in conf.ini prevents this
- artifacts with the default water textures in some modpacks  
  **I recommend using Carsmaster's water mod to avoid artifacts.**
- forests are flat (but using a parallax effect)
- no water reflections
- no 3D waves
- no cloud shadows
- no shore waves / wave foam


## License

IL2GE is available as open source under the terms of the [GNU Lesser General Public License (LGPL)](LICENSE).  
Additional third party code used by this project is available under LGPL-compatible licenses.
